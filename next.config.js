/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')
const { withPartytown, partytownSnippet } = require('@builder.io/partytown')

/** @type {import('next').NextConfig} */

module.exports = {
  trailingSlash: true,
  reactStrictMode: false,
  webpack: config => {
    config.resolve.alias = {
      ...config.resolve.alias,
      apexcharts: path.resolve(__dirname, './node_modules/apexcharts-clevision')
    }

    return config
  },
  experimental: {
    nextScriptWorkers: true
  }
}
