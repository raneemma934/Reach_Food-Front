import { useTheme } from '@mui/system'
import React from 'react'

export default function Email() {
  const theme = useTheme()

  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
      <path
        d='M3.45031 6.5721C3.76564 6.37778 4.12872 6.27465 4.49946 6.27295H19.4995C19.8702 6.27465 20.2333 6.37778 20.5486 6.5721L12.3779 12.0199C12.1486 12.1733 11.8504 12.1733 11.6211 12.0199L3.45031 6.5721ZM13.1321 13.1542C12.4452 13.6136 11.5495 13.6136 10.8626 13.1542L2.58194 7.63159C2.49927 7.85147 2.45666 8.08414 2.4541 8.31851V17.864C2.4541 18.9932 3.37029 19.9094 4.49956 19.9094H19.4996C20.6288 19.9094 21.545 18.9932 21.545 17.864V8.31851C21.5425 8.08414 21.4998 7.85147 21.418 7.63159L13.1321 13.1542Z'
        fill={theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light}
      />
    </svg>
  )
}
