import { useTheme } from '@mui/system'
import React from 'react'

export default function Eyes() {
  const theme = useTheme()

  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M12 8C9.79085 8 8 9.79085 8 12C8 14.2092 9.79085 16 12 16C14.2092 16 16 14.2092 16 12C16 9.79085 14.2092 8 12 8ZM9.25 12C9.25 10.4812 10.4812 9.25 12 9.25C13.5188 9.25 14.75 10.4812 14.75 12C14.75 13.5188 13.5188 14.75 12 14.75C10.4812 14.75 9.25 13.5188 9.25 12Z'
        fill={theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light}
      />
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M12 5C7.99438 5 4.45501 7.29595 2.32728 10.8029C1.89091 11.5221 1.89091 12.4779 2.32728 13.1971C4.45501 16.7041 7.99438 19 12 19C16.0056 19 19.545 16.7041 21.6727 13.1971C22.1091 12.4779 22.1091 11.5221 21.6727 10.8029C19.545 7.29595 16.0056 5 12 5ZM3.24777 11.5462C5.17335 8.37247 8.37494 6.2963 12 6.2963C15.6251 6.2963 18.8266 8.37247 20.7522 11.5462C20.9178 11.8191 20.9178 12.1809 20.7522 12.4538C18.8266 15.6275 15.6251 17.7037 12 17.7037C8.37494 17.7037 5.17335 15.6275 3.24777 12.4538C3.0822 12.1809 3.0822 11.8191 3.24777 11.5462Z'
        fill={theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light}
      />
    </svg>
  )
}
