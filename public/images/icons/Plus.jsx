import React from 'react'

export default function Plus() {
  return (
    <svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none'>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M19.636 13.0906H13.0906V19.636H10.9087V13.0906H4.36328V10.9087H10.9087V4.36328H13.0906V10.9087H19.636V13.0906Z'
        fill='#F2F4F8'
      />
    </svg>
  )
}
