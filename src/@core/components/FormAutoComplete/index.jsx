import React from 'react'
import { Controller } from 'react-hook-form'
import { Autocomplete, TextField } from '@mui/material'

const FormAutocomplete = ({ name, control, options, label, placeholder, error, helperText, onChangeHandler }) => (
  <Controller
    name={name}
    control={control}
    rules={{ required: `${label} is required` }}
    render={({ field: { value, onChange }, fieldState: { error } }) => (
      <Autocomplete
        options={options}
        fullWidth
        getOptionLabel={option => option.name}
        value={options.find(option => option.id === value) || null}
        onChange={(event, newValue) => {
          onChange(newValue ? newValue.id : '')
          if (onChangeHandler) onChangeHandler(newValue)
        }}
        renderInput={params => (
          <TextField
            {...params}
            placeholder={placeholder}
            fullWidth
            error={Boolean(error)}
            helperText={error ? `${label} is required` : ''}
          />
        )}
      />
    )}
  />
)

export default FormAutocomplete
