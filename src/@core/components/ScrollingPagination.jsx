import { useEffect } from 'react'

const ScrollingPagination = ({ data, setCurrentPage, currentPage }) => {
  useEffect(() => {
    const handleScroll = () => {
      const scrollY = window.scrollY
      const windowHeight = window.innerHeight
      const documentHeight = document.documentElement.scrollHeight

      // Check if user has scrolled to the bottom (within 100 pixels)
      if (scrollY + windowHeight >= documentHeight - 100) {
        // Ensure we are not fetching data unnecessarily
        if (currentPage < data?.data?.last_page) {
          setCurrentPage(prevPage => prevPage + 1)
        }
      }
    }

    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [currentPage, data, setCurrentPage])

  return null // Render nothing, as this component only handles side-effects
}

export default ScrollingPagination
