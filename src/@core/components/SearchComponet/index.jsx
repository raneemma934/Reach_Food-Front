const getNestedValue = (obj, path) => {
  return path.split('.').reduce((acc, part) => acc && acc[part], obj)
}

export const search = (data, query, keys) => {
  if (!Array.isArray(data)) {
    // console.log(data)

    return []
  }

  if (typeof query !== 'string') {
    // console.log(query)

    return []
  }

  if (!Array.isArray(keys)) {
    // console.log(keys)

    return []
  }

  return (
    data &&
    data.filter(item =>
      keys.some(key => {
        const value = getNestedValue(item, key)
        if (value === undefined || value === null) {
          return false
        }

        return value.toString().toLowerCase().includes(query.toLowerCase())
      })
    )
  )
}
