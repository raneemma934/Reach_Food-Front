import { Button, Card, CardContent, Modal } from '@mui/material'
import { borderRadius, Box } from '@mui/system'
import React, { useEffect, useRef, useState } from 'react'
import DatePicker, { registerLocale } from 'react-datepicker'
import { useTranslation } from 'react-i18next'

import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker'
import { DateFormateOfDay } from 'src/@core/utils/DateFormateOfDay'
import { DateFormateOfMonth } from 'src/@core/utils/DateFormateOfMonth'
import ar from 'date-fns/locale/ar'
import { useDispatch } from 'react-redux'
import { setDay } from 'src/store/apps/requests'
import moment from 'moment'

const CustomDatePicker = ({ setStartDate, handleClose, startDate, datePickerOpen }) => {
  console.log('🚀 ~ CustomDatePicker ~ startDate:', startDate)
  const [showMonthPicker, setShowMonthPicker] = useState('month')
  const dispatch = useDispatch()
  const closeRef = useRef(null)
  registerLocale('ar', ar)
  const { t } = useTranslation()

  const toggleDatePicker = type => {
    setStartDate(startDate)
    setShowMonthPicker(type)
  }

  const handleDateSend = date => {
    const formattedDate =
      showMonthPicker === 'day'
        ? moment(date).locale('en').format('YYYY-MM-DD')
        : moment(date).locale('en').format('YYYY-MM')

    dispatch(setDay(formattedDate))
    setStartDate(date)
    handleClose()
  }

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    backgroundColor: theme => (theme.palette.mode === 'dark' ? theme.palette.gray.dark : theme.palette.gray.light),
    boxShadow: 24,
    p: 4,
    zIndex: 9999,
    borderRadius: '8px'
  }

  return (
    <Modal open={datePickerOpen} onClose={handleClose}>
      <Box sx={style}>
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <Button
            onClick={() => toggleDatePicker('month')}
            sx={{
              mr: 2,

              borderBottom: showMonthPicker === 'month' ? '3px solid #73A3D0' : 'none',
              color: theme =>
                showMonthPicker === 'month'
                  ? '#73A3D0'
                  : theme.palette.mode === 'dark'
                  ? theme.palette.secondary.dark
                  : theme.palette.secondary.light,
              borderRadius: showMonthPicker === 'month' ? '0px' : '4px',
              padding: '4px 16px'
            }}
          >
            شهر
          </Button>
          <Button
            onClick={() => toggleDatePicker('day')}
            sx={{
              mr: 2,
              borderBottom: showMonthPicker === 'day' ? '3px solid #73A3D0' : 'none',
              color: theme =>
                showMonthPicker === 'day'
                  ? '#73A3D0'
                  : theme.palette.mode === 'dark'
                  ? theme.palette.secondary.dark
                  : theme.palette.secondary.light,
              borderRadius: showMonthPicker === 'day' ? '0px' : '4px',
              padding: '4px 16px'
            }}
          >
            يوم
          </Button>
        </Box>
        <DatePickerWrapper
          sx={{
            width: '100%',
            backgroundColor: theme =>
              theme.palette.mode === 'dark' ? theme.palette.gray.dark : theme.palette.gray.light,
            display: 'flex',
            justifyContent: 'center',
            '& .react-datepicker': {
              boxShadow: 'none !important',
              border: 'none !important',
              backgroundColor: theme =>
                theme.palette.mode === 'dark' ? theme.palette.gray.dark : theme.palette.gray.light
            },
            '& .react-datepicker__header ': {
              backgroundColor: theme =>
                theme.palette.mode === 'dark' ? theme.palette.gray.dark : theme.palette.gray.light
            },
            '& .react-datepicker .react-datepicker__month--selected:hover ': {
              backgroundColor: theme =>
                `${theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light} !important`
            },
            '& .react-datepicker .react-datepicker__day--selected:hover ': {
              backgroundColor: theme =>
                `${theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light} !important`
            },
            '& .react-datepicker__header': {
              backgroundColor: `transparent !important`
            },
            '& .react-datepicker__month-text--today:not(.react-datepicker__month--selected)': {
              color: theme =>
                ` ${
                  theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                } !important`,
              border: theme =>
                `1px solid ${
                  theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                } !important`
            },
            '& .react-datepicker__day--today:not(.react-datepicker__day--selected):not(:empty)': {
              color: theme =>
                ` ${
                  theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                } !important`,
              border: theme =>
                `1px solid ${
                  theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                } !important`
            }
          }}
        >
          <DatePicker
            inline
            locale='ar'
            selected={startDate}
            onChange={handleDateSend}
            dateFormat={showMonthPicker === 'month' ? 'MMM' : 'yyyy'}
            showMonthYearPicker={showMonthPicker === 'month'}
            showYearPicker={showMonthPicker === 'year'}
            calendarClassName='rasta-stripes'
            showFourColumnMonthYearPicker
          />
        </DatePickerWrapper>
      </Box>
    </Modal>
  )
}

export default CustomDatePicker
