import { Box, Stack } from '@mui/system'
import React, { useEffect, useState } from 'react'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button, useTheme } from '@mui/material'
import styled from '@emotion/styled'
import InputBase from '@mui/material/InputBase'
import CustomDatePicker from './DatePicker'
import { useSelector } from 'react-redux'
import { search } from '../SearchComponet'
import DateLogo from '../../../../public/images/cards/date'

const SearchIconWrapper = styled('div')({
  padding: '0 8px',
  display: 'flex',
  alignItems: 'center'
})

const StyledInputBase = styled(InputBase)({
  flex: 1
})

export default function Filter({ setFilterData, fdata }) {
  const theme = useTheme()

  const Search = styled('div')({
    width: '332px',
    padding: '8px 12px',
    borderRadius: '4px',
    display: 'flex',
    alignItems: 'center',
    backgroundColor: theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1'
  })
  const [query, setQuery] = useState('')

  const { data, archivedDay } = useSelector(state => state.request)

  const [datePickerOpen, setDatePickerOpen] = useState(false)

  const handleOpenDatePicker = () => {
    setDatePickerOpen(true)
  }

  const handleCloseDatePicker = () => {
    setDatePickerOpen(false)
  }

  const handleInputChange = e => {
    setQuery(e.target.value)
  }

  useEffect(() => {
    const filteredData = search(fdata, query, ['id', 'status', 'total_price', 'customer.name'])

    setFilterData(filteredData ? filteredData : fdata)
  }, [fdata, query, setFilterData])

  const [startDate, setStartDate] = useState(new Date(archivedDay))

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme.palette.mode === 'dark' ? '#3B3B3B' : '#F8F8F8',
          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box sx={{ display: 'flex', alignItems: 'center', gap: '24px' }}>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
          </Search>
          <Box
            sx={{
              display: 'flex',
              width: '46px',
              height: '46px',
              padding: '7px',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: '8px',
              backgroundColor: theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1',
              cursor: 'pointer'
            }}
            onClick={handleOpenDatePicker}
          >
            <DateLogo />
          </Box>
          <Box>{archivedDay.length > 0 ? archivedDay : 'الرجاء ادخال التاريخ'}</Box>
        </Box>
      </Stack>

      {datePickerOpen && (
        <CustomDatePicker
          datePickerOpen={datePickerOpen}
          setStartDate={setStartDate}
          startDate={startDate}
          handleClose={handleCloseDatePicker}
        />
      )}
    </>
  )
}
