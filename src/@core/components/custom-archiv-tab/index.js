import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import { styled } from '@mui/material/styles'
import MuiTabList from '@mui/lab/TabList'
import ServerSideToolbar from 'src/views/table/data-grid/ServerSideToolbar'
import { GridToolbarExport } from '@mui/x-data-grid'
import { Box, Stack } from '@mui/system'
import { Button, Typography } from '@mui/material'
import Download from '../../../../public/images/cards/download'
import { useState } from 'react'
import DrawerUpload from 'src/features/archive/DrawerUpload'
import Filter from './fillter'
import { useSelector } from 'react-redux'
import { useTheme } from '@mui/material/styles'

const TabList = styled(MuiTabList)(({ theme }) => ({
  borderBottom: '0 !important',
  '& .MuiTabs-root': { p: '0px !important', m: '0px important' },
  '& .MuiTabs-indicator': {
    backgroundColor: '#73A3D0',
    height: '3px'
  },
  '& .MuiTab-root.Mui-selected': {
    backgroundColor: 'transparent',
    color: '#73A3D0 ',
    '&:hover': {
      color: '#73A3D0',
      backgroundColor: 'transparent'
    }
  },

  '& .MuiTab-root': {
    lineHeight: 1,
    borderRadius: theme.shape.borderRadius,
    color: theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
    backgroundColor: theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
    padding: '8px 24px 8px 24px !important',
    borderRadius: '8px',
    height: '48px',
    fontSize: '18px',
    marginRight: '24px',
    '&:hover': {
      color: '#73A3D0',
      backgroundColor: 'rgba(115, 163, 208, 0.25);'
    }
  },
  '&:last-child': {
    marginRight: 0
  }
}))

const CustomArchiveTabs = ({ value, handleChange }) => {
  const [open, setOpen] = useState(false)
  const theme = useTheme()

  const handleOpenDrawer = () => {
    setOpen(true)
  }

  const handleCloseDrawer = () => {
    setOpen(false)
  }

  return (
    <>
      <Stack direction={'row'} justifyContent={'space-between'}>
        <TabContext value={value}>
          <TabList onChange={handleChange} aria-label='customized tabs example'>
            <Tab value='1' label='كل الطلبات' />
            <Tab value='2' label='الطلبات تم تسليمها' />
            <Tab value='3' label=' الطلبات المرفوضة' />
          </TabList>
        </TabContext>
        <Box
          onClick={handleOpenDrawer}
          sx={{
            display: 'flex',
            padding: '8px 24px  ',
            alignItems: 'center',
            gap: '8px',
            backgroundColor: theme =>
              theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
            cursor: 'pointer',
            borderRadius: '8px',
            marginRight: '25px',
            height: '48px'
          }}
        >
          <Download />
          <Button
            sx={{
              fontSize: '16px',
              color: theme =>
                theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
              fontWeight: '600',
              fontStyle: 'normal',
              lineHeight: 'normal',
              padding: ' 0 ',
              fontFamily: 'Noto Kufi Arabic'
            }}
          >
            تصدير التقرير
          </Button>
        </Box>
      </Stack>

      {open && <DrawerUpload open={open} handleCloseDrawer={handleCloseDrawer} />}
    </>
  )
}

export default CustomArchiveTabs
