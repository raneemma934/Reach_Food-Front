import React, { useState } from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Typography } from '@mui/material'
import City from '../../../../public/images/cards/city'
import Location from '../../../../public/images/cards/Location'
import Arrowleft from '../../../../public/images/cards/arrowleft'
import SidebarEditBranches from 'src/features/branches/edit/EditBranches'
import CustomDialogDelete from '../custom-delete'
import toast, { Toaster } from 'react-hot-toast'
import Delete from '../../../../public/images/cards/delete'
import Edit from '../../../../public/images/cards/edit'
import CustomDialogDeleteBranch from 'src/features/branches/delete/CustomDialogDeleteBranch'
import { useDispatch } from 'react-redux'
import { deleteUser } from 'src/store/apps/user'
import { deletebranches } from 'src/store/apps/branches'
import CustomToast from 'src/@core/utils/RollBackToast'
import RollBackToast from 'src/@core/utils/RollBackToast'

export default function CustomBranchesCards({ branch }) {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [isDialogOpen, setIsDialogOpen] = useState(false)
  const [isEditing, setIsEditing] = useState(false)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [password, setPassword] = useState(null)
  const dispatch = useDispatch()

  const handleCardClick = () => {
    setIsDrawerOpen(true)
    setIsEditing(false)
    setIsSidebarOpen(true)
  }

  const handleCloseSidebar = () => {
    setIsSidebarOpen(false)
  }

  const handleDeleteClick = () => {
    setIsDialogOpen(true)
  }

  const handleCloseDialog = () => {
    setIsDialogOpen(false)
  }

  const handleOpenDelete = event => {
    event.stopPropagation()
    setIsDialogOpen(true)
  }

  const handleEditClick = event => {
    event.stopPropagation()
    setIsSidebarOpen(true)
    setIsEditing(true)
  }

  const DeleteComplaints = data => {
    toast(t => <RollBackToast data={data} t={t} />, {
      style: {
        padding: '0px', // Remove default padding
        // Remove default background
        boxShadow: 'none' // Remove default box-shadow
      }
    })

    setIsDrawerOpen(false)
    setIsSidebarOpen(false)

    setIsDialogOpen(false)
  }
  const hex2rgb = hex => [...hex.match(/\w\w/g)].map(x => parseInt(x, 16))

  return (
    <>
      <Card
        onClick={handleCardClick}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          borderRadius: '8px',
          boxShadow: 'rgba(0, 0, 0, 0.05)',
          padding: '16px',
          gap: '10px',
          cursor: 'pointer',
          '&:hover': {
            boxShadow: theme =>
              `0px 2px 9px 0px rgba(${hex2rgb(theme.palette.accent.main)}
              , 0.25) !important`
          }
        }}
        className='Cards'
      >
        <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }}>
          <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
            <City /> {branch?.country_name}
          </Typography>
          <div className='cards-hover'>
            <Box onClick={handleOpenDelete}>
              <Delete />
            </Box>
            <Box onClick={handleEditClick}>
              <Edit />
            </Box>
          </div>
        </Stack>
        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <Location /> {branch?.name}
        </Typography>
        <Typography
          variant='h6'
          sx={{
            display: 'flex',
            alignItems: 'center',
            gap: '4px',
            fontSize: '16px',
            fontWeight: '500'
          }}
        >
          <Arrowleft />

          {branch?.branches?.map(
            (category, i) =>
              i < 4 && (
                <span key={category.id}>
                  {category.name.slice(0, 5)}
                  {' / '}
                </span>
              )
          )}
        </Typography>
        <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
          <Avatar sx={{ width: '32px', height: '32px' }} src='/images/icons/avatar.png' alt='Olivia Sparks' />
          <Box>
            <Typography
              variant='h6'
              sx={{
                fontSize: '16px',
                fontWeight: '500px'
              }}
            >
              {' '}
              {branch?.admin_name ? branch?.admin_name : 'لا يوجد مدير'}
            </Typography>
          </Box>
        </Box>
      </Card>

      {isSidebarOpen && (
        <SidebarEditBranches
          open={isSidebarOpen}
          handleCloseDrawer={handleCloseSidebar}
          handleDeleteClick={handleDeleteClick}
          Editing={isEditing}
          data={branch}
        />
      )}
      <CustomDialogDeleteBranch
        open={isDialogOpen}
        setPassword={setPassword}
        password={password}
        onDelete={() => DeleteComplaints({ id: branch.id, password: password })}
        handleClose={handleCloseDialog}
        question={`هل أنت متأكد من حذف  (${branch?.name}) ؟`}
        decsription={` هذه العملية لا يمكن التراجع عنها عند إتمامها وسيتم حذف التصنيف نهائياً وجميع المعلومات المتعلقة به من زبائن ومندوبين ومنتجات ومديري مبيعات وما يتعلق بهم.`}
      />
    </>
  )
}
