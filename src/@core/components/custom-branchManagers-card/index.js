import React, { useState } from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Typography } from '@mui/material'
import City from '../../../../public/images/cards/city'
import Location from '../../../../public/images/cards/Location'
import Telephone from '../../../../public/images/cards/Telephone'
import Delete from '../../../../public/images/cards/delete'
import Edit from '../../../../public/images/cards/edit'
import ArrowLeftDisable from '../../../../public/images/cards/arrowleftDisable'
import ArrowLeft from '../../../../public/images/cards/arrowleft'
import CustomDialogDelete from '../custom-delete'
import { deleteUser, fetchData } from 'src/store/apps/user'
import { useDispatch } from 'react-redux'
import SidebarEditUsers from 'src/features/users/clients/edit/EditUsers'
import SidebarEditSalesManager from 'src/features/users/salesmanager/edit/EditSalesManager'
import SidebarEditBranchManagers from 'src/features/branchManagers/edit/EditBranchManagers'

export default function CustomCardsBranchManagers({ client }) {
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [deleteId, setDeleteId] = useState(null)
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [isEditing, setIsEditing] = useState(false)
  const [isDialogOpen, setIsDialogOpen] = useState(false)

  const handleDelete = id => {
    setIsDeletePopupOpen(true)
  }

  const dispatch = useDispatch()

  const DeleteBranchManagers = id => {
    const props = {
      type: 'sales manager',
      id: id
    }
    dispatch(deleteUser(props))

    // setIsDeletePopupOpen(false)
    setIsDrawerOpen(false)
    setIsSidebarOpen(false)
  }

  const handleCloseSidebar = () => {
    setIsSidebarOpen(false)
  }

  const handleDeleteClick = () => {
    setIsDialogOpen(true)
    setIsDeletePopupOpen(true)
  }

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  }

  const handleOpenDelete = (id, event) => {
    if (event) {
      event.stopPropagation()
    }
    setIsDeletePopupOpen(true)
  }

  const handleCardClick = () => {
    setIsSidebarOpen(true)
    setIsEditing(false)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  const handleEditClick = event => {
    event.stopPropagation()
    setIsSidebarOpen(true)
    setIsEditing(true)
  }

  const formatPhoneNumbers = contacts => {
    return contacts.map(contact => contact.phone_number).join(' - ')
  }
  const hex2rgb = hex => [...hex.match(/\w\w/g)].map(x => parseInt(x, 16))

  return (
    <>
      <Card
        onClick={handleCardClick}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          borderRadius: '8px',
          boxShadow: 'rgba(0, 0, 0, 0.05)',
          padding: '16px',
          gap: '10px',
          cursor: 'pointer',
          '&:hover': {
            boxShadow: theme =>
              `0px 2px 9px 0px rgba(${hex2rgb(theme.palette.accent.main)}
              , 0.25) !important`
          }
        }}
        className='Cards'
      >
        <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }} spacing={3}>
          <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
            <Avatar src={process.env.NEXT_PUBLIC_IMAGES + '/' + client?.user_details?.image} alt='Olivia Sparks'>
              <img src='/images/icons/avatar.png' alt='' />
            </Avatar>{' '}
            <Box>
              <Typography variant='h6'> {client?.name}</Typography>
              <Typography variant='p'>{client?.customer_type}</Typography>
            </Box>
          </Box>
          <div className='cards-hover'>
            <Box onClick={event => handleOpenDelete(client.id, event)}>
              <Delete />
            </Box>
            <Box onClick={handleEditClick}>
              <Edit />
            </Box>
          </div>
        </Stack>

        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <Location /> {client?.location}
        </Typography>
        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <Telephone />{' '}
          {client?.contacts && client.contacts.length > 0
            ? formatPhoneNumbers(client.contacts)
            : 'No contacts available'}
        </Typography>
        {client?.city?.name ? (
          <Typography sx={{ display: 'flex', alignItems: 'center' }}>
            {' '}
            <ArrowLeft /> مدير فرع {client?.city?.name}
          </Typography>
        ) : (
          <Typography sx={{ display: 'flex', alignItems: 'center' }}>
            {' '}
            <ArrowLeftDisable /> غير مسؤول عن فرع
          </Typography>
        )}
      </Card>

      {isSidebarOpen && (
        <SidebarEditBranchManagers
          open={isSidebarOpen}
          handleCloseDrawer={handleCloseSidebar}
          handleDeleteClick={handleDeleteClick}
          Editing={isEditing}
          data={client}
        />
      )}

      {isDeletePopupOpen && (
        <CustomDialogDelete
          open={isDeletePopupOpen}
          onDelete={() => DeleteBranchManagers(client.id)}
          handleClose={handleClose}
          question={`هل أنت متأكد من حذف  (${client.name} / ${client.role})؟`}
          decsription={` هذه العملية لا يمكن التراجع عنها عند إتمامها وسيتم حذف المستخدم نهائياً وكل المعلومات المتعلقة به.`}
        />
      )}
      {console.log('🚀 ~ CustomCardsBranchManagers ~ client:', client)}
    </>
  )
}
