import styled from '@emotion/styled'
import { Box, Stack } from '@mui/system'
import React, { useEffect, useState } from 'react'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button, TextField, Typography } from '@mui/material'
import MenuItem from '@mui/material/MenuItem'
import SidebarAddBranches from 'src/features/branches/add/AddBranches'
import Plus from '../../../../public/images/icons/Plus'
import { useForm } from 'react-hook-form'
import { search } from '../SearchComponet'

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: theme.palette.mode === 'dark' ? theme.palette.light_gray.dark : theme.palette.light_gray.light,
  maxWidth: '432px',
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto'
  }
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '40ch'
    }
  }
}))

export default function CustomTabsBranches({ data, setFilterData }) {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [query, setQuery] = useState('')

  const handleInputChange = e => {
    setQuery(e.target.value)
  }
  useEffect(() => {
    const filteredData = search(data, query, ['name', 'admin_name', 'country_name', 'name'])

    setFilterData(filteredData ? filteredData : data)
  }, [data, query, setFilterData])

  const methods = useForm({
    defaultValues: {
      address_id: '',
      city_id: ''
    },
    mode: 'onChange'
  })
  const { reset } = methods

  const handleCardClick = () => {
    setIsDrawerOpen(true)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box>
          <Search
            style={{
              Width: '432px',
              padding: '8px 12px',
              borderRadius: '4px',
              display: 'flex',
              alignItems: 'center'
            }}
          >
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
          </Search>
        </Box>
        <Box
          sx={{
            display: 'flex',
            padding: 'padding: 8px 12px',
            alignItems: 'center',
            borderRadius: '4px',
            backgroundColor: '#73A3D0'
          }}
        >
          <Button
            sx={{
              lineHeight: 'normal',
              color: '#f2f4f8',
              fontSize: '16px',
              fontWeight: '600',
              display: 'flex',
              alignItems: 'center'
            }}
            onClick={handleCardClick}
          >
            <Plus /> إضافة فرع
          </Button>
        </Box>
      </Stack>
      <Box>
        <SidebarAddBranches handleCloseDrawer={handleCloseDrawer} open={isDrawerOpen} />
      </Box>
    </>
  )
}
