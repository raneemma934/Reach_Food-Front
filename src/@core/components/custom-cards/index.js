import React, { useState } from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Typography, useTheme } from '@mui/material'
import City from '../../../../public/images/cards/city'
import Location from '../../../../public/images/cards/Location'
import Telephone from '../../../../public/images/cards/Telephone'
import Delete from '../../../../public/images/cards/delete'
import Edit from '../../../../public/images/cards/edit'
import CustomDialogDelete from '../custom-delete'
import { deleteUser, fetchData } from 'src/store/apps/user'
import { useDispatch } from 'react-redux'
import SidebarEditUsers from 'src/features/users/clients/edit/EditUsers'
import SidebarEditSalesManager from 'src/features/users/salesmanager/edit/EditSalesManager'
import Link from 'next/link'
import SidebarEditDelegates from 'src/features/users/delegates/edit/EditDelegates'

export default function CustomCards({ client, type, value }) {
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [deleteId, setDeleteId] = useState(null)
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [isEditing, setIsEditing] = useState(false)

  const handleDelete = id => {
    setIsDeletePopupOpen(true)
  }

  const dispatch = useDispatch()

  const DeleteComplaints = id => {
    const props = {
      type: type,
      id: id
    }
    dispatch(deleteUser(props))
    setIsDeletePopupOpen(false)
    setIsDrawerOpen(false)
    setIsSidebarOpen(false)
  }

  const handleCloseSidebar = () => {
    setIsSidebarOpen(false)
  }

  const handleDeleteClick = () => {
    setIsDeletePopupOpen(true)
  }

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  }

  const handleOpenDelete = (id, event) => {
    if (event) {
      event.stopPropagation()
    }
    setIsDeletePopupOpen(true)
  }

  const handleCardClick = () => {
    setIsSidebarOpen(true)
    setIsEditing(false)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  const handleEditClick = event => {
    event.stopPropagation()
    setIsSidebarOpen(true)
    setIsEditing(true)
  }

  const formatPhoneNumbers = contacts => {
    return contacts.map(contact => contact.phone_number).join(' - ')
  }

  const typeTranslations = {
    customer: 'الزبون ',
    delegates: 'المندوب',
    'sales manager': 'مندوب المبيعات'
  }

  const defaultTypeTranslation = 'النوع غير معروف'

  const translatedType = typeTranslations[type] || defaultTypeTranslation

  const hex2rgb = hex => [...hex.match(/\w\w/g)].map(x => parseInt(x, 16))

  return (
    <>
      {type === 'delegates' ? (
        <Card
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            backgroundColor: theme => (theme.palette.mode === 'dark' ? '#3B3C3D' : '#F8F8F8'),
            borderRadius: '8px',
            boxShadow: 'rgba(0, 0, 0, 0.05)',
            padding: '16px',
            cursor: 'pointer',
            gap: '12px',
            '&:hover': {
              boxShadow: theme =>
                `0px 2px 9px 0px rgba(${hex2rgb(theme.palette.accent.main)}
                , 0.25) !important`
            }
          }}
          className='Cards'
        >
          <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }} spacing={3}>
            <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
              <Avatar
                sx={{
                  fontSize: '20px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + client?.image}
                alt='dlivia Sparks'
              >
                {client?.name.slice(0, 1).toUpperCase()}
              </Avatar>
              <Box>
                <Link style={{ textDecoration: 'none' }} href={`/users/${client?.id}`} passHref>
                  <Typography
                    variant='h6'
                    sx={{ color: theme => (theme.palette.mode === 'dark' ? '#D2D2D3' : '#3B3B3B') }}
                  >
                    {' '}
                    {client?.name}
                  </Typography>
                </Link>
                <Typography
                  sx={{ color: theme => (theme.palette.mode === 'dark' ? '#D2D2D3' : '#3B3B3B') }}
                  variant='p'
                >
                  {client?.customer_type_ar}
                </Typography>
              </Box>
            </Box>
            <div className='cards-hover'>
              <Box onClick={event => handleOpenDelete(client.id, event)}>
                <Delete />
              </Box>
              <Box onClick={handleEditClick}>
                <Edit />
              </Box>
            </div>
          </Stack>

          {/* <Typography className='custom-style-label '>
            <City /> {client?.user_details?.address?.city?.name} - {client?.user_details?.address?.city?.country?.name}
          </Typography> */}

          <Typography
            variant='h6'
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '4px',
              color: theme => (theme.palette.mode === 'dark' ? '#D2D2D3' : '#3B3B3B')
            }}
          >
            <Location /> {client?.location?.slice(0, 40)}
          </Typography>
          <Typography
            variant='h6'
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '4px',
              color: theme => (theme.palette.mode === 'dark' ? '#D2D2D3' : '#3B3B3B')
            }}
          >
            <Telephone />{' '}
            {client?.contacts && client.contacts.length > 0
              ? formatPhoneNumbers(client.contacts)
              : 'No contacts available'}
          </Typography>
        </Card>
      ) : (
        <Card
          onClick={handleCardClick}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            backgroundColor: theme => (theme.palette.mode === 'dark' ? '#3B3C3D' : '#F8F8F8'),
            color: theme => (theme.palette.mode === 'dark' ? '#3B3B3B' : '#D2D2D3'),
            borderRadius: '8px',
            boxShadow: 'rgba(0, 0, 0, 0.05)',
            padding: '16px',
            cursor: 'pointer',
            gap: '12px',
            '&:hover': {
              boxShadow: theme =>
                `0px 2px 9px 0px rgba(${hex2rgb(theme.palette.accent.main)}
                , 0.25) !important`
            }
          }}
          className='Cards'
        >
          <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }} spacing={3}>
            <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
              <Avatar
                sx={{
                  fontSize: '20px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + client?.image}
                alt='dlivia Sparks'
              >
                {client?.name.slice(0, 1).toUpperCase()}
              </Avatar>
              <Box>
                <Typography
                  variant='h6'
                  sx={{ color: theme => (theme.palette.mode === 'dark' ? '#D2D2D3' : '#3B3B3B') }}
                >
                  {' '}
                  {client?.name}
                </Typography>
                <Typography
                  variant='p'
                  sx={{ color: theme => (theme.palette.mode === 'dark' ? '#D2D2D3' : '#3B3B3B') }}
                >
                  {client?.customer_type_ar}
                </Typography>
              </Box>
            </Box>
            <div className='cards-hover'>
              <Box onClick={event => handleOpenDelete(client.id, event)}>
                <Delete />
              </Box>
              <Box onClick={handleEditClick}>
                <Edit />
              </Box>
            </div>
          </Stack>

          {/* <Typography className='custom-style-label '>
            <City /> {client?.user_details?.address?.city?.name} - {client?.user_details?.address?.city?.country?.name}
          </Typography> */}

          <Typography
            variant='h6'
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '4px',
              color: theme => (theme.palette.mode === 'dark' ? '#D2D2D3' : '#3B3B3B')
            }}
          >
            <Location />{' '}
            {type === 'customer'
              ? `${client?.address?.area?.slice(0, 20)} - ${client?.location?.slice(0, 40)} `
              : client?.location}
          </Typography>
          <Typography
            variant='h6'
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '4px',
              color: theme => (theme.palette.mode === 'dark' ? '#D2D2D3' : '#3B3B3B')
            }}
          >
            <Telephone />{' '}
            {client?.contacts && client.contacts.length > 0
              ? formatPhoneNumbers(client.contacts)
              : 'No contacts available'}
          </Typography>
        </Card>
      )}

      {isSidebarOpen && type === 'customer' && (
        <SidebarEditUsers
          open={isSidebarOpen}
          handleCloseDrawer={handleCloseSidebar}
          handleDeleteClick={handleDeleteClick}
          Editing={isEditing}
          data={client}
        />
      )}
      {isSidebarOpen && type === 'sales manager' && (
        <SidebarEditSalesManager
          open={isSidebarOpen}
          handleCloseDrawer={handleCloseSidebar}
          handleDeleteClick={handleDeleteClick}
          Editing={isEditing}
          data={client}
        />
      )}
      {isSidebarOpen && type === 'delegates' && (
        <SidebarEditDelegates
          open={isSidebarOpen}
          handleCloseDrawer={handleCloseSidebar}
          handleDeleteClick={handleDeleteClick}
          Editing={isEditing}
          data={client}
        />
      )}

      {isDeletePopupOpen && (
        <CustomDialogDelete
          open={isDeletePopupOpen}
          onDelete={() => DeleteComplaints(client.id)}
          handleClose={handleClose}
          question={`هل أنت متأكد من حذف ${translatedType} (${client?.name})؟`}
          decsription={` هذه العملية لا يمكن التراجع عنها عند إتمامها، سيتم حذف المستخدم نهائياً وكل المعلومات المتعلقة به.`}
        />
      )}
    </>
  )
}
