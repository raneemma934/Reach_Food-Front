import React, { useEffect, useMemo, useState } from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Typography } from '@mui/material'
import City from '../../../../public/images/cards/city'
import Location from '../../../../public/images/cards/Location'
import Telephone from '../../../../public/images/cards/Telephone'
import Delete from '../../../../public/images/cards/delete'
import Edit from '../../../../public/images/cards/edit'
import CustomDialogDelete from '../custom-delete'
import DrawerDetails from 'src/features/complaints/DrawerDetails'
import moment from 'moment'
import { deleteFeedback, fetchFeedback } from 'src/store/apps/complaints'
import { useDispatch, useSelector } from 'react-redux'

export default function CustomComplaintsCards({ client }) {
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const dispatch = useDispatch()

  const DeleteComplaints = id => {
    dispatch(deleteFeedback(id))
    setIsDeletePopupOpen(false)
    setIsDrawerOpen(false)
    dispatch(fetchFeedback())
  }

  const handleOpenDelete = (id, event) => {
    if (event) {
      event.stopPropagation()
    }
    setIsDeletePopupOpen(true)
  }

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  }

  const handleCardClick = () => {
    setIsDrawerOpen(true)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  const date = client?.date
  const formattedDate = date ? moment(date).local('en').format('l - hh:mm A') : ''
  const hex2rgb = hex => [...hex.match(/\w\w/g)].map(x => parseInt(x, 16))

  return (
    <>
      <Card
        onClick={handleCardClick}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          borderRadius: '8px',
          boxShadow: 'rgba(0, 0, 0, 0.05)',
          padding: '16px',
          cursor: 'pointer',
          gap: '12px',
          '&:hover': {
            boxShadow: theme =>
              `0px 2px 9px 0px rgba(${hex2rgb(theme.palette.accent.main)}
              , 0.25) !important`
          }
        }}
        className='Cards'
      >
        <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }} spacing={3}>
          <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
            <Avatar
              sx={{
                width: 52,
                height: 52,
                fontSize: '24px',
                fontWeight: '700',
                backgroundColor: 'rgba(115, 163, 208, 0.25)',
                color: theme => theme.palette.accent.main
              }}
              src={process.env.NEXT_PUBLIC_IMAGES + '/' + client?.user?.image}
              alt='dlivia Sparks'
            >
              {client?.user?.name?.slice(0, 1).toUpperCase()}
            </Avatar>
            <Typography variant='h6'> {client?.user?.name}</Typography>
            <Box></Box>
          </Box>

          <div className='cards-hover'>
            <Box onClick={event => handleOpenDelete(client.id, event)}>
              <Delete />
            </Box>
          </div>
        </Stack>
        <Stack direction={'column'} justifyContent={'space-between'} gap={'12px'}>
          <Typography
            variant='h6'
            sx={{
              color: '#3B3B3B',
              textAlign: 'left',
              fontSize: '16px',
              fontWeight: 400
            }}
          >
            {client?.content?.slice(0, 107)}
          </Typography>

          <Typography
            sx={{
              fontSize: '14px',
              fontWeight: 400
            }}
          >
            {formattedDate}
          </Typography>
        </Stack>
      </Card>
      {isDeletePopupOpen && (
        <CustomDialogDelete
          open={isDeletePopupOpen}
          handleClose={handleClose}
          question={'هل أنت متأكد من حذف هذه الشكوى ؟'}
          decsription={''}
          onDelete={() => DeleteComplaints(client.id)}
        />
      )}

      <Box>
        <DrawerDetails
          content={client?.content}
          img={client?.user?.image}
          handleCloseDrawer={handleCloseDrawer}
          open={isDrawerOpen}
          userName={client?.user?.name}
          setIsDeletePopupOpen={setIsDeletePopupOpen}
          onDelete={() => handleOpenDelete()}
          handleOpenDelete={handleOpenDelete}
        />
      </Box>
    </>
  )
}
