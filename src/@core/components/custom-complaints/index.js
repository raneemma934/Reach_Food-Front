import styled from '@emotion/styled'
import { Box, Stack } from '@mui/system'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '../../../../public/images/icons/Search'
import React, { useEffect, useState } from 'react'
import { search } from '../SearchComponet'

const Search = styled('div')(({ theme }) => ({
  width: '432px',
  padding: '8px 12px',
  borderRadius: '4px',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: theme.palette.mode === 'dark' ? theme.palette.light_gray.dark : theme.palette.light_gray.light
}))

const SearchIconWrapper = styled('div')({
  padding: '0 8px',
  display: 'flex',
  alignItems: 'center'
})

const StyledInputBase = styled(InputBase)({
  flex: 1
})

export default function CustomComplaintsTabs({ setFilterData, data }) {
  const [query, setQuery] = useState('')

  const handleInputChange = e => {
    setQuery(e.target.value)
  }
  useEffect(() => {
    const filteredData = search(data, query, ['content', 'user.name'])

    setFilterData(filteredData ? filteredData : data)
  }, [data, query, setFilterData])

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
          </Search>
        </Box>
      </Stack>
    </>
  )
}
