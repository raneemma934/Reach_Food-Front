import React, { useState } from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Button, Card, Divider, makeStyles, Typography } from '@mui/material'
import Location from '../../../../public/images/cards/Location'
import Telephone from '../../../../public/images/cards/Telephone'
import Arrowleft from '../../../../public/images/cards/arrowleft'
import Edit from '../../../../public/images/cards/editBlue'
import Delete from '../../../../public/images/cards/deleteRed'
import Flights from 'src/pages/flights'
import { TabContext, TabPanel } from '@mui/lab'
import CustomFlightsTabs from '../custom-flight-tab'
import { useDispatch, useSelector } from 'react-redux'
import List from 'src/features/flights/list'
import { fetchTrips } from 'src/store/apps/trips'
import SidebarEditDelegates from 'src/features/users/delegates/edit/EditDelegates'
import CustomDialogDelete from '../custom-delete'
import { deleteUser, fetchData } from 'src/store/apps/user'

export default function CustomDelegatesTabs({ data }) {
  console.log('🚀 ~ CustomDelegatesTabs ~ data:', data)
  const [value, setValue] = useState('1')
  const [Select, setSelect] = useState(false)
  const [SelectDelete, setSelectDelete] = useState()

  const store = useSelector(state => state.trips)
  const dispatch = useDispatch()
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [type, setType] = useState('')
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [isEditing, setIsEditing] = useState(false)

  const days = ['Saturday', 'Sunday', 'Monday', 'Thursday', 'wednesday', 'Tuesday', 'Friday']

  const handleSelectDelete = event => {
    if (event) {
      event.stopPropagation()
    }
    setSelectDelete(!SelectDelete)
  }

  const handleSelect = () => {
    setSelect(!Select)
  }

  const handleChange = (event, newValue) => {
    if (newValue >= 1 && newValue <= 7) {
      dispatch(fetchTrips(`day=${days[newValue - 1]}`))
    }
    setValue(newValue)
  }

  const handleEditClick = event => {
    if (event) {
      event.stopPropagation()
    }
    setIsSidebarOpen(true)
    setIsEditing(true)
  }

  const handleCloseSidebar = () => {
    setIsSidebarOpen(false)
  }

  const handleDeleteClick = () => {
    setIsDeletePopupOpen(true)
  }

  const handleCloseDeletePopup = () => {
    setIsDeletePopupOpen(false)
  }

  const DeleteComplaints = id => {
    dispatch(deleteUser(id))
    setIsDeletePopupOpen(false)

    dispatch(fetchData())
  }

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  }

  const handleCardClick = () => {
    setIsSidebarOpen(true)
    setIsEditing(false)
  }

  return (
    <>
      <Card
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          borderRadius: '8px',
          boxShadow: 'rgba(0, 0, 0, 0.05)',
          padding: '16px',
          gap: '10px'
        }}
        className='Cards'
      >
        <Stack
          direction={'row'}
          justifyContent={'space-between'}
          alignItems={'start'}
          width={'100%'}
          sx={{ mb: '8px' }}
        >
          <Stack direction={'row'} alignItems={'center'} sx={{ gap: '16px' }}>
            <Box>
              <Avatar
                sx={{
                  width: 80,
                  height: 80,
                  fontSize: '32px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
                alt='dlivia Sparks'
              >
                {data?.name?.slice(0, 1).toUpperCase()}
              </Avatar>{' '}
            </Box>
            <Stack sx={{ gap: '4px' }}>
              <Typography
                sx={{
                  fontSize: '16px',
                  fontWeight: '700',
                  lineHeight: '32px'
                }}
              >
                {data?.name}
              </Typography>
              <Typography
                sx={{
                  fontSize: '16px',
                  fontWeight: '500',
                  lineHeight: '25px'
                }}
              >
                اسم المستخدم: {data?.user_name}
              </Typography>
              <Typography
                sx={{
                  fontSize: '16px',
                  fontWeight: '500',
                  lineHeight: '25px'
                }}
              >
                كلمة السر:
              </Typography>
            </Stack>
          </Stack>
          <Stack direction={'row'} sx={{ gap: '24px' }}>
            <Box
              onClick={handleEditClick}
              sx={{
                display: 'flex',
                padding: '8px 12px',
                justifyContent: 'center',
                alignItems: 'center',
                gap: '8px',
                borderRadius: '4px',
                background: `rgba(115, 163, 208, 0.20)`,
                cursor: 'pointer'
              }}
            >
              <Edit />
              <Typography
                sx={{
                  p: 0,
                  color: `#73A3D0`,
                  textAlign: 'center',
                  fontSize: '16px',
                  fontWeight: '600',
                  lineHeight: '30px'
                }}
              >
                تعديل
              </Typography>
            </Box>
            <Box
              onClick={handleDeleteClick}
              sx={{
                display: 'flex',
                padding: '8px 12px',
                justifyContent: 'center',
                alignItems: 'center',
                gap: '8px',
                borderRadius: '4px',
                background: `rgba(209, 46, 61, 0.10)`,
                cursor: 'pointer'

                // '&:hover': {
                //   backgroundColor: '#CE3446',
                //   color: '#F2F4F8'
                // }
              }}
            >
              <Delete />
              <Typography
                sx={{
                  color: '#CE3446',
                  p: 0,
                  textAlign: 'center',
                  fontSize: '16px',
                  fontWeight: '600',
                  lineHeight: '30px'
                }}
              >
                حذف
              </Typography>
            </Box>
          </Stack>
        </Stack>

        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <Telephone /> أرقام التواصل:
          {data?.contacts?.map((phone, index) => (
            <span key={index}>
              {phone.phone_number} {index < data?.contacts?.length - 1 ? ' - ' : ''}
            </span>
          ))}
        </Typography>
        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <Location /> العنوان: {data?.location}
        </Typography>
        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <Arrowleft /> الفرع:{' '}
          {data?.work_branches?.map((branch, i) => (
            <span key={i}>
              {branch?.branch?.name} {i < data?.work_branches?.length - 1 ? ' - ' : ''}
            </span>
          ))}
        </Typography>
        <Stack direction={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'}>
          <Box onClick={handleCardClick}>
            <Button
              sx={{
                textAlign: 'center',
                fontSize: '14px',
                fontStyle: 'normal',
                fontWeight: '500',
                lineHeight: '160%',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                '&:hover': {
                  backgroundColor: 'transparent',
                  color: '#73A3D0'
                }
              }}
            >
              عرض تفاصيل الأفرع
            </Button>
          </Box>
        </Stack>
      </Card>
      <div style={{ padding: '0px' }}>
        <TabContext value={value}>
          <CustomFlightsTabs value={value} handleChange={handleChange} handleSelect={handleSelect} Select={Select} />
          <TabPanel value='1'>
            <Box sx={{ padding: '24px 0px ' }}>
              <List data={store?.trips?.data} handleSelectDelete={handleSelectDelete} SelectDelete={SelectDelete} />
            </Box>
          </TabPanel>
          <TabPanel value='2'>
            <Box sx={{ padding: '24px 0px' }}>
              <List data={store?.trips?.data} handleSelectDelete={handleSelectDelete} SelectDelete={SelectDelete} />
            </Box>
          </TabPanel>
          <TabPanel value='3'>
            <List data={store?.trips?.data} handleSelectDelete={handleSelectDelete} SelectDelete={SelectDelete} />
          </TabPanel>
          <TabPanel value='4'>
            <List data={store?.trips?.data} handleSelectDelete={handleSelectDelete} SelectDelete={SelectDelete} />
          </TabPanel>
          <TabPanel value='5'>
            <List data={store?.trips?.data} handleSelectDelete={handleSelectDelete} SelectDelete={SelectDelete} />
          </TabPanel>
          <TabPanel value='6'>
            <List data={store?.trips?.data} handleSelectDelete={handleSelectDelete} SelectDelete={SelectDelete} />
          </TabPanel>
          <TabPanel value='7'>
            <List data={store?.trips?.data} handleSelectDelete={handleSelectDelete} SelectDelete={SelectDelete} />
          </TabPanel>
        </TabContext>
      </div>

      {isSidebarOpen && (
        <SidebarEditDelegates
          open={isSidebarOpen}
          handleCloseDrawer={handleCloseSidebar}
          handleDeleteClick={handleDeleteClick}
          Editing={isEditing}
          data={data}
        />
      )}

      {isDeletePopupOpen && (
        <CustomDialogDelete
          open={isDeletePopupOpen}
          onDelete={() => DeleteComplaints(client.id)}
          handleClose={handleClose}
          question={`هل أنت متأكد من حذف (نوع المستخدم) (اسم المستخدم)؟`}
          decsription={
            'هذه العملية لا يمكن التراجع عنها عند إتمامها وسيتم حذف المستخدم نهائياً وكل المعلومات المتعلقة به.'
          }
        />
      )}
    </>
  )
}
