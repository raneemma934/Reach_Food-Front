// ** React Imports
import { Fragment, useState } from 'react'

// ** MUI Imports
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import { useTheme } from '@mui/material/styles'
import DialogTitle from '@mui/material/DialogTitle'
import useMediaQuery from '@mui/material/useMediaQuery'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import { Box } from '@mui/system'

const CustomDialogDelete = ({ open, handleClose, question, decsription, onDelete }) => {
  // ** State

  // ** Hooks
  const theme = useTheme()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))

  return (
    <Fragment>
      <Dialog fullScreen={fullScreen} open={open} onClose={handleClose} aria-labelledby='responsive-dialog-title'>
        <DialogTitle
          id='responsive-dialog-title'
          sx={{
            textAlign: 'center',
            fontWeight: '600',
            fontSize: '20px',
            lineHeight: '25px',
            color: theme => (theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light)
          }}
        >
          حذف
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            sx={{
              textAlign: 'center',
              fontWeight: '400',
              fontSize: '16px',
              lineHeight: '25px',
              color: theme => (theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light)
            }}
          >
            {question}
            <br />
            {decsription}
          </DialogContentText>
        </DialogContent>
        <DialogActions className='dialog-actions-dense' sx={{ textAlign: 'center', width: '100%' }}>
          <Box
            sx={{
              textAlign: 'center',
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
              gap: '24px',
              pb: '12px'
            }}
          >
            <Button
              onClick={onDelete}
              sx={{
                width: '103px',
                height: '46px',
                borderRadius: '8px',
                padding: '8px 32px 8px 32px',
                backgroundColor: '#ce3446',
                color: '#F2F4F8',
                fontSize: '16px',
                fontWeight: '600',
                lineHeight: '30px',
                '&:hover': {
                  backgroundColor: '#ce3446'
                }
              }}
            >
              تأكيد
            </Button>

            <Button
              onClick={handleClose}
              sx={{
                width: '99px',
                height: '46px',
                borderRadius: '8px',
                padding: '8px 32px 8px 32px',
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,

                fontWeight: '600',
                fontSize: '16px',
                lineHeight: '30px',
                '&:hover': {
                  backgroundColor: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                }
              }}
              variant='none'
            >
              الغاء
            </Button>
          </Box>
        </DialogActions>
      </Dialog>
    </Fragment>
  )
}

export default CustomDialogDelete
