import React, { useState } from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Typography } from '@mui/material'

import Delete from '../../../../public/images/cards/delete'
import Edit from '../../../../public/images/cards/edit'
import FaqProducts from '../../../../public/images/cards/fakeproducts'
import CustomDialogDelete from '../custom-delete'
import { deleteProducts, fetchProducts } from 'src/store/apps/products'
import { useDispatch } from 'react-redux'
import SidebarEditProducts from 'src/features/products/edit/EditProducts'
import { FormProvider, useForm } from 'react-hook-form'

export default function CustomCardsDetailsProducts({ client }) {
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [isEditing, setIsEditing] = useState(false)
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [isDialogOpen, setIsDialogOpen] = useState(false)

  const methods = useForm({
    mode: 'onChange'
  })
  const { control, formState, getValues, setValue } = methods
  const dispatch = useDispatch()

  const handleOpenDelete = (id, event) => {
    if (event) {
      event.stopPropagation()
    }
    setIsDeletePopupOpen(true)
  }

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  }

  const handleCloseSidebar = () => {
    setIsSidebarOpen(false)
  }

  const DeleteProducts = id => {
    dispatch(deleteProducts(id))
    setIsDeletePopupOpen(false)
    setIsDrawerOpen(false)
    dispatch(fetchProducts())
  }

  const handleDeleteClick = () => {
    setIsDialogOpen(true)
    setIsDeletePopupOpen(true)
  }

  const handleCardClick = () => {
    setIsDrawerOpen(true)
    setIsEditing(false)
    setIsSidebarOpen(true)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  const handleEditClick = event => {
    event.stopPropagation()
    setIsSidebarOpen(true)
    setIsEditing(true)
  }

  return (
    <>
      <Card
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.light_gray.dark : theme.palette.light_gray.light,
          borderRadius: '8px',
          boxShadow: 'none',
          padding: '16px',
          gap: '10px'
        }}
      >
        <Stack direction={'row'} justifyContent={'start'} sx={{ width: '100%' }} spacing={3}>
          <Box>
            {client.image ? (
              <Avatar
                sx={{ bgcolor: '#73A3D026', width: '124px', height: '124px' }}
                variant='rounded'
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + client?.image}
              />
            ) : (
              <Avatar sx={{ bgcolor: '#73A3D026', width: '120px', height: '120px' }} variant='rounded'>
                <FaqProducts />
              </Avatar>
            )}
          </Box>
          <Box>
            <Stack direction='column' justifyContent='flex-start' alignItems='flex-start' spacing={2}>
              <Stack direction={'row'} justifyContent={'space-between'} width={'110%'}>
                <Box>
                  <Typography
                    sx={{
                      textAlign: 'left',
                      fontSize: '15px',
                      fontWeight: 500,
                      lineHeight: '160%',
                      color: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                    }}
                  >
                    {client?.name}
                  </Typography>

                  <Typography
                    sx={{
                      textAlign: 'right',
                      fontSize: '14px',
                      fontWeight: 500,
                      lineHeight: '160%',
                      color: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                    }}
                  >
                    سعر الواحدة : {client?.retail_price}
                    <span style={{ fontSize: '13px', fontWeight: 400, lineHeight: '160%' }}> ل.س</span>
                  </Typography>
                </Box>
                {/* <div
                  className='card-hover-products'
                  style={{ display: 'flex', gap: '8px', position: 'absolute', left: '14px' }}
                >
                  <Box onClick={event => handleOpenDelete(client.id, event)}>
                    <Delete />
                  </Box>
                  <Box onClick={handleEditClick}>
                    <Edit />
                  </Box>
                </div> */}
              </Stack>

              <Typography
                sx={{
                  textAlign: 'right',
                  fontSize: '15px',
                  fontWeight: 500,
                  lineHeight: '160%',
                  color: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                }}
              >
                الكمية الكلية : {client?.amount}
                <span style={{ fontSize: '13px', fontWeight: 400, lineHeight: '160%' }}> قطع </span>
              </Typography>
              <Typography
                sx={{
                  textAlign: 'right',
                  fontSize: '15px',
                  fontWeight: 500,
                  lineHeight: '160%',
                  color: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                }}
              >
                السعر الكلي : {client?.wholesale_price}
                <span style={{ fontSize: '13px', fontWeight: 400, lineHeight: '160%' }}> ل.س </span>
              </Typography>
            </Stack>
          </Box>
        </Stack>
      </Card>
    </>
  )
}
