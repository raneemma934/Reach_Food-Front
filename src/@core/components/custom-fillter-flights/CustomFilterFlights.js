import styled from '@emotion/styled'
import { Box, Stack } from '@mui/system'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import React, { useEffect, useState } from 'react'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button, TextField, Typography } from '@mui/material'
import MenuItem from '@mui/material/MenuItem'
import SidebarAddProducts from 'src/features/products/add/AddProducts'

import Plus from '../../../../public/images/icons/Plus'
import Delete from '../../../../public/images/cards/deleteWihte'
import SidebarAddFlights from 'src/features/flights/add/AddFlights'
import CustomDialogDelete from '../custom-delete'
import { search } from '../SearchComponet'

const schema = yup.object().shape({
  day: yup.string().required('الاسم مطلوب').min(3, 'يجب أن يكون الاسم على الأقل 3 أحرف'),
  address_id: yup.string().required('المسمى الوظيفي مطلوب')
})

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: theme.palette.mode === 'dark' ? theme.palette.light_gray.dark : theme.palette.light_gray.light,
  maxWidth: '432px',
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto'
  }
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '40ch'
    }
  }
}))

export default function CustomFilterFlights({ handleSelectDelete, SelectDelete, setFilterData, data }) {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [isDialogOpen, setIsDialogOpen] = useState(false)
  const [query, setQuery] = useState('')

  const handleCardClick = () => {
    setIsDrawerOpen(true)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  const handleOpenDialog = () => {
    setIsDialogOpen(true)
  }

  const handleCloseDialog = () => {
    setIsDialogOpen(false)
  }

  const handleDelete = () => {
    setIsDialogOpen(false)
  }

  const handleInputChange = e => {
    setQuery(e.target.value)
  }
  useEffect(() => {
    const filteredData = search(data, query, ['start_time', 'customers.name', 'address.area', 'salesman.name'])

    setFilterData(filteredData ? filteredData : data)
  }, [data, query, setFilterData])

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,

          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box>
          <Search
            style={{
              Width: '432px',
              padding: '8px 12px',
              borderRadius: '4px',
              display: 'flex',
              alignItems: 'center'
            }}
          >
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
          </Search>
        </Box>
        {SelectDelete ? (
          <>
            <Stack direction={'row'} sx={{ gap: '24px' }}>
              <Box
                sx={{
                  padding: '6px 12px',
                  borderRadius: 1,
                  fontSize: '16px',
                  color: '#fff',
                  backgroundColor: '#CE3446',
                  display: 'flex',
                  alignItems: 'center',
                  cursor: 'pointer',
                  '&:hover': {
                    backgroundColor: '#CE3446'
                  }
                }}
                onClick={handleOpenDialog}
              >
                <Delete />

                <Button
                  size='small'
                  sx={{
                    fontSize: '16px',
                    color: '#fff',
                    '&:hover': {
                      backgroundColor: 'transparent',
                      color: '#fff'
                    }
                  }}
                >
                  حذف
                </Button>
              </Box>
              <Box
                onClick={handleSelectDelete}
                sx={{
                  padding: '6px 12px',
                  borderRadius: 1,
                  fontSize: '16px',
                  color: '#fff',
                  backgroundColor: '#E0E0E0',
                  display: 'flex',
                  alignItems: 'center',
                  cursor: 'pointer',
                  '&:hover': {
                    backgroundColor: '#E0E0E0'
                  }
                }}
              >
                <Button
                  size='small'
                  sx={{
                    fontSize: '16px',

                    '&:hover': {
                      backgroundColor: 'transparent',
                      color: '#5A5A5A'
                    }
                  }}
                >
                  إلغاء التحديد
                </Button>
              </Box>
            </Stack>
          </>
        ) : (
          <Box
            sx={{
              display: 'flex',
              padding: '8px 12px',
              alignItems: 'center',
              gap: '8px',
              borderRadius: '4px',
              backgroundColor: '#73A3D0',
              height: '48px',
              mt: { xs: 0, sm: 2, md: 0 },
              cursor: 'pointer'
            }}
            onClick={handleCardClick}
          >
            <Stack direction={'row'} alignItems={'center'} spacing={-0.7}>
              <Plus />
              <Button
                sx={{
                  color: '#f2f4f8',
                  fontSize: '15px',
                  fontWeight: '600',
                  fontStyle: 'normal',
                  lineHeight: 'normal',
                  padding: ' 1px 4px ',
                  fontFamily: 'Noto Kufi Arabic'
                }}
              >
                إضافة رحلة
              </Button>
            </Stack>
          </Box>
        )}
      </Stack>
      {isDrawerOpen && (
        <Box>
          <SidebarAddFlights handleCloseDrawer={handleCloseDrawer} open={isDrawerOpen} />
        </Box>
      )}

      <CustomDialogDelete
        open={isDialogOpen}
        handleClose={handleCloseDialog}
        question={`هل أنت متأكد من حذف جميع الرحلات المحددة؟`}
        decsription='هذه العملية لا يمكن التراجع عنها عند إتمامها وسيتم حذف العناصر نهائياً وكل المعلومات المتعلقة بها.'
        onDelete={handleDelete}
      />
    </>
  )
}
