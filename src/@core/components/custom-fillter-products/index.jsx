import styled from '@emotion/styled'
import { Box, Stack } from '@mui/system'
import React, { useEffect, useState } from 'react'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button, TextField, Typography } from '@mui/material'
import MenuItem from '@mui/material/MenuItem'
import SidebarAddProducts from 'src/features/products/add/AddProducts'
import { FormProvider, useForm } from 'react-hook-form'
import Plus from '../../../../public/images/icons/Plus'
import SearchComponent, { search } from '../SearchComponet'
import Download from '../../../../public/images/icons/Download'
import SidebarSupplyProducts from 'src/features/products/supplyProduct/SupplyProducts'

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: theme.palette.mode === 'dark' ? theme.palette.light_gray.dark : theme.palette.light_gray.light,
  maxWidth: '432px',
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto'
  }
}))

const data = ['Apple', 'Banana', 'Cherry', 'Date', 'Elderberry', 'Fig', 'Grape']

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '40ch'
    }
  }
}))

export default function CustomFilterProducts({ setFilterData, data }) {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [isSupplyDrawerOpen, setIsSupplyDrawerOpen] = useState(false)
  const [query, setQuery] = useState('')

  const handleCardClick = () => {
    setIsDrawerOpen(true)
  }

  const handleSupplyClick = () => {
    setIsSupplyDrawerOpen(true)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  const handleCloseSupplyDrawer = () => {
    setIsSupplyDrawerOpen(false)
  }

  const method = useForm({
    mode: 'onChange'
  })

  const handleInputChange = e => {
    setQuery(e.target.value)
  }
  useEffect(() => {
    const filteredData = search(data, query, ['name', 'description', 'amount_unit'])

    setFilterData(filteredData ? filteredData : data)
  }, [data, query, setFilterData])

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box>
          <Search
            style={{
              Width: '432px',
              padding: '8px 12px',
              borderRadius: '4px',
              display: 'flex',
              alignItems: 'center'
            }}
          >
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
          </Search>
        </Box>

        <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
          <Box
            onClick={handleSupplyClick}
            sx={{
              display: 'flex',
              padding: '8px 12px',
              alignItems: 'center',
              borderRadius: '4px',
              backgroundColor: theme => (theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1'),
              cursor: 'pointer',
              gap: '8px'
            }}
          >
            <Download />
            <Button
              sx={{
                fontSize: '16px',
                fontWeight: '600',
                fontStyle: 'normal',
                lineHeight: 'normal',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                backgroundColor: theme => (theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1'),
                padding: ' 0 ',
                '&:hover': { backgroundColor: theme => (theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1') }
              }}
            >
              توريد المنتجات
            </Button>
          </Box>
          <Box
            sx={{
              display: 'flex',
              padding: '8px 12px',
              alignItems: 'center',
              borderRadius: '4px',
              backgroundColor: '#73A3D0',
              cursor: 'pointer',
              gap: '8px'
            }}
            onClick={handleCardClick}
          >
            <Plus />
            <Button
              sx={{
                color: '#f2f4f8',
                fontSize: '16px',
                fontWeight: '600',
                fontStyle: 'normal',
                lineHeight: 'normal',
                padding: ' 0 '
              }}
            >
              إضافة منتج{' '}
            </Button>
          </Box>
        </Stack>
      </Stack>
      <Box>
        <FormProvider {...method}>
          <SidebarAddProducts handleCloseDrawer={handleCloseDrawer} open={isDrawerOpen} />
        </FormProvider>
        <FormProvider {...method}>
          <SidebarSupplyProducts handleCloseDrawer={handleCloseSupplyDrawer} open={isSupplyDrawerOpen} />
        </FormProvider>
      </Box>
    </>
  )
}
