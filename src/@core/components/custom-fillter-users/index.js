import { styled } from '@mui/system'
import { Box, Stack, useTheme } from '@mui/system'
import React, { useEffect, useState } from 'react'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button, TextField, Typography } from '@mui/material'
import MenuItem from '@mui/material/MenuItem'
import Plus from '../../../../public/images/icons/Plus'
import { search } from '../SearchComponet'

const Search = styled('div')(({ theme }) => ({
  width: '432px',
  padding: '8px 12px',
  borderRadius: '4px',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1'
}))

const SearchIconWrapper = styled('div')({
  padding: '0 8px',
  display: 'flex',
  alignItems: 'center'
})

const StyledInputBase = styled(InputBase)({
  flex: 1
})

const CustomFilterUsers = props => {
  const { toggle, setFilterData, data } = props

  // console.log('🚀 ~ CustomFilterUsers ~ data:', data)
  const [query, setQuery] = useState('')

  const handleInputChange = e => {
    setQuery(e.target.value)
  }
  useEffect(() => {
    const filteredData = search(data, query, ['name', 'location', 'customer_type_ar', 'user_name'])

    setFilterData(filteredData ? filteredData : data)
  }, [data, query, setFilterData])

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme => (theme.palette.mode === 'dark' ? '#3B3C3D' : '#F8F8F8'),
          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
          </Search>
        </Box>
        <Box
          onClick={toggle}
          sx={{
            display: 'flex',
            padding: '8px 12px',
            alignItems: 'center',
            borderRadius: '4px',
            gap: '8px',
            backgroundColor: '#73A3D0',

            cursor: 'pointer',
            mt: { xs: 0, sm: 2, md: 0 }
          }}
        >
          <Plus />
          <Button sx={{ color: '#F2F4F8', fontSize: '16px', fontWeight: '600', padding: '0' }}>إضافة مستخدم</Button>
        </Box>
      </Stack>
    </>
  )
}

export default CustomFilterUsers
