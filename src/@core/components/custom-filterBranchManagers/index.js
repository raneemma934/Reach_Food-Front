import styled from '@emotion/styled'
import { Box, Stack } from '@mui/system'
import React, { useEffect, useState } from 'react'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button, TextField, Typography } from '@mui/material'
import MenuItem from '@mui/material/MenuItem'
import Plus from '../../../../public/images/icons/Plus'
import { search } from '../SearchComponet'

const Search = styled('div')(({ theme }) => ({
  width: '432px',
  padding: '8px 12px',
  borderRadius: '4px',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: theme.palette.mode === 'dark' ? theme.palette.light_gray.dark : theme.palette.light_gray.light
}))

const SearchIconWrapper = styled('div')({
  padding: '0 8px',
  display: 'flex',
  alignItems: 'center'
})

const StyledInputBase = styled(InputBase)({
  flex: 1
})

const CustomFilterBranchManagers = props => {
  const { toggle, data, setFilterData } = props

  // console.log('🚀 ~ CustomFilterBranchManagers ~ data:', data)
  const [query, setQuery] = useState('')

  const handleInputChange = e => {
    setQuery(e.target.value)
  }
  useEffect(() => {
    const filteredData = search(data, query, ['name', 'location'])

    setFilterData(filteredData ? filteredData : data)
  }, [data, query, setFilterData])

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
          </Search>
        </Box>

        <Box
          onClick={toggle}
          sx={{
            display: 'flex',
            padding: '8px 12px',
            alignItems: 'center',
            borderRadius: '4px',
            gap: '8px',
            backgroundColor: '#73A3D0',
            height: '48px',
            cursor: 'pointer',
            mt: { xs: 0, sm: 2, md: 0 }
          }}
        >
          <Plus />
          <Button sx={{ color: '#f2f4f8', fontSize: '16px', fontWeight: '600', padding: '0' }}>إضافة مدير</Button>
        </Box>
      </Stack>
    </>
  )
}

export default CustomFilterBranchManagers
