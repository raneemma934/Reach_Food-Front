import React, { useState } from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Checkbox, FormControlLabel, Typography } from '@mui/material'
import Delete from '../../../../public/images/cards/delete'
import Edit from '../../../../public/images/cards/edit'
import CustomDialogDelete from '../custom-delete'
import { useDispatch } from 'react-redux'
import Location from '../../../../public/images/cards/Location'
import Clock from '../../../../public/images/cards/clock'
import SidebarEditFlight from 'src/features/flights/edit/EditFlight'
import styled from '@emotion/styled'
import { deleteTrips } from 'src/store/apps/trips'
import { formatTimeAr } from 'src/@core/utils/FormatTimeAr'

const BpIcon = styled('span')(({ theme }) => ({
  borderRadius: 3,
  width: 16,
  height: 16,
  boxShadow:
    theme.palette.mode === 'dark'
      ? '0 0 0 1px rgb(16 22 26 / 40%)'
      : 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
  backgroundColor: theme.palette.mode === 'dark' ? '#394b59' : '#f5f8fa',
  backgroundImage:
    theme.palette.mode === 'dark'
      ? 'linear-gradient(180deg,hsla(0,0%,100%,.05),hsla(0,0%,100%,0))'
      : 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
  '.Mui-focusVisible &': {
    outline: '2px auto rgba(19,124,189,.6)',
    outlineOffset: 2
  },
  'input:hover ~ &': {
    backgroundColor: theme.palette.mode === 'dark' ? '#30404d' : '#ebf1f5'
  },
  'input:disabled ~ &': {
    boxShadow: 'none',
    background: theme.palette.mode === 'dark' ? 'rgba(57,75,89,.5)' : 'rgba(206,217,224,.5)'
  }
}))

const BpCheckedIcon = styled(BpIcon)({
  backgroundColor: '#137cbd',
  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
  '&::before': {
    display: 'block',
    width: 16,
    height: 16,
    backgroundImage:
      "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
      " fillRule='evenodd' clipRule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
      "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
    content: '""'
  },
  'input:hover ~ &': {
    backgroundColor: '#106ba3'
  }
})

function BpCheckbox(props) {
  return (
    <Checkbox
      sx={{
        '&:hover': { bgcolor: 'transparent' }
      }}
      disableRipple
      color='default'
      checkedIcon={<BpCheckedIcon />}
      icon={<BpIcon />}
      inputProps={{ 'aria-label': 'Checkbox demo' }}
      {...props}
    />
  )
}

export default function CustomCardsFlight({ client, handleSelectDelete, SelectDelete, type }) {
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [deleteId, setDeleteId] = useState(null)
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [isEditing, setIsEditing] = useState(false)

  const handleDelete = id => {
    setIsDeletePopupOpen(true)
  }

  const dispatch = useDispatch()

  const DeleteComplaints = id => {
    const props = {
      type: type,
      id: id
    }

    dispatch(deleteTrips(props))

    setIsDeletePopupOpen(false)
    setIsDrawerOpen(false)
    setIsSidebarOpen(false)
  }

  const handleCloseSidebar = () => {
    setIsSidebarOpen(false)
  }

  const handleDeleteClick = () => {
    setIsDeletePopupOpen(true)
  }

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  }

  const handleOpenDelete = (id, event) => {
    if (event) {
      event.stopPropagation()
    }
    setIsDeletePopupOpen(true)
  }

  const handleCardClick = () => {
    setIsSidebarOpen(true)
    setIsEditing(false)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  const handleEditClick = event => {
    event.stopPropagation()
    setIsSidebarOpen(true)
    setIsEditing(true)
  }

  const [checked, setChecked] = useState(false)

  const handleChange = event => {
    setChecked(event.target.checked)
  }
  const hex2rgb = hex => [...hex.match(/\w\w/g)].map(x => parseInt(x, 16))

  return (
    <>
      <Card
        onClick={handleCardClick}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          borderRadius: '8px',
          boxShadow: 'rgba(0, 0, 0, 0.05)',
          padding: '16px',
          cursor: 'pointer',
          gap: '8px',
          '&:hover': {
            boxShadow: theme =>
              `0px 2px 9px 0px rgba(${hex2rgb(theme.palette.accent.main)}
              , 0.25) !important`
          }
        }}
        className='Cards'
      >
        <Stack direction={'row'} sx={{ width: '100%' }}>
          <Stack direction={'column'} sx={{ gap: '8px', width: '100%' }}>
            <Typography
              sx={{
                display: 'flex',
                alignItems: 'center',
                gap: '4px',
                fontSize: '16px',
                fontWeight: '600'
              }}
            >
              <Location /> {client?.address?.area}
            </Typography>

            <Typography
              variant='h6'
              sx={{
                display: 'flex',
                alignItems: 'center',
                gap: '4px',
                fontSize: '14px',
                fontWeight: '500',
                lineHeight: '25px'
              }}
            >
              <Clock /> وقت الأنطلاق: {formatTimeAr(client?.start_time)}
            </Typography>
          </Stack>
          <div className='cards-hover'>
            <Box onClick={event => handleOpenDelete(client.id, event)}>
              <Delete />
            </Box>
            <Box onClick={handleEditClick}>
              <Edit />
            </Box>
            {/* <Box onClick={handleSelectDelete} sx={{ position: 'relative', bottom: '9px', right: '4px' }}>
              <BpCheckbox checked={SelectDelete} onChange={handleChange} inputProps={{ 'aria-label': 'controlled' }} />
            </Box> */}
          </div>
        </Stack>
        <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }} spacing={3}>
          <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
            <Avatar
              sx={{
                fontSize: '20px',
                fontWeight: '700',
                backgroundColor: 'rgba(115, 163, 208, 0.25)',
                color: theme => theme.palette.accent.main
              }}
              src={process.env.NEXT_PUBLIC_IMAGES + '/' + client?.salesman?.image}
              alt='dlivia Sparks'
            >
              {client?.salesman?.name ? client?.salesman?.name.slice(0, 1).toUpperCase() : 'A'}
            </Avatar>
            <Box>
              <Typography
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '4px',
                  fontSize: '14px',
                  fontWeight: '500',
                  lineHeight: '25px'
                }}
              >
                {' '}
                {client?.salesman?.name ? client?.salesman?.name : 'لا يوجد مندوب للرحلة'}
              </Typography>
            </Box>
          </Box>
        </Stack>

        <Stack direction={'row'} padding={'12px 0px 0px 0px'}>
          <Box>
            <Typography
              sx={{
                fontSize: '14px',
                fontWeight: '500',
                lineHeight: '25px'
              }}
            >
              {' '}
              الزبائن عدد :{' '}
            </Typography>
          </Box>
          <Box>
            {client?.customer_times.length > 0 ? (
              client?.customer_times.map((customer, index) => (
                <Typography
                  key={index}
                  sx={{
                    fontSize: '14px',
                    fontWeight: '500',
                    lineHeight: '25px',

                    display: 'inline-flex',
                    paddingX: '5px'
                  }}
                >
                  {customer?.customer?.name}
                  {index < client.customer_times.length - 1 && '  -   '}
                </Typography>
              ))
            ) : (
              <Typography
                sx={{
                  fontSize: '14px',
                  fontWeight: '500',
                  lineHeight: '25px',

                  display: 'inline-flex'
                }}
              >
                {' '}
                لا يوجد زبائن
              </Typography>
            )}
          </Box>
        </Stack>
      </Card>
      {isSidebarOpen && (
        <SidebarEditFlight
          open={isSidebarOpen}
          handleCloseDrawer={handleCloseSidebar}
          handleDeleteClick={handleDeleteClick}
          Editing={isEditing}
          data={client}
        />
      )}

      {isDeletePopupOpen && (
        <CustomDialogDelete
          open={isDeletePopupOpen}
          onDelete={() => DeleteComplaints(client.id)}
          handleClose={handleClose}
          question={`هل أنت متأكد من حذف رحلة (${client?.address?.area})؟`}
          decsription={`  هذه العملية لا يمكن التراجع عنها عند إتمامها وسيتم حذف الرحلة نهائياً وكل المعلومات المتعلقة بها.`}
        />
      )}
    </>
  )
}
