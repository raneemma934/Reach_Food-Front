import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import { styled } from '@mui/material/styles'
import MuiTabList from '@mui/lab/TabList'
import { border, borderBottom, Stack } from '@mui/system'
import { Button, Checkbox, FormControlLabel, Typography } from '@mui/material'

const CustomFlightsTabs = ({ value, handleSelect, Select, handleChange }) => {
  const TabList = styled(MuiTabList)(({ theme }) => ({
    borderBottom: '0 !important',
    '&, & .MuiTabs-scroller': {
      boxSizing: 'content-box',
      padding: theme.spacing(1.25, 1.25, 2),
      margin: theme.spacing(-1.25, 1.75, -2)
    },
    '& .MuiTabs-indicator': {
      backgroundColor: '#73A3D0',
      height: '3px',
      display: Select ? 'none' : ''
    },
    '& .MuiTab-root.Mui-selected': {
      backgroundColor: 'transparent',
      color: '#73A3D0 ',
      '&:hover': {
        color: '#73A3D0',
        backgroundColor: 'transparent'
      }
    },

    '& .MuiTab-root': {
      lineHeight: 1,
      borderRadius: theme.shape.borderRadius,
      backgroundColor: Select
        ? 'transparent'
        : theme.palette.mode === 'dark'
        ? theme.palette.base.dark
        : theme.palette.base.light,
      padding: '8px 24px 8px 24px !important',
      borderRadius: Select ? '0px' : '8px',
      color: Select
        ? '#73A3D0'
        : theme.palette.mode === 'dark'
        ? theme.palette.secondary.dark
        : theme.palette.secondary.light,
      borderBottom: Select ? '3px solid #73A3D0' : '',
      marginRight: '24px',
      '&:hover': {
        color: '#73A3D0',
        backgroundColor: 'rgba(115, 163, 208, 0.25);'
      }
    },
    '&:last-child': {
      marginRight: 0
    }
  }))

  return (
    <Stack direction={'row'} alignItems={'center'}>
      <Typography
        sx={{
          color: theme => (theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light),

          fontSize: '24px',
          fontWeight: '600',
          lineHeight: '38.4px'
        }}
      >
        الرحلات
      </Typography>
      <TabContext value={value}>
        <TabList sx={{ flex: '1' }} onChange={handleChange} aria-label='customized tabs example'>
          <Tab value='1' label='السبت' />
          <Tab value='2' label='الأحد' />
          <Tab value='3' label='الأثنين' />
          <Tab value='4' label='الثلاثاء' />
          <Tab value='5' label='الأربعاء' />
          <Tab value='6' label='الخميس' />
          <Tab value='7' label='الجمعة' />
        </TabList>
      </TabContext>

      <FormControlLabel onClick={handleSelect} control={<Checkbox />} label='عرض الكل' />
    </Stack>
  )
}

export default CustomFlightsTabs
