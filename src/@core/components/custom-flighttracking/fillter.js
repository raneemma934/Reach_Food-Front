import { Box, Stack } from '@mui/system'
import React, { useMemo, useState } from 'react'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button } from '@mui/material'
import styled from '@emotion/styled'
import InputBase from '@mui/material/InputBase'
import { CustomDatePicker } from './DatePicker'
import DateLogo from '../../../../public/images/cards/date'

const Search = styled('div')(({ theme }) => ({
  width: '332px',
  padding: '8px 12px',
  borderRadius: '4px',
  display: 'flex',
  alignItems: 'center',
  backgroundColor: theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1'
}))

const SearchIconWrapper = styled('div')({
  padding: '0 8px',
  display: 'flex',
  alignItems: 'center'
})

const StyledInputBase = styled(InputBase)({
  flex: 1
})

const handleSearchChange = event => {
  setfirst(event.target.value)
}

export default function Filter() {
  const [datePickerOpen, setDatePickerOpen] = useState(false)

  const handleOpenDatePicker = () => {
    setDatePickerOpen(true)
  }

  const handleCloseDatePicker = () => {
    setDatePickerOpen(false)
  }

  const [startDate, setStartDate] = useState(new Date())

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: '#fff',
          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box sx={{ display: 'flex', alignItems: 'center', gap: '24px' }}>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleSearchChange} />
          </Search>
          <Box
            sx={{
              display: 'flex',
              width: '46px',
              height: '46px',
              padding: '7px',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: '8px',
              backgroundColor: theme => (theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1'),
              cursor: 'pointer'
            }}
            onClick={handleOpenDatePicker}
          >
            <DateLogo />
          </Box>
        </Box>
      </Stack>

      {datePickerOpen && (
        <CustomDatePicker
          datePickerOpen={datePickerOpen}
          setStartDate={setStartDate}
          startDate={startDate}
          handleClose={handleCloseDatePicker}
        />
      )}
    </>
  )
}
