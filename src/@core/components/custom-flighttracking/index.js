import styled from '@emotion/styled'
import { Box, Stack } from '@mui/system'
import React, { useEffect, useMemo, useState } from 'react'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button, TextField, Typography } from '@mui/material'
import Download from 'public/images/cards/download'
import DrawerUpload from 'src/features/flighttracking/DrawerUpload'
import CustomDatePicker from '../custom-archiv-tab/DatePicker'
import { search } from '../SearchComponet'
import { useSelector } from 'react-redux'
import DateLogo from 'public/images/cards/date'

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: theme.palette.mode === 'dark' ? theme.palette.light_gray.dark : theme.palette.light_gray.light,
  maxWidth: '432px',
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto'
  }
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '40ch'
    }
  }
}))

export default function CustomFlightTracking({ setFilterData, data }) {
  const [open, setOpen] = useState(false)
  const [datePickerOpen, setDatePickerOpen] = useState(false)
  const [checked, setChecked] = useState([])
  const [searchTerm, setSearchTerm] = useState('')
  const [query, setQuery] = useState('')
  const { archivedDay } = useSelector(state => state.request)

  const handleInputChange = e => {
    setQuery(e.target.value)
  }
  useEffect(() => {
    const filteredData = search(data, query, ['start_date', 'start_time', 'trip.day_ar', 'trip.salesman.name'])

    setFilterData(filteredData ? filteredData : data)
  }, [data, query, setFilterData])

  const handleOpenDatePicker = () => {
    setDatePickerOpen(true)
  }

  const handleCloseDatePicker = () => {
    setDatePickerOpen(false)
  }

  const handleOpenDrawer = () => {
    setOpen(true)
  }

  const handleCloseDrawer = () => {
    setOpen(false)
    setChecked([])
    setSearchTerm('')
  }

  useMemo(() => setSearchTerm, [setSearchTerm])

  const [startDate, setStartDate] = useState(new Date(archivedDay))

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          padding: '16px 0px 16px 16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Stack direction={'row'} sx={{ gap: '24px' }}>
          <Box>
            <Search
              style={{
                Width: '432px',
                padding: '8px 12px',
                borderRadius: '4px',
                display: 'flex',
                alignItems: 'center'
              }}
            >
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
            </Search>
          </Box>
          <Box
            sx={{
              display: 'flex',
              width: '46px',
              height: '46px',
              padding: '7px',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: '8px',
              backgroundColor: theme => (theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1'),
              cursor: 'pointer'
            }}
            onClick={handleOpenDatePicker}
          >
            <DateLogo />
          </Box>
          <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            {archivedDay.length > 0 ? archivedDay : 'الرجاء ادخال التاريخ'}
          </Box>
        </Stack>

        <Box
          onClick={handleOpenDrawer}
          sx={{
            display: 'flex',
            padding: '8px 24px 8px 24px ',
            alignItems: 'center',
            gap: '8px',
            backgroundColor: theme =>
              theme.palette.mode === 'dark' ? theme.palette.light_gray.dark : theme.palette.light_gray.light,
            cursor: 'pointer',
            borderRadius: '8px',
            marginRight: '25px'
          }}
        >
          <Download />
          <Typography>تصدير التقرير</Typography>
        </Box>
      </Stack>

      {datePickerOpen && (
        <CustomDatePicker
          datePickerOpen={datePickerOpen}
          setStartDate={setStartDate}
          startDate={startDate}
          handleClose={handleCloseDatePicker}
        />
      )}

      {open && (
        <DrawerUpload
          open={open}
          handleCloseDrawer={handleCloseDrawer}
          setChecked={setChecked}
          checked={checked}
          setSearchTerm={setSearchTerm}
          searchTerm={searchTerm}
        />
      )}
    </>
  )
}
