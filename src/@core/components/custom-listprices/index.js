import styled from '@emotion/styled'
import { Box, Stack } from '@mui/system'
import React, { useCallback, useEffect, useState } from 'react'
import InputBase from '@mui/material/InputBase'
import SearchIcon from '../../../../public/images/icons/Search'
import { Button, TextField, Typography } from '@mui/material'
import EditIcon from '../../../../public/images/cards/edit'
import { useMyContext } from 'src/features/ListPrices/hook/editPriceContext'
import { useFormContext } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { editPricePProducts } from 'src/store/apps/products'
import { search } from '../SearchComponet'

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,

  backgroundColor: theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1',
  maxWidth: '432px',

  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto'
  }
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '40ch'
    }
  }
}))

export default function CustomTapsListPrices({ setFilterData, data }) {
  const [isEditing, setIsEditing] = useState(false)
  const { setEdit } = useMyContext()
  const [query, setQuery] = useState('')

  const handleInputChange = e => {
    setQuery(e.target.value)
  }
  useEffect(() => {
    const filteredData = search(data, query, ['name', 'retail_price', 'wholesale_price'])

    setFilterData(filteredData ? filteredData : data)
  }, [data, query, setFilterData])

  const methods = useFormContext()
  const dispatch = useDispatch()

  const { getValues } = methods

  const toggleEditPrice = useCallback(
    shouldEdit => {
      setIsEditing(shouldEdit)
      setEdit(shouldEdit)
    },
    [setEdit, setIsEditing]
  )

  const handleSavePriceProducts = () => {
    dispatch(editPricePProducts(getValues()))
    setEdit(false)
    setIsEditing(false)
  }

  return (
    <>
      <Stack
        sx={{
          display: 'flex',
          flexDirection: { xs: 'column', sm: 'row', md: 'row' },
          gap: { xs: '10px', sm: '0', md: '0' },
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          padding: '16px',
          borderRadius: '8px'
        }}
        justifyContent={'space-between'}
      >
        <Box>
          <Search
            style={{
              Width: '432px',
              padding: '8px 12px',
              borderRadius: '4px',
              display: 'flex',
              alignItems: 'center'
            }}
          >
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase placeholder='بحث' inputProps={{ 'aria-label': 'search' }} onChange={handleInputChange} />
          </Search>
        </Box>

        {isEditing === true ? (
          <>
            <Box sx={{ display: 'flex', alignItems: 'center', gap: '24px' }}>
              <Button
                sx={{
                  color: '#f2f4f8',
                  fontSize: '16px',
                  fontWeight: '600',
                  display: 'flex',
                  alignItems: 'center',
                  p: '8px 24px',
                  backgroundColor: '#73A3D0',
                  '&:hover': { backgroundColor: '#73A3D0' }
                }}
                onClick={handleSavePriceProducts}
              >
                حفظ
              </Button>
              <Button
                sx={{
                  color: '#f2f4f8',
                  fontSize: '16px',
                  fontWeight: '600',
                  display: 'flex',
                  alignItems: 'center',
                  p: '8px 24px',
                  color: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light,
                  backgroundColor: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                  '&:hover': {
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                  }
                }}
                onClick={() => toggleEditPrice(false)}
              >
                إلغاء{' '}
              </Button>
            </Box>
          </>
        ) : (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              borderRadius: '4px',
              backgroundColor: '#73A3D0'
            }}
          >
            <Button
              sx={{
                color: '#f2f4f8',
                fontSize: '16px',
                fontWeight: '600',
                display: 'flex',
                alignItems: 'center',
                p: '8px 24px',
                gap: '8px',
                backgroundColor: '#73A3D0',
                '&:hover': { backgroundColor: '#73A3D0' }
              }}
              onClick={() => toggleEditPrice(true)}
            >
              <EditIcon color={'w'} />
              تعديل الأسعار
            </Button>
          </Box>
        )}
      </Stack>
    </>
  )
}
