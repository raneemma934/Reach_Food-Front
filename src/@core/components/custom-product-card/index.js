import React, { useState } from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Typography } from '@mui/material'

import Delete from '../../../../public/images/cards/delete'
import Edit from '../../../../public/images/cards/edit'
import FaqProducts from '../../../../public/images/cards/fakeproducts'
import CustomDialogDelete from '../custom-delete'
import { deleteProducts, fetchProducts } from 'src/store/apps/products'
import { useDispatch } from 'react-redux'
import SidebarEditProducts from 'src/features/products/edit/EditProducts'
import { FormProvider, useForm } from 'react-hook-form'

export default function CustomCardsProducts({ client }) {
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)
  const [isEditing, setIsEditing] = useState(false)
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)
  const [isDialogOpen, setIsDialogOpen] = useState(false)

  const methods = useForm({
    mode: 'onChange'
  })
  const { control, formState, getValues, setValue } = methods
  const dispatch = useDispatch()

  const handleOpenDelete = (id, event) => {
    if (event) {
      event.stopPropagation()
    }
    setIsDeletePopupOpen(true)
  }

  const handleClose = () => {
    setIsDeletePopupOpen(false)
  }

  const handleCloseSidebar = () => {
    setIsSidebarOpen(false)
  }

  const DeleteProducts = id => {
    dispatch(deleteProducts(id))
    setIsDeletePopupOpen(false)
    setIsDrawerOpen(false)
    setIsSidebarOpen(false)
  }

  const handleDeleteClick = () => {
    setIsDialogOpen(true)
    setIsDeletePopupOpen(true)
  }

  const handleCardClick = () => {
    setIsDrawerOpen(true)
    setIsEditing(false)
    setIsSidebarOpen(true)
  }

  const handleCloseDrawer = () => {
    setIsDrawerOpen(false)
  }

  const handleEditClick = event => {
    event.stopPropagation()
    setIsSidebarOpen(true)
    setIsEditing(true)
  }
  const hex2rgb = hex => [...hex.match(/\w\w/g)].map(x => parseInt(x, 16))

  return (
    <>
      <Card
        onClick={handleCardClick}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          backgroundColor: '#F8F8F8',
          borderRadius: '8px',
          boxShadow: 'rgba(0, 0, 0, 0.05)',
          padding: '16px',
          gap: '10px',
          cursor: 'pointer',
          position: 'relative',
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          '&:hover': {
            boxShadow: theme =>
              `0px 2px 9px 0px rgba(${hex2rgb(theme.palette.accent.main)}
              , 0.25) !important`
          }
        }}
        className='Cards'
      >
        <Stack direction={'row'} justifyContent={'start'} sx={{ width: '100%' }} spacing={3}>
          <Box>
            <Avatar
              sx={{
                borderRadius: '10px',
                width: 152,
                height: 152,
                fontSize: '32px',
                fontWeight: '700',
                backgroundColor: 'rgba(115, 163, 208, 0.25)',
                color: theme => theme.palette.accent.main
              }}
              src={process.env.NEXT_PUBLIC_IMAGES + '/' + client?.image}
              alt='dlivia Sparks'
            >
              {client?.name.slice(0, 1).toUpperCase()}
            </Avatar>
          </Box>
          <Box>
            <Stack direction='column' justifyContent='flex-start' alignItems='flex-start' spacing={2}>
              <Stack direction={'row'} justifyContent={'space-between'} width={'110%'}>
                <Box>
                  <Typography
                    sx={{
                      textAlign: 'left',
                      fontSize: '15px',
                      fontWeight: 500,
                      lineHeight: '160%',
                      color: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                    }}
                  >
                    {client?.name?.slice(0, 20)}
                    {client?.name?.length > 20 ? '...' : ''}
                  </Typography>
                  <Typography sx={{ textAlign: 'left', fontSize: '14px', fontWeight: 400, lineHeight: '160%' }}>
                    {client?.amount_unit}
                  </Typography>
                </Box>
                <div className='card-hover-products' style={{ position: 'absolute', left: '14px' }}>
                  <Box onClick={event => handleOpenDelete(client.id, event)}>
                    <Delete />
                  </Box>
                  <Box onClick={handleEditClick}>
                    <Edit />
                  </Box>
                </div>
              </Stack>

              <Typography
                sx={{
                  textAlign: 'right',
                  fontSize: '15px',
                  fontWeight: 500,
                  lineHeight: '160%',
                  color: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                }}
              >
                السعر للجملة :{' '}
                <span style={{ fontSize: '13px', fontWeight: 400, lineHeight: '160%' }}>
                  {client?.wholesale_price} ل.س
                </span>
              </Typography>
              <Typography
                sx={{
                  textAlign: 'right',
                  fontSize: '15px',
                  fontWeight: 500,
                  lineHeight: '160%',
                  color: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                }}
              >
                السعر للمفرق :{' '}
                <span style={{ fontSize: '13px', fontWeight: 400, lineHeight: '24px' }}>
                  {client?.retail_price} ل.س{' '}
                </span>
              </Typography>
              {client?.description && (
                <>
                  <Typography
                    sx={{
                      textAlign: 'right',
                      fontSize: '14px',
                      mt: 0,
                      color: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light
                    }}
                  >
                    {client?.description?.length > 30 ? client?.description?.slice(0, 30) + '...' : client?.description}
                  </Typography>
                </>
              )}
            </Stack>
          </Box>
        </Stack>
      </Card>
      {isSidebarOpen && (
        <FormProvider {...methods}>
          <SidebarEditProducts
            open={isSidebarOpen}
            handleCloseDrawer={handleCloseSidebar}
            handleDeleteClick={handleDeleteClick}
            Editing={isEditing}
            data={client}
          />
        </FormProvider>
      )}
      {isDeletePopupOpen && (
        <CustomDialogDelete
          open={isDeletePopupOpen}
          handleClose={handleClose}
          question={`هل أنت متأكد من حذف المنتج (${client?.name})؟`}
          decsription={`هذه العملية لا يمكن التراجع عنها عند إتمامها وسيتم حذف المنتج نهائياً وكل المعلومات المتعلقة به .`}
          onDelete={() => DeleteProducts(client.id)}
        />
      )}
    </>
  )
}
