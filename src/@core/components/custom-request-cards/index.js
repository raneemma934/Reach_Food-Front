import React from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Divider, makeStyles, Typography } from '@mui/material'
import City from '../../../../public/images/cards/city'
import Location from '../../../../public/images/cards/Location'
import Telephone from '../../../../public/images/cards/Telephone'
import Delete from '../../../../public/images/cards/delete'
import Edit from '../../../../public/images/cards/edit'
import DeliveryTime from '../../../../public/images/cards/timedelivery'
import TotalPrices from '../../../../public/images/cards/totalprices'
import Link from 'next/link'
import { useParams } from 'next/navigation'
import { formatDateAr } from 'src/@core/utils/FormatTimeAr'

export default function CustomRequestsCards({ client }) {
  const id = useParams()

  const hex2rgb = hex => [...hex.match(/\w\w/g)].map(x => parseInt(x, 16))

  return (
    <Link style={{ textDecoration: 'none' }} href={`/requests/${client.id}`}>
      {' '}
      <Card
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          backgroundColor: theme =>
            theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
          borderRadius: '8px',
          boxShadow: 'rgba(0, 0, 0, 0.05)',
          padding: '16px',
          gap: '10px',
          cursor: 'pointer',
          '&:hover': {
            boxShadow: theme =>
              `0px 2px 9px 0px rgba(${hex2rgb(theme.palette.accent.main)}
              , 0.25) !important`
          }
        }}
        className='Cards-Requests'
      >
        <Stack direction={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'}>
          <Box>
            <Typography
              sx={{
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light,
                fontSize: '20px',
                fontWeight: '600',
                '&:hover': {
                  Color: 'blue'
                }
              }}
              className='request'
            >
              الطلب #{client?.id}
            </Typography>
          </Box>
          <Box>
            {client?.is_base === 0 ? (
              <Typography
                sx={{
                  textAlign: 'center',
                  fontSize: '14px',
                  fontWeight: '500',
                  lineHeight: '160%'
                }}
              >
                تم التعديل
              </Typography>
            ) : (
              ''
            )}
          </Box>
        </Stack>
        <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }} spacing={3}>
          <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
            <Avatar
              sx={{
                fontSize: '20px',
                fontWeight: '700',
                backgroundColor: 'rgba(115, 163, 208, 0.25)',
                color: theme => theme.palette.accent.main
              }}
              src={process.env.NEXT_PUBLIC_IMAGES + '/' + client?.customer?.image}
              alt='dlivia Sparks'
            >
              {client?.customer?.name?.slice(0, 1).toUpperCase()}
            </Avatar>
            <Box>
              <Typography variant='h6'> {client?.customer?.name}</Typography>
            </Box>
          </Box>
        </Stack>
        <Typography variant='h6'>المندوب : {client?.trip_date?.trip?.salesman?.name}</Typography>
        <Divider
          sx={{
            margin: '4px 0px 4px 0px !important',

            width: '100%'
          }}
        />
        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <Location /> المنطقة: {client?.trip_date?.address.area}
        </Typography>
        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <DeliveryTime /> موعد التسليم : {client?.delivery_date + ' - ' + client?.delivery_time}
        </Typography>
        <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
          <TotalPrices />
          السعر الكلي : {client?.total_price} ل.س
        </Typography>
        <Stack direction={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'}>
          <Box>
            <Typography
              variant='h6'
              sx={{
                textAlign: 'center',
                fontSize: '14px',
                fontStyle: 'normal',
                fontWeight: '500',
                lineHeight: '160%'
              }}
            >
              تاريخ الطلب : {formatDateAr(client?.order_date) ? formatDateAr(client?.order_date) : 'لايوجد تاريخ'}
            </Typography>
          </Box>
          <Box
            sx={{
              display: 'flex',
              padding: '8px 16px',
              justifyContent: 'center',
              alignItems: 'center',
              gap: '10px',
              borderRadius: '4px',
              background: `${client.status === 'accepted' ? 'rgba(115, 163, 208, 0.10)' : 'rgba(209, 46, 61, 0.10)'}`
            }}
          >
            <Typography
              sx={{
                color: `${client.status === 'accepted' ? '#73A3D0' : '#CE3446'}`,
                textAlign: 'center',
                fontSize: '14px',
                fontWeight: '500',
                lineHeight: '160%'
              }}
            >
              {client.status === 'accepted' ? 'مقبول' : 'مرفوض'}
            </Typography>
          </Box>
          {/* <Box
          sx={{
            display: 'flex',
            padding: '8px 16px',
            justifyContent: 'center',
            alignItems: 'center',
            gap: '10px',
            borderRadius: '4px',
           background: "rgba(209, 46, 61, 0.10)",


          }}
        >
          <Typography
            sx={{
              color: '#CE3446',
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: '500',
              lineHeight: '160%'
            }}
          >
            {'مرفوض'}
          </Typography>
        </Box> */}
        </Stack>
      </Card>
    </Link>
  )
}
