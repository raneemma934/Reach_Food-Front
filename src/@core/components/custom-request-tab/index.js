import React from 'react'
import { Box, Stack } from '@mui/system'
import { Avatar, Card, Divider, makeStyles, Typography } from '@mui/material'
import Location from '../../../../public/images/cards/Location'
import Telephone from '../../../../public/images/cards/Telephone'

export default function CustomRequestsTabs({ data }) {
  return (
    <Card
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        backgroundColor: theme => (theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light),
        borderRadius: '8px',
        boxShadow: 'rgba(0, 0, 0, 0.05)',
        padding: '16px',
        gap: '10px'
      }}
      className='Cards'
    >
      <Stack direction={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'}>
        <Box>
          <Typography
            sx={{
              color: theme =>
                theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light,
              fontSize: '20px',
              fontWeight: '600',
              '&:hover': {
                Color: 'blue'
              }
            }}
            className='request'
          >
            الطلب #{data?.id}
          </Typography>
        </Box>
        <Box
          sx={{
            display: 'flex',
            padding: '8px 16px',
            justifyContent: 'center',
            alignItems: 'center',
            gap: '10px',
            borderRadius: '4px',
            background: `${data?.status === 'accepted' ? 'rgba(115, 163, 208, 0.10)' : 'rgba(209, 46, 61, 0.10)'}`
          }}
        >
          <Typography
            sx={{
              color: `${data?.status === 'accepted' ? '#73A3D0' : '#CE3446'}`,
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: '500',
              lineHeight: '160%'
            }}
          >
            {data?.status === 'accepted' ? 'مقبول' : 'مرفوض'}
          </Typography>
        </Box>
      </Stack>
      <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }} spacing={3}>
        <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
          <Avatar src='/images/icons/avatar.png' alt='Olivia Sparks' />
          <Box>
            <Typography variant='h6'> {data?.customer.name}</Typography>
            <Typography variant='p'>{data?.customer_type}</Typography>
          </Box>
        </Box>
      </Stack>

      <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
        <Location /> المنطقة: {data?.trip_date?.trip?.address?.area}
      </Typography>
      <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
        <Telephone /> أرقام التواصل :{' '}
        {data?.customer?.contacts.map((phone, index) => (
          <Box key={index}>{phone?.phone_number}</Box>
        ))}
      </Typography>
    </Card>
  )
}
