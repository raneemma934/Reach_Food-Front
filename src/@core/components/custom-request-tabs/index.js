import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import { styled } from '@mui/material/styles'
import MuiTabList from '@mui/lab/TabList'

const TabList = styled(MuiTabList)(({ theme }) => ({
  borderBottom: '0 !important',
  '& .MuiTabs-root': { p: '0px !important', m: '0px important' },
  '& .MuiTabs-indicator': {
    backgroundColor: '#73A3D0',
    height: '3px'
  },
  '& .MuiTab-root.Mui-selected': {
    backgroundColor: 'transparent',
    color: '#73A3D0 ',
    '&:hover': {
      color: '#73A3D0',
      backgroundColor: 'transparent'
    }
  },

  '& .MuiTab-root': {
    lineHeight: 1,
    borderRadius: theme.shape.borderRadius,
    color: theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
    backgroundColor: theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
    padding: '8px 24px 8px 24px !important',
    borderRadius: '8px',

    fontSize: '18px',
    marginRight: '24px',
    '&:hover': {
      color: '#73A3D0',
      backgroundColor: 'rgba(115, 163, 208, 0.25);'
    }
  },
  '&:last-child': {
    marginRight: 0
  }
}))

const CustomRequestsTabs = ({ value, handleChange }) => {
  return (
    <TabContext value={value}>
      <TabList onChange={handleChange} aria-label='customized tabs example'>
        <Tab value='1' label='كل الطلبات' />
        <Tab value='2' label='الطلبات المقبولة' />
        <Tab value='3' label=' الطلبات المرفوضة' />
      </TabList>
    </TabContext>
  )
}

export default CustomRequestsTabs
