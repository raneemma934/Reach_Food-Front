import { useTheme } from '@emotion/react'
import { Card, Grid } from '@mui/material'
import { Box } from '@mui/system'
import { DataGrid } from '@mui/x-data-grid'

const CustomTable = ({ rows, columns, NoRowText, isPrice }) => {
  const theme = useTheme()

  return (
    <>
      <Grid container spacing={6}>
        <Grid item xs={12}>
          <Box sx={{ height: 550 }}>
            <DataGrid
              rowHeight={70}
              localeText={{ noRowsLabel: `${NoRowText}` }}
              columnHeaderHeight={70}
              hideFooter
              columns={columns}
              rows={rows}
              sx={{
                '& .MuiDataGrid-row': {
                  backgroundColor: theme.palette.mode === 'dark' ? '#3B3B3B' : '#F8F8F8',
                  borderBottom: !isPrice ? `1px solid ${theme.palette.divider}` : `0px solid`
                },
                '& .MuiDataGrid-columnHeaders': {
                  backgroundColor: theme.palette.mode === 'dark' ? '#3B3B3B' : '#F8F8F8'
                },
                '& .MuiDataGrid-cell': {
                  borderBottom: `0px solid ${theme.palette.divider}`
                },
                '& .MuiDataGrid-footerContainer': {
                  display: 'none'
                },
                '& .MuiDataGrid-virtualScroller': {
                  overflowY: 'auto',
                  overflowX: 'hidden',
                  backgroundColor: theme.palette.mode === 'dark' ? '#3B3B3B' : '#F8F8F8'
                },
                '& .MuiDataGrid-columnHeader:last-of-type': {
                  paddingRight: '3rem '
                },

                '@media (max-width: 600px)': {
                  '& .MuiDataGrid-columnHeaders, & .MuiDataGrid-cell': {
                    fontSize: '16px',
                    padding: '8px'
                  }
                },
                '& .MuiDataGrid-virtualScroller::-webkit-scrollbar': {
                  width: '0.4em'
                },
                '& .MuiDataGrid-virtualScroller::-webkit-scrollbar-track': {
                  backgroundColor: theme => (theme.palette.mode === 'dark' ? '#494A4B' : '#F1F1F1')
                },
                '& .MuiDataGrid-virtualScroller::-webkit-scrollbar-thumb': {
                  backgroundColor: '#888'
                },
                '& .MuiDataGrid-virtualScroller::-webkit-scrollbar-thumb:hover': {
                  background: '#555'
                },
                '& .MuiDataGrid-overlay': {
                  fontSize: ' 16px'
                },
                '& .MuiDataGrid-columnHeaderTitle': {
                  textAlign: 'center',
                  fontFamily: 'Noto Kufi Arabic',
                  fontSize: ' 16px',
                  fontStyle: 'normal',
                  fontWeight: '600',
                  lineHeight: 'normal',
                  letterSpacing: 0
                }
              }}
            />
          </Box>
        </Grid>
      </Grid>
    </>
  )
}

export default CustomTable
