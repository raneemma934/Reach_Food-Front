import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import { styled } from '@mui/material/styles'
import MuiTabList from '@mui/lab/TabList'
import { useAuth } from 'src/hooks/useAuth'

const TabList = styled(MuiTabList)(({ theme }) => ({
  borderBottom: '0 !important',
  '& .MuiTabs-root': { p: '0px !important', m: '0px important' },

  '& .MuiTabs-indicator': {
    backgroundColor: '#73A3D0',
    height: '3px'
  },
  '& .MuiTab-root.Mui-selected': {
    backgroundColor: 'transparent',
    color: '#73A3D0 ',
    '&:hover': {
      color: '#73A3D0',
      backgroundColor: 'transparent'
    }
  },
  '& .MuiTabs-flexContainer': {
    gap: '24px'
  },

  '& .MuiTab-root': {
    lineHeight: '160%',
    color: theme.palette.mode === 'dark' ? '#B3B3B3' : '#5A5A5A',
    backgroundColor: theme.palette.mode === 'dark' ? '#3B3C3D' : '#F8F8F8',
    padding: '8px 24px  !important',
    borderRadius: '8px',

    fontSize: '18px',
    '&:hover': {
      color: '#73A3D0',
      backgroundColor: 'rgba(115, 163, 208, 0.25);'
    }
  },

  '&:last-child': {
    marginRight: 0
  }
}))

const CustomTabs = ({ value, handleChange }) => {
  const auth = useAuth()

  return (
    <TabContext value={value}>
      <TabList onChange={handleChange}>
        <Tab value='1' label='الزبائن' />
        <Tab value='2' label='المندوبين' />
        {auth.user.role !== 'sales manager' ? <Tab value='3' label='مديري المبيعات' /> : null}
      </TabList>
    </TabContext>
  )
}

export default CustomTabs
