import { Typography } from '@mui/material'

const CustomTypographySection = ({ text, fontSize, fontWeight = '600', lineHeight = '160%' }) => {
  return <Typography style={{ fontSize, fontWeight, lineHeight }}>{text}</Typography>
}

export default CustomTypographySection
