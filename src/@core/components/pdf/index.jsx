import React from 'react'
import { Page, Text, View, Document, StyleSheet, Font } from '@react-pdf/renderer'
import moment from 'moment'
import 'moment/locale/ar'

const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    backgroundColor: '#E4E4E4',
    fontFamily: 'Amiri',
    padding: 10,
    height: 'auto'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
    direction: 'rtl'
  },
  text: {
    fontSize: 12,
    textAlign: 'right'
  },
  table: {
    display: 'table',
    width: 'auto',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#bfbfbf',
    marginBottom: 10
  },
  tableRow: {
    margin: 'auto',
    flexDirection: 'row'
  },
  tableCol: {
    width: '25%',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#bfbfbf'
  },
  tableCell: {
    margin: 5,
    fontSize: 10,
    textAlign: 'right'
  },
  list: {
    width: '100%'
  },
  listItem: {
    fontSize: 9,
    textAlign: 'right',
    marginBottom: 10,
    width: '100%',
    paddingRight: '20px'
  },
  idNum: {
    fontSize: '16px',
    fontWeight: '600',
    color: '#CE3446',
    textAlign: 'right',
    marginLeft: '20px'
  },
  title: {
    fontWeight: '700',
    fontSize: '16px',
    color: '#333434',
    textAlign: 'right',
    marginBottom: 5,
    paddingRight: '20px'
  },
  dateOrder: {
    color: '#73A3D0',
    fontWeight: '900',
    textAlign: 'right',
    fontSize: '16px'
  },
  TitleSection: {
    color: '#333434',
    fontWeight: '300',
    textAlign: 'center',
    fontSize: '17',
    width: '100%'
  }
})

Font.register({
  family: 'Amiri',
  src: '/fonts/Cairo-VariableFont_slnt,wght.ttf'
})

const MyDocument = props => {
  const { data } = props
  moment.locale('ar')

  return (
    <Document>
      <Page size='A4' style={styles.page}>
        <View style={styles.section}>
          <Text style={styles.TitleSection}>تقرير الطلبات - تاريخ التقرير</Text>
          {data?.data?.data
            ?.filter(ele => ele.status === 'delivered')
            .map((ele, i) => (
              <React.Fragment key={i}>
                <Text style={styles.dateOrder}>
                  {moment(ele?.order_date).format('dddd')} ، {ele?.order_date?.slice(0, 10)}
                </Text>
                <Text style={styles.title}>الطلبات التي تم تسليمها</Text>

                <View style={styles.list}>
                  <Text style={styles.idNum}>الطلب #{ele?.id}</Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>{ele?.customer?.name} : الزبون</Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>
                    {'المندوب المسؤول'} : {ele?.trip_date?.trip?.salesman?.name}
                  </Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>{ele?.order_date?.slice(0, 10)} : تاريخ الطلب</Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>{ele?.total_price} : قيمة الفاتورة</Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>{ele?.delivery_date} : تاريخ التسليم</Text>
                </View>
                <View style={styles.table}>
                  {/* Table Header */}
                  <View style={styles.tableRow}>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>السعر الكلي</Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>سعر الواحدة</Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>الكمية</Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>المنتج</Text>
                    </View>
                  </View>
                  {/* Table Data */}
                  {ele?.products?.map((product, idx) => (
                    <View style={styles.tableRow} key={idx}>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{ele?.total_price}</Text>
                      </View>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{product?.wholesale_price}</Text>
                      </View>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{product?.amount}</Text>
                      </View>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{product?.name}</Text>
                      </View>
                    </View>
                  ))}
                </View>
              </React.Fragment>
            ))}
        </View>
      </Page>

      {/* *****************************************************************************************  */}

      {/* الطلبات المرفوضة */}
      <Page size='A4' style={styles.page}>
        <View style={styles.section}>
          <Text style={styles.title}>الطلبات المرفوضة</Text>

          {data?.data?.data
            ?.filter(order => order.status === 'canceled')
            .map((order, index) => (
              <React.Fragment key={index}>
                <View style={styles.list}>
                  <Text style={styles.idNum}>الطلب #{order?.id}</Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>{order?.customer?.name} : الزبون</Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>
                    {'المندوب المسؤول'} : {order?.trip?.salesman?.name}
                  </Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>{order?.order_date?.slice(0, 10)} : تاريخ الطلب</Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>{order?.total_price} : قيمة الفاتورة</Text>
                </View>
                <View style={styles.list}>
                  <Text style={styles.listItem}>{order?.delivery_date} : تاريخ التسليم</Text>
                </View>
                <View style={styles.table}>
                  {/* Table Header */}
                  <View style={styles.tableRow}>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>السعر الكلي</Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>سعر الواحدة</Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>الكمية</Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>المنتج</Text>
                    </View>
                  </View>
                  {/* Table Data */}

                  {order?.products?.map((product, idx) => (
                    <View style={styles.tableRow} key={idx}>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{order?.total_price}</Text>
                      </View>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{product?.wholesale_price}</Text>
                      </View>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{product?.amount}</Text>
                      </View>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{product?.name}</Text>
                      </View>
                    </View>
                  ))}
                </View>
              </React.Fragment>
            ))}
        </View>
      </Page>
    </Document>
  )
}

export default MyDocument
