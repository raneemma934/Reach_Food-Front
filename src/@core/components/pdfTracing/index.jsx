import React from 'react'
import { Page, Text, View, Document, StyleSheet, Font } from '@react-pdf/renderer'
import moment from 'moment'
import 'moment/locale/ar'

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'column',
    backgroundColor: '#E4E4E4',
    fontFamily: 'Amiri',
    padding: 10,
    height: 'auto'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
    direction: 'rtl'
  },
  text: {
    fontSize: 12,
    textAlign: 'right'
  },
  table: {
    display: 'table',
    width: '60%',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#bfbfbf',
    marginBottom: 10
  },
  tableRow: {
    margin: 'auto',
    flexDirection: 'row'
  },
  tableCol: {
    width: '50%',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#bfbfbf'
  },
  tableCell: {
    margin: 5,
    fontSize: 10,
    textAlign: 'right'
  },
  list: {
    width: '100%'
  },
  listItem: {
    fontSize: 9,
    textAlign: 'right',
    marginBottom: 5,
    width: '100%',
    paddingRight: '20px'
  },
  idNum: {
    fontSize: '16px',
    fontWeight: '600',
    color: '#CE3446',
    textAlign: 'right',
    marginLeft: '20px'
  },
  title: {
    fontSize: 11,
    textAlign: 'right',
    marginBottom: 5,
    width: '100%',
    paddingRight: '20px',
    fontWeight: '800'
  },
  dateOrder: {
    color: '#73A3D0',
    fontWeight: '900',
    textAlign: 'right',
    fontSize: '16px'
  },
  TitleSection: {
    color: '#333434',
    fontWeight: '300',
    textAlign: 'center',
    fontSize: 17,
    width: '100%'
  }
})

Font.register({
  family: 'Amiri',
  src: '/fonts/Cairo-VariableFont_slnt,wght.ttf' // Adjust the path based on your project structure
})

// Function to translate status
const translateStatus = status => {
  switch (status) {
    case 'start':
      return 'بدء الرحلة'
    case 'stop':
      return 'إنهاء الرحلة'
    case 'resume':
      return 'استئناف'
    case 'pause':
      return 'توقف'
    default:
      return ''
  }
}

// Create Document Component
const MyDocumentTracing = props => {
  const { data } = props

  return (
    <Document>
      <Page size='A4' style={styles.page}>
        <View style={styles.section}>
          <Text style={styles.TitleSection}> تقرير تتبع الرحلات - تاريخ التقرير </Text>
          {data?.map((ele, i) => (
            <React.Fragment key={i}>
              <Text style={styles.dateOrder}>
                {moment(ele?.start_date).format('dddd')} ، {ele?.start_date?.slice(0, 10)}
              </Text>

              <View style={styles.list}>
                <Text style={styles.title}>
                  {' المندوب'} : {ele?.trip?.salesman?.name}
                </Text>
              </View>
              <View style={styles.list}>
                <Text style={styles.listItem}>
                  {' رحلة'} : {ele?.trip?.salesman?.location}
                </Text>
              </View>

              <View style={styles.list}>
                <Text style={styles.listItem}>{ele?.start_time?.slice(0, 8)} : موعد الانطلاق</Text>
              </View>

              <View style={styles.table}>
                {/* Table Header */}
                <View style={styles.tableRow}>
                  <View style={styles.tableCol}>
                    <Text style={styles.tableCell}> الحالة</Text>
                  </View>
                  <View style={styles.tableCol}>
                    <Text style={styles.tableCell}> الوقت</Text>
                  </View>
                </View>
                {/* Table Data */}
                {Array.isArray(ele?.trip_trace) ? (
                  ele?.trip_trace?.map((product, idx) => (
                    <View style={styles.tableRow} key={idx}>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{translateStatus(product?.status)}</Text>
                      </View>
                      <View style={styles.tableCol}>
                        <Text style={styles.tableCell}>{product?.duration}</Text>
                      </View>
                    </View>
                  ))
                ) : (
                  <View style={styles.tableRow}>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>{translateStatus(ele?.trip_trace?.status)}</Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>{ele?.trip_trace?.duration}</Text>
                    </View>
                  </View>
                )}
              </View>
            </React.Fragment>
          ))}
        </View>
      </Page>
    </Document>
  )
}

export default MyDocumentTracing
