import React from 'react'
import toast, { Toaster } from 'react-hot-toast'

const notify = () => {
  const rollbackAction = () => {
    // Your rollback action logic here
    console.log('Rollback action executed')
  }

  const CustomToast = ({ id }) => {
    const [countdown, setCountdown] = React.useState(5)
    const timerRef = React.useRef(null)

    React.useEffect(() => {
      timerRef.current = setInterval(() => {
        setCountdown(prev => prev - 1)
      }, 1000)

      return () => {
        clearInterval(timerRef.current)
      }
    }, [])

    React.useEffect(() => {
      if (countdown === 0) {
        clearInterval(timerRef.current)
        toast.dismiss(id)
      }
    }, [countdown, id])

    return (
      <span>
        Custom and <b>bold</b>
        <button
          onClick={() => {
            clearInterval(timerRef.current)
            rollbackAction()
            toast.dismiss(id)
          }}
        >
          Rollback
        </button>
        <span> - {countdown} seconds</span>
      </span>
    )
  }

  toast(<CustomToast />, {
    duration: 5000 // Automatically close the toast after 5 seconds
  })
}

const ToastNotification = () => (
  <div>
    <button onClick={notify}>Show Toast</button>
  </div>
)

export default ToastNotification
