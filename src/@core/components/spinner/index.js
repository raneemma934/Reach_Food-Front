import { useTheme } from '@mui/material/styles'
import Box from '@mui/material/Box'

// import 'animate.css'
import React from 'react'
import { LinearProgress } from '@mui/material'

const FallbackSpinner = ({ sx }) => {
  const theme = useTheme()
  const [progress, setProgress] = React.useState(0)

  React.useEffect(() => {
    const timer = setInterval(() => {
      setProgress(oldProgress => {
        if (oldProgress === 50) {
          return 0
        }
        const diff = Math.random() * 10

        return Math.min(oldProgress + diff, 50)
      })
    }, 800)

    return () => {
      clearInterval(timer)
    }
  }, [])

  return (
    <Box
      sx={{
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        ...sx
      }}
    >
      <Box sx={{ width: '50%', position: 'relative', display: 'flex', alignItems: 'center', direction: 'rtl' }}>
        <LinearProgress
          variant='determinate'
          value={progress}
          sx={{
            width: '100%',
            transform: 'rotateY(180deg)'
          }}
        />
        <Box
          sx={{
            position: 'absolute',
            right: `${progress + 3}%`,
            transform: 'translateX(50%)',
            display: 'flex',
            alignItems: 'center',
            transition: 'all 1s '
          }}
        >
          <img width={48} height={48} src='/images/icons/loading.svg' alt='loading' />
        </Box>
      </Box>
    </Box>
  )
}

export default FallbackSpinner
