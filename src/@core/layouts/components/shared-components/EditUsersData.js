import { yupResolver } from '@hookform/resolvers/yup'
import { Avatar, Button, Divider, Drawer, MenuItem, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useState } from 'react'
import CustomTextField from 'src/@core/components/mui/text-field'
import { useForm, Controller, useFormContext, FormProvider } from 'react-hook-form'
import Password from '../../../../../public/images/cards/password'
import Email from '../../../../../public/images/cards/email'
import { styled } from '@mui/material/styles'
import * as yup from 'yup'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

export default function EditUsersData({ open, handleCloseDrawer }) {
  const [selectedImage, setSelectedImage] = useState(null)

  const methods = useForm({
    defaultValues: {
      emails: [{ email: '' }],
      company: '',
      country: '',
      billing: '',
      fullName: '',
      username: '',
      contact: ''
    },
    resolver: yupResolver(yup.Schema)
  })
  const { control, formState, getValues, watch } = methods

  const { errors } = formState

  const handleImageChange = event => {
    if (event.target.files && event.target.files[0]) {
      setSelectedImage(event.target.files[0])
    }
  }

  const handleImageUpload = async () => {
    if (!selectedImage) return

    // const formData = new FormData();
    // formData.append('avatar', selectedImage);
  }

  return (
    <FormProvider {...methods}>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header>
          <Typography sx={{ fontSize: '16px', fontWeight: '600', lineHeight: '25px' }}>الملف الشخصي</Typography>

          {/* <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64 }}>N</Avatar> */}
          <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
            <Button
              size='small'
              onClick={handleCloseDrawer}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                color: '#ffff',
                fontSize: '16px',
                gap: '8px',
                backgroundColor: '#73A3D0',
                '&:hover': {
                  backgroundColor: '#73A3D0'
                }
              }}
            >
              حفظ
            </Button>
            <Button
              size='small'
              onClick={handleCloseDrawer}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                fontSize: '16px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                '&:hover': {
                  backgroundColor: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                }
              }}
            >
              إلغاء
            </Button>
          </Stack>
        </Header>
        <Divider sx={{ marginY: '4px' }} />
        <Stack direction={'column'} alignItems={'center'} justifyContent={'center'} marginTop={'14px'}>
          <Avatar
            sx={{
              bgcolor: 'rgba(115, 163, 208, 0.25)',
              width: 180,
              height: 180,
              backgroundColor: '#73A3D0',
              marginBottom: '12px'
            }}
            src={selectedImage ? URL.createObjectURL(selectedImage) : null}
          >
            {!selectedImage && <img src='/images/icons/avatar.png' alt='' />}
          </Avatar>
          <Button
            component='label'
            sx={{
              fontSize: '14px',
              fontWeight: '500',
              lineHeight: '32px',
              margin: '1px 0px',
              '&:hover': {
                color: '#73A3D0',
                backgroundColor: 'transparent'
              }
            }}
          >
            تعديل الصورة
            <input type='file' hidden accept='image/*' onChange={handleImageChange} />
          </Button>
        </Stack>
        <Stack padding={4} spacing={3}>
          <Typography className='custom-style-label '>اسم الشركة</Typography>
          <Controller
            name='name'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                sx={{ mb: 4 }}
                onChange={onChange}
                placeholder='اسم الشركة'
                error={Boolean(errors.fullName)}
                {...(errors.fullName && { helperText: errors.fullName.message })}
              />
            )}
          />

          <Typography className='custom-style-label '>
            <Email /> البريد الالكتروني
          </Typography>

          <Controller
            name='email'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                sx={{ mb: 4 }}
                onChange={onChange}
                placeholder='البريد الالكتروني'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />

          <Typography className='custom-style-label '>
            <Password /> كلمة السر
          </Typography>

          <Controller
            name='password'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                sx={{ mb: 4 }}
                onChange={onChange}
                placeholder='johndoe'
                error={Boolean(errors.username)}
                {...(errors.username && { helperText: errors.username.message })}
              />
            )}
          />
        </Stack>
      </Drawer>
    </FormProvider>
  )
}
