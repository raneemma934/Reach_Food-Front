// ** MUI Imports
import { Box } from '@mui/material'
import IconButton from '@mui/material/IconButton'
import { useTheme } from '@mui/material/styles'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

const ModeToggler = props => {
  // ** Props
  const { settings, saveSettings } = props
  const theme = useTheme()

  const handleModeChange = mode => {
    saveSettings({ ...settings, mode: mode })
  }

  const handleModeToggle = () => {
    if (settings.mode === 'light') {
      handleModeChange('dark')
    } else {
      handleModeChange('light')
    }
  }

  return (
    <Box
      sx={{
        width: '50px',
        height: '50px',
        backgroundColor: theme => (theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light),
        textAlign: 'center',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: '8px',
        marginRight: '8px',
        boxShadow: ' rgba(0, 0, 0, 0.06)',
        cursor: 'pointer'
      }}
    >
      <IconButton
        sx={{ '&:hover': { backgroundColor: 'transparent' } }}
        color='inherit'
        aria-haspopup='true'
        onClick={handleModeToggle}
      >
        <Icon fontSize='1.625rem' icon={settings.mode === 'dark' ? 'tabler:sun' : 'tabler:moon-stars'} />
      </IconButton>
    </Box>
  )
}

export default ModeToggler
