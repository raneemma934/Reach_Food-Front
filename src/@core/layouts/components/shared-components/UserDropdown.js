// ** React Imports
import { useState, Fragment, useEffect, useCallback } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** MUI Imports
import Box from '@mui/material/Box'
import Badge from '@mui/material/Badge'
import Avatar from '@mui/material/Avatar'
import Divider from '@mui/material/Divider'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import Drawer from '@mui/material/Drawer'
import Radio from '@mui/material/Radio'
import Stack from '@mui/material/Stack'

// ** Icon Imports
import Edit from '../../../../../public/images/cards/edit'
import ArrowColor from '../../../../../public/images/cards/arrowcolor'

// ** Context
import { useAuth } from 'src/hooks/useAuth'
import { useDispatch, useSelector } from 'react-redux'
import { fetchCategoriesSuper } from 'src/store/apps/branches'
import EditUsersData from './EditUsersData'
import { setOpenDrawer } from 'src/store/apps/user'

// ** Styled Components
const BadgeContentSpan = styled('span')(({ theme }) => ({
  width: 8,
  height: 8,
  borderRadius: '50%',
  backgroundColor: theme.palette.success.main,
  boxShadow: `0 0 0 2px ${theme.palette.background.paper}`
}))

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(5),
  justifyContent: 'space-between',
  width: '100%'
}))

const UserDropdown = ({ settings }) => {
  // ** States

  const { openDrawer } = useSelector(state => state.user)
  console.log('🚀 ~ UserDropdown ~ openDrawer:', openDrawer)
  const [isEdit, setIsEdit] = useState(false)
  const [selectedCategoryId, setSelectedCategoryId] = useState(null)

  const store = useSelector(state => state.branches)
  const dispatch = useDispatch()
  const router = useRouter()
  const { logout } = useAuth()

  const { direction } = settings

  // ** Functions
  const handleDropdownOpen = useCallback(
    event => {
      dispatch(setOpenDrawer(true))
    },
    [dispatch]
  )

  const handleDropdownClose = useCallback(
    url => {
      if (url) {
        router.push(url)
      }

      dispatch(setOpenDrawer(null))
    },
    [router, dispatch]
  )

  const handleEditClick = useCallback(() => {
    setIsEdit(true)
  }, [])

  const handleEditClose = useCallback(() => {
    setIsEdit(false)
  }, [])

  const handleLogout = useCallback(() => {
    logout()
    handleDropdownClose()
  }, [logout, handleDropdownClose])

  const storeSelectedBranch = useCallback(category => {
    setSelectedCategoryId(category.id)

    localStorage.setItem('branch_id', category.id)
    localStorage.setItem('City_id', category.city_id)
    location.reload()
  }, [])

  useEffect(() => {
    dispatch(fetchCategoriesSuper())
    const branch_id = localStorage.getItem('branch_id')
    setSelectedCategoryId(branch_id)
  }, [dispatch])

  return (
    <Fragment>
      <Badge
        overlap='circular'
        onClick={handleDropdownOpen}
        sx={{ ml: 2, cursor: 'pointer' }}
        badgeContent={<BadgeContentSpan />}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
      >
        <Avatar
          onClick={handleDropdownOpen}
          sx={{ bgcolor: '#73A3D0', color: '#fff', width: 50, height: 50, borderRadius: '50%' }}
          src={''}
        />
      </Badge>
      {openDrawer ? (
        <Drawer
          anchor='right'
          open={openDrawer}
          onClose={() => handleDropdownClose()}
          ModalProps={{ keepMounted: true }}
          sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
        >
          <Header>
            <Typography sx={{ fontSize: '16px', fontWeight: '600', lineHeight: '25px' }}>الملف الشخصي</Typography>
            <Stack direction='row' alignItems='center' spacing={3}>
              <Button
                size='small'
                onClick={handleEditClick}
                sx={{
                  p: '8px 12px',
                  borderRadius: 1,

                  lineHeight: 'normal',
                  fontSize: '16px',
                  gap: '8px',
                  boxShadow: 'rgba(0, 0, 0, 0.06)',
                  backgroundColor: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                  color: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                  '&:hover': {
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                  },
                  display: 'flex',
                  alignItems: 'center'
                }}
              >
                <Edit /> تعديل
              </Button>
              <Button
                size='small'
                onClick={handleLogout}
                sx={{
                  p: '8px 12px',
                  borderRadius: 1,
                  lineHeight: 'normal',
                  fontSize: '14px',
                  color: '#fff',
                  backgroundColor: '#D12E3D',
                  '&:hover': { backgroundColor: '#D12E3D' }
                }}
              >
                تسجيل الخروج
              </Button>
            </Stack>
          </Header>
          <Divider sx={{ marginY: '12px !important' }} />
          <Stack direction='column' alignItems='center' justifyContent='center'>
            <Avatar
              sx={{ bgcolor: '#73A3D0', color: '#fff', width: 180, height: 180, borderRadius: '111px' }}
              src={''}
            />
            <Typography sx={{ fontSize: '20px', fontWeight: '500', lineHeight: '32px', margin: '12px 0px' }}>
              اسم الشركة
            </Typography>
          </Stack>
          <Stack padding='0px 24px' direction='column'>
            <Stack spacing={4}>
              <Typography sx={{ fontWeight: '500', fontSize: '20px', lineHeight: '32px' }}>الأفرع</Typography>
              {store.CategoriesSuper.map((city, index) => (
                <Fragment key={index}>
                  <Typography sx={{ fontWeight: '500', fontSize: '16px', color: '#73A3D0' }}>{city.name}</Typography>
                  {city?.cities.map((country, i) => (
                    <Stack direction='column' spacing={2} key={i} sx={{ pl: 5 }}>
                      <Typography
                        sx={{
                          fontWeight: '400',
                          fontSize: '16px',
                          color: '#73A3D0',
                          display: 'flex',
                          alignItems: 'center'
                        }}
                      >
                        <ArrowColor /> {country.name}
                      </Typography>
                      {country.branch.map((category, index) => (
                        <Box key={index}>
                          <Typography
                            sx={{
                              fontWeight: '400',
                              fontSize: '16px',
                              color: '#73A3D0',
                              display: 'flex',
                              alignItems: 'center',
                              flexDirection: 'row'
                            }}
                          >
                            <Radio
                              sx={{ p: 0, pr: 2 }}
                              checked={selectedCategoryId == category.id}
                              onChange={() => storeSelectedBranch(category)}
                            />
                            {category.name}
                          </Typography>
                        </Box>
                      ))}
                      <hr style={{ opacity: 0.2 }} />
                    </Stack>
                  ))}
                </Fragment>
              ))}
            </Stack>
            <Divider sx={{ marginY: '4px' }} />
          </Stack>
        </Drawer>
      ) : null}

      {isEdit && <EditUsersData open={isEdit} handleCloseDrawer={handleEditClose} />}
    </Fragment>
  )
}

export default UserDropdown
