const DefaultPalette = (mode, skin) => {
  // ** Vars
  const whiteColor = '#EEE'
  const lightColor = '47, 43, 61'
  const HoverColor = '115, 163, 208'
  const darkColor = '208, 212, 241'
  const darkPaperBgColor = '#333434'
  const lightTextColor = '#3B3B3B'
  const darkTextColor = '#D2D2D3'

  const mainColor = mode === 'light' ? lightColor : darkColor
  const textColor = mode === 'light' ? lightTextColor : darkTextColor
  const mainHover = mode === 'light' ? HoverColor : darkColor

  const defaultBgColor = () => {
    if (skin === 'bordered' && mode === 'light') {
      return whiteColor
    } else if (skin === 'bordered' && mode === 'dark') {
      return darkPaperBgColor
    } else if (mode === 'light') {
      return '#eeeeee'
    } else return '#242525'
  }

  return {
    customColors: {
      dark: darkColor,
      main: mainColor,
      light: lightColor,
      lightPaperBg: whiteColor,
      darkPaperBg: darkPaperBgColor,
      bodyBg: mode === 'light' ? '#eeeeee' : '#25293C',
      trackBg: mode === 'light' ? '#eeeeee' : '#363B54',
      avatarBg: mode === 'light' ? '#DBDADE' : '#4A5072',
      tableHeaderBg: mode === 'light' ? '#F6F6F7' : '#4A5072',
      drawer: mode === 'light' ? '#EEE' : 'red'
    },
    mode: mode,
    common: {
      black: '#000',
      white: whiteColor
    },
    primary: {
      light: '#3B3B3B',
      main: '#3B3B3B',
      dark: '#D2D2D3',
      contrastText: whiteColor
    },
    secondary: {
      light: '#5A5A5A',
      main: '#5A5A5A',
      dark: '#B3B3B3',
      contrastText: whiteColor
    },
    background: {
      light: '#EEEEEE',
      main: '#EEEEEE',
      dark: '#333434',
      contrastText: whiteColor
    },
    gray: {
      light: '#E0E0E0',
      main: '#E0E0E0',
      dark: '#555657',
      contrastText: whiteColor
    },
    light_gray: {
      light: '#F1F1F1',
      main: '#F1F1F1',
      dark: '#494A4B',
      contrastText: whiteColor
    },
    base: {
      light: '#F8F8F8',
      main: '#F8F8F8',
      dark: '#3B3C3D',
      contrastText: whiteColor
    },
    main_color: {
      white: '#F2F4F8',
      black: '#3B3B3B',
      red: '#CE3446',
      contrastText: whiteColor
    },
    accent: {
      main: '#73A3D0'
    },

    error: {
      light: '#ED6F70',
      main: '#EA5455',
      dark: '#CE4A4B',
      contrastText: whiteColor
    },
    warning: {
      light: '#FFAB5A',
      main: '#FF9F43',
      dark: '#E08C3B',
      contrastText: whiteColor
    },
    info: {
      light: '#1FD5EB',
      main: '#00CFE8',
      dark: '#00B6CC',
      contrastText: whiteColor
    },
    success: {
      light: '#42CE80',
      main: '#28C76F',
      dark: '#23AF62',
      contrastText: whiteColor
    },
    grey: {
      50: '#FAFAFA',
      100: '#F5F5F5',
      200: '#EEEEEE',
      300: '#E0E0E0',
      400: '#BDBDBD',
      500: '#9E9E9E',
      600: '#757575',
      700: '#616161',
      800: '#424242',
      900: '#212121',
      A100: '#F5F5F5',
      A200: '#EEEEEE',
      A400: '#BDBDBD',
      A700: '#616161'
    },
    text: {
      primary: `${textColor}`,
      secondary: `rgba(${mainColor}, 0.68)`,
      disabled: `rgba(${mainColor}, 0.42)`
    },
    divider: `rgba(${mainColor}, 0.16)`,
    background: {
      paper: mode === 'light' ? whiteColor : darkPaperBgColor,
      default: defaultBgColor()
    },
    action: {
      active: `rgba(${mainColor}, 0.54)`,
      hover: `rgba(${mainHover}, 0.20)`,
      selected: `rgba(${mainColor}, 0.06)`,
      selectedOpacity: 0.06,
      disabled: `rgba(${mainColor}, 0.26)`,
      disabledBackground: `rgba(${mainColor}, 0.12)`,
      focus: `rgba(${mainColor}, 0.12)`
    }
  }
}

export default DefaultPalette
