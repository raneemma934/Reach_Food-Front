export function DateFormateOfDay(date) {
  // Check if the input date is valid
  if (isNaN(new Date(date).getTime())) {
    // Handle invalid date, you might want to throw an error or return an appropriate value

    return null; // or throw new Error("Invalid date format");
  }

  var originalDate = new Date(date);
  var day = originalDate.getUTCDate() +1;
  var month = originalDate.getUTCMonth() + 1;
  var year = originalDate.getUTCFullYear();


  var formattedDay =year + '-' + (month < 10 ? '0' : '') +month+"-" + (day < 10 ? '0' : '') + day ;


  return formattedDay;
}
