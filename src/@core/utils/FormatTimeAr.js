import moment from 'moment'

export const formatTimeAr = momentDate => {
  moment.locale('ar')
  const time = moment(momentDate, 'HH:mm')

  return time.format('hh:mm A')
}

export const formatDateAr = momentDate => {
  moment.locale('ar')
  const time = moment(momentDate)

  return time.format('dddd l')
}
