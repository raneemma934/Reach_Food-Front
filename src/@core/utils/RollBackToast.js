import React, { useState, useEffect, useRef } from 'react'
import toast from 'react-hot-toast'
import { Button, CircularProgress, Typography } from '@mui/material'
import { useDispatch } from 'react-redux'
import { Box, Stack } from '@mui/system'
import { deletebranches } from 'src/store/apps/branches'

const RollBackToast = ({ data, t }) => {
  const [rollbackInitiated, setRollbackInitiated] = useState(false)
  const [countdown, setCountdown] = useState(5)
  const timerRef = useRef(null)
  const dispatch = useDispatch()

  const rollbackAction = () => {
    console.log('Rollback action executed')
    setRollbackInitiated(true) // Set the flag to true when rollback is executed
  }

  useEffect(() => {
    timerRef.current = setInterval(() => {
      setCountdown(prev => prev - 1)
    }, 1000)

    return () => {
      clearInterval(timerRef.current)
    }
  }, [])

  useEffect(() => {
    if (countdown === 0) {
      clearInterval(timerRef.current)
      toast.dismiss(t.id)
      if (!rollbackInitiated) {
        // Only dispatch if rollback hasn't been initiated
        dispatch(deletebranches(data))
      }
    }
  }, [countdown, data, rollbackInitiated, dispatch, t.id])

  const handleRollback = () => {
    clearInterval(timerRef.current)
    rollbackAction()
    toast.dismiss(t.id)
  }

  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        padding: '12px',
        backgroundColor: '#73A3D0', // Blue background color
        color: '#FFFFFF', // White text color
        borderRadius: '8px',
        boxShadow: '0px 4px 9px 0px rgba(0, 0, 0, 0.06)'
      }}
    >
      <Stack direction={'row'} alignItems={'center'} spacing={2} sx={{ width: '100%' }}>
        <Box>
          <span style={{ flex: 1, marginRight: '8px' }}>تم حذف الفرع بنجاح</span>
        </Box>
        <Box sx={{ display: 'flex', alignItems: 'center', marginLeft: 'auto' }}>
          <Button
            onClick={handleRollback}
            style={{
              color: '#FFFFFF', // White color for text
              textTransform: 'none' // Prevents uppercase transformation
            }}
            variant='text'
            disableElevation
          >
            تراجع
          </Button>
          <Box sx={{ position: 'relative', display: 'inline-flex', marginLeft: '8px' }}>
            <CircularProgress
              variant='determinate'
              value={(countdown / 5) * 100} // Adjusted value to fit within 0-100 range for CircularProgress
              size={30} // Adjust size of CircularProgress
              thickness={4} // Adjust thickness of CircularProgress
              style={{
                color: '#FFFFFF' // White color for CircularProgress
              }}
            />
            <Box
              sx={{
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                position: 'absolute',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Typography color='#fff' variant='caption'>
                {countdown}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Stack>
    </div>
  )
}

export default RollBackToast
