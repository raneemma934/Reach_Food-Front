import { useEffect, useState } from 'react'

export default function ScrollingPagination({ data, setCurrentPage, currentPage }) {
  console.log('🚀 ~ ScrollingPagination ~ data:', data?.data?.last_page)

  const handleScroll = () => {
    const scrollY = window.scrollY
    const windowHeight = window.innerHeight
    const documentHeight = document.documentElement.scrollHeight
    if (scrollY + windowHeight >= documentHeight - 100 && currentPage <= data?.data?.last_page) {
      setCurrentPage(currentPage + 1)
    }
  }
  useEffect(() => {
    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage])

  console.log(currentPage)

  return <></>
}
