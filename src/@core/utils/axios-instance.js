import axios from 'axios'
import Router from 'next/router' // Or 'next/router' if using Next.js

// Assuming you get the branch ID from an environment variable or some global state
const getBranchId = () => process.env.NEXT_PUBLIC_BRANCH_ID || '1'

const api = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASE_URL // replace with your API base URL
})

// // Request interceptor to add branch ID to each request
// api.interceptors.request.use(
//   // config => {
//   //   const branchId = getBranchId();
//   //   if (branchId) {
//   //     // Append the branch ID as a query parameter
//   //     config.params = {
//   //       ...config.params,
//   //       branch_id: branchId,
//   //     };
//   //   }

//   //   return config;
//   // },
//   error => Promise.reject(error)
// )

// Response interceptor
api.interceptors.response.use(
  response => response, // simply return the response if successful
  error => {
    console.error(error.response)
    if (error.response && error.response.status === 401) {
      // Redirect to the login page if 401 is encountered
      window.location.href = '/login' // replace '/login' with your login route
    }

    return Promise.reject(error) // otherwise, return the error
  }
)

export default api
