import { Card } from '@mui/material'
import React, { useEffect, useState } from 'react'
import ListPricesColumns from '../hook/useListPricesColumns'
import CustomTapsListPrices from 'src/@core/components/custom-listprices'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPriceProducts } from 'src/store/apps/products'
import CustomTable from 'src/@core/components/custom-table'

export default function TablePrices() {
  const { data, status, error } = useSelector(state => state.products)
  console.log('🚀 ~ TablePrices ~ status:', status)
  const [filterData, setFilterData] = useState([])

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchPriceProducts())
  }, [dispatch])

  const columns = ListPricesColumns()

  // Ensure the data passed to CustomTable has unique keys
  const rowsWithKeys = filterData.map((row, index) => ({
    ...row,
    key: row.id || index // Use a unique identifier from your data, or the index as a fallback
  }))

  return (
    <>
      <CustomTapsListPrices setFilterData={setFilterData} data={data?.data} />
      <Card sx={{ marginTop: '23px' }}>
        <CustomTable isPrice={1} columns={columns} rows={rowsWithKeys} NoRowText={'لا يوجد منتحات'} />
      </Card>
    </>
  )
}
