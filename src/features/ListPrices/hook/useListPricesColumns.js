import { Avatar, Typography } from '@mui/material'
import FakeProducts from '../../../../public/images/cards/fakeproducts'
import { Box, Stack } from '@mui/system'
import { useMyContext } from './editPriceContext'
import { useMemo } from 'react'
import CustomTextField from 'src/@core/components/mui/text-field'
import { Controller, useFormContext } from 'react-hook-form'
import { InputAdornment } from '@mui/material'

const useListPricesColumns = () => {
  const { Edit } = useMyContext()

  const methods = useFormContext()

  const { control, formState, setValue } = methods
  const { errors } = formState

  return useMemo(
    () => [
      {
        flex: 2,
        field: 'name',

        headerName: 'المنتج',
        renderCell: params => {
          setValue(`product.[${params.row.id}].id`, params.row.id)

          return (
            <Stack direction={'row'} alignItems={'center'} spacing={4}>
              <Box>
                <Avatar
                  src={process.env.NEXT_PUBLIC_IMAGES + '/' + params.row.image}
                  sx={{
                    bgcolor: '#73A3D026',
                    width: '56px',
                    height: '56px',
                    '@media (max-width: 600px)': {
                      width: '35px',
                      height: '35px'
                    },
                    borderRadius: '5px'
                  }}
                >
                  <FakeProducts />
                </Avatar>
              </Box>
              <Box>
                <Typography
                  sx={{
                    fontSize: '16px',
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 'normal',
                    '@media (max-width: 600px)': {
                      fontSize: '12px'
                    }
                  }}
                >
                  {params.row.name}
                </Typography>
                <Typography
                  sx={{
                    fontSize: '16px',
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 'normal',
                    '@media (max-width: 600px)': {
                      fontSize: '10px'
                    }
                  }}
                >
                  {params.row.amount_unit}
                </Typography>
              </Box>
            </Stack>
          )
        }
      },

      {
        flex: 3,
        field: 'wholesale_price',
        align: 'center',
        headerAlign: 'center',
        headerName: ' السعر للجملة',
        renderCell: params => {
          return (
            <>
              {Edit ? (
                <Controller
                  name={`product[${params.row.id}].wholesale_price`}
                  control={control}
                  defaultValue={params.row.wholesale_price}
                  rules={{ required: true }}
                  render={({ field: { value, onChange } }) => (
                    <CustomTextField
                      type='number'
                      value={value}
                      onChange={onChange}
                      error={Boolean(errors.country)}
                      {...(errors.country && { helperText: errors.country.message })}
                      InputProps={{
                        endAdornment: <InputAdornment position='end'>ل.س</InputAdornment>
                      }}
                    />
                  )}
                />
              ) : (
                <Typography className='custom-style-label2'>{params.row.wholesale_price} ل.س </Typography>
              )}
            </>
          )
        }
      },
      {
        flex: 3,
        field: 'retail_price',
        align: 'center',
        headerAlign: 'center',
        headerName: 'السعر للمفرق',
        renderCell: params => {
          return (
            <>
              {Edit ? (
                <Controller
                  name={`product.[${params.row.id}].retail_price`}
                  className='custom-style-label'
                  control={control}
                  defaultValue={params.row.retail_price}
                  rules={{ required: true }}
                  render={({ field: { value, onChange } }) => (
                    <CustomTextField
                      type='number'
                      value={value}
                      onChange={onChange}
                      error={Boolean(errors.country)}
                      {...(errors.country && { helperText: errors.country.message })}
                      InputProps={{
                        endAdornment: <InputAdornment position='end'>ل.س</InputAdornment>
                      }}
                    />
                  )}
                />
              ) : (
                <Typography className='custom-style-label2'> {params.row.retail_price} ل.س </Typography>
              )}
            </>
          )
        }
      },
      {
        flex: 3,
        field: 'space',
        align: 'center',
        headerAlign: 'center',
        headerName: ''
      }
    ],
    [Edit, control, errors, setValue]
  )
}

export default useListPricesColumns
