import { Card, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import useListArchiveColumns from '../hook/useListArchiveColumns'
import Filter from 'src/@core/components/custom-archiv-tab/fillter'
import { fetchRequests } from 'src/store/apps/requests'
import DrawerEdit from '../Drawer'
import CustomDialogDelete from 'src/@core/components/custom-delete'
import CustomTable from 'src/@core/components/custom-table'

export default function TableArchive({ status }) {
  const { data, archivedDay } = useSelector(state => state.request)
  console.log('🚀 ~ TableArchive ~ archivedDay:', archivedDay)
  const dispatch = useDispatch()
  const [filterData, setFilterData] = useState([])

  useEffect(() => {
    dispatch(fetchRequests(`is_archived=1&date=${archivedDay ? archivedDay : new Date()}&${status}`))
  }, [dispatch, archivedDay, status])

  const {
    columns,
    open,
    drawerData,
    handleCloseDrawer,
    isDeletePopupOpen,
    handleCloseDeletePopup,
    handleDelete,
    selectedClientId
  } = useListArchiveColumns()

  return (
    <>
      <Filter setFilterData={setFilterData} fdata={data?.data?.data} />
      <Card sx={{ marginTop: '23px' }}>
        {filterData && (
          <>
            <CustomTable
              isPrice={0}
              columns={columns}
              rows={filterData}
              NoRowText={'لا يوجد طلبات في التاريخ المحدد الرجاء اختيار تاريخ اخر '}
            />
          </>
        )}
      </Card>
      <Box sx={{ display: 'flex', p: '0 12px', gap: '24px', mt: '12px' }}>
        <Box display={'flex'} alignItems={'center'} gap={'12px'}>
          <Box
            sx={{
              width: '14px',
              height: '14px',
              backgroundColor: '#73A3D0',
              borderRadius: '4px'
            }}
          ></Box>
          <Typography>الطلبات التي تم تسليمها</Typography>
        </Box>
        <Box display={'flex'} alignItems={'center'} gap={'12px'}>
          <Box
            sx={{
              width: '14px',
              height: '14px',
              backgroundColor: '#CE3446',
              borderRadius: '4px'
            }}
          ></Box>
          <Typography>الطلبات المرفوضة</Typography>
        </Box>
      </Box>
      {open && <DrawerEdit open={open} handleCloseDrawer={handleCloseDrawer} data={drawerData} />}
      {isDeletePopupOpen && (
        <CustomDialogDelete
          open={isDeletePopupOpen}
          handleClose={handleCloseDeletePopup}
          question={`هل أنت متأكد من حذف الطلب (${selectedClientId})؟`}
          decsription={`  هذه العملية لا يمكن التراجع عنها عند إتمامها وسيتم حذف الطلب نهائياً وكل المعلومات المتعلقة به.`}
          onDelete={handleDelete}
        />
      )}
    </>
  )
}
