import { Avatar, Button, Checkbox, Divider, Drawer, Radio, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useEffect } from 'react'

import styled from '@emotion/styled'
import CustomTextField from 'src/@core/components/mui/text-field'
import { Controller, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPermissions } from 'src/store/apps/user'
import Clock from 'public/images/cards/clock'
import { editArchiveRequests } from 'src/store/apps/requests'
import DateLogo from 'public/images/cards/date'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: '24px',
  justifyContent: 'space-between',
  width: '100%',
  margin: '0px'
}))

const types = [
  { name: 'تم التسليم', value: 'delivered' },
  { name: 'مرفوض', value: 'canceled' }
]

export default function DrawerEdit({ open, handleCloseDrawer, data }) {
  const [selectedValue, setSelectedValue] = React.useState(0)
  const dispatch = useDispatch()

  const handleChange = (event, onChange) => {
    setSelectedValue(event)
    onChange(event)
  }

  const handelSaveData = () => {
    dispatch(editArchiveRequests({ data: getValues(), id: data?.id }))
    handleCloseDrawer()
  }

  const methods = useForm({})
  const { control, formState, getValues, watch } = methods

  const { errors } = formState

  return (
    <Drawer
      open={open}
      anchor='right'
      variant='temporary'
      onClose={handleCloseDrawer}
      ModalProps={{ keepMounted: true }}
      sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      PaperProps={{
        sx: {
          width: { xs: 300, sm: 400 }

          // backgroundColor: drawerColor
        }
      }}
    >
      <Header>
        <Typography
          sx={{
            fontSize: '16px',
            fontWeight: '500',
            lineHeight: '32px'
          }}
        >
          الطلب #{data?.id}
        </Typography>
        <Box sx={{ display: 'flex', gap: '24px', marginLeft: '59px' }}>
          <Button
            sx={{
              lineHeight: 'normal',
              display: 'flex',
              padding: '8px 24px',
              alignItems: 'center',
              gap: '8px',
              borderRadius: '8px',
              background: '#73A3D0',
              color: '#F2F4F8',
              '&:hover': {
                color: '#F2F4F8',
                backgroundColor: '#73A3D0'
              }
            }}
            onClick={handelSaveData}
          >
            حفظ
          </Button>
          <Button
            onClick={handleCloseDrawer}
            sx={{
              display: 'flex',
              padding: '8px 24px',
              alignItems: 'center',
              gap: '8px',
              borderRadius: '8px',
              color: theme =>
                theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
              backgroundColor: theme =>
                theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
              '&:hover': {
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
              }
            }}
          >
            إلغاء
          </Button>
        </Box>
      </Header>
      <Divider sx={{ marginY: '4px' }} />
      <Stack padding={4} sx={{ gap: '8px' }}>
        <Typography sx={{ fontSize: '16px', fontWeight: '500', mt: '16px' }}> تعديل حالة الطلب</Typography>

        <Stack direction={'column'} justifyContent={'start'} width={'100%'}>
          {types.map((category, i) => (
            <Controller
              key={i}
              name={`action`}
              control={control}
              defaultValue={null}
              render={({ field: { onChange, value } }) => (
                <Typography sx={{ fontWeight: '500', fontSize: '16px', color: '#73A3D0' }}>
                  <Radio checked={value === category.value} onChange={() => handleChange(category.value, onChange)} />{' '}
                  {category.name}
                </Typography>
              )}
            />
          ))}
        </Stack>

        {selectedValue === 'delivered' ? (
          <>
            <Typography className='custom-style-label '>
              {' '}
              <DateLogo /> تاريخ التسليم
            </Typography>
            <Controller
              name='delivery_date'
              control={control}
              defaultValue={data?.order_date?.slice(0, 10)}
              rules={{ required: true }}
              render={({ field: { value, onChange } }) => (
                <CustomTextField
                  type='date'
                  fullWidth
                  value={value}
                  onChange={onChange}
                  placeholder='تحديد التاريخ للحصول على تقرير بطلباته'
                  error={Boolean(errors.country)}
                  {...(errors.country && { helperText: errors.country.message })}
                />
              )}
            />
            <Typography className='custom-style-label2 '>
              {' '}
              <Clock /> وقت التسليم
            </Typography>

            <Controller
              name='delivery_time'
              control={control}
              rules={{ required: true }}
              render={({ field: { value, onChange } }) => (
                <CustomTextField
                  type='time'
                  fullWidth
                  value={value}
                  onChange={onChange}
                  placeholder='تحديد التاريخ للحصول على تقرير بطلباته'
                  error={Boolean(errors.country)}
                  {...(errors.country && { helperText: errors.country.message })}
                />
              )}
            />
          </>
        ) : null}
      </Stack>
    </Drawer>
  )
}
