import React, { useEffect, useMemo, useState } from 'react'
import { Button, Checkbox, Divider, Drawer, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import { pdf } from '@react-pdf/renderer'
import { saveAs } from 'file-saver'
import styled from '@emotion/styled'
import CustomTextField from 'src/@core/components/mui/text-field'
import { useDispatch, useSelector } from 'react-redux'
import MyDocument from 'src/@core/components/pdf'
import { fetchRequestsArc } from 'src/store/apps/requests'
import DateLogo from 'public/images/cards/date'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: '24px',
  justifyContent: 'space-between',
  width: '100%',
  margin: '0px'
}))

const types = [
  { name: 'الطلبات التي تم تسليمها', value: 0 },
  { name: 'الطلبات المرفوضة', value: 1 }
]

const DrawerUpload = ({ open, handleCloseDrawer }) => {
  const store = useSelector(state => state.user)
  const { arcRequests } = useSelector(state => state.request)
  const dispatch = useDispatch()

  const [checkedStates, setCheckedStates] = useState(types.map(type => Boolean(type.value)))
  const [selectedDate, setSelectedDate] = useState()

  const memoizedFetchPermissions = useMemo(() => {
    return () => dispatch(fetchRequestsArc(`is_archived=1&date=${selectedDate}`))
  }, [dispatch, selectedDate])

  useEffect(() => {
    memoizedFetchPermissions()
  }, [memoizedFetchPermissions])

  const handleCheckboxChange = (index, value) => {
    setCheckedStates(prevStates => {
      const updatedStates = [...prevStates]
      updatedStates[index] = value

      return updatedStates
    })
  }

  const handleDownload = async () => {
    const doc = <MyDocument data={arcRequests} />
    const asPdf = pdf([])
    asPdf.updateContainer(doc)
    const blob = await asPdf.toBlob()
    saveAs(blob, 'my_document.pdf')
  }

  return (
    <Drawer
      open={open}
      anchor='right'
      variant='temporary'
      onClose={handleCloseDrawer}
      ModalProps={{ keepMounted: true }}
      sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      PaperProps={{ sx: { width: { xs: 300, sm: 400 } } }}
    >
      <Header>
        <Typography sx={{ color: '#3B3B3B', fontSize: '16px', fontWeight: '500', lineHeight: '32px' }}>
          تصدير التقرير
        </Typography>
        <Box sx={{ display: 'flex', gap: '24px', marginLeft: '59px' }}>
          <Button
            sx={{
              lineHeight: 'normal',
              display: 'flex',
              padding: '8px 24px',
              alignItems: 'center',
              gap: '8px',
              borderRadius: '8px',
              background: '#73A3D0',
              color: '#F2F4F8',
              '&:hover': {
                color: '#F2F4F8',
                backgroundColor: '#73A3D0'
              }
            }}
            onClick={handleDownload}
          >
            تصدير
          </Button>
          <Button
            onClick={handleCloseDrawer}
            sx={{
              display: 'flex',
              padding: '8px 24px',
              alignItems: 'center',
              gap: '8px',
              borderRadius: '8px',
              background: '#E0E0E0',

              '&:hover': {
                backgroundColor: '#E0E0E0'
              }
            }}
          >
            إلغاء
          </Button>
        </Box>
        <Divider sx={{ marginY: '4px' }} />
      </Header>
      <Stack padding={4} sx={{ gap: '8px' }}>
        <Typography className='custom-style-label '>
          <DateLogo /> التاريخ
        </Typography>

        <CustomTextField
          type='date'
          fullWidth
          value={selectedDate}
          sx={{ mb: 4 }}
          onChange={e => setSelectedDate(e.target.value)}
          placeholder='تحديد التاريخ للحصول على تقرير بطلباته'
        />

        <Typography className='custom-style-label'> نوع الطلبات</Typography>

        <Stack direction='column' justifyContent='start' width='100%'>
          {types.map((type, i) => (
            <Typography key={i} sx={{ fontWeight: '500', fontSize: '16px', color: '#73A3D0' }}>
              <Checkbox checked={checkedStates[i]} onChange={e => handleCheckboxChange(i, e.target.checked)} />{' '}
              {type.name}
            </Typography>
          ))}
        </Stack>
      </Stack>
    </Drawer>
  )
}

export default DrawerUpload
