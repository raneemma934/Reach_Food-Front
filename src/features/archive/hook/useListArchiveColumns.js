import React, { useMemo, useState, useCallback } from 'react'
import { Avatar, Box, Stack, Typography, Button } from '@mui/material'
import Link from 'next/link'
import FakeProducts from '../../../../public/images/cards/fakeproducts'
import Eyes from '../../../../public/images/cards/eyes'
import Edit from '../../../../public/images/cards/edit'
import Delete from '../../../../public/images/cards/delete'
import DrawerEdit from '../Drawer'
import { useDispatch } from 'react-redux'
import { deleteRequest } from 'src/store/apps/requests'
import { formatDateAr } from 'src/@core/utils/FormatTimeAr'

const useListArchiveColumns = () => {
  const [open, setOpen] = useState(false)
  const [drawerData, setDrawerData] = useState(null)
  const [isDeletePopupOpen, setIsDeletePopupOpen] = useState(false)
  const [selectedClientId, setSelectedClientId] = useState(null)
  const dispatch = useDispatch()

  const handleOpenDrawer = useCallback(data => {
    setDrawerData(data)
    setOpen(true)
  }, [])

  const handleCloseDrawer = useCallback(() => {
    setOpen(false)
    setDrawerData(null)
  }, [])

  const handleOpenDeletePopup = useCallback(clientId => {
    setSelectedClientId(clientId)
    setIsDeletePopupOpen(true)
  }, [])

  const handleCloseDeletePopup = useCallback(() => {
    setIsDeletePopupOpen(false)
    setSelectedClientId(null)
  }, [])

  const handleDelete = useCallback(() => {
    if (selectedClientId) {
      dispatch(deleteRequest(selectedClientId))
      handleCloseDeletePopup()
    }
  }, [selectedClientId, dispatch, handleCloseDeletePopup])

  const columns = useMemo(
    () => [
      {
        flex: 0.4,
        field: 'id',
        headerName: 'رقم الطلب',
        renderCell: params => (
          <Stack direction={'row'} alignItems={'center'} sx={{ gap: '12px' }}>
            <Box
              sx={{
                width: '20px',
                height: '20px',
                backgroundColor: params.row.status === 'delivered' ? '#73A3D0' : '#CE3446',
                borderRadius: '4px'
              }}
            ></Box>
            <Typography>{params.row.id}</Typography>
          </Stack>
        )
      },
      {
        flex: 0.4,
        field: 'name',
        headerName: 'الزبون',
        renderCell: params => (
          <Stack direction={'row'} alignItems={'center'} spacing={4}>
            <Avatar
              sx={{
                width: 42,
                height: 42,
                fontSize: '24px',
                fontWeight: '700',
                backgroundColor: 'rgba(115, 163, 208, 0.25)',
                color: theme => theme.palette.accent.main
              }}
              src={process.env.NEXT_PUBLIC_IMAGES + '/' + params?.row?.customer?.image}
              alt='dlivia Sparks'
            >
              {params.row?.customer?.name?.slice(0, 1).toUpperCase()}
            </Avatar>
            <Box>
              <Typography
                sx={{
                  fontSize: '16px',
                  fontWeight: '500',
                  '@media (max-width: 600px)': {
                    fontSize: '12px'
                  }
                }}
              >
                {params.row?.customer?.name}
              </Typography>
              <Typography
                sx={{
                  fontSize: '14px',
                  fontWeight: '500',
                  '@media (max-width: 600px)': {
                    fontSize: '10px'
                  }
                }}
              >
                {params.row?.customer?.customer_type_ar}
              </Typography>
            </Box>
          </Stack>
        )
      },
      {
        flex: 0.4,
        field: 'date',
        headerName: 'تاريخ الطلب',
        renderCell: params => <Typography>{formatDateAr(params.row?.order_date)}</Typography>
      },
      {
        flex: 0.4,
        field: 'salesman',
        headerName: 'المندوب',
        renderCell: params => <Typography>{params.row?.trip_date?.trip?.salesman?.name}</Typography>
      },
      {
        flex: 0.4,
        field: 'payment',
        headerName: 'قيمة الفاتورة',
        renderCell: params => <Typography>{params.row?.total_price}</Typography>
      },
      {
        flex: 0.3,
        field: 'actions',
        headerName: 'خيارات',
        renderCell: params => (
          <Stack direction={'row'} sx={{ alignItems: 'center', gap: '12px' }}>
            <Link href={`/requests/${params.row.id}`} passHref>
              <Box sx={{ cursor: 'pointer' }}>
                <Eyes />
              </Box>
            </Link>
            <Box sx={{ cursor: 'pointer' }} onClick={() => handleOpenDrawer(params.row)}>
              <Edit />
            </Box>
            <Box sx={{ cursor: 'pointer' }} onClick={() => handleOpenDeletePopup(params.row.id)}>
              <Delete />
            </Box>
          </Stack>
        )
      }
    ],
    [handleOpenDrawer, handleOpenDeletePopup]
  )

  return {
    columns,
    open,
    drawerData,
    handleCloseDrawer,
    isDeletePopupOpen,
    handleCloseDeletePopup,
    handleDelete,
    selectedClientId
  }
}

export default useListArchiveColumns
