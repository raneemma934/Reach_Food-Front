import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'

import Box from '@mui/material/Box'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller, useFieldArray, useFormContext } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { addUser, fetchLocation } from 'src/store/apps/user'
import { Autocomplete, Avatar, Divider, Typography } from '@mui/material'
import { Stack } from '@mui/system'
import Phone from '../../../../public/images/icons/phone'
import Delete from '../../../../public/images/cards/delete'
import Address from '../../../../public/images/cards/Location'
import User from '../../../../public/images/cards/user'
import Password from '../../../../public/images/cards/password'
import { fetchAllCountry } from 'src/store/apps/branches'
import BranchManagersAvatar from '../../../../public/images/icons/BranchManagersAvatar'
import DeleteRed from 'public/images/cards/deleteRed'

const showErrors = (field, valueLen, min) => {
  if (valueLen === 0) {
    return `${field} field is required`
  } else if (valueLen > 0 && valueLen < min) {
    return `${field} must be at least ${min} characters`
  } else {
    return ''
  }
}

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const schema = yup.object().shape({
  company: yup.string().required(),
  billing: yup.string().required(),
  country: yup.string().required(),
  email: yup.string().email().required(),
  contact: yup
    .number()
    .typeError('Contact Number field is required')
    .min(10, obj => showErrors('Contact Number', obj.value.length, obj.min))
    .required(),
  fullName: yup
    .string()
    .min(3, obj => showErrors('First Name', obj.value.length, obj.min))
    .required(),
  username: yup
    .string()
    .min(3, obj => showErrors('Username', obj.value.length, obj.min))
    .required()
})

const SidebarAddBranchManagers = props => {
  // ** Props
  const { open, toggle } = props
  const methods = useFormContext()

  const { control, formState, getValues, setValue } = methods
  const { errors } = formState
  const AllCountry = useSelector(state => state.branches)

  // console.log('🚀 ~ SidebarAddBranchManagers ~ AllCountry:', AllCountry)
  const [jobTitle, setJobTitle] = useState('customer')

  // ** Hooks
  const dispatch = useDispatch()
  const store = useSelector(state => state.user)

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'emails'
  })

  useEffect(() => {
    dispatch(fetchAllCountry())
  }, [dispatch])

  function handleSaveUser() {
    setValue('role', 'admin')
    dispatch(addUser(getValues()))
    handleClose()
  }

  const handleClose = () => {
    toggle()
  }

  useEffect(() => {
    if (fields.length === 0) {
      append({})
    }
  }, [fields, append])

  return (
    <Drawer
      open={open}
      anchor='right'
      variant='temporary'
      onClose={handleClose}
      ModalProps={{ keepMounted: true }}
      sx={{ '& .MuiDrawer-paper': { width: { xs: '100%', sm: 400 } } }}
    >
      <Header>
        <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64 }}>
          <BranchManagersAvatar />
        </Avatar>
        <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
          <Button
            size='small'
            onClick={handleSaveUser}
            sx={{
              p: '8px 24px',
              borderRadius: 1,
              lineHeight: 'normal',
              color: '#ffff',
              fontSize: '16px',
              gap: '8px',
              backgroundColor: '#73A3D0',
              '&:hover': {
                backgroundColor: '#73A3D0'
              }
            }}
          >
            إضافة
          </Button>
          <Button
            size='small'
            onClick={handleClose}
            sx={{
              p: '8px 24px',
              lineHeight: 'normal',
              borderRadius: 1,
              fontSize: '16px',
              color: theme =>
                theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
              backgroundColor: theme =>
                theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
              '&:hover': {
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
              }
            }}
          >
            إلغاء
          </Button>
        </Stack>
      </Header>
      <Divider sx={{ marginY: '4px' }} />
      <Box sx={{ p: theme => theme.spacing(0, 6, 6) }}>
        <Controller
          name='name'
          control={control}
          rules={{ required: true }}
          render={({ field: { value, onChange } }) => (
            <CustomTextField
              fullWidth
              value={value}
              label='الاسم'
              onChange={onChange}
              placeholder='الاسم'
              error={Boolean(errors.fullName)}
              {...(errors.fullName && { helperText: errors.fullName.message })}
            />
          )}
        />

        <Typography className='custom-style-label '>
          <Phone /> أرقام التواصل{' '}
        </Typography>

        {fields.map((field, index) => (
          <div key={field.id}>
            <Controller
              name={`phone_number.[${index}]`}
              control={control}
              rules={{ required: 'Phone is required' }}
              render={({ field: { value, onChange } }) => (
                <CustomTextField
                  fullWidth
                  value={value}
                  sx={{ mb: '8px' }}
                  onChange={onChange}
                  error={Boolean(errors.emails?.[index]?.email)}
                  placeholder='الرقم'
                  {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                  InputProps={{
                    endAdornment: (
                      <Box>
                        {index > 0 ? (
                          <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                            <DeleteRed />
                          </Box>
                        ) : (
                          ''
                        )}
                      </Box>
                    )
                  }}
                />
              )}
            />
          </div>
        ))}

        <Typography
          sx={{
            cursor: 'pointer',
            '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' },
            marginBottom: '16px'
          }}
          onClick={() => append({})}
        >
          + اضافة رقم{' '}
        </Typography>

        <Typography className='custom-style-label '>
          <Address /> العنوان
        </Typography>
        <Controller
          name='location'
          control={control}
          rules={{ required: true }}
          render={({ field: { value, onChange } }) => (
            <CustomTextField
              fullWidth
              value={value}
              onChange={onChange}
              placeholder='عنوان المنطقة'
              error={Boolean(errors.country)}
              {...(errors.country && { helperText: errors.country.message })}
            />
          )}
        />

        <Typography className='custom-style-label '>
          <User /> اسم المستخدم
        </Typography>

        <Controller
          name='user_name'
          control={control}
          rules={{ required: true }}
          render={({ field: { value, onChange } }) => (
            <CustomTextField
              fullWidth
              value={value}
              onChange={onChange}
              placeholder='اسم المستخدم'
              error={Boolean(errors.username)}
              {...(errors.username && { helperText: errors.username.message })}
            />
          )}
        />
        <Typography className='custom-style-label '>
          <Password /> كلمة السر
        </Typography>

        <Controller
          name='password'
          control={control}
          rules={{ required: true }}
          render={({ field: { value, onChange } }) => (
            <CustomTextField
              fullWidth
              value={value}
              onChange={onChange}
              placeholder='johndoe'
              error={Boolean(errors.username)}
              {...(errors.username && { helperText: errors.username.message })}
            />
          )}
        />

        <Typography className='custom-style-label '> الفرع المسؤول عنه</Typography>

        <Controller
          name='city_id'
          control={control}
          render={({ field: { value, onChange } }) => (
            <Autocomplete
              options={AllCountry?.AllCountry?.data || []}
              fullWidth
              id='autocomplete-city-select'
              getOptionLabel={option => ` ${option.name}`}
              value={AllCountry?.AllCountry?.data?.find(city => city.id === value) || null}
              onChange={(event, newValue) => {
                onChange(newValue ? newValue.id : '')
              }}
              renderInput={params => (
                <CustomTextField
                  {...params}
                  placeholder='الفرع المسؤول عنه'
                  fullWidth
                  id='validation-billing-select'
                  aria-describedby='validation-billing-select'
                  error={Boolean(errors.city_id)}
                  helperText={errors.city_id?.message || ''}
                />
              )}
            />
          )}
        />
      </Box>
    </Drawer>
  )
}

export default SidebarAddBranchManagers
