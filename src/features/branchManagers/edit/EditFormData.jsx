import React, { useEffect, useState } from 'react'
import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import {
  Autocomplete,
  Avatar,
  Button,
  Checkbox,
  Divider,
  Drawer,
  FormControlLabel,
  FormGroup,
  MenuItem,
  Typography
} from '@mui/material'
import { styled } from '@mui/material/styles'
import { Box, Stack } from '@mui/system'
import City from '../../../../public/images/cards/city'
import CustomTextField from 'src/@core/components/mui/text-field'
import Telephone from '../../../../public/images/cards/Telephone'
import Delete from '../../../../public/images/cards/delete'
import Address from '../../../../public/images/cards/Location'
import User from '../../../../public/images/cards/user'

import Password from '../../../../public/images/cards/password'
import { useDispatch, useSelector } from 'react-redux'
import { editUser, fetchDataSalesMan } from 'src/store/apps/user'
import BranchManagersAvatar from '../../../../public/images/icons/BranchManagersAvatar'
import DeleteRed from 'public/images/cards/deleteRed'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

export default function EditFormData({
  methods,
  handleCloseDrawer,
  handleEditClick,
  handleDeleteClick,
  data,

  fields,
  append,
  remove
}) {
  const [checked, setChecked] = useState(false)
  const [selected, setSelected] = useState([])
  const { control, formState, getValues, setValue, watch } = methods

  const { errors } = formState
  const store = useSelector(state => state.user)
  const AllCountry = useSelector(state => state.branches)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchDataSalesMan())
  }, [dispatch])

  const handleSelectAll = event => {
    const isChecked = event.target.checked
    setChecked(isChecked)
    if (isChecked) {
      setSelected(store?.SalesMan?.map(item => item.id))
    } else {
      setSelected([])
    }
  }

  const handleSaveDrawer = () => {
    setValue('id', data.id)
    dispatch(editUser(getValues()))
    handleCloseDrawer()
  }

  const handleSelectItem = id => event => {
    const selectedIndex = selected.indexOf(id)
    let newSelected = []

    if (selectedIndex === -1) {
      newSelected = [...selected, id]
    } else {
      newSelected = selected.filter(item => item !== id)
    }

    setSelected(newSelected)
    setChecked(newSelected.length === store?.SalesMan?.length)
  }

  return (
    <FormProvider {...methods}>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header>
          <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64 }}>
            <BranchManagersAvatar />
          </Avatar>
          <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
            <Button
              size='small'
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                color: '#ffff',
                fontSize: '16px',
                gap: '8px',
                backgroundColor: '#73A3D0',
                '&:hover': {
                  backgroundColor: '#73A3D0'
                }
              }}
              onClick={handleSaveDrawer}
            >
              حفظ
            </Button>
            <Button
              size='small'
              onClick={handleCloseDrawer}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                fontSize: '16px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                '&:hover': {
                  backgroundColor: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                }
              }}
            >
              إلغاء
            </Button>
          </Stack>
        </Header>
        <Divider sx={{ marginY: '4px' }} />
        <Box sx={{ p: theme => theme.spacing(0, 6, 6), display: 'flex', flexDirection: 'column' }}>
          <Typography className='custom-style-label '>
            <Telephone />
            الاسم
          </Typography>
          <Controller
            name='name'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='John Doe'
                error={Boolean(errors.fullName)}
                {...(errors.fullName && { helperText: errors.fullName.message })}
              />
            )}
          />

          <Typography className='custom-style-label '>
            <Telephone /> أرقام التواصل{' '}
          </Typography>

          {fields.map((field, index) => (
            <div key={field.id}>
              <Controller
                name={`phone_number.[${index}]`}
                control={control}
                rules={{ required: 'Phone is required' }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    sx={{ mb: '8px' }}
                    onChange={onChange}
                    error={Boolean(errors.emails?.[index]?.email)}
                    placeholder='0934343428'
                    {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                    InputProps={{
                      endAdornment: (
                        <Box>
                          {index > 0 ? (
                            <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                              <DeleteRed />
                            </Box>
                          ) : (
                            ''
                          )}
                        </Box>
                      )
                    }}
                  />
                )}
              />
            </div>
          ))}

          <Typography
            sx={{
              cursor: 'pointer',

              '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
            }}
            onClick={() => append({})}
            variant='text'
          >
            + اضافة رقم{' '}
          </Typography>

          <Typography className='custom-style-label '>
            <Address /> العنوان
          </Typography>
          <Controller
            name='location'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='Australia'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />

          <Typography className='custom-style-label '>
            <User /> اسم المستخدم
          </Typography>

          <Controller
            name='user_name'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='johndoe'
                error={Boolean(errors.username)}
                {...(errors.username && { helperText: errors.username.message })}
              />
            )}
          />
          <Typography className='custom-style-label '>
            <Password /> كلمة السر
          </Typography>

          <Controller
            name='password'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='johndoe'
                error={Boolean(errors.username)}
                {...(errors.username && { helperText: errors.username.message })}
              />
            )}
          />

          <Typography className='custom-style-label '> الفرع المسؤول عنه</Typography>

          <Controller
            name='city_id'
            control={control}
            render={({ field: { value, onChange } }) => (
              <Autocomplete
                options={AllCountry?.AllCountry?.data || []}
                fullWidth
                id='autocomplete-city-select'
                getOptionLabel={option => `${option.name}`}
                value={AllCountry?.AllCountry?.data?.find(city => city.id === value) || null}
                onChange={(event, newValue) => {
                  onChange(newValue ? newValue.id : '')
                }}
                renderInput={params => (
                  <CustomTextField
                    {...params}
                    placeholder='الفرع المسؤول عنه'
                    fullWidth
                    id='validation-billing-select'
                    aria-describedby='validation-billing-select'
                    error={Boolean(errors.city_id)}
                    helperText={errors.city_id?.message || ''}
                  />
                )}
              />
            )}
          />
        </Box>
      </Drawer>
    </FormProvider>
  )
}
