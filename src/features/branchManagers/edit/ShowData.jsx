import { Avatar, Button, Divider, Drawer, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React from 'react'
import Address from '../../../../public/images/cards/Location'

import City from '../../../../public/images/cards/city'
import Phone from '../../../../public/images/cards/Telephone'
import Delete from '../../../../public/images/cards/deleteWihte'
import User from '../../../../public/images/cards/user'
import Password from '../../../../public/images/cards/password'
import { styled } from '@mui/material/styles'
import Edit from '../../../../public/images/cards/edit'
import BranchManagersAvatar from '../../../../public/images/icons/BranchManagersAvatar'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

export default function ShowData({ data, handleDeleteClick, handleEditClick, handleCloseDrawer }) {
  return (
    <>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header>
          <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64 }}>
            <BranchManagersAvatar />
          </Avatar>
          <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
            <Box
              onClick={handleEditClick}
              sx={{
                padding: '8px 12px 8px 12px',
                borderRadius: 1,

                fontSize: '16px',
                gap: '4px',
                boxShadow: '0px 0.8px 0.9px 0px ',
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                display: 'flex',
                alignItems: 'center',
                cursor: 'pointer',
                '&:hover': {
                  backgroundColor: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                }
              }}
              className='hover-edit'
            >
              <Edit />

              <Button
                size='small'
                sx={{
                  fontSize: '16px',
                  padding: '0px',
                  fontWeight: '600',
                  color: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                }}
              >
                تعديل
              </Button>
            </Box>
            <Box
              onClick={handleDeleteClick}
              sx={{
                padding: '8px 12px 8px 12px',
                borderRadius: 1,

                fontSize: '16px',
                gap: '2px',
                boxShadow: '0px 0.8px 0.9px 0px ',
                backgroundColor: '#CE3446',
                display: 'flex',
                alignItems: 'center',
                cursor: 'pointer',
                '&:hover': {
                  backgroundColor: '#CE3446'
                }
              }}
              className='hover-edit'
            >
              <Box sx={{ marginTop: '4px' }}>
                <Delete />
              </Box>
              <Button
                size='small'
                sx={{
                  color: '#fff',
                  fontSize: '16px',
                  padding: '0px',
                  fontWeight: '600',
                  '&:hover': {
                    color: '#fff',
                    backgroundColor: 'transparent'
                  }
                }}
              >
                حذف
              </Button>
            </Box>
          </Stack>
        </Header>
        <Divider sx={{ marginY: '4px' }} />
        <Stack padding={'0px 24px'} direction={'column'}>
          <Box>
            <Typography className='custom-style-label3 '>الاسم</Typography>
            <Typography
              sx={{
                fontWeight: '400',
                fontSize: '14px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
              }}
            >
              {data?.name}
            </Typography>
          </Box>

          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <Typography className='custom-style-label'>
              <Phone /> أرقام التواصل
            </Typography>

            {data?.contacts && data.contacts.length > 0 ? (
              <ul style={{ marginTop: 0, marginBottom: 0 }}>
                {data.contacts.map((contact, index) => (
                  <li key={index} style={{ fontWeight: '400', fontSize: '14px', margin: '3px 0px' }}>
                    {contact.phone_number}
                  </li>
                ))}
              </ul>
            ) : (
              'لا يوجد ارقام اتصال'
            )}
          </Box>

          <Box>
            <Typography className='custom-style-label '>
              <Address /> العنوان
            </Typography>
            <Typography
              sx={{
                fontWeight: '400',
                fontSize: '14px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
              }}
            >
              {data?.location}
            </Typography>
          </Box>
          <Divider sx={{ marginY: '4px ' }} />
          <Box>
            <Typography className='custom-style-label '>
              <User /> اسم المستخدم
            </Typography>
            <Typography
              sx={{
                fontWeight: '400',
                fontSize: '14px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
              }}
            >
              {data?.user_name}
            </Typography>
          </Box>
          <Typography className='custom-style-label '>
            <Password /> كلمة السر
          </Typography>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px',
              color: theme =>
                theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
            }}
          >
            {data?.user_password?.password}
          </Typography>

          <Divider sx={{ marginY: '10px' }} />
          {data.city ? (
            <>
              <Typography className='custom-style-label3 '>الفرع المسؤول عنه</Typography>
              <Stack direction={'row'} alignItems={'center'} spacing={4}>
                <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 52, height: 52 }}>
                  {' '}
                  <img src='/images/icons/avatar.png' alt='' />
                </Avatar>
                <Box>
                  <Typography className='custom-style-label2 '>{data?.city?.name}</Typography>
                </Box>
              </Stack>
            </>
          ) : (
            <>
              <Typography className='custom-style-label3 '>لايوجد فرع مسؤول عنه</Typography>
            </>
          )}
        </Stack>
      </Drawer>
    </>
  )
}
