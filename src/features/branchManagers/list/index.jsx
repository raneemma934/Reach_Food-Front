import { Grid } from '@mui/material'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomCardsBranchManagers from 'src/@core/components/custom-branchManagers-card'
import CustomCards from 'src/@core/components/custom-cards'
import { fetchData } from 'src/store/apps/user'

export default function List({ data }) {
  return (
    <div>
      <Grid container spacing={6}>
        {data &&
          data.map((client, index) => (
            <Grid item sm={4} xs={12} key={index}>
              <CustomCardsBranchManagers client={client} />
            </Grid>
          ))}
      </Grid>
    </div>
  )
}
