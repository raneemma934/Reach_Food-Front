import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'
import DeleteIcon from '../../../../public/images/cards/delete'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'

import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import { Autocomplete, Avatar, Divider, Typography } from '@mui/material'

import Address from '../../../../public/images/cards/Location'
import BranchesAvatar from '../../../../public/images/icons/BranchesAvatar'

import City from '../../../../public/images/cards/city'
import Arrowleft from '../../../../public/images/cards/arrowleft'

import { Stack } from '@mui/system'
import { useDispatch, useSelector } from 'react-redux'
import { addbranches, fetchAdminWithoutBranch, fetchCity, fetchCountry } from 'src/store/apps/branches'
import { fetchData } from 'src/store/apps/user'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const SidebarAddBranches = props => {
  // ** Props
  const { open, handleCloseDrawer } = props
  const store = useSelector(state => state.branches)
  console.log('🚀 ~ SidebarAddBranches ~ store:', store.admin.data)

  // console.log('🚀 ~ SidebarAddBranches ~ store:', store)
  const storeUser = useSelector(state => state.user)

  const [isCity, setCity] = useState(true)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchCountry())
    dispatch(fetchAdminWithoutBranch())
  }, [dispatch])
  const [selectedAddressId, setSelectedAddressId] = useState(null)

  const chooseCity = (id, onChange) => {
    // console.log('🚀 ~ chooseCity ~ id:', id)
    onChange(id)
    setSelectedAddressId(id)
    dispatch(fetchCity(id))
    if (store?.cities?.data?.length > 0) {
      setCity(true)
    } else setCity(false)
  }

  const handleSaveDrawer = () => {
    dispatch(addbranches(getValues()))
    reset({})
    handleCloseDrawer()
  }

  const methods = useForm({
    defaultValues: {
      address_id: '',
      city_id: ''
    },
    mode: 'onChange'
  })

  const { control, formState, getValues, watch, reset } = methods

  const { errors } = formState

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'branche'
  })

  useEffect(() => {
    if (fields.length === 0) {
      append({})
    }
  }, [fields, append])

  return (
    <FormProvider {...methods}>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header>
          <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64 }}>
            <BranchesAvatar />{' '}
          </Avatar>
          <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
            <Button
              size='small'
              onClick={handleSaveDrawer}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                color: '#ffff',
                fontSize: '16px',
                lineHeight: 'normal',
                gap: '8px',
                backgroundColor: '#73A3D0',
                '&:hover': {
                  backgroundColor: '#73A3D0'
                }
              }}
            >
              إضافة
            </Button>
            <Button
              size='small'
              onClick={handleCloseDrawer}
              sx={{
                p: '8px 24px',
                lineHeight: 'normal',
                borderRadius: 1,
                fontSize: '16px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                '&:hover': {
                  backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                }
              }}
            >
              إلغاء
            </Button>
          </Stack>
        </Header>
        <Divider sx={{ marginY: '4px' }} />
        <Box sx={{ p: theme => theme.spacing(0, 6, 6), display: 'flex', flexDirection: 'column' }}>
          <Typography className='custom-style-label '>
            <City /> الدولة
          </Typography>
          <Controller
            name='address_id'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <Autocomplete
                options={store?.country?.data || []}
                fullWidth
                id='autocomplete-address-select'
                getOptionLabel={option => option.name}
                value={store?.country?.data?.find(country => country.id === value) || null}
                onChange={(event, newValue) => {
                  chooseCity(newValue?.id, onChange)
                }}
                renderInput={params => (
                  <CustomTextField
                    {...params}
                    fullWidth
                    placeholder='الدولة'
                    id='validation-billing-select'
                    aria-describedby='validation-billing-select'
                  />
                )}
              />
            )}
          />

          <Typography className='custom-style-label '>
            <Address /> المدينة
          </Typography>
          {!isCity ? (
            <>
              <Controller
                name='city_id'
                control={control}
                rules={{ required: 'City is required' }}
                render={({ field: { value, onChange } }) => (
                  <Autocomplete
                    options={store?.cities?.data || []}
                    fullWidth
                    id='autocomplete-city-select'
                    getOptionLabel={option => option.name}
                    value={store?.cities?.data?.find(city => city.id === value) || null}
                    onChange={(event, newValue) => {
                      onChange(newValue ? newValue.id : '')
                    }}
                    renderInput={params => (
                      <CustomTextField
                        {...params}
                        placeholder='المدينة'
                        fullWidth
                        id='validation-billing-select'
                        aria-describedby='validation-billing-select'
                      />
                    )}
                  />
                )}
              />

              <Typography className='custom-style-label '>
                <Arrowleft /> تصنيفات الفرع{' '}
              </Typography>

              {fields.map((field, index) => (
                <div key={field.id}>
                  <Controller
                    name={`branches.${index}`}
                    control={control}
                    rules={{ required: 'Email is required' }}
                    render={({ field: { value, onChange } }) => (
                      <CustomTextField
                        fullWidth
                        value={value}
                        sx={{ mb: '8px' }}
                        onChange={onChange}
                        placeholder='تصنيف الفرع'
                        error={Boolean(errors.branches?.[index])}
                        helperText={errors.branches?.[index]?.message || ''}
                        InputProps={{
                          endAdornment: index > 0 && (
                            <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                              <DeleteIcon />
                            </Box>
                          )
                        }}
                      />
                    )}
                  />
                </div>
              ))}
              <Typography
                sx={{
                  marginTop: '8px',

                  cursor: 'pointer',
                  '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
                }}
                onClick={() => append({ name: '' })}
                variant='text'
              >
                + اضافة تصنيف{' '}
              </Typography>
              <Typography className='custom-style-label '>مدير الفرع</Typography>
              <Controller
                name='salesManager_id'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <Autocomplete
                    options={store.admin.data || []}
                    fullWidth
                    id='autocomplete-sales-manager-select'
                    getOptionLabel={option => option.name}
                    value={store?.admin.data.find(user => user.id === value) || null}
                    onChange={(event, newValue) => {
                      onChange(newValue ? newValue.id : '')
                    }}
                    renderInput={params => (
                      <CustomTextField
                        {...params}
                        placeholder='مدير الفرع'
                        fullWidth
                        id='validation-billing-select'
                        aria-describedby='validation-billing-select'
                        error={Boolean(errors.salesManager_id)}
                        helperText={errors.salesManager_id?.message || ''}
                      />
                    )}
                  />
                )}
              />
            </>
          ) : (
            <Typography>جميع المدن تحوي أفرع مسبقاً.</Typography>
          )}
        </Box>
      </Drawer>
    </FormProvider>
  )
}

export default SidebarAddBranches
