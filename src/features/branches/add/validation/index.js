import * as yup from 'yup'

export const Schema = yup.object().shape({
  salesManager_id: yup.string().required('يجب اختيار  مدير الفرع'),
  address_id: yup.string().required('يجب اختيار المنطقة'),
  branches: yup.array().of(
    yup.object().shape({
      salesManager_id: yup.string().required('مدير المبيعات مطلوب')
    })
  )
})
