// ** React Imports
import { Fragment, useState } from 'react'

// ** MUI Imports
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import { useTheme } from '@mui/material/styles'
import DialogTitle from '@mui/material/DialogTitle'
import useMediaQuery from '@mui/material/useMediaQuery'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import { Box, Stack } from '@mui/system'
import { Typography } from '@mui/material'
import CustomTextField from 'src/@core/components/mui/text-field'
import { Controller, useForm } from 'react-hook-form'
import Password from 'public/images/cards/password'
import { yupResolver } from '@hookform/resolvers/yup'
import RollBackToast from 'src/@core/components/rollbackToast'

const CustomDialogDeleteBranch = ({ open, handleClose, decsription, onDelete, setPassword, password }) => {
  // ** State

  // ** Hooks
  const theme = useTheme()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))

  const methods = useForm({
    defaultValues: {
      password: ''
    }
  })
  const { control, formState, getValues, watch } = methods

  const { errors } = formState

  return (
    <Fragment>
      <Dialog fullScreen={fullScreen} open={open} onClose={handleClose} aria-labelledby='responsive-dialog-title'>
        <DialogTitle
          id='responsive-dialog-title'
          sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
            fontWeight: '600',
            fontSize: '20px',
            lineHeight: '32px',
            color: '#3B3B3B',
            gap: '8px '
          }}
        >
          {<img src='/images/icons/warning.svg' alt='' />}حذف
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            sx={{ textAlign: 'start', fontWeight: '400', fontSize: '16px', lineHeight: '25px', color: '#5A5A5A' }}
          >
            {decsription}

            <Stack direction={'column'} textAlign={'start'} sx={{ padding: '24px 0px 0px 0px', gap: '8px' }}>
              <Typography>الرجاء إدخال كلمة المرور لتأكيد العملية</Typography>
              <Controller
                name={`password`}
                control={control}
                rules={{ required: 'password is required' }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={password}
                    sx={{ mb: 4 }}
                    onChange={e => setPassword(e.target.value)}
                    error={Boolean(errors.emails?.[index]?.email)}
                    placeholder='كلمة المرور'
                    {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                    InputProps={{
                      endAdornment: (
                        <Box>
                          <Box style={{ cursor: 'pointer', marginTop: '6px' }}>
                            <Password />
                          </Box>
                        </Box>
                      )
                    }}
                  />
                )}
              />
            </Stack>
          </DialogContentText>
        </DialogContent>
        <DialogActions className='dialog-actions-dense' sx={{ textAlign: 'center', width: '100%' }}>
          <Box sx={{ textAlign: 'center', width: '100%', display: 'flex', justifyContent: 'center', gap: '24px' }}>
            <Button
              onClick={onDelete}
              sx={{
                width: '103px',
                height: '46px',
                borderRadius: '8px',
                padding: '8px 32px 8px 32px',
                backgroundColor: '#ce3446',
                color: '#F2F4F8',
                fontSize: '16px',
                fontWeight: '600',
                lineHeight: '30px',
                '&:hover': {
                  backgroundColor: '#ce3446'
                }
              }}
            >
              تأكيد
            </Button>

            <Button
              onClick={handleClose}
              sx={{
                width: '99px',
                height: '46px',
                borderRadius: '8px',
                padding: '8px 32px 8px 32px',
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                fontWeight: '600',
                fontSize: '16px',
                lineHeight: '30px',
                '&:hover': {
                  backgroundColor: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                }
              }}
              variant='none'
            >
              الغاء
            </Button>
          </Box>
        </DialogActions>
      </Dialog>
    </Fragment>
  )
}

export default CustomDialogDeleteBranch
