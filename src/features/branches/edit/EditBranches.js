import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import { Autocomplete, Avatar, Divider, Typography } from '@mui/material'

import Address from '../../../../public/images/cards/Location'
import BranchesAvatar from '../../../../public/images/icons/BranchesAvatar'

import City from '../../../../public/images/cards/city'
import Delete from '../../../../public/images/cards/deleteWihte'
import Arrowleft from '../../../../public/images/cards/arrowleft'

import { Stack } from '@mui/system'
import { useDispatch, useSelector } from 'react-redux'
import { addbranches, editbranches, fetchAdminWithoutBranch, fetchCity, fetchCountry } from 'src/store/apps/branches'
import { fetchData } from 'src/store/apps/user'
import Edit from '../../../../public/images/cards/edit'
import DeleteRed from 'public/images/cards/deleteRed'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const SidebarAddBranches = props => {
  // ** Props
  const { open, handleCloseDrawer, data, handleDeleteClick, Editing } = props
  console.log('🚀 ~ SidebarAddBranches ~ data:', data)

  // console.log('🚀 ~ SidebarAddBranches ~ data:', data)
  const store = useSelector(state => state.branches)

  // console.log('🚀 ~ SidebarAddBranches ~ store:', store)
  const storeUser = useSelector(state => state.user)

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchAdminWithoutBranch())
    dispatch(fetchData('admin'))
  }, [dispatch])
  const [selectedAddressId, setSelectedAddressId] = useState(null)
  const [isEditing, setIsEditing] = useState(false)

  const chooseCity = id => {
    setSelectedAddressId(id)
    dispatch(fetchCity(id))
  }

  const handleSaveDrawer = () => {
    setValue('id', data.id)
    dispatch(editbranches(getValues()))
    handleCloseDrawer()
  }

  const methods = useForm({
    defaultValues: {
      branches: data?.branches?.map(branch => ({ id: branch.id, name: branch.name })) || [],
      admin_id: ''
    },
    resolver: yupResolver(yup.Schema)
  })
  const { control, formState, getValues, setValue, watch } = methods

  const { errors } = formState

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'branches'
  })

  const handleEditClick = () => {
    setIsEditing(true)
  }

  return (
    <>
      {!isEditing ? (
        <>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
          >
            <Header>
              {data.image ? (
                <Avatar
                  sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64, borderRadius: '50%' }}
                  src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
                ></Avatar>
              ) : (
                <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64, borderRadius: '50%' }}>
                  <BranchesAvatar />{' '}
                </Avatar>
              )}

              <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
                <Box
                  onClick={handleEditClick}
                  sx={{
                    padding: '8px 12px 8px 12px',
                    borderRadius: 1,

                    fontSize: '16px',
                    gap: '4px',
                    boxShadow: '0px 0.8px 0.9px 0px ',
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                    display: 'flex',
                    alignItems: 'center',
                    cursor: 'pointer',
                    '&:hover': {
                      backgroundColor: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                    }
                  }}
                  className='hover-edit'
                >
                  <Box sx={{ marginTop: '4px' }}>
                    <Edit />
                  </Box>
                  <Button
                    size='small'
                    sx={{
                      p: 0,
                      fontSize: '16px',
                      color: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                      padding: '0px',
                      fontWeight: '600'
                    }}
                  >
                    تعديل
                  </Button>
                </Box>

                <Box
                  onClick={handleDeleteClick}
                  sx={{
                    padding: '8px 12px 8px 12px',
                    borderRadius: 1,

                    fontSize: '16px',
                    gap: '2px',
                    boxShadow: '0px 0.8px 0.9px 0px ',
                    backgroundColor: '#CE3446',
                    display: 'flex',
                    alignItems: 'center',
                    cursor: 'pointer',
                    '&:hover': {
                      backgroundColor: '#CE3446'
                    }
                  }}
                  className='hover-edit'
                >
                  <Box sx={{ marginTop: '4px' }}>
                    <Delete />
                  </Box>
                  <Button
                    size='small'
                    sx={{
                      color: '#fff',
                      fontSize: '16px',
                      padding: '0px',
                      fontWeight: '600',
                      '&:hover': {
                        color: '#fff',
                        backgroundColor: 'transparent'
                      }
                    }}
                  >
                    حذف
                  </Button>
                </Box>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />
            <Stack padding={'24px'} direction={'column'} sx={{ gap: '24px' }}>
              <Stack sx={{ gap: '8px' }}>
                <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
                  <City />
                  <Box>
                    <Typography className='custom-style-label2 '>الدولة</Typography>
                  </Box>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {/* {data?.city?.country?.name} */}
                  {data?.country_name}
                </Typography>
              </Stack>

              <Stack sx={{ gap: '8px' }}>
                <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
                  <Address />
                  <Box>
                    <Typography className='custom-style-label2 '>المدينة</Typography>
                  </Box>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.name}
                </Typography>
              </Stack>

              <Box>
                <Typography className='custom-style-label2 '>
                  <Arrowleft /> تصنيفات الفرع
                </Typography>

                {data?.branches?.map((val, i) => (
                  <ul key={i}>
                    <li>{val?.name}</li>
                  </ul>
                ))}
              </Box>

              <Box>
                <Typography className='custom-style-label2 '> مدير الفرع </Typography>
                {data?.admin_name ? (
                  <Stack direction={'row'} alignItems={'center'} spacing={2} marginTop={'8px'}>
                    <Avatar
                      sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 32, height: 32, borderRadius: '50%' }}
                      src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.admin_name}
                    ></Avatar>
                    <Typography sx={{ fontSize: '14px', fontWeight: '400', lineHeight: '25px' }}>
                      {data?.admin_name}
                    </Typography>
                  </Stack>
                ) : (
                  <Typography sx={{ fontSize: '14px', fontWeight: '400', lineHeight: '25px' }}>
                    {' '}
                    لا يوجد مدير{' '}
                  </Typography>
                )}
              </Box>
            </Stack>
          </Drawer>
        </>
      ) : (
        <FormProvider {...methods}>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
          >
            <Header>
              <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64 }}>
                <img src='/images/icons/avatar.png' alt='' />
              </Avatar>
              <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
                <Button
                  size='small'
                  onClick={handleSaveDrawer}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    color: '#ffff',
                    fontSize: '16px',
                    gap: '8px',
                    backgroundColor: '#73A3D0',
                    '&:hover': {
                      backgroundColor: '#73A3D0'
                    }
                  }}
                >
                  تعديل
                </Button>
                <Button
                  size='small'
                  onClick={handleCloseDrawer}
                  sx={{
                    p: '8px 24px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                    borderRadius: 1,
                    fontSize: '16px',
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                    '&:hover': {
                      backgroundColor: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                    }
                  }}
                >
                  إلغاء
                </Button>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />
            <Box sx={{ p: theme => theme.spacing(0, 6, 6) }}>
              <Stack sx={{ gap: '8px' }}>
                <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
                  <City />
                  <Box>
                    <Typography className='custom-style-label '>الدولة</Typography>
                  </Box>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {/* {data?.city?.country?.name} */}
                  {data?.country_name}
                </Typography>
              </Stack>

              <Stack sx={{ gap: '8px' }}>
                <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
                  <Address />
                  <Box>
                    <Typography className='custom-style-label '>المدينة</Typography>
                  </Box>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.name}
                </Typography>
              </Stack>

              <Typography className='custom-style-label '>
                <Arrowleft /> تصنيفات الفرع{' '}
              </Typography>

              {fields.map((field, index) => (
                <div key={field.id}>
                  <Controller
                    name={`branches.${index}.name`}
                    control={control}
                    rules={{ required: 'Email is required' }}
                    render={({ field: { value, onChange } }) => (
                      <CustomTextField
                        fullWidth
                        value={value}
                        sx={{ mb: 4 }}
                        onChange={onChange}
                        error={Boolean(errors.emails?.[index]?.email)}
                        placeholder='تصنيف الفرع'
                        {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                        InputProps={{
                          endAdornment: (
                            <Box>
                              {index > 0 ? (
                                <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                                  <DeleteRed />
                                </Box>
                              ) : (
                                ''
                              )}
                            </Box>
                          )
                        }}
                      />
                    )}
                  />
                </div>
              ))}
              <Typography
                sx={{
                  cursor: 'pointer',

                  '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
                }}
                onClick={() => append({ name: '', id: '' })}
                variant='text'
              >
                + اضافة تصنيف{' '}
              </Typography>

              <Controller
                name='admin_id'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <Autocomplete
                    options={store.admin.data || []}
                    fullWidth
                    id='autocomplete-sales-manager-select'
                    getOptionLabel={option => option.name}
                    value={store.admin.data.find(user => user.id === value) || null}
                    onChange={(event, newValue) => {
                      onChange(newValue ? newValue.id : '')
                    }}
                    renderInput={params => (
                      <CustomTextField
                        {...params}
                        placeholder='مدير الفرع'
                        fullWidth
                        sx={{ marginY: 4 }}
                        label='مدير الفرع'
                        id='validation-billing-select'
                        aria-describedby='validation-billing-select'
                        error={Boolean(errors.salesManager_id)}
                        helperText={errors.salesManager_id?.message || ''}
                      />
                    )}
                  />
                )}
              />
            </Box>
          </Drawer>
        </FormProvider>
      )}

      {Editing ? (
        <FormProvider {...methods}>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
          >
            <Header>
              <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64, textAlign: 'center' }}>
                <BranchesAvatar />
              </Avatar>
              <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
                <Button
                  size='small'
                  onClick={handleSaveDrawer}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    color: '#ffff',
                    fontSize: '16px',
                    gap: '8px',
                    backgroundColor: '#73A3D0',
                    '&:hover': {
                      backgroundColor: '#73A3D0'
                    }
                  }}
                >
                  تعديل
                </Button>
                <Button
                  size='small'
                  onClick={handleCloseDrawer}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    fontSize: '16px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                    '&:hover': {
                      backgroundColor: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                    }
                  }}
                >
                  إلغاء
                </Button>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />
            <Box sx={{ p: theme => theme.spacing(0, 6, 6), display: 'flex', flexDirection: 'column', gap: '12px' }}>
              <Stack sx={{ gap: '8px' }}>
                <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
                  <City />
                  <Typography className='custom-style-label2 '>الدولة</Typography>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {/* {data?.city?.country?.name} */}
                  {data?.country_name}
                </Typography>
              </Stack>

              <Stack sx={{ gap: '8px' }}>
                <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
                  <Address />
                  <Box>
                    <Typography className='custom-style-label2 '>المدينة</Typography>
                  </Box>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.name}
                </Typography>
              </Stack>
              <Typography className='custom-style-label '>
                <Arrowleft /> تصنيفات الفرع{' '}
              </Typography>

              {fields.map((field, index) => (
                <div key={field.id}>
                  <Controller
                    name={`branches.${index}.name`}
                    control={control}
                    rules={{ required: 'Email is required' }}
                    render={({ field: { value, onChange } }) => (
                      <CustomTextField
                        fullWidth
                        value={value}
                        onChange={onChange}
                        error={Boolean(errors.emails?.[index]?.email)}
                        placeholder='تصنيف الفرع'
                        {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                        InputProps={{
                          endAdornment: (
                            <Box>
                              {index > 0 ? (
                                <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                                  <DeleteRed />
                                </Box>
                              ) : (
                                ''
                              )}
                            </Box>
                          )
                        }}
                      />
                    )}
                  />
                  <Controller
                    name={`branches.${index}.id`}
                    control={control}
                    rules={{ required: 'Email is required' }}
                    render={({ field: { value, onChange } }) => <input name='id' hidden />}
                  />
                </div>
              ))}
              <Typography
                sx={{
                  cursor: 'pointer',
                  '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
                }}
                onClick={() => append({ name: '', id: '' })}
                variant='text'
              >
                + اضافة تصنيف{' '}
              </Typography>

              <Typography className='custom-style-label '>مدير الفرع</Typography>
              <Controller
                name='admin_id'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <Autocomplete
                    options={store.admin.data || []}
                    fullWidth
                    id='autocomplete-sales-manager-select'
                    getOptionLabel={option => option.name}
                    value={store.admin.data?.find(user => user.id === value) || null}
                    onChange={(event, newValue) => {
                      onChange(newValue ? newValue.id : '')
                    }}
                    renderInput={params => (
                      <CustomTextField
                        {...params}
                        placeholder='مدير الفرع'
                        fullWidth
                        id='validation-billing-select'
                        aria-describedby='validation-billing-select'
                        error={Boolean(errors.salesManager_id)}
                        helperText={errors.salesManager_id?.message || ''}
                      />
                    )}
                  />
                )}
              />
            </Box>
          </Drawer>
        </FormProvider>
      ) : (
        ''
      )}
    </>
  )
}

export default SidebarAddBranches
