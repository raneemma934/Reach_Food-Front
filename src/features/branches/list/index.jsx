import { Grid } from '@mui/material'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomBranchesCards from 'src/@core/components/custom-branch-card'
import { fetchbranchesByCity } from 'src/store/apps/branches'

export default function List({ data }) {
  return (
    <>
      <div>
        <Grid container spacing={6}>
          {data &&
            data?.map((branch, index) => (
              <Grid item sm={4} xs={12} key={index}>
                <CustomBranchesCards branch={branch} />
              </Grid>
            ))}
        </Grid>
      </div>
    </>
  )
}
