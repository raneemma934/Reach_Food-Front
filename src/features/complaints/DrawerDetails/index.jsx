import styled from '@emotion/styled'
import { Avatar, Button, Divider, Drawer, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React from 'react'
import Delete from '../../../../public/images/cards/deleteWihte'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

export default function DrawerDetails({ img, content, open, handleCloseDrawer, userName, onDelete, handleOpenDelete }) {
  return (
    <Drawer
      open={open}
      anchor='right'
      variant='temporary'
      onClose={handleCloseDrawer}
      ModalProps={{ keepMounted: true }}
      sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
    >
      <Header>
        <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
          <Avatar
            sx={{
              width: 64,
              height: 64,
              fontSize: '24px',
              fontWeight: '700',
              backgroundColor: 'rgba(115, 163, 208, 0.25)',
              color: theme => theme.palette.accent.main
            }}
            src={process.env.NEXT_PUBLIC_IMAGES + '/' + img}
            alt='dlivia Sparks'
          >
            {userName?.slice(0, 1).toUpperCase()}
          </Avatar>
          <Typography
            sx={{
              fontWeight: '500',
              fontSize: '16px',
              lineHeight: '32px'
            }}
          >
            {userName}
          </Typography>
        </Stack>
        <Stack direction={'row'} alignItems={'center'} spacing={2}>
          <Box
            onClick={event => handleOpenDelete(event)}
            sx={{
              padding: '8px 12px 8px 12px',
              borderRadius: 1,

              fontSize: '16px',
              gap: '2px',
              boxShadow: '0px 0.8px 0.9px 0px ',
              backgroundColor: '#D12E3D',
              display: 'flex',
              alignItems: 'center',
              cursor: 'pointer',
              '&:hover': {
                backgroundColor: '#D12E3D'
              }
            }}
            className='hover-edit'
          >
            <Box sx={{ marginTop: '4px' }}>
              <Delete />
            </Box>
            <Button
              onClick={onDelete}
              size='small'
              sx={{
                color: '#fff',
                fontSize: '16px',
                padding: '0px',
                fontWeight: '600',
                '&:hover': {
                  color: '#fff',
                  backgroundColor: 'transparent'
                }
              }}
            >
              حذف
            </Button>
          </Box>
        </Stack>
      </Header>
      <Divider sx={{ marginY: '4px' }} />
      <Box sx={{ p: theme => theme.spacing(0, 6, 6) }}>
        <Typography
          sx={{
            textAlign: 'left',
            fontSize: '16px',
            fontStyle: 'normal',
            fontWeight: '400',
            lineHeight: '25px'
          }}
        >
          {content}
        </Typography>
      </Box>
    </Drawer>
  )
}
