import { Grid } from '@mui/material'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomCards from 'src/@core/components/custom-cards'
import CustomComplaintsCards from 'src/@core/components/custom-complaints-card'
import { deleteFeedback, fetchFeedback } from 'src/store/apps/complaints'

export default function List({ data }) {
  return (
    <div>
      <Grid container spacing={6}>
        {data &&
          data.map((client, index) => (
            <Grid item sm={4} xs={12} key={index}>
              <CustomComplaintsCards client={client} />
            </Grid>
          ))}
      </Grid>
    </div>
  )
}
