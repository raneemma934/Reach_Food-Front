import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'
import 'react-datepicker/dist/react-datepicker.css'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { Controller, useFormContext, FormProvider, useForm } from 'react-hook-form'
import { Autocomplete, Avatar, Divider, TextField, Typography } from '@mui/material'
import { fontSize, fontWeight, Stack, textAlign } from '@mui/system'
import { useDispatch, useSelector } from 'react-redux'
import { addProducts } from 'src/store/apps/products'
import City from '../../../../public/images/cards/city'
import Location from '../../../../public/images/cards/Location'
import Day from '../../../../public/images/cards/day'
import Clock from '../../../../public/images/cards/clock'
import CustomAutocomplete from 'src/@core/components/mui/autocomplete'
import { addTrip, fetchCustomerByAddress, fetchData, fetchLocation } from 'src/store/apps/user'
import DatePicker from 'react-datepicker'
import PickersComponent from 'src/views/forms/form-elements/pickers/PickersCustomInput'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const daysMap = {
  Saturday: 'السبت',
  Sunday: 'الأحد',
  Monday: 'الأثنين',
  Tuesday: 'الثلاثاء',
  Wednesday: 'الأربعاء',
  Thursday: 'الخميس',
  Friday: 'الجمعة'
}

const schema = yup.object().shape({
  start_time: yup
    .string()
    .required('وقت الأنطلاق مطلوب')
    .matches(/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/, 'يجب أن يكون الوقت بصيغة HH:MM'),
  end_time: yup
    .string()
    .required('وقت الوصول مطلوب')
    .matches(/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/, 'يجب أن يكون الوقت بصيغة HH:MM'),
  day: yup.string().required('اليوم مطلوب'),
  address_id: yup.string().required('المنطقة مطلوبة')
})

const defaultValues = {
  start_time: '',
  end_time: '',
  day: '',
  address_id: ''
}

const SidebarAddFlights = props => {
  // ** Props
  const { open, handleCloseDrawer } = props
  const store = useSelector(state => state.user)
  const dispatch = useDispatch()
  const [time, setTime] = useState(new Date())
  const [dateTime, setDateTime] = useState(new Date())
  const BranchDetails = JSON.parse(localStorage.getItem('branch'))

  const { reset, formState, watch, control, getValues } = useForm({
    defaultValues,
    mode: 'onChange',
    resolver: yupResolver(schema)
  })
  const { isValid, dirtyFields, errors } = formState

  useEffect(() => {
    dispatch(fetchLocation())
    const branch_id = localStorage.getItem('branch_id')
    dispatch(fetchData(`role=salesman&branch_id=${branch_id}`))
  }, [dispatch])

  const [selectedAddress, SetSelectedAddress] = useState('')

  const handleSaveData = () => {
    dispatch(addTrip(getValues()))
    reset({
      ...defaultValues
    })

    handleCloseDrawer()
  }

  const FetchCustomers = id => {
    dispatch(fetchCustomerByAddress(id))
  }

  const top100Films = [
    {
      id: '1',
      name: 'dd'
    }
  ]

  const defaultProps = {
    options: top100Films,
    getOptionLabel: option => option.title
  }

  const flatProps = {
    options: top100Films.map(option => option.title)
  }

  const [Locations, setLocations] = useState(null)

  return (
    <Drawer
      open={open}
      anchor='right'
      variant='temporary'
      onClose={handleCloseDrawer}
      ModalProps={{ keepMounted: true }}
      sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
    >
      <Header>
        <Stack direction={'column'} alignItems={'center'} spacing={2}>
          <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64, borderRadius: '50%' }} src={''}>
            <img src='/images/icons/avatar.png' alt='' />
          </Avatar>
        </Stack>
        <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
          <Button
            size='small'
            onClick={handleSaveData}
            sx={{
              p: '8px 24px',
              lineHeight: 'normal',
              borderRadius: 1,
              color: !isValid ? '#fff' : '#fff',
              opacity: !dirtyFields ? 1 : 0.6,
              fontSize: '16px',
              gap: '8px',
              backgroundColor: '#73A3D0',
              '&:hover': {
                backgroundColor: '#73A3D0'
              },
              '&.Mui-disabled': {
                opacity: '70%',
                color: '#fff'
              }
            }}
          >
            إضافة
          </Button>
          <Button
            size='small'
            onClick={handleCloseDrawer}
            sx={{
              p: '8px 24px',
              lineHeight: 'normal',
              borderRadius: 1,
              fontSize: '16px',
              color: theme =>
                theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
              backgroundColor: theme =>
                theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
              '&:hover': {
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
              }
            }}
          >
            إلغاء
          </Button>
        </Stack>
      </Header>
      <Divider sx={{ marginY: '4px' }} />
      <Box sx={{ p: '24px' }}>
        <Box>
          <Typography className='custom-style-label2 '>
            <City />
            {BranchDetails?.data?.city?.country?.name + ' - ' + BranchDetails?.data?.city?.name}
          </Typography>
        </Box>

        <Stack>
          <Box>
            <Typography className='custom-style-label '>
              <Location /> المنطقة
            </Typography>
          </Box>
          <Controller
            name='address_id'
            control={control}
            rules={{ required: 'Location is required' }}
            render={({ field: { onChange, value } }) => (
              <CustomAutocomplete
                options={store?.Locations}
                fullWidth
                id='autocomplete-controlled'
                getOptionLabel={option => option.area || ''}
                value={store?.Locations.find(location => location.id === value) || null}
                onChange={(event, newValue) => {
                  onChange(newValue?.id || null)
                  FetchCustomers(newValue?.id)
                }}
                renderInput={params => (
                  <CustomTextField
                    {...params}
                    placeholder='الرجاء اختيار المنطقة'
                    error={Boolean(errors.location)}
                    helperText={errors.location ? errors.location.message : ''}
                  />
                )}
              />
            )}
          />
          <Box>
            <Typography className='custom-style-label '>
              <Day /> اليوم
            </Typography>
          </Box>
          <Controller
            name='day'
            control={control}
            rules={{ required: 'Day is required' }}
            render={({ field: { value, onChange } }) => (
              <Autocomplete
                options={[
                  { label: 'السبت', value: 'Saturday' },
                  { label: 'الأحد', value: 'Sunday' },
                  { label: 'الأثنين', value: 'Monday' },
                  { label: 'الثلاثاء', value: 'Tuesday' },
                  { label: 'الأربعاء', value: 'Wednesday' },
                  { label: 'الخميس', value: 'Thursday' },
                  { label: 'الجمعة', value: 'Friday' }
                ]}
                fullWidth
                id='autocomplete-day-select'
                getOptionLabel={option => option.label}
                value={value ? { label: daysMap[value], value } : null}
                onChange={(event, newValue) => {
                  onChange(newValue ? newValue.value : null)
                }}
                renderInput={params => (
                  <CustomTextField
                    {...params}
                    placeholder='اليوم'
                    error={Boolean(errors.day)}
                    helperText={errors.day ? errors.day.message : ''}
                  />
                )}
              />
            )}
          />
          <Box>
            <Typography className='custom-style-label '>
              <Clock /> وقت الأنطلاق
            </Typography>
          </Box>
          <Controller
            name='start_time'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                id='start_time'
                name='start_time'
                type='time'
                value={value}
                onChange={onChange}
                autoFocus
                required
                placeholder='وقت الأنطلاق'
                error={!!errors.start_time}
                helperText={errors?.start_time?.message}
              />
            )}
          />
          <Box>
            <Typography className='custom-style-label '>
              <Clock /> وقت الانتهاء
            </Typography>
          </Box>
          <Controller
            name='end_time'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                type='time'
                value={value}
                onChange={onChange}
                placeholder='وقت الأنطلاق'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />
          <Box>
            <Typography className='custom-style-label '>المندوب</Typography>
          </Box>

          <Controller
            name='salesman_id'
            control={control}
            rules={{ required: 'salesman is required' }}
            render={({ field: { onChange, value } }) => (
              <CustomAutocomplete
                options={store?.data}
                fullWidth
                id='autocomplete-controlled'
                getOptionLabel={option => option.name || ''}
                value={store?.data.find(location => location.id === value) || null}
                onChange={(event, newValue) => {
                  onChange(newValue?.id || null)
                }}
                renderInput={params => (
                  <CustomTextField
                    {...params}
                    placeholder='الرجاء اختيار مندوب مبيعات' // Add placeholder here
                    error={Boolean(errors.salesman_id)}
                    helperText={errors.location ? errors.location.message : ''}
                  />
                )}
              />
            )}
          />
        </Stack>

        <Divider sx={{ marginY: '24px' }} />

        {store.Customer.length > 0 ? (
          <>
            <Box>
              <Typography className='custom-style-label '>الزبائن ومواعيد التسليم</Typography>
            </Box>
            {store.Customer.map((customer, index) => (
              <Stack
                direction={'row'}
                alignItems={'center'}
                justifyContent={'space-between'}
                sx={{ margin: '16px 0px' }}
                key={index}
              >
                <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
                  <Avatar
                    sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 42, height: 42, borderRadius: '50%' }}
                    src={''}
                  />
                  <Box>
                    <Typography className='custom-style-label2 '>{customer?.name}</Typography>
                    <Typography sx={{ fontSize: '14px', fontWeight: '400', lineHeight: '22px', color: '#5A5A5A' }}>
                      {' '}
                      {customer?.customer_type_ar}{' '}
                    </Typography>
                  </Box>
                </Box>
                <Box
                  sx={{
                    width: '130px'
                  }}
                >
                  <Controller
                    key={customer.id}
                    name={`customerTimes.[${index}].time`}
                    control={control}
                    rules={{ required: true }}
                    render={({ field: { value, onChange }, fieldState: { error } }) => (
                      <CustomTextField
                        fullWidth
                        type='time'
                        value={value}
                        onChange={onChange}
                        error={Boolean(error)}
                        helperText={error ? error.message : ''}
                      />
                    )}
                  />

                  <Controller
                    name={`customerTimes.[${index}].id`}
                    control={control}
                    defaultValue={customer.id} // Ensure a default value is provided
                    rules={{ required: true }}
                    render={({ field: { value, onChange } }) => (
                      <input type='hidden' value={value} onChange={onChange} />
                    )}
                  />
                </Box>
              </Stack>
            ))}
          </>
        ) : (
          <Stack direction={'column'} alignItems={'center'} spacing={3}>
            <Typography
              sx={{
                textAlign: 'center',
                fontSize: '16px',
                fontWeight: '500',
                lineHeight: '32px'
              }}
            >
              لا يوجد زبائن في هذه المنطقة
            </Typography>
            <div style={{ textAlign: 'center' }}>
              <svg xmlns='http://www.w3.org/2000/svg' width='240' height='240' viewBox='0 0 240 240' fill='none'>
                <path d='M174.85 202.96L214.49 201.95V223.44L174.85 224.76V202.96Z' fill='#E0E0E0' />
                <path d='M175.34 203.19L214.99 202.18V223.67L175.34 224.99V203.19Z' fill='#D1D1D1' />
                <path d='M176.439 203.38L216.089 202.37V223.87L176.439 225.19V203.38Z' fill='#E0E0E0' />
                <path
                  d='M178.419 109.37C178.419 109.37 188.759 118.8 190.279 154.69C191.799 190.58 190.279 193.29 190.279 193.29C190.279 193.29 185.109 201.08 185.409 202.6H189.209C189.209 202.6 187.029 216.84 188.649 216.84C190.269 216.84 191.999 206.91 191.999 206.91C191.999 206.91 191.899 215.73 193.829 216.84C195.759 217.96 195.549 206.8 195.549 206.8C195.549 206.8 196.969 215.52 198.989 215.72C201.019 215.93 198.579 199.3 195.439 192.7C195.439 192.7 201.219 116.35 184.479 95.6599C184.479 95.6599 177.179 100.22 178.399 109.35L178.419 109.37Z'
                  fill='#5A89AF'
                />
                <path
                  d='M96.3593 164.34L108.929 221.33C108.929 221.33 88.0393 218.28 88.4493 223.16C88.8593 228.03 115.629 223.16 115.629 223.16V162.93L96.3593 164.35V164.34Z'
                  fill='#5A89AF'
                />
                <path
                  d='M144.221 164.34L131.651 221.33C131.651 221.33 152.541 218.28 152.131 223.16C151.721 228.03 124.961 223.16 124.961 223.16V162.93L144.221 164.35V164.34Z'
                  fill='#5A89AF'
                />
                <path
                  d='M80.7498 162.35C80.7498 162.35 80.6198 163.32 80.5098 164.89C80.7398 163.42 80.7498 162.35 80.7498 162.35Z'
                  fill='#73A3D0'
                />
                <path
                  d='M63.8906 47.57C64.3306 47.37 64.6206 47.22 64.6206 47.22C64.3806 47.33 64.1406 47.44 63.8906 47.57Z'
                  fill='#73A3D0'
                />
                <path
                  d='M104.891 167.79C105.041 166.4 105.081 165.55 105.081 165.55C105.081 165.55 104.901 166.46 104.891 167.79Z'
                  fill='#73A3D0'
                />
                <path
                  d='M174.651 53.4C179.721 53.2 177.691 48.13 177.691 48.13C164.111 21.57 124.901 29.47 124.901 29.47C124.901 29.47 140.381 25.21 135.711 21.15C131.051 17.09 122.131 25.21 122.131 25.21C122.131 25.21 131.461 13.85 125.981 13.04C120.501 12.23 117.661 22.17 117.661 22.17C117.661 22.17 118.271 11.02 112.801 11.02C107.331 11.02 110.971 29.48 110.971 29.48C77.1508 19.19 60.8308 41.59 59.7608 46.01C58.9308 49.45 62.3508 48.26 63.9008 47.58C64.1408 47.46 64.3808 47.34 64.6308 47.23C64.6308 47.23 64.3408 47.39 63.9008 47.58C62.7408 48.18 61.5908 49.03 60.4808 50.08C66.0308 49.16 71.6708 46.26 76.8008 44.5C76.8008 45.11 77.1008 45.72 76.8008 46.02C73.4508 50.58 66.4608 54.53 68.2908 59.41C70.1208 64.58 78.6308 61.54 83.5008 59.11C89.5808 55.76 95.3608 51.81 101.751 49.38C101.451 54.86 99.0208 59.72 99.6208 65.2C109.961 67.63 116.961 57.59 120.921 49.69C126.091 52.42 125.481 60.03 130.961 61.86C137.961 63.99 141.601 56.99 140.691 50.6C140.691 49.99 141.601 50.3 141.911 50.3C147.991 54.25 152.871 61.56 160.161 58.51C164.421 56.68 161.681 50.3 162.291 46.04C163.811 46.34 164.721 47.56 165.331 48.47C169.101 54.01 174.011 58.91 179.931 60.33C177.201 54.66 174.671 53.44 174.671 53.44V53.42L174.651 53.4Z'
                  fill='#6C9DC4'
                />
                <path
                  d='M181.639 130.36C185.489 130.77 185.699 125.39 185.699 125.39C188.429 86.88 183.949 68.7 179.909 60.29C173.989 58.87 169.089 53.97 165.309 48.43C164.699 47.52 163.789 46.3 162.269 46C161.659 50.26 164.399 56.64 160.139 58.47C152.839 61.51 147.969 54.21 141.889 50.26C141.589 50.26 140.669 49.96 140.669 50.56C141.579 56.95 137.929 63.95 130.939 61.82C125.459 59.99 126.079 52.39 120.899 49.65C116.949 57.56 109.939 67.6 99.5993 65.16C98.9893 59.68 101.429 54.82 101.729 49.34C95.3393 51.77 89.5593 55.73 83.4793 59.07C78.6093 61.5 70.0893 64.55 68.2693 59.37C66.4393 54.5 73.4393 50.55 76.7793 45.98C77.0793 45.68 76.7793 45.07 76.7793 44.46C71.6493 46.22 66.0193 49.12 60.4593 50.04C50.3493 59.49 42.5093 84.91 43.3193 90.54C44.2293 96.93 47.5793 96.16 47.5793 96.16C45.8893 108.63 46.3693 128.92 48.6293 142.95C51.8993 140.73 55.1593 138.33 59.1393 137.83C59.1393 142.69 57.6193 149.69 61.5693 151.82C66.4393 154.25 70.0793 146.65 74.9593 144.52C75.5693 144.22 76.4793 144.52 76.4793 144.82C77.3893 149.38 76.1793 155.16 79.2093 157.6C82.5593 160.03 85.2893 153.34 88.9393 150.91C90.7693 149.69 92.2893 151.21 92.5893 153.04C93.1993 157.3 94.1093 164.6 99.2793 161.86C103.539 159.13 104.149 153.04 108.099 150C108.709 149.39 109.929 149.39 110.829 150C114.179 153.04 115.089 157.3 117.829 160.65C120.569 163.69 123.909 161.26 125.739 158.52C127.569 155.17 129.089 147.26 132.739 150.91C136.999 154.56 140.949 163.08 146.729 160.34C150.079 158.82 147.949 153.04 150.379 149.38C150.679 149.08 151.599 149.08 152.209 149.68C155.259 153.94 159.819 159.41 163.769 156.68C166.509 154.85 165.599 149.38 166.809 145.72C167.109 145.11 168.029 144.2 168.329 144.5C170.879 147.61 174.439 151.15 178.039 152.36C182.479 141.59 181.609 130.3 181.609 130.3L181.629 130.34L181.639 130.36Z'
                  fill='#73A3D0'
                />
                <path
                  d='M168.36 144.55C168.06 144.25 167.14 145.16 166.84 145.77C165.62 149.42 166.54 154.9 163.8 156.73C159.85 159.47 155.29 154 152.24 149.74C151.63 149.13 150.72 149.13 150.41 149.44C147.98 153.09 150.11 158.87 146.76 160.4C140.98 163.13 137.03 154.62 132.77 150.97C129.12 147.32 127.6 155.23 125.77 158.58C123.94 161.32 120.6 163.75 117.86 160.71C115.13 157.36 114.21 153.1 110.86 150.06C109.94 149.45 108.73 149.45 108.12 150.06C104.17 153.1 103.56 159.19 99.3004 161.92C94.1304 164.66 93.2204 157.36 92.6104 153.1C92.3104 151.27 90.7804 149.75 88.9604 150.97C85.3104 153.4 82.5704 160.1 79.2304 157.66C76.1904 155.23 77.4004 149.45 76.5004 144.88C76.5004 144.58 75.5904 144.27 74.9804 144.58C70.1104 146.71 66.4704 154.31 61.5904 151.88C57.6404 149.75 59.1604 142.75 59.1604 137.89C55.1804 138.39 51.9304 140.78 48.6504 143.01C49.4504 147.97 50.4704 152.15 51.7004 154.93C56.4204 165.58 65.3904 155.08 65.3904 155.08C65.3904 155.08 65.0904 172.42 72.8404 173.33C78.2704 173.97 79.9704 168.34 80.5104 164.91C80.6204 163.34 80.7504 162.37 80.7504 162.37C80.7504 162.37 80.7304 163.44 80.5104 164.91C80.0804 171.17 80.0504 186.98 90.6404 188.69C101.43 190.43 104.23 173.95 104.9 167.82C104.92 166.48 105.09 165.58 105.09 165.58C105.09 165.58 105.05 166.44 104.9 167.82C104.87 170.95 105.72 176.44 111.78 178.35C120.45 181.08 123.49 170.44 123.49 170.44C124.71 173.94 129.11 169.38 129.11 169.38C130.33 176.07 135.04 170.75 135.04 170.75C135.8 176.07 139.3 172.58 139.3 172.58C138.84 177.3 140.82 177.9 140.82 177.9C144.42 178.55 146.08 176.81 146.82 174.8C146.9 172.11 147.36 169.99 147.36 169.99C147.36 169.99 147.65 172.56 146.82 174.8C146.68 179.21 147.52 185.18 152.93 186.06C161.65 187.48 161.85 169.23 161.85 169.23C170.38 165.79 175.26 159.22 178.05 152.46C174.44 151.24 170.89 147.7 168.34 144.6V144.57L168.36 144.55Z'
                  fill='#6193BC'
                />
                <path
                  d='M147.36 169.96C147.36 169.96 146.91 172.08 146.82 174.77C147.65 172.53 147.36 169.96 147.36 169.96Z'
                  fill='#73A3D0'
                />
                <path
                  d='M92.8092 94.0699C90.8592 96.6099 88.7092 99.7299 88.2292 102.95C87.9292 104.92 88.8092 106.46 90.7892 106.94C93.2192 107.53 95.5292 106.44 96.9792 104.48C97.9192 103.21 98.5892 101.66 98.9392 100.1C99.1892 98.9699 96.8492 99.4799 96.5592 100.32C96.0892 101.67 95.7092 103.06 95.3892 104.45C94.8792 106.69 94.3792 109.19 94.8392 111.48C95.3092 113.76 97.2392 114.61 99.4492 114.13C102.519 113.46 104.829 111.27 105.849 108.34C106.489 106.51 106.729 104.48 106.539 102.54C106.449 101.62 104.459 102.15 104.239 102.76C103.779 104.03 103.429 105.35 103.209 106.69C102.879 108.64 102.689 111.14 103.989 112.8C105.609 114.89 108.999 114.52 111.129 113.54C113.509 112.45 114.909 110.2 115.549 107.74C115.959 106.17 116.099 104.48 115.979 102.85C115.909 101.89 113.539 102.43 113.609 103.49C113.849 106.76 114.959 111.18 118.039 112.9C119.879 113.93 122.419 113.87 123.749 112.06C124.879 110.51 124.869 108.31 124.779 106.48C124.709 105.01 124.539 103.53 124.229 102.1C124.019 101.1 121.669 101.71 121.859 102.74C122.469 105.87 123.749 109.66 126.329 111.7C127.939 112.97 130.449 113.29 132.089 111.9C133.529 110.69 133.569 108.6 133.449 106.88C133.349 105.43 133.089 103.96 132.689 102.56L130.419 103.38C131.059 104.42 131.819 105.47 132.719 106.31C133.709 107.22 135.809 108.04 136.669 106.45C137.239 105.41 136.329 103.8 135.889 102.87C135.449 101.94 134.949 101 134.419 100.1C133.989 99.3699 131.809 100.03 132.149 100.92C133.169 103.54 135.229 106.72 138.319 106.89C139.789 106.97 141.579 106.32 141.639 104.62C141.689 103.13 140.799 101.63 140.079 100.4C139.479 99.3599 138.799 98.3299 138.059 97.3699C137.469 96.5899 135.199 97.4799 135.879 98.3699C137.329 100.28 139.129 102.69 139.219 105.15C139.219 105.3 139.059 105.5 139.269 105.32C139.429 105.17 139.239 105.34 139.059 105.32C138.869 105.3 138.729 105.28 138.549 105.22C137.659 104.94 136.949 104.21 136.379 103.51C135.599 102.55 134.949 101.44 134.499 100.29L132.229 101.11C133.059 102.52 134.199 104.25 134.399 105.9C134.399 105.92 134.419 106.33 134.399 106.32C134.259 106.19 135.609 106.06 135.359 105.68C135.249 105.51 134.889 105.34 134.729 105.18C134.499 104.97 134.299 104.73 134.089 104.49C133.529 103.83 133.019 103.13 132.559 102.39C132.099 101.65 130.019 102.28 130.289 103.21C130.909 105.36 131.629 108.35 130.729 110.49C130.519 111 130.309 111.25 129.949 111.23C129.179 111.18 128.509 110.77 127.949 110.27C126.589 109.03 125.779 107.23 125.159 105.54C124.759 104.43 124.409 103.29 124.189 102.13L121.819 102.77C122.349 105.25 122.759 108.16 122.069 110.64C121.919 111.15 121.609 111.84 121.259 112.04C121.139 112.04 121.449 111.93 121.259 112.04C121.129 112.04 121.009 112.04 120.879 112.02C120.419 111.95 119.979 111.77 119.589 111.51C117.989 110.49 117.169 108.51 116.649 106.76C116.279 105.51 116.009 104.2 115.919 102.89L113.549 103.53C113.569 103.73 113.549 103.51 113.549 103.6C113.529 103.85 113.549 104.11 113.549 104.36C113.549 105.14 113.479 105.92 113.349 106.69C113.059 108.54 112.429 110.57 110.919 111.8C109.609 112.87 107.169 113.33 106.039 111.8C105.009 110.4 105.239 108.28 105.489 106.68C105.629 105.8 105.829 104.93 106.079 104.07C106.159 103.78 106.249 103.49 106.339 103.2C106.359 103.12 106.419 103.02 106.429 102.93C106.429 102.98 106.359 103.13 106.399 103.01L104.099 103.23C104.129 103.58 104.139 103.84 104.119 104.34C104.099 105.28 104.009 106.21 103.809 107.12C103.309 109.43 102.059 111.71 99.6992 112.52C99.0092 112.76 98.3792 112.9 97.8592 112.46C97.2792 111.95 97.1192 111.06 97.0392 110.34C96.7992 108.32 97.2492 106.17 97.7192 104.2C98.0392 102.84 98.4092 101.47 98.8692 100.15L96.4892 100.37C96.0592 102.3 94.6492 106.02 92.1292 105.5C88.4792 104.75 91.7992 99.1699 92.8192 97.4899C93.2392 96.7899 93.6992 96.0899 94.1792 95.4199C94.3726 95.1399 94.5492 94.8966 94.7092 94.6899C95.6092 93.5299 93.3492 93.3299 92.7592 94.0999L92.7792 94.0799L92.8092 94.0699Z'
                  fill='#6C9DC4'
                />
                <path
                  d='M86.4703 85.8299C86.3703 90.3899 87.6503 94.1399 89.3303 94.1999C91.0103 94.2699 92.4503 90.6199 92.5503 86.0499C92.6503 81.4899 91.3703 77.7399 89.6903 77.6799C88.0203 77.6099 86.5703 81.2599 86.4703 85.8299Z'
                  fill='#181818'
                />
                <path
                  d='M134.871 85.8299C134.771 90.3899 136.051 94.1399 137.731 94.1999C139.411 94.2699 140.851 90.6199 140.951 86.0499C141.051 81.4899 139.771 77.7399 138.091 77.6799C136.421 77.6099 134.971 81.2599 134.871 85.8299Z'
                  fill='#181818'
                />
                <path
                  d='M83.0397 82.6801C81.2397 82.5501 80.5097 80.2801 81.8197 79.0501C85.5697 75.5401 94.3197 68.0301 97.0297 71.6001C99.8297 75.3001 88.7697 83.1001 83.0497 82.6801H83.0397Z'
                  fill='#6193BC'
                />
                <path
                  d='M146.001 82.68C147.801 82.55 148.531 80.28 147.221 79.05C143.471 75.54 134.711 68.04 132.011 71.61C129.211 75.31 140.281 83.1 146.001 82.68Z'
                  fill='#6193BC'
                />
                <path
                  d='M103.05 86.25C102.99 94.17 97.9795 101.47 90.4695 104.13C82.9595 106.79 74.5595 104.46 69.4795 98.39C64.3995 92.32 63.6995 83.2 67.8695 76.37C72.0395 69.54 80.0795 66.08 87.8195 67.64C96.6195 69.43 102.99 77.31 103.05 86.25C103.05 87.15 104.45 87.15 104.44 86.25C104.38 77.78 99.0595 70.08 91.0895 67.13C83.1195 64.18 73.7295 66.71 68.3195 73.33C62.9195 79.95 62.1995 89.52 66.6595 96.83C71.1195 104.14 79.8195 107.88 88.1795 106.19C97.5795 104.28 104.36 95.77 104.43 86.24C104.43 85.34 103.04 85.34 103.04 86.24L103.05 86.25Z'
                  fill='#DDDDDD'
                />
                <path
                  d='M162.76 86.25C162.7 94.17 157.69 101.47 150.18 104.13C142.67 106.79 134.27 104.46 129.19 98.39C124.11 92.32 123.41 83.2 127.58 76.37C131.75 69.54 139.79 66.08 147.53 67.64C156.33 69.43 162.7 77.31 162.76 86.25C162.76 87.15 164.16 87.15 164.15 86.25C164.09 77.78 158.77 70.08 150.8 67.13C142.83 64.18 133.44 66.71 128.03 73.33C122.63 79.95 121.91 89.52 126.37 96.83C130.83 104.14 139.53 107.88 147.89 106.19C157.29 104.28 164.07 95.77 164.14 86.24C164.14 85.34 162.75 85.34 162.75 86.24L162.76 86.25Z'
                  fill='#DDDDDD'
                />
                <path
                  d='M76.6906 95.3101L58.3906 102.42L74.8106 139.69L99.7706 130.26L84.0606 96.9801C81.5906 96.4301 79.1406 95.8801 76.6706 95.3101H76.6906Z'
                  fill='#E0E0E0'
                />
                <path
                  d='M47.0298 102.42V117.63C47.0298 117.63 36.9898 122.65 37.7498 134.67C38.5098 146.69 59.6498 140.75 64.5198 120.82C64.5198 120.82 62.7398 119.67 64.1898 115.56L65.5698 118.69C65.5698 118.69 71.0698 108.8 71.9898 109.41C72.8998 110.02 70.1598 117.09 70.1598 117.09C70.1598 117.09 74.3398 112.22 75.5598 113.97C76.7798 115.72 72.3698 118.92 72.3698 118.92C72.3698 118.92 77.7698 117.02 78.1498 119.08C78.5298 121.14 68.2598 123.8 68.2598 123.8C68.2598 123.8 63.8498 145.63 39.2098 148.66C14.5698 151.7 26.5898 110.41 47.0498 102.42H47.0298Z'
                  fill='#5A89AF'
                />
                <path
                  d='M78.5092 99.1501L84.0892 96.9901C81.6292 96.4201 79.1692 95.8601 76.6992 95.3201L78.5092 99.1601V99.1501Z'
                  fill='#D1D1D1'
                />
                <path
                  d='M104.1 86.8599C105.82 85.5799 107.93 84.7099 109.99 84.1699C114.85 82.8999 119.69 83.9699 123.76 86.8599C124.49 87.3699 125.19 86.1699 124.46 85.6599C120.23 82.6599 115.15 81.5399 110.09 82.7199C107.76 83.2599 105.32 84.2199 103.39 85.6599C102.68 86.1899 103.37 87.3999 104.09 86.8599H104.1Z'
                  fill='#DDDDDD'
                />
                <path
                  d='M112.07 11.1299C112.07 11.1299 117.92 8.8299 117.66 22.1599C117.66 22.1599 120.62 11.9499 125.98 13.0299C131.33 14.1199 122.13 25.1999 122.13 25.1999C122.13 25.1999 131.89 16.5999 135.71 21.1399C139.54 25.6799 124.9 29.4599 124.9 29.4599C124.9 29.4599 161.06 19.5299 177.69 48.1199C177.69 48.1199 158.47 23.4799 123.04 31.9999C123.04 31.9999 119.74 30.1699 124.46 28.4499C129.18 26.7299 134.9 23.9799 134.09 22.0099C133.28 20.0299 120.35 27.5399 120.35 27.5399C120.35 27.5399 126.29 16.8499 123.85 15.5699C121.42 14.2999 116.9 26.1699 116.9 26.1699C116.9 26.1699 117.94 12.6799 112.07 11.1299Z'
                  fill='#82B1D8'
                />
                <path
                  d='M62.7598 40.9499C62.7598 40.9499 77.1498 18.5699 110.96 29.4699C110.96 29.4699 81.8698 24.6399 62.7598 40.9499Z'
                  fill='#82B1D8'
                />
                <path
                  d='M58.3901 52.28C58.3901 52.28 43.8401 71.55 43.3301 90.57C43.3301 90.57 47.1801 72.04 58.3901 52.28Z'
                  fill='#82B1D8'
                />
              </svg>
            </div>
          </Stack>
        )}
      </Box>
    </Drawer>
  )
}

export default SidebarAddFlights
