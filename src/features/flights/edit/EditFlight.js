import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import { Avatar, Divider, Typography } from '@mui/material'

import Address from '../../../../public/images/cards/Location'

import City from '../../../../public/images/cards/city'
import Phone from '../../../../public/images/cards/Telephone'
import Delete from '../../../../public/images/cards/deleteWihte'
import Arrowleft from '../../../../public/images/cards/arrowleft'
import User from '../../../../public/images/cards/user'
import Password from '../../../../public/images/cards/password'
import { Stack } from '@mui/system'
import { useSelector } from 'react-redux'
import Edit from '../../../../public/images/cards/edit'
import DefaultPalette from 'src/@core/theme/palette'
import { useTheme } from '@emotion/react'
import ShowData from './ShowData'
import EditFromData from './EditFromData'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const SidebarEditFlight = props => {
  // ** Props
  const [isEditing, setIsEditing] = useState(false)
  const store = useSelector(state => state.user)

  const { open, handleCloseDrawer, handleDeleteClick, Editing, data, mode } = props

  const methods = useForm({
    defaultValues: {
      address_id: data.address_id,
      day: data.day,
      start_time: data.fix_start_time,
      end_time: data.fix_end_time,
      salesman_id: data?.salesman?.id,
      customerTimes: data.customer_times.map(times => ({ time: times.fix_arrival_time, id: times.customer_id }))
    },
    resolver: yupResolver(yup.Schema)
  })

  const { control, formState, getValues, setValue, watch } = methods

  const { errors } = formState

  // ** State

  const handleEditClick = () => {
    setIsEditing(true)
  }

  // const theme = useTheme()
  // const mode = theme.palette.mode

  const drawerColor = DefaultPalette('light', 'bordered').drawer

  return (
    <>
      {!isEditing ? (
        <>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
            PaperProps={{
              sx: {
                width: { xs: 300, sm: 400 },
                backgroundColor: drawerColor
              }
            }}
          >
            <Header>
              <Avatar
                sx={{
                  width: 64,
                  height: 64,
                  fontSize: '24px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.salesman?.image}
                alt='dlivia Sparks'
              >
                {data?.salesman?.name?.slice(0, 1).toUpperCase()}
              </Avatar>
              <Stack direction={'row'} alignItems={'center'} sx={{ gap: '24px' }}>
                <Box
                  className='hover-edit'
                  onClick={handleEditClick}
                  sx={{
                    p: '8px 12px',
                    borderRadius: 1,

                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                    display: 'flex',
                    alignItems: 'center',
                    gap: '4px',
                    cursor: 'pointer',
                    '&:hover': {
                      backgroundColor: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                    }
                  }}
                >
                  <Edit />
                  <Button
                    size='small'
                    sx={{
                      p: 0,
                      fontSize: '16px',
                      color: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                      '&:hover': {
                        backgroundColor: 'transparent'
                      }
                    }}
                  >
                    تعديل
                  </Button>
                </Box>

                <Box
                  onClick={handleDeleteClick}
                  sx={{
                    padding: '8px 12px',
                    borderRadius: '4px',
                    fontSize: '16px',
                    color: '#fff',

                    backgroundColor: '#CE3446',
                    display: 'flex',
                    alignItems: 'center',
                    cursor: 'pointer',
                    '&:hover': {
                      backgroundColor: '#CE3446'
                    }
                  }}
                >
                  <Delete />

                  <Button
                    size='small'
                    sx={{
                      fontSize: '16px',
                      p: 0,
                      color: '#fff',
                      '&:hover': {
                        backgroundColor: 'transparent',
                        color: '#fff'
                      }
                    }}
                  >
                    حذف
                  </Button>
                </Box>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />
            <ShowData data={data} />
          </Drawer>
        </>
      ) : (
        <EditFromData
          handleCloseDrawer={handleCloseDrawer}
          methods={methods}
          FormProvider={FormProvider}
          Header={Header}
          Controller={Controller}
          control={control}
          errors={errors}
          open={open}
          data={data}
          getValues={getValues}
        />
      )}

      {Editing ? (
        <EditFromData
          handleCloseDrawer={handleCloseDrawer}
          methods={methods}
          FormProvider={FormProvider}
          Header={Header}
          Controller={Controller}
          control={control}
          errors={errors}
          open={open}
          setValue={setValue}
          data={data}
          getValues={getValues}
        />
      ) : (
        ''
      )}
    </>
  )
}

export default SidebarEditFlight
