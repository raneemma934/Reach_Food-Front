import { Avatar, Divider, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React from 'react'
import Address from '../../../../public/images/cards/Location'

import City from '../../../../public/images/cards/city'
import Day from '../../../../public/images/cards/day'
import Clock from '../../../../public/images/cards/clock'

export default function ShowData({ data }) {
  const BranchDetails = JSON.parse(localStorage.getItem('branch'))

  return (
    <Stack padding={'0px 24px'} direction={'column'}>
      <Box>
        <Typography className='custom-style-label '>
          <City /> {BranchDetails?.data?.city?.country?.name + ' - ' + BranchDetails?.data?.city?.name}
        </Typography>
      </Box>

      <Typography className='custom-style-label'>
        {' '}
        <Address />
        المنطقة
      </Typography>

      <Typography
        sx={{
          fontWeight: '400',
          fontSize: '14px',
          color: theme => (theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light)
        }}
      >
        {data.address.area}
      </Typography>

      <Box>
        <Typography className='custom-style-label '>
          {' '}
          <Day />
          اليوم
        </Typography>
      </Box>

      <Typography
        sx={{
          fontWeight: '400',
          fontSize: '14px',
          color: theme => (theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light)
        }}
      >
        {data?.day}
      </Typography>

      <Typography className='custom-style-label '>
        {' '}
        <Clock />
        وقت الأنطلاق
      </Typography>

      <Typography
        sx={{
          fontWeight: '400',
          fontSize: '14px',
          color: theme => (theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light)
        }}
      >
        {data?.start_time}
      </Typography>

      <Typography className='custom-style-label '> المندوب</Typography>
      <Typography
        sx={{
          fontWeight: '400',
          fontSize: '14px',
          color: theme => (theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light)
        }}
      >
        {data?.salesman?.name}
      </Typography>

      <Divider />

      <Typography className='custom-style-label '>الزبائن ومواعيد التسليم</Typography>

      <Stack>
        {data?.customer_times.map((customer, i) => (
          <Box
            key={i}
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              marginTop: '16px',
              borderBottom: '1px solid rgba(100,100,100,0.1)',
              pb: '8px'
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
              <Avatar
                sx={{ bgcolor: 'rgba(115, 163, 208, 0.25)', width: 42, height: 42, borderRadius: '50%' }}
                src={''}
              >
                <img src='/images/icons/avatar.png' alt='' />
              </Avatar>
              <Box>
                <Typography className='custom-style-label2 '>{customer?.customer?.name}</Typography>
                <Typography
                  sx={{
                    fontSize: '14px',
                    fontWeight: '400',
                    lineHeight: '22px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {' '}
                  {customer?.customer?.customer_type}{' '}
                </Typography>
              </Box>
            </Box>
            <Box
              sx={{
                display: 'flex',
                padding: '4px 8px',
                justifyContent: 'center',
                alignItems: 'center',
                gap: '10px'
              }}
            >
              <Typography
                sx={{
                  textAlign: 'center',
                  fontSize: '14px',
                  fontWeight: '400'
                }}
              >
                {customer?.fix_arrival_time}
              </Typography>
            </Box>
          </Box>
        ))}
      </Stack>
    </Stack>
  )
}
