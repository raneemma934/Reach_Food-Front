import { Grid } from '@mui/material'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomCardsFlight from 'src/@core/components/custom-flight-cards'
import CustomCardsProducts from 'src/@core/components/custom-product-card'
import { fetchProducts } from 'src/store/apps/products'

export default function List({ handleSelectDelete, SelectDelete, data, type }) {
  return (
    <div>
      <Grid container spacing={6}>
        {data &&
          data?.map((client, index) => (
            <Grid item sm={4} xs={12} key={index}>
              <CustomCardsFlight
                type={type}
                handleSelectDelete={handleSelectDelete}
                SelectDelete={SelectDelete}
                client={client}
              />
            </Grid>
          ))}
      </Grid>
    </div>
  )
}
