import { Card, CardHeader } from '@mui/material'
import { Box } from '@mui/system'
import React, { useEffect, useState } from 'react'
import { DataGrid } from '@mui/x-data-grid'

import useFlightTrackingColumns from '../hook/useFlightTrackingColumns'
import CustomFlightTracking from 'src/@core/components/custom-flighttracking'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPriceProducts } from 'src/store/apps/products'
import { fetchTracing } from 'src/store/apps/trips'
import { useTheme } from '@emotion/react'
import CustomTable from 'src/@core/components/custom-table'

export default function TableFlightTracking() {
  const { tracer, status, error } = useSelector(state => state.trips)
  const { data, archivedDay } = useSelector(state => state.request)
  const [filterData, setFilterData] = useState([])

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchTracing(`start_date=${archivedDay}`))
  }, [dispatch, archivedDay])

  const rows = Array.isArray(filterData) ? filterData : []

  const columns = useFlightTrackingColumns()

  const theme = useTheme()

  return (
    <>
      <CustomFlightTracking setFilterData={setFilterData} data={tracer?.data} />
      <Card sx={{ marginTop: '23px' }}>
        <CustomTable
          isPrice={0}
          columns={columns}
          rows={rows}
          NoRowText={' لا يوجد رحلات في التاريخ المحدد الرجاء اختيار تاريخ أخر'}
        />
      </Card>
    </>
  )
}
