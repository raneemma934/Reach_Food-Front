import {
  Avatar,
  Button,
  Checkbox,
  Divider,
  Drawer,
  FormControlLabel,
  FormGroup,
  IconButton,
  InputAdornment,
  TextField,
  Typography
} from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useEffect, useMemo, useState } from 'react'
import { saveAs } from 'file-saver'
import styled from '@emotion/styled'
import CustomTextField from 'src/@core/components/mui/text-field'
import { Controller, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPermissions } from 'src/store/apps/user'
import { fetchTracing, fetchTracingUpload } from 'src/store/apps/trips'
import Search from '../../../../public/images/icons/Search'
import MyDocument from 'src/@core/components/pdf'
import { pdf } from '@react-pdf/renderer'
import MyDocumentTracing from 'src/@core/components/pdfTracing'
import DateLogo from 'public/images/cards/date'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: '24px',
  justifyContent: 'space-between',
  width: '100%',
  margin: '0px'
}))

export default function DrawerUpload({ open, handleCloseDrawer, setChecked, checked, setSearchTerm, searchTerm }) {
  const { upload, status, error } = useSelector(state => state.trips)

  const dispatch = useDispatch()
  const [selectedDate, setSelectedDate] = useState()

  const handleChange = (event, category) => {
    const isChecked = event.target.checked

    if (isChecked) {
      setChecked(prevChecked => [...prevChecked, category])
    } else {
      setChecked(prevChecked => prevChecked.filter(item => item !== category))
    }
  }

  const handleSelectAll = event => {
    const isChecked = event.target.checked

    if (isChecked) {
      const allSalesmenIds = upload.data.map(category => category)
      setChecked(allSalesmenIds)
    } else {
      setChecked([])
    }
  }

  const handleDownload = async () => {
    try {
      const doc = <MyDocumentTracing data={checked} />
      const asPdf = pdf([])
      asPdf.updateContainer(doc)
      const blob = await asPdf.toBlob()
      saveAs(blob, 'my_document.pdf')
      handleCloseDrawer()
    } catch (error) {
      alert('some thing error')
    }
  }

  const isAllChecked = checked.length === upload?.data?.length

  const filteredData = upload?.data?.filter(category => category.trip.salesman.name.toLowerCase().includes(searchTerm))

  useMemo(() => {
    dispatch(fetchTracingUpload(`start_date=${selectedDate}`))
  }, [dispatch, selectedDate])

  return (
    <Drawer
      open={open}
      anchor='right'
      variant='temporary'
      onClose={handleCloseDrawer}
      ModalProps={{ keepMounted: true }}
      sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      PaperProps={{
        sx: {
          width: { xs: 300, sm: 400 }

          // backgroundColor: drawerColor
        }
      }}
    >
      <Header>
        <Typography
          sx={{
            color: '#3B3B3B',
            fontSize: '16px',
            fontWeight: '500',
            lineHeight: '32px'
          }}
        >
          تصدير التقرير
        </Typography>
        <Box sx={{ display: 'flex', gap: '24px', marginLeft: '59px' }}>
          <Button
            sx={{
              lineHeight: 'normal',
              display: 'flex',
              padding: '8px 24px',
              alignItems: 'center',
              gap: '8px',
              borderRadius: '8px',
              background: '#73A3D0',
              color: '#F2F4F8',
              '&:hover': {
                color: '#F2F4F8',
                backgroundColor: '#73A3D0'
              }
            }}
            onClick={handleDownload}
          >
            تصدير
          </Button>
          <Button
            onClick={handleCloseDrawer}
            sx={{
              display: 'flex',
              padding: '8px 24px',
              alignItems: 'center',
              gap: '8px',
              borderRadius: '8px',
              background: '#E0E0E0',

              '&:hover': {
                backgroundColor: '#E0E0E0'
              }
            }}
          >
            إلغاء
          </Button>
        </Box>
        <Divider sx={{ marginY: '4px' }} />
      </Header>
      <Stack padding={4} sx={{ gap: '8px' }}>
        <Typography className='custom-style-label '>
          {' '}
          <DateLogo /> التاريخ
        </Typography>

        <CustomTextField
          type='date'
          fullWidth
          value={selectedDate}
          sx={{ mb: 4 }}
          onChange={e => setSelectedDate(e.target.value)}
          placeholder='تحديد التاريخ للحصول على تقرير بطلباته'
        />

        {filteredData?.length === 0 ? (
          ''
        ) : (
          <Typography sx={{ fontSize: '13px', fontWeight: '500', lineHeight: '25px' }}>
            <Checkbox size='small' onChange={handleSelectAll} checked={isAllChecked} />
            تحديد الكل
          </Typography>
        )}

        <Stack direction={'column'} justifyContent={'space-between'} width={'100%'} sx={{ gap: '12px' }}>
          {filteredData?.map((category, i) => (
            <Stack key={i} direction={'row'} alignItems={'center'} justifyContent={'space-between'}>
              <Stack direction='row' alignItems='center' sx={{ gap: '12px' }}>
                <Avatar
                  src={process.env.NEXT_PUBLIC_IMAGES + '/' + category?.trip?.salesman?.image}
                  alt={category?.trip?.salesman?.name || 'Salesman'}
                />
                <Typography sx={{ fontWeight: '500', fontSize: '16px', color: '#73A3D0' }}>
                  {category?.trip?.salesman?.name}
                </Typography>
              </Stack>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Checkbox
                      onChange={event => handleChange(event, category)}
                      checked={checked.includes(category)}
                      size='small'
                    />
                  }
                />
              </FormGroup>
            </Stack>
          ))}
        </Stack>
      </Stack>
    </Drawer>
  )
}
