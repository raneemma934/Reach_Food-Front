import { Avatar, Typography } from '@mui/material'
import FakeProducts from '../../../../public/images/cards/fakeproducts'
import { Box, Stack } from '@mui/system'
import moment from 'moment'
import 'moment/locale/ar'
import CustomAvatar from 'src/@core/components/mui/avatar'
import { formatDateAr, formatTimeAr } from 'src/@core/utils/FormatTimeAr'

const { useMemo } = require('react')

const daysOfWeek = {
  Sunday: 'الأحد',
  Monday: 'الاثنين',
  Tuesday: 'الثلاثاء',
  Wednesday: 'الأربعاء',
  Thursday: 'الخميس',
  Friday: 'الجمعة',
  Saturday: 'السبت'
}

const useFlightTrackingColumns = () => {
  moment.locale('ar')

  return useMemo(
    () => [
      {
        flex: 0.6,
        field: 'id',
        headerName: 'المندوب',

        renderCell: params => {
          return (
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Avatar
                sx={{
                  width: 52,
                  height: 52,
                  mr: 3,
                  fontSize: '24px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + params.row.trip?.salesman?.image}
                alt='dlivia Sparks'
              >
                {params.row.trip?.salesman?.name?.slice(0, 1).toUpperCase()}
              </Avatar>
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                {' '}
                <Typography
                  sx={{
                    fontSize: '16px',
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 'normal',
                    '@media (max-width: 600px)': {
                      fontSize: '12px'
                    }
                  }}
                >
                  {params.row.trip?.salesman?.name}
                </Typography>
              </Box>
            </Box>
          )
        }
      },
      {
        flex: 1,
        field: 'full_name',
        align: 'center',
        headerAlign: 'center',
        headerName: 'يوم وتاريخ الرحلة',

        renderCell: params => {
          // const date = params.row.trip?.day
          // const Day = date ? daysOfWeek[date] : ''

          return (
            <Stack direction={'row'} alignItems={'center'} spacing={4}>
              <Box>
                <Typography
                  sx={{
                    fontSize: '16px',
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 'normal',
                    '@media (max-width: 600px)': {
                      fontSize: '12px'
                    }
                  }}
                >
                  {formatDateAr(params.row.start_date)}
                  {/* {Day} - {params.row.start_date} */}
                </Typography>
              </Box>
            </Stack>
          )
        }
      },
      {
        flex: 1,
        field: 'email',
        headerName: 'الوقت',
        align: 'center',
        headerAlign: 'center',

        renderCell: params => {
          return (
            <Stack direction={'row'} alignItems={'center'} spacing={4}>
              <Box>
                <Typography
                  sx={{
                    fontSize: '16px',
                    fontStyle: 'normal',
                    fontWeight: '500',
                    lineHeight: 'normal',
                    '@media (max-width: 600px)': {
                      fontSize: '12px'
                    }
                  }}
                >
                  {formatTimeAr(params.row.start_time)}
                </Typography>
              </Box>
            </Stack>
          )
        }
      },
      {
        flex: 1.5,
        field: 'س',
        headerName: 'الحالة',
        align: 'right',
        headerAlign: 'right',

        renderCell: params => {
          return (
            <Box
              sx={{
                backgroundColor:
                  params?.row?.trip_trace?.status === 'stop'
                    ? 'rgba(223, 46, 56, 0.10)'
                    : Boolean(params?.row?.trip_trace?.status?.includes('stop'))
                    ? 'rgba(106, 178, 223, 0.10)'
                    : params?.row?.trip_trace?.status === 'start'
                    ? 'rgba(106, 178, 223, 0.10)'
                    : params?.row?.trip_trace?.status === 'pause'
                    ? '#E0E0E0'
                    : params?.row?.trip_trace?.status === 'resume'
                    ? 'transparent'
                    : '',
                borderRadius: '4px',
                padding: '8px 16px'
              }}
            >
              {' '}
              <Typography
                sx={{
                  fontSize: '14px',
                  fontStyle: 'normal',
                  fontWeight: 500,
                  lineHeight: '130%',
                  color:
                    params?.row?.trip_trace?.status === 'stop'
                      ? '#DF2E38'
                      : Boolean(params?.row?.trip_trace?.status?.includes('Late'))
                      ? '#6AB2DF'
                      : params?.row?.trip_trace?.status === 'start'
                      ? '#6AB2DF'
                      : params?.row?.trip_trace?.status === 'pause'
                      ? '#5A5A5A'
                      : params?.row?.trip_trace?.status === 'resume'
                      ? '#5A5A5A'
                      : ''
                }}
              >
                {params?.row?.trip_trace?.status === 'start'
                  ? 'بدء الرحلة'
                  : params?.row?.trip_trace?.status === 'stop'
                  ? 'إنهاء الرحلة'
                  : params?.row?.trip_trace?.status === 'resume'
                  ? 'استئناف'
                  : params?.row?.trip_trace?.status === 'pause'
                  ? 'توقف'
                  : ''}
              </Typography>
            </Box>
          )
        }
      }
    ],
    []
  )
}

export default useFlightTrackingColumns
