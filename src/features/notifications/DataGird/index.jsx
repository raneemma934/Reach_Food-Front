import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Card, Box } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
import useNotificationsColumns from '../hook/useNotificationsColumns'
import CustomTapsNotifications from 'src/@core/components/custom-TabNotifications'
import { fetchNotification } from 'src/store/apps/notefication'
import CustomTable from 'src/@core/components/custom-table'

const TableNotifications = () => {
  const dispatch = useDispatch()
  const { notification, status, error } = useSelector(state => state.notification)

  const [filterData, setFilterData] = useState([])

  useEffect(() => {
    dispatch(fetchNotification())
  }, [dispatch])

  const rows = Array.isArray(filterData) ? filterData : []
  const columns = useNotificationsColumns()

  return (
    <>
      <CustomTapsNotifications setFilterData={setFilterData} data={notification} />
      <Card sx={{ marginTop: '23px' }}>
        <CustomTable isPrice={0} columns={columns} rows={rows} NoRowText={'لا يوجد اشعارات'} />
      </Card>
    </>
  )
}

export default TableNotifications
