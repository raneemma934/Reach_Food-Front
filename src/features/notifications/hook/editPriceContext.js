import React, { createContext, useContext, useState } from 'react'

const MyContext = createContext()

export const EditPriceList = ({ children }) => {
  const [Edit, setEdit] = useState(false)

  return <MyContext.Provider value={{ Edit, setEdit }}>{children}</MyContext.Provider>
}

export const useMyContext = () => {
  return useContext(MyContext)
}
