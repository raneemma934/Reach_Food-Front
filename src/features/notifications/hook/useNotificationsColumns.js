import React, { useCallback, useMemo } from 'react'
import { Avatar, Box, Button, Typography } from '@mui/material'
import CustomAvatar from 'src/@core/components/mui/avatar'
import { useDispatch } from 'react-redux'
import { RollBackNotification } from 'src/store/apps/notefication'

const useNotificationsColumns = () => {
  const dispatch = useDispatch()

  const handelClick = useCallback(
    id => {
      dispatch(RollBackNotification(id))
    },
    [dispatch]
  )

  return useMemo(
    () => [
      {
        flex: 1.2,
        field: 'name',
        headerName: 'المستخدم',

        // renderHeader: () => <Typography className='custom-style-columns'>المستخدم</Typography>,
        renderCell: params => {
          return (
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
              <Avatar
                sx={{
                  width: 52,
                  height: 52,
                  mr: 3,
                  fontSize: '24px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + params.row?.user?.image}
                alt='dlivia Sparks'
              >
                {params.row?.user?.name.slice(0, 1).toUpperCase()}
              </Avatar>
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                {' '}
                <Typography
                  sx={{
                    fontSize: '16px',
                    fontStyle: 'normal',
                    fontWeight: '500',

                    '@media (max-width: 600px)': {
                      fontSize: '12px'
                    }
                  }}
                >
                  {params.row.user.name}
                </Typography>
                <Typography
                  sx={{
                    fontSize: '14px',
                    fontStyle: 'normal',
                    fontWeight: '400',

                    '@media (max-width: 600px)': {
                      fontSize: '12px'
                    }
                  }}
                >
                  {params.row.user.role}
                </Typography>
              </Box>
            </Box>
          )
        }
      },

      {
        flex: 1,
        field: 'type',
        headerName: 'نوع الاشعار',

        // headerName: <Typography className='custom-style-columns'>نوع الاشعار</Typography>,
        renderCell: params => <Typography>{params.row.type}</Typography>
      },
      {
        flex: 2.5,
        field: 'content',
        headerName: 'محتوى الاشعار',

        // headerName: <Typography className='custom-style-columns'>محتوى الاشعار</Typography>,
        renderCell: params => <Typography>{params.row.content}</Typography>
      },
      {
        flex: 1,
        field: 'date',
        headerName: 'تاريخ الاشعار',

        // headerName: <Typography className='custom-style-columns'>تاريخ الاشعار</Typography>,
        renderCell: params => <Typography>{params.row.date}</Typography>
      },
      {
        flex: 0.5,
        field: 'there_back',
        align: 'center',
        headerAlign: 'center',
        headerName: 'خيارات',

        // headerName: <Typography className='custom-style-columns'>خيارات</Typography>,
        renderCell: params =>
          params.row.there_back ? (
            <Button
              variant='text'
              sx={{
                textAlign: 'center',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                fontSize: '14px',
                marginRight: '15px',

                '&:hover': { backgroundColor: 'transparent', color: 'red' }
              }}
              onClick={() => handelClick(params.row.id)}
            >
              تراجع
            </Button>
          ) : null
      }
    ],
    [handelClick]
  )
}

export default useNotificationsColumns
