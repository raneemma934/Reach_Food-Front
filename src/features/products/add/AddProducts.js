import { useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { Controller, useFormContext, FormProvider } from 'react-hook-form'
import { Autocomplete, Avatar, Divider, Typography } from '@mui/material'
import { Stack } from '@mui/system'
import { useDispatch, useSelector } from 'react-redux'
import { addProducts } from 'src/store/apps/products'
import EditPhoto from '../../../../public/images/cards/editphoto'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const SidebarAddProducts = props => {
  // ** Props
  const { open, handleCloseDrawer } = props
  const [image, setImage] = useState(null)
  const dispatch = useDispatch()

  const methods = useFormContext()

  const { control, formState, getValues, setValue } = methods
  const { errors } = formState

  const handleSaveData = () => {
    dispatch(addProducts(getValues()))
    handleCloseDrawer()
  }

  const handleImageChange = event => {
    const file = event.target.files[0]
    if (file) {
      const reader = new FileReader()
      reader.onload = e => {
        setImage(e.target.result)
      }
      setValue('image', file)
      reader.readAsDataURL(file)
    }
  }

  return (
    <FormProvider {...methods}>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header sx={{ alignItems: 'start', p: '24px' }}>
          <Stack direction={'column'} alignItems={'center'}>
            <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.15)', width: 120, height: 120, borderRadius: '4px' }}>
              {image ? (
                <img src={image} alt='' width={'100%'} />
              ) : (
                <img src='/images/icons/avatar2.png' alt='' width={'20%'} />
              )}
            </Avatar>

            <Stack
              sx={{
                '&:hover': {
                  color: '#73A3D0'
                }
              }}
              direction={'row'}
              alignItems={'center'}
            >
              <Box sx={{ p: ' 12px 0', display: 'flex', alignItems: 'center' }}>
                <EditPhoto />
              </Box>
              <Box>
                <Typography
                  component='label'
                  sx={{
                    marginLeft: 2,
                    cursor: 'pointer',

                    fontWeight: '500',
                    fontSize: '16px',
                    lineHeight: '25px',
                    fontFamily: 'Noto Kufi Arabic',

                    '&:hover': {
                      color: '#73A3D0'
                    }
                  }}
                >
                  إضافة صورة
                  <input type='file' accept='image/*' onChange={handleImageChange} hidden />
                </Typography>
              </Box>
            </Stack>
          </Stack>
          <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
            <Button
              size='small'
              onClick={handleSaveData}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                color: '#ffff',
                fontSize: '16px',

                backgroundColor: '#73A3D0',
                '&:hover': {
                  backgroundColor: '#73A3D0'
                }
              }}
            >
              إضافة
            </Button>
            <Button
              size='small'
              onClick={handleCloseDrawer}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                fontSize: '16px',
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.gray.dark : theme.palette.gray.light,
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,

                '&:hover': {
                  backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                }
              }}
            >
              إلغاء
            </Button>
          </Stack>
        </Header>
        <Divider />
        <Box sx={{ p: '24px' }}>
          <Controller
            name='name'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                sx={{ mb: 4 }}
                onChange={onChange}
                label='اسم المنتج'
                placeholder='اسم المنتج'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />
          <Controller
            name='amount_unit'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <Autocomplete
                options={[
                  { value: 'kg', label: 'كيلو غرام' },
                  { value: 'piece', label: 'قطعة' }
                ]}
                fullWidth
                id='autocomplete-amount-unit'
                getOptionLabel={option => option.label}
                value={value ? { value, label: value === 'kg' ? 'كيلو غرام' : 'قطعة' } : null}
                onChange={(event, newValue) => {
                  onChange(newValue ? newValue.value : '')
                }}
                renderInput={params => (
                  <CustomTextField
                    {...params}
                    fullWidth
                    sx={{ mb: 4 }}
                    placeholder='وحدة البيع'
                    label='وحدة البيع'
                    id='validation-billing-select'
                    aria-describedby='validation-billing-select'
                    error={Boolean(errors.amount_unit)}
                    helperText={errors.amount_unit?.message || ''}
                  />
                )}
              />
            )}
          />

          <Controller
            name='wholesale_price'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                sx={{ mb: 4 }}
                onChange={onChange}
                label=' السعر للجملة'
                placeholder=' السعر للجملة'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />
          <Controller
            name='retail_price'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                sx={{ mb: 4 }}
                onChange={onChange}
                label=' السعر للمفرق'
                placeholder=' السعر للمفرق'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />
          <Divider sx={{ marginY: '14px' }} />

          <Controller
            name='description'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                multiline
                rows={4}
                sx={{ mb: 4 }}
                onChange={onChange}
                label='  وصف المنتج'
                placeholder='  وصف المنتج'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />
        </Box>
      </Drawer>
    </FormProvider>
  )
}

export default SidebarAddProducts
