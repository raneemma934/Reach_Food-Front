import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import { Autocomplete, Avatar, Divider, Typography } from '@mui/material'
import Delete from '../../../../public/images/cards/deleteWihte'
import DeleteRed from '../../../../public/images/cards/deleteRed'
import Arrowleft from '../../../../public/images/cards/arrowleft'
import EditPhoto from '../../../../public/images/cards/editphoto'

import { Stack } from '@mui/system'
import { editProducts } from 'src/store/apps/products'
import { useDispatch } from 'react-redux'
import Edit from '../../../../public/images/cards/edit'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const AvatarWrapper = styled(Box)(({ theme }) => ({
  position: 'relative',
  '&:hover .delete-btn': {
    display: 'block'
  }
}))

const DeleteButton = styled(Box)(({ theme }) => ({
  position: 'absolute',
  top: '1px',
  right: '0px',
  display: 'none',
  zIndex: 999,
  cursor: 'pointer',
  backgroundColor: '#F5E5E7',
  width: '17px',
  height: '17px'
}))

const SidebarEditProducts = props => {
  // ** Props
  const { open, handleCloseDrawer, handleDeleteClick, Editing, data } = props
  const [isEditing, setIsEditing] = useState(false)
  const [image, setImage] = useState(process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image)
  const dispatch = useDispatch()

  const methods = useForm({
    defaultValues: {
      name: data?.name,
      amount_unit: data?.amount_unit,
      wholesale_price: data?.wholesale_price,
      retail_price: data?.retail_price,
      description: data?.description
    },
    resolver: yupResolver(yup.Schema)
  })
  const { control, formState, getValues, setValue } = methods

  const { errors } = formState

  const handleSaveData = () => {
    setValue('id', data.id)
    dispatch(editProducts(getValues()))
    handleCloseDrawer()
  }

  const handleEditClick = () => {
    setIsEditing(true)
  }

  const handleImageChange = event => {
    const file = event.target.files[0]
    if (file) {
      const reader = new FileReader()
      reader.onload = e => {
        setImage(e.target.result)
      }
      setValue('image', file)
      reader.readAsDataURL(file)
    }
  }

  const handleDeleteImage = () => {
    setImage(null)
    setValue('image', null)
  }
  console.log(data)

  return (
    <>
      {!isEditing ? (
        <>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } }, color: '#E0E0E0' }}
          >
            <Header sx={{ alignItems: 'start', p: '24px' }}>
              <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.15)', width: 120, height: 120, borderRadius: '4px' }}>
                {data?.image ? (
                  <img src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image} alt='' width={'100%'} />
                ) : (
                  <img src='/images/icons/avatar2.png' alt='' width={'20%'} />
                )}
              </Avatar>

              <Stack direction={'row'} alignItems={'center'} spacing={3}>
                <Box
                  className='hover-edit'
                  onClick={handleEditClick}
                  sx={{
                    p: '8px 12px',
                    borderRadius: '8px',
                    gap: '8px',
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                    display: 'flex',
                    alignItems: 'center',
                    cursor: 'pointer',
                    '&:hover': {
                      backgroundColor: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                    }
                  }}
                >
                  <Edit />
                  <Button
                    size='small'
                    sx={{
                      fontSize: '16px',
                      color: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light,

                      p: 0,
                      '&:hover': {
                        backgroundColor: 'transparent',
                        color: '#5A5A5A'
                      }
                    }}
                  >
                    تعديل
                  </Button>
                </Box>

                <Box
                  onClick={handleDeleteClick}
                  sx={{
                    p: '8px 12px',
                    borderRadius: '8px',
                    fontSize: '16px',
                    color: '#fff',
                    backgroundColor: '#CE3446',
                    display: 'flex',
                    alignItems: 'center',
                    cursor: 'pointer',
                    gap: '4px',
                    '&:hover': {
                      backgroundColor: '#CE3446'
                    }
                  }}
                >
                  <Delete />

                  <Button
                    size='small'
                    sx={{
                      fontSize: '16px',
                      color: '#fff',
                      p: 0,
                      '&:hover': {
                        backgroundColor: 'transparent',
                        color: '#fff'
                      }
                    }}
                  >
                    حذف
                  </Button>
                </Box>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />
            <Stack padding={'0 24px 24px 24px '} direction={'column'}>
              <Typography sx={{ mt: 0 }} className='custom-style-label '>
                اسم المنتج
              </Typography>
              <Typography sx={{ fontSize: '14px', fontWeight: '400', lineHeight: '25px' }}> {data?.name}</Typography>

              <Typography className='custom-style-label '>واحدة البيع</Typography>
              <Typography sx={{ fontSize: '14px', fontWeight: '400', lineHeight: '25px' }}>
                {' '}
                {data?.amount_unit}
              </Typography>

              <Typography className='custom-style-label '>السعر للجملة </Typography>
              <Typography sx={{ fontSize: '14px', fontWeight: '400', lineHeight: '25px' }}>
                {' '}
                {data?.wholesale_price} ل.س{' '}
              </Typography>

              <Typography className='custom-style-label '> السعر للمفرق </Typography>
              <Typography sx={{ fontSize: '14px', fontWeight: '400', lineHeight: '25px' }}>
                {data?.retail_price} ل.س{' '}
              </Typography>

              <Divider sx={{ marginY: '4px' }} />

              <Typography className='custom-style-label '> الوصف </Typography>
              <Typography sx={{ fontSize: '14px', fontWeight: '400', lineHeight: '25px' }}>
                {' '}
                {data?.description}{' '}
              </Typography>
            </Stack>
          </Drawer>
        </>
      ) : (
        <FormProvider {...methods}>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
          >
            <Header sx={{ alignItems: 'start', p: '24px' }}>
              <Stack direction={'column'} alignItems={'center'} spacing={2}>
                <AvatarWrapper>
                  <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.15)', width: 120, height: 120, borderRadius: '4px' }}>
                    {image ? (
                      <img src={image} alt='' width={'100%'} />
                    ) : (
                      <img src='/images/icons/avatar2.png' alt='' width={'20%'} />
                    )}
                  </Avatar>
                  <DeleteButton className='delete-btn' onClick={handleDeleteImage}>
                    <DeleteRed />
                  </DeleteButton>
                </AvatarWrapper>

                <Typography
                  component='label'
                  sx={{
                    marginLeft: 2,
                    cursor: 'pointer',

                    fontWeight: '500',
                    fontSize: '14px',
                    lineHeight: '25px',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '4px',
                    '&:hover': {
                      color: '#73A3D0'
                    }
                  }}
                >
                  <EditPhoto />{' '}
                  <Typography
                    sx={{
                      '&:hover': {
                        color: '#73A3D0'
                      }
                    }}
                  >
                    تعديل الصورة
                  </Typography>
                  <input type='file' accept='image/*' onChange={handleImageChange} hidden />
                </Typography>
              </Stack>
              <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
                <Button
                  size='small'
                  onClick={handleSaveData}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    color: '#ffff',
                    fontSize: '16px',
                    gap: '8px',
                    backgroundColor: '#73A3D0',
                    '&:hover': {
                      backgroundColor: '#73A3D0'
                    }
                  }}
                >
                  تعديل{' '}
                </Button>
                <Button
                  size='small'
                  onClick={handleCloseDrawer}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    fontSize: '16px',

                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light,
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                    '&:hover': {
                      backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                    }
                  }}
                >
                  إلغاء
                </Button>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />
            <Box sx={{ p: theme => theme.spacing(0, 6, 6) }}>
              <Controller
                name='name'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    label='اسم المنتج'
                    placeholder='اسم المنتج'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
              <Controller
                name='amount_unit'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <Autocomplete
                    options={[
                      { value: 'kg', label: 'كيلو غرام' },
                      { value: 'piece', label: 'قطعة' }
                    ]}
                    fullWidth
                    id='autocomplete-amount-unit'
                    getOptionLabel={option => option.label}
                    value={value ? { value, label: value === 'kg' ? 'كيلو غرام' : 'قطعة' } : ''}
                    onChange={(event, newValue) => {
                      onChange(newValue ? newValue.value : '')
                    }}
                    renderInput={params => (
                      <CustomTextField
                        {...params}
                        fullWidth
                        sx={{ mb: 4 }}
                        placeholder='وحدة البيع'
                        label='وحدة البيع'
                        id='validation-billing-select'
                        aria-describedby='validation-billing-select'
                        error={Boolean(errors.amount_unit)}
                        helperText={errors.amount_unit?.message || ''}
                      />
                    )}
                  />
                )}
              />

              <Controller
                name='wholesale_price'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    label=' السعر للجملة'
                    placeholder=' السعر للجملة'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
              <Controller
                name='retail_price'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    label=' السعر للمفرق'
                    placeholder=' السعر للمفرق'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
              <Divider sx={{ marginY: '14px' }} />

              <Controller
                name='description'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    multiline
                    rows={4}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    label='  وصف المنتج'
                    placeholder='  وصف المنتج'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
            </Box>
          </Drawer>
        </FormProvider>
      )}

      {Editing ? (
        <FormProvider {...methods}>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
          >
            <Header sx={{ alignItems: 'start', p: '24px' }}>
              {' '}
              <Stack direction={'column'} alignItems={'center'} spacing={2}>
                <AvatarWrapper>
                  <Avatar sx={{ bgcolor: 'rgba(115, 163, 208, 0.15)', width: 120, height: 120, borderRadius: '4px' }}>
                    {image ? (
                      <img src={image} alt='' width={'100%'} />
                    ) : (
                      <img src='/images/icons/avatar2.png' alt='' width={'20%'} />
                    )}
                  </Avatar>
                  <DeleteButton className='delete-btn' onClick={handleDeleteImage}>
                    <DeleteRed />
                  </DeleteButton>
                </AvatarWrapper>

                <Typography
                  component='label'
                  sx={{
                    marginLeft: 2,
                    cursor: 'pointer',

                    fontWeight: '500',
                    fontSize: '14px',
                    lineHeight: '25px',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '4px',
                    '&:hover': {
                      color: '#73A3D0'
                    }
                  }}
                >
                  <EditPhoto />{' '}
                  <Typography
                    sx={{
                      '&:hover': {
                        color: '#73A3D0'
                      }
                    }}
                  >
                    تعديل الصورة
                  </Typography>
                  <input type='file' accept='image/*' onChange={handleImageChange} hidden />
                </Typography>
              </Stack>
              <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
                <Button
                  size='small'
                  onClick={handleSaveData}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    color: '#ffff',
                    fontSize: '16px',
                    gap: '8px',
                    backgroundColor: '#73A3D0',
                    '&:hover': {
                      backgroundColor: '#73A3D0'
                    }
                  }}
                >
                  تعديل{' '}
                </Button>
                <Button
                  size='small'
                  onClick={handleCloseDrawer}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    fontSize: '16px',
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light,

                    '&:hover': {
                      backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                    }
                  }}
                >
                  إلغاء
                </Button>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />
            <Box sx={{ p: theme => theme.spacing(0, 6, 6) }}>
              <Controller
                name='name'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    label='اسم المنتج'
                    placeholder='اسم المنتج'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
              <Controller
                name='amount_unit'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <Autocomplete
                    options={[
                      { value: 'kg', label: 'كيلو غرام' },
                      { value: 'piece', label: 'قطعة' }
                    ]}
                    fullWidth
                    id='autocomplete-amount-unit'
                    getOptionLabel={option => option.label}
                    value={value ? { value, label: value === 'kg' ? 'كيلو غرام' : 'قطعة' } : ''}
                    onChange={(event, newValue) => {
                      onChange(newValue ? newValue.value : '')
                    }}
                    renderInput={params => (
                      <CustomTextField
                        {...params}
                        fullWidth
                        sx={{ mb: 4 }}
                        placeholder='وحدة البيع'
                        label='وحدة البيع'
                        id='validation-billing-select'
                        aria-describedby='validation-billing-select'
                        error={Boolean(errors.amount_unit)}
                        helperText={errors.amount_unit?.message || ''}
                      />
                    )}
                  />
                )}
              />

              <Controller
                name='wholesale_price'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    label=' السعر للجملة'
                    placeholder=' السعر للجملة'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
              <Controller
                name='retail_price'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    label=' السعر للمفرق'
                    placeholder=' السعر للمفرق'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
              <Divider sx={{ marginY: '14px' }} />

              <Controller
                name='description'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    multiline
                    rows={4}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    label='  وصف المنتج'
                    placeholder='  وصف المنتج'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
            </Box>
          </Drawer>
        </FormProvider>
      ) : (
        ''
      )}
    </>
  )
}

export default SidebarEditProducts
