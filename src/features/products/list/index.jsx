import { Grid } from '@mui/material'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomCardsProducts from 'src/@core/components/custom-product-card'
import { fetchProducts } from 'src/store/apps/products'

export default function List({ data }) {
  console.log('🚀 ~ List ~ data:', data)

  return (
    <div>
      <Grid container spacing={6}>
        {data &&
          data?.map((client, index) => (
            <Grid item sm={4} xs={12} key={index}>
              <CustomCardsProducts client={client} />
            </Grid>
          ))}
      </Grid>
    </div>
  )
}
