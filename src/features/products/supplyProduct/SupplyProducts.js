import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { Controller, useFormContext, FormProvider } from 'react-hook-form'
import { Autocomplete, Avatar, Divider, Typography } from '@mui/material'
import { Stack } from '@mui/system'
import { useDispatch, useSelector } from 'react-redux'
import { addProducts } from 'src/store/apps/products'
import EditPhoto from '../../../../public/images/cards/editphoto'
import Arrowleft from 'public/images/cards/arrowleft'
import { fetchAllCountry } from 'src/store/apps/branches'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const SidebarSupplyProducts = props => {
  // ** Props
  const { open, handleCloseDrawer } = props
  const [image, setImage] = useState(null)
  const dispatch = useDispatch()
  const AllCountry = useSelector(state => state.branches)
  console.log('🚀 ~ SidebarSupplyProducts ~ AllCountry:', AllCountry)
  const methods = useFormContext()

  const { control, formState, getValues, setValue } = methods
  const { errors } = formState

  const handleSaveData = () => {
    dispatch(addProducts(getValues()))
    handleCloseDrawer()
  }

  useEffect(() => {
    dispatch(fetchAllCountry())
  }, [dispatch])

  const handleImageChange = event => {
    const file = event.target.files[0]
    if (file) {
      const reader = new FileReader()
      reader.onload = e => {
        setImage(e.target.result)
      }
      setValue('image', file)
      reader.readAsDataURL(file)
    }
  }

  return (
    <FormProvider {...methods}>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header sx={{ alignItems: 'center', p: '24px' }}>
          <Typography sx={{ fontSize: '20px', fontWeight: '500' }}>توريد المنتجات</Typography>
          <Stack direction={'row'} gap={'24px'} alignItems={'center'}>
            <Button
              size='small'
              onClick={handleSaveData}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                color: '#ffff',
                fontSize: '16px',

                backgroundColor: '#73A3D0',
                '&:hover': {
                  backgroundColor: '#73A3D0'
                }
              }}
            >
              توريد
            </Button>
            <Button
              size='small'
              onClick={handleCloseDrawer}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                fontSize: '16px',
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.light,
                '&:hover': {
                  backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                }
              }}
            >
              إلغاء
            </Button>
          </Stack>
        </Header>
        <Divider />
        <Box sx={{ p: '24px' }}>
          <Box>
            <Typography className='custom-style-label3'>
              <Arrowleft /> الفرع
            </Typography>
            <Controller
              name='city_id'
              control={control}
              render={({ field: { value, onChange } }) => (
                <Autocomplete
                  options={AllCountry?.AllCountry?.data || []}
                  fullWidth
                  id='autocomplete-city-select'
                  getOptionLabel={option => ` ${option.name}`}
                  value={AllCountry?.AllCountry?.data?.find(city => city.id === value) || null}
                  onChange={(event, newValue) => {
                    onChange(newValue ? newValue.id : '')
                  }}
                  renderInput={params => (
                    <CustomTextField
                      {...params}
                      placeholder='الفرع المسؤول عنه'
                      fullWidth
                      id='validation-billing-select'
                      aria-describedby='validation-billing-select'
                      error={Boolean(errors.city_id)}
                      helperText={errors.city_id?.message || ''}
                    />
                  )}
                />
              )}
            />
          </Box>
          <Box sx={{ paddingY: '48px', gap: '32px', display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
            <Typography sx={{ fontSize: '14px', fontWeight: '400', textAlign: 'center', alignItems: 'center' }}>
              قم بتحديد فرع ليتم عرض المنتجات الموجودة ضمنه وتوريدها مع كافة بياناتها إلى الفرع الحالي
            </Typography>
            <img width={'240px'} src='/images/icons/supplyProduct.svg' alt='supplyProduct' />
          </Box>
        </Box>
      </Drawer>
    </FormProvider>
  )
}

export default SidebarSupplyProducts
