import { Grid } from '@mui/material'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomRequestsCards from 'src/@core/components/custom-request-cards'
import EmptyData from 'src/@core/components/empty-data'
import { fetchRequests } from 'src/store/apps/requests'

export default function List({ data }) {
  return (
    <>
      {data && data?.length > 0 ? (
        <Grid container spacing={6}>
          {data.length > 0 &&
            data.map((client, index) => (
              <Grid item sm={4} xs={12} key={index}>
                <CustomRequestsCards client={client} />
              </Grid>
            ))}
        </Grid>
      ) : (
        <EmptyData desc={'لا يوجد طلبات  حالية لعرضها'} />
      )}
    </>
  )
}
