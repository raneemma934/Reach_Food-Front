import { Avatar, Button, Card, Grid, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useState } from 'react'
import CustomRequestsTabs from 'src/@core/components/custom-request-tab'
import DateRequest from '../../../../public/images/cards/daterequest'
import DeliveryTime from '../../../../public/images/cards/timedelivery'
import TotalPrice from '../../../../public/images/cards/totalprice'
import ArrowDown from '../../../../public/images/cards/arrowdown'
import ArrowUp from '../../../../public/images/cards/arrowup'
import CustomCardsDetailsProducts from 'src/@core/components/custom-detalisProducts-card'
import moment from 'moment'

export default function DetailsProducts({ data }) {
  const [Show, setShow] = useState(false)

  const handleShow = () => {
    setShow(!Show)
  }

  return (
    <>
      <Stack direction={'column'} spacing={5}>
        <CustomRequestsTabs data={data} />

        <Card
          sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            backgroundColor: theme =>
              theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
            borderRadius: '8px',
            boxShadow: 'rgba(0, 0, 0, 0.05)',
            padding: '16px',
            gap: '10px'
          }}
          className='Cards-Requests'
        >
          <Stack direction={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'}>
            <Stack direction={'row'} justifyContent={'space-between'} sx={{ width: '100%' }} spacing={3}>
              <Box sx={{ display: 'flex', gap: '10px', alignItems: 'center' }}>
                <Avatar src='/images/icons/avatar.png' alt='Olivia Sparks' />
                <Box>
                  <Typography variant='h6'> المندوب</Typography>
                  <Typography variant='p'>{data?.trip_date?.trip?.salesman?.name}</Typography>
                </Box>
              </Box>
            </Stack>
            <Button
              sx={{
                display: 'flex',
                padding: '8px 16px',
                justifyContent: 'center',
                alignItems: 'center',
                gap: '8px',
                borderRadius: '4px',
                background: 'rgba(224, 224, 224, 0.40)',
                width: '200px'
              }}
              onClick={handleShow}
            >
              <Typography
                sx={{
                  textAlign: 'center',
                  fontSize: '12px',
                  fontWeight: '500',
                  lineHeight: '160%'
                }}
              >
                عرض تاريخ التعديل
              </Typography>
              {Show ? <ArrowUp /> : <ArrowDown />}
            </Button>
          </Stack>

          <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
            <DateRequest /> تاريخ الطلب: {data?.order_date?.slice(0, 10)}
          </Typography>
          <Typography variant='h6' sx={{ display: 'flex', alignItems: 'center', gap: '4px' }}>
            <DeliveryTime /> موعد التسليم: {data?.delivery_date} - {data?.delivery_time}
          </Typography>
          <Stack direction={'row'} justifyContent={'space-between'} alignItems={'center'} width={'100%'}>
            <Box>
              <Typography
                variant='h6'
                sx={{
                  textAlign: 'center',
                  fontSize: '14px',
                  fontStyle: 'normal',
                  fontWeight: '500',
                  lineHeight: '160%',
                  display: 'flex',
                  alignItems: 'center',
                  gap: '4px'
                }}
              >
                <TotalPrice /> السعر الكلي : {data?.total_price}
              </Typography>
            </Box>
          </Stack>
          <Typography
            sx={{
              color: '#3B3B3B',
              textAlign: 'right',
              fontSize: '18px',
              fontStyle: 'normal',
              fontWeight: '500',
              paddingTop: '15px'
            }}
          >
            محتويات الطلب{' '}
          </Typography>
          {Show ? ` تاريخ التعديل : ${moment(data?.updated_at).format('d/mm/yyyy hh:mm')}` : ''}
          {Show ? (
            <>
              <Grid container spacing={6}>
                {data?.products?.map((client, index) => (
                  <Grid item sm={4} xs={12} key={index}>
                    <CustomCardsDetailsProducts client={client} />
                  </Grid>
                ))}
              </Grid>

              {data?.child_orders
                .slice()
                .reverse()
                .map((order, index) => (
                  <Box key={index} sx={{ width: '100%' }}>
                    <Typography>
                      {order.is_base === 1
                        ? `طلب قبل التعديل :${data?.order_date}`
                        : `تاريخ التعديل:${moment(data?.updated_at).format('D/MM/YYYY hh:mm')}`}
                    </Typography>
                    <Grid container spacing={6}>
                      {order?.products?.map((client, index) => (
                        <Grid item sm={4} xs={12} key={index}>
                          <CustomCardsDetailsProducts client={client} />
                        </Grid>
                      ))}
                    </Grid>
                  </Box>
                ))}
            </>
          ) : (
            <Grid container spacing={6}>
              {data?.products?.map((client, index) => (
                <Grid item sm={4} xs={12} key={index}>
                  <CustomCardsDetailsProducts client={client} />
                </Grid>
              ))}
            </Grid>
          )}
        </Card>
      </Stack>
    </>
  )
}
