import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'
import { useForm, Controller, useFormContext } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import { addUser } from 'src/store/apps/user'
import { Autocomplete, Avatar, Divider, Typography } from '@mui/material'
import Delegates from './delegates'
import Client from './client'
import SalesManager from './salesManager'
import { Stack } from '@mui/system'
import CustomTextField from 'src/@core/components/mui/text-field'
import City from 'public/images/cards/city'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const roles = [
  { label: 'زبون', value: 'customer' },
  { label: 'مندوب', value: 'salesman' },
  { label: 'مدير مبيعات', value: 'sales manager' }
]

const SidebarAddClients = ({ open, toggle }) => {
  const methods = useFormContext()

  const {
    control,
    formState: { errors },
    getValues,
    reset,
    setValue,
    trigger
  } = methods

  const [jobTitle, setJobTitle] = useState('customer')

  const dispatch = useDispatch()

  useEffect(() => {
    const subscription = methods.watch((value, { name }) => {
      if (name === 'role') {
        setJobTitle(value.role)
      }
    })

    return () => subscription.unsubscribe()
  }, [methods])

  const handleSaveUser = async () => {
    const valid = await trigger()
    if (valid) {
      dispatch(addUser(getValues()))
      handleClose()
    }
  }

  const handleClose = () => {
    toggle()
    reset({})
  }
  const BranchDetails = JSON.parse(localStorage.getItem('branch'))

  return (
    <Drawer
      open={open}
      anchor='right'
      variant='temporary'
      onClose={handleClose}
      ModalProps={{ keepMounted: true }}
      sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
    >
      <Header>
        <Avatar sx={{ backgroundColor: 'rgba(115, 163, 208, 0.25)', width: 64, height: 64 }}>
          <img src='/images/icons/avatar.png' alt='' />
        </Avatar>
        <Stack direction='row' alignItems='center' spacing={'24px'}>
          <Button
            size='small'
            onClick={handleSaveUser}
            sx={{
              p: '8px 24px',
              lineHeight: 'normal',
              borderRadius: 1,
              color: '#F2F4F8',
              fontSize: '16px',
              gap: '8px',
              backgroundColor: '#73A3D0',
              '&:hover': { backgroundColor: '#73A3D0' }
            }}
          >
            إضافة
          </Button>
          <Button
            size='small'
            onClick={handleClose}
            sx={{
              p: '8px 24px',
              borderRadius: 1,
              lineHeight: 'normal',
              fontSize: '16px',
              color: theme =>
                theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
              backgroundColor: theme => (theme.palette.mode === 'dark' ? '#555657' : '#E0E0E0'),
              '&:hover': {
                backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
              }
            }}
          >
            إلغاء
          </Button>
        </Stack>
      </Header>
      <Divider sx={{ my: 4 }} />
      <Box sx={{ p: theme => theme.spacing(0, 6, 6) }}>
        <Typography className='custom-style-label2 '>
          <City />
          {BranchDetails?.data?.city?.country?.name + ' - ' + BranchDetails?.data?.city?.name}
        </Typography>
        <Typography className='custom-style-label '>الاسم</Typography>
        <Controller
          name='name'
          control={control}
          render={({ field: { value, onChange } }) => (
            <CustomTextField
              fullWidth
              value={value ?? ''}
              onChange={onChange}
              placeholder='الاسم'
              error={Boolean(errors.name)}
              helperText={errors.name ? errors.name.message : ''}
            />
          )}
        />
        <Typography className='custom-style-label '>المسمى الوظيفي</Typography>
        <Controller
          name='role'
          control={control}
          render={({ field: { value, onChange }, fieldState: { error } }) => (
            <CustomTextField
              select
              fullWidth
              value={value || 'customer'}
              id='validation-role-select'
              error={Boolean(error)}
              aria-describedby={error ? 'validation-role-error' : undefined}
              helperText={error ? error.message : ''}
              onChange={e => onChange(e.target.value)}
            >
              {roles.map((role, i) => (
                <MenuItem value={role.value} key={i}>
                  {role.label}
                </MenuItem>
              ))}
            </CustomTextField>
          )}
        />
        {jobTitle === 'salesman' && <Delegates control={control} Controller={Controller} errors={errors} />}
        {jobTitle === 'customer' && <Client control={control} Controller={Controller} errors={errors} />}
        {jobTitle === 'sales manager' && <SalesManager control={control} Controller={Controller} errors={errors} />}
      </Box>
    </Drawer>
  )
}

export default SidebarAddClients
