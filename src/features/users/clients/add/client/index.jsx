import React, { useEffect, memo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Controller, useFieldArray } from 'react-hook-form'
import { MenuItem, Typography, Box, Autocomplete } from '@mui/material'
import CustomTextField from 'src/@core/components/mui/text-field'
import DeleteIcon from '../../../../../../public/images/cards/delete'
import PhoneIcon from '../../../../../../public/images/icons/phone'
import AddressIcon from '../../../../../../public/images/cards/Location'
import UserIcon from '../../../../../../public/images/cards/user'
import PasswordIcon from '../../../../../../public/images/cards/password'
import { fetchLocation } from 'src/store/apps/user'

const ControllerWrapper = memo(({ name, control, rules, defaultValue, render }) => (
  <Controller
    name={name}
    control={control}
    rules={rules}
    render={({ field }) => render(field)}
    defaultValue={defaultValue}
  />
))

const PhoneNumberField = ({ index, control, errors, remove }) => (
  <ControllerWrapper
    name={`phone_number.[${index}]`}
    control={control}
    rules={{ required: 'Phone is required' }}
    render={({ value, onChange }) => (
      <CustomTextField
        fullWidth
        value={value ? value : ''}
        type='number'
        sx={{ mb: 4 }}
        onChange={onChange}
        placeholder='الرقم'
        error={Boolean(errors?.phone_number?.[index])}
        {...(errors?.phone_number?.[index] && {
          helperText: errors?.phone_number?.[index]?.message
        })}
        InputProps={{
          endAdornment: index > 0 && (
            <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
              <DeleteIcon />
            </Box>
          )
        }}
      />
    )}
  />
)

const customer = [
  { label: 'متجر', value: 'shop' },
  { label: 'مركز', value: 'center' }
]

const Client = ({ Controller, control, errors }) => {
  const store = useSelector(state => state.user)
  const dispatch = useDispatch()

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'emails'
  })

  useEffect(() => {
    dispatch(fetchLocation())
  }, [dispatch])

  useEffect(() => {
    if (fields.length === 0) {
      append({})
    }
  }, [fields, append])

  const commonStyles = {
    fullWidth: true
  }

  return (
    <>
      <Typography className='custom-style-label'>النوع</Typography>
      <Controller
        name='customer_type'
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange } }) => (
          <Autocomplete
            options={customer}
            fullWidth
            id='autocomplete-customer_type-select'
            getOptionLabel={option => option.label}
            value={customer.find(role => role.value === value)}
            onChange={(event, newValue) => {
              onChange(newValue ? newValue.value : '')
            }}
            renderInput={params => (
              <CustomTextField
                {...params}
                placeholder='النوع'
                error={Boolean(errors.customer_type)}
                helperText={errors.customer_type ? errors.customer_type.message : ''}
              />
            )}
          />
        )}
      />

      <Typography className='custom-style-label'>
        <PhoneIcon /> أرقام التواصل
      </Typography>
      {fields.map((field, index) => (
        <PhoneNumberField
          sx={{ mb: '8px' }}
          key={field.id}
          index={index}
          control={control}
          errors={errors}
          remove={remove}
        />
      ))}
      <Typography
        sx={{
          cursor: 'pointer',
          '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' },
          marginBottom: '24px'
        }}
        onClick={() => append({})}
      >
        + اضافة رقم
      </Typography>
      <Typography className='custom-style-label' sx={{ marginTop: '11px !important' }}>
        <AddressIcon /> المنطقة
      </Typography>
      <Controller
        name='address_id'
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange } }) => (
          <Autocomplete
            options={store?.Locations || []} // استخدام متغير locations المعرف في store.Locations
            fullWidth
            id='autocomplete-address_id-select'
            getOptionLabel={option => option.area}
            value={store?.Locations?.find(area => area.id === value)}
            onChange={(event, newValue) => {
              onChange(newValue ? newValue.id : '')
            }}
            renderInput={params => (
              <CustomTextField
                {...params}
                placeholder='المنطقة'
                error={Boolean(errors.address_id)}
                helperText={errors.address_id ? errors.address_id.message : ''}
              />
            )}
          />
        )}
      />
      <Typography className='custom-style-label'>
        <AddressIcon /> تفاصيل الموقع
      </Typography>
      <ControllerWrapper
        name='location'
        control={control}
        rules={{ required: true }}
        render={({ value, onChange }) => (
          <CustomTextField
            {...commonStyles}
            value={value ? value : ''}
            onChange={onChange}
            placeholder='المنطقة-تفاصيل الموقع'
            error={Boolean(errors.location)}
            helperText={errors.location?.message}
          />
        )}
      />
      <Typography className='custom-style-label'>
        <UserIcon /> اسم المستخدم
      </Typography>
      <ControllerWrapper
        name='user_name'
        control={control}
        rules={{ required: true }}
        render={({ value, onChange }) => (
          <CustomTextField
            {...commonStyles}
            value={value ? value : ''}
            onChange={onChange}
            placeholder=' اسم المستخدم'
            error={Boolean(errors.user_name)}
            helperText={errors.user_name?.message}
          />
        )}
      />
      <Typography className='custom-style-label'>
        <PasswordIcon /> كلمة المرور
      </Typography>
      <ControllerWrapper
        name='password'
        control={control}
        rules={{ required: true }}
        render={({ value, onChange }) => (
          <CustomTextField
            {...commonStyles}
            value={value ? value : ''}
            onChange={onChange}
            placeholder='كلمة المرور'
            error={Boolean(errors.password)}
            helperText={errors.password?.message}
          />
        )}
      />
    </>
  )
}

export default memo(Client)
