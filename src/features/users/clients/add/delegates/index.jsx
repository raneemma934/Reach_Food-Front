import { Autocomplete, Avatar, Checkbox, Icon, MenuItem, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React, { useEffect, useMemo, useState } from 'react'
import CustomTextField from 'src/@core/components/mui/text-field'
import Delete from '../../../../../../public/images/cards/delete'
import { useFieldArray, useFormContext } from 'react-hook-form'
import Phone from '../../../../../../public/images/icons/phone'
import Address from '../../../../../../public/images/cards/Location'
import User from '../../../../../../public/images/cards/user'
import Password from '../../../../../../public/images/cards/password'
import Day from '../../../../../../public/images/cards/day'
import Divider from '@mui/material/Divider'
import { fetchDataDrawer, fetchDataManager, fetchPermissions } from 'src/store/apps/user'
import Star from '../../../../../../public/images/cards/star'
import Grid from '@mui/material/Grid'
import Clock from '../../../../../../public/images/cards/clock'
import { useDispatch, useSelector } from 'react-redux'
import { fetchbranches } from 'src/store/apps/branches'
import Arrowleft from '../../../../../../public/images/cards/arrowleft'
import DeleteRed from 'public/images/cards/deleteRed'

const days = [
  { name: 'Sunday', name_ar: 'الاحد' },
  { name: 'Monday', name_ar: 'الاثنين' },
  { name: 'Tuesday', name_ar: 'الثلاثاء' },
  { name: 'Wednesday', name_ar: 'الاربعاء' },
  { name: 'Thursday', name_ar: 'الخميس' },
  { name: 'Friday', name_ar: 'الجمعة' },
  { name: 'Saturday', name_ar: 'السبت' }
]

export default function Delegates({ Controller, control, errors }) {
  const store = useSelector(state => state.user)
  const StoreBranches = useSelector(state => state.branches)
  const [selectedBranch, setSelectedBranch] = useState('')
  const branchName = JSON.parse(localStorage.getItem('branch'))
  const [selectSalesManager, SetSelectSalesManager] = useState([])
  console.log('🚀 ~ Delegates ~ selectSalesManager:', selectSalesManager)

  const methods = useFormContext()

  const { setValue } = methods

  const {
    fields: FieldsTravel,
    append: AppendTravel,
    remove: RemoveTravel
  } = useFieldArray({
    control,
    name: 'trips'
  })

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'phoneNumber'
  })

  const [selectedItems, setSelectedItems] = useState([])

  // console.log('🚀 ~ Delegates ~ selectedItems:', selectedItems)
  const data = ['232', '23']
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchDataDrawer('sales manager'))

    dispatch(fetchPermissions())
    dispatch(fetchDataManager(`branch_id=${branchName.data.id}}`))

    dispatch(fetchbranches())
  }, [dispatch, branchName.data.id])

  useEffect(() => {
    if (fields.length === 0) {
      append({})
    }
  }, [fields, append])

  const handleAddBranch = () => {
    append({ branch_id: '', salesManager_id: '' })
  }

  const handaleChoos = e => {
    setSelectedBranch(e)
    dispatch(fetchDataManager(`branch_id=${e}`))
  }

  return (
    <>
      {' '}
      <Typography className='custom-style-label '>
        <Phone /> أرقام التواصل
      </Typography>
      {fields.map((field, index) => (
        <div key={field.id}>
          <Controller
            name={`phone_number.[${index}]`}
            control={control}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                type='number'
                sx={{ mb: '8px' }}
                value={value ? value : ''}
                onChange={onChange}
                error={Boolean(errors.emails?.[index]?.email)}
                placeholder='الرقم'
                {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                InputProps={{
                  endAdornment: (
                    <Box>
                      {index > 0 ? (
                        <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                          <DeleteRed />
                        </Box>
                      ) : (
                        ''
                      )}
                    </Box>
                  )
                }}
              />
            )}
          />
        </div>
      ))}
      <Typography
        sx={{
          cursor: 'pointer',
          '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' },
          marginBottom: '24px',
          marginTop: '8px'
        }}
        onClick={() => append({})}
      >
        + اضافة رقم{' '}
      </Typography>
      <Typography className='custom-style-label '>
        <Address /> العنوان
      </Typography>
      <Controller
        name='location'
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange } }) => (
          <CustomTextField
            fullWidth
            value={value ? value : ''}
            onChange={onChange}
            placeholder='المنطقة-تفاصيل الموقع'
            error={Boolean(errors.location)}
            helperText={errors.location?.message}
          />
        )}
      />
      <Typography className='custom-style-label '>
        {' '}
        <Star />
        صلاحيات المندوب{' '}
      </Typography>
      <Stack direction={'column'} justifyContent={'start'} width={'100%'}>
        {store?.permissioons?.map((category, i) => (
          <Controller
            key={i}
            name={`permissions[${i}].status`}
            control={control}
            render={({ field }) => (
              <Typography sx={{ fontWeight: '500', fontSize: '16px', color: '#73A3D0' }}>
                <Checkbox
                  {...field}
                  checked={Boolean(field.value)}
                  onChange={e => field.onChange(e.target.checked ? 1 : 0)}
                />{' '}
                {category?.name_ar + '  ' + 'الزبائن'}
              </Typography>
            )}
          />
        ))}
      </Stack>
      <Typography className='custom-style-label '>
        <User /> اسم المستخدم
      </Typography>
      <Controller
        name='user_name'
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange } }) => (
          <CustomTextField
            fullWidth
            value={value ? value : ''}
            onChange={onChange}
            placeholder='اسم المستخدم'
            error={Boolean(errors.user_name)}
            helperText={errors.user_name?.message}
          />
        )}
      />
      <Typography className='custom-style-label '>
        <Password /> كلمة السر{' '}
      </Typography>
      <Controller
        name='password'
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange } }) => (
          <CustomTextField
            fullWidth
            value={value ? value : ''}
            onChange={onChange}
            placeholder='كلمة المرور'
            error={Boolean(errors.password)}
            helperText={errors.password?.message}
          />
        )}
      />
      <Divider sx={{ paddingTop: '24px' }} />
      <Box sx={{ marginTop: '24px' }}>
        <Typography className='custom-style-label '>الأفرع التي يعمل بها</Typography>
        {fields.map((field, index) => (
          <div key={field.id}>
            <Stack direction='row' alignItems='center' justifyContent='space-between'>
              <Typography className='custom-style-label2 '>
                <Arrowleft /> الفرع
                {index + 1}
              </Typography>
              <Typography
                style={{
                  cursor: 'pointer',
                  marginTop: '6px'
                }}
                onClick={() => {
                  remove(field.id)
                }}
              >
                ازالة
              </Typography>
            </Stack>
            <Typography className='custom-style-label2 '>الفرع</Typography>

            <Controller
              name={`branches.${index}.branch_id`}
              control={control}
              render={({ field: { value, onChange } }) =>
                index == 0 ? (
                  <CustomTextField
                    fullWidth
                    value={branchName.data.name ? branchName.data.name : ''}
                    error={Boolean(errors?.branches?.[index]?.branch_id)}
                    onChange={setValue('branches[0].branch_id', branchName.data.id)}
                    InputProps={{
                      readOnly: true
                    }}
                  />
                ) : (
                  <Autocomplete
                    options={StoreBranches?.data?.data || []}
                    fullWidth
                    id={`autocomplete-branch-${index}`}
                    getOptionLabel={option => option.name}
                    value={StoreBranches?.data?.data?.find(branch => branch.id === value) || null}
                    onChange={(event, newValue) => {
                      onChange(newValue ? newValue.id : '')
                      handaleChoos(newValue ? newValue.id : '')
                    }}
                    renderInput={params => (
                      <CustomTextField
                        {...params}
                        value={value ? value : ''}
                        fullWidth
                        placeholder='الفرع'
                        id={`validation-branch-${index}-select`}
                        aria-describedby={`validation-branch-${index}-select`}
                        error={Boolean(errors?.branches?.[index]?.branch_id)}
                        helperText={errors?.branches?.[index]?.branch_id?.message || ''}
                      />
                    )}
                  />
                )
              }
            />
            <Typography className='custom-style-label3 '>مدير المبيعات المسؤول</Typography>
            <Controller
              name={`branches.${index}.salesManager_id`}
              control={control}
              render={({ field: { value, onChange } }) => (
                <Autocomplete
                  options={store?.manager || []}
                  fullWidth
                  id={`autocomplete-sales-manager-${index}`}
                  getOptionLabel={option => option.name || selectSalesManager[index].name}
                  value={store?.manager?.find(manager => manager.id === value) || null}
                  onChange={(event, newValue) => {
                    onChange(newValue ? newValue.id : '')
                    SetSelectSalesManager(prevItems => {
                      const newItems = [...prevItems]
                      newItems[index] = { ...newItems[index], name: newValue ? newValue.name : '' }

                      return newItems
                    })
                  }}
                  renderInput={params => (
                    <CustomTextField
                      {...params}
                      fullWidth
                      value={value || ''}
                      placeholder='مدير المبيعات'
                      id={`validation-sales-manager-${index}-select`}
                      aria-describedby={`validation-sales-manager-${index}-select`}
                      error={Boolean(errors?.branches?.[index]?.salesManager_id)}
                      helperText={errors?.branches?.[index]?.salesManager_id?.message || ''}
                    />
                  )}
                />
              )}
            />
          </div>
        ))}
        <Typography
          sx={{
            cursor: 'pointer',
            m: '8px 0',
            '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
          }}
          onClick={handleAddBranch}
        >
          + إضافة فرع{' '}
        </Typography>
      </Box>
      <Divider sx={{ paddingY: '12px' }} />
      <Typography className='custom-style-label'> إضافة رحلة للفرع الحالي </Typography>
      {FieldsTravel.map((field, index) => (
        <div key={field.id}>
          <Stack direction={'row'} alignItems={'center'} justifyContent={'space-between'}>
            <Typography>الرحلة {index + 1}</Typography>
            <Typography
              style={{
                cursor: 'pointer',
                marginTop: '6px'
              }}
              onClick={() => RemoveTravel(field.id)}
            >
              ازالة
            </Typography>
          </Stack>
          <Typography className='custom-style-label '>
            <Address /> المنطقة
          </Typography>
          <Controller
            name={`trips.${index}.address_id`}
            control={control}
            rules={{ required: 'المنطقة مطلوبة' }}
            render={({ field: { value, onChange } }) => (
              <Autocomplete
                options={store?.Locations || []}
                fullWidth
                id={`autocomplete-address-${index}`}
                getOptionLabel={option => option.area}
                value={store?.Locations?.find(location => location.id === value) || null}
                onChange={(event, newValue) => {
                  onChange(newValue ? newValue.id : '')
                }}
                renderInput={params => (
                  <CustomTextField
                    {...params}
                    placeholder='المنطقة'
                    fullWidth
                    id={`validation-address-${index}-select`}
                    aria-describedby={`validation-address-${index}-select`}
                    error={Boolean(errors?.trips?.[index]?.address_id)}
                    helperText={errors?.trips?.[index]?.address_id?.message || ''}
                  />
                )}
              />
            )}
          />
          <Typography className='custom-style-label '>
            <Day /> اليوم
          </Typography>
          <Controller
            name={`trips.${index}.day`}
            control={control}
            rules={{ required: 'يوم الرحلة مطلوب' }}
            render={({ field: { value, onChange } }) => (
              <Autocomplete
                options={days || []}
                fullWidth
                id={`autocomplete-day-${index}`}
                getOptionLabel={option => option.name_ar}
                value={days?.find(day => day.name === value) || null}
                onChange={(event, newValue) => {
                  onChange(newValue ? newValue.name : '')
                }}
                renderInput={params => (
                  <CustomTextField
                    {...params}
                    placeholder='يوم الرحلة'
                    fullWidth
                    id={`validation-day-${index}-select`}
                    aria-describedby={`validation-day-${index}-select`}
                    helperText={errors?.trips?.[index]?.day?.message || ''}
                  />
                )}
              />
            )}
          />
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Typography className='custom-style-label '>
                <Clock /> وقت الانطلاق
              </Typography>
              <Controller
                name={`trips.${index}.start_time`}
                control={control}
                rules={{ required: 'الوقت مطلوب ' }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    type='time'
                    value={value}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    error={Boolean(errors?.trips?.[index]?.start_time)}
                    placeholder='Departure Time'
                    {...(errors?.trips?.[index]?.start_time && {
                      helperText: errors?.trips?.[index]?.start_time.message
                    })}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Typography className='custom-style-label '>
                <Clock /> وقت الانتهاء
              </Typography>
              <Controller
                name={`trips.${index}.end_time`}
                control={control}
                rules={{ required: 'الوقت مطلوب ' }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    type='time'
                    value={value}
                    sx={{ mb: 4 }}
                    onChange={onChange}
                    error={Boolean(errors?.trips?.[index]?.start_time)}
                    placeholder='Departure Time'
                    {...(errors?.trips?.[index]?.start_time && {
                      helperText: errors?.trips?.[index]?.start_time.message
                    })}
                  />
                )}
              />
            </Grid>
          </Grid>
        </div>
      ))}
      <Typography
        sx={{
          cursor: 'pointer',
          '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
        }}
        onClick={() => AppendTravel({})}
        variant='text'
      >
        + اضافة رحلة{' '}
      </Typography>
    </>
  )
}
