import { Box, Stack } from '@mui/system'
import React, { useEffect, useState } from 'react'
import CustomTextField from 'src/@core/components/mui/text-field'
import Delete from '../../../../../../public/images/cards/delete'
import { useFieldArray } from 'react-hook-form'
import { Autocomplete, Avatar, Checkbox, Icon, MenuItem, Typography } from '@mui/material'
import Phone from '../../../../../../public/images/icons/phone'
import Address from '../../../../../../public/images/cards/Location'
import User from '../../../../../../public/images/cards/user'
import Password from '../../../../../../public/images/cards/password'
import Day from '../../../../../../public/images/cards/day'
import Clock from '../../../../../../public/images/cards/clock'
import { useDispatch, useSelector } from 'react-redux'
import { fetchData, fetchDataSalesMan, fetchLocation } from 'src/store/apps/user'
import DeleteRed from 'public/images/cards/deleteRed'

export default function SalesManager({ Controller, control, errors }) {
  const store = useSelector(state => state.user)

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'PhoneNumber'
  })

  const [selectedItems, setSelectedItems] = useState([])
  const data = ['232', '23']
  const dispatch = useDispatch()
  useEffect(() => {
    const branch_id = localStorage.getItem('branch_id')
    dispatch(fetchDataSalesMan(`branch_id=${branch_id}`))
    dispatch(fetchLocation())
  }, [dispatch])

  useEffect(() => {
    if (fields.length === 0) {
      append({})
    }
  }, [fields, append])

  return (
    <>
      <Typography className='custom-style-label '>
        <Phone /> أرقام التواصل
      </Typography>
      {fields.map((field, index) => (
        <div key={field.id}>
          <Controller
            name={`phone_number.${index}`}
            control={control}
            rules={{ required: 'Email is required' }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                type='number'
                value={value ? value : ''}
                sx={{ mb: '8px' }}
                onChange={onChange}
                error={Boolean(errors.emails?.[index]?.email)}
                placeholder='الرقم'
                {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                InputProps={{
                  endAdornment: (
                    <Box>
                      {index > 0 ? (
                        <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                          <DeleteRed />
                        </Box>
                      ) : (
                        ''
                      )}
                    </Box>
                  )
                }}
              />
            )}
          />
        </div>
      ))}
      <Typography
        sx={{
          cursor: 'pointer',
          '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' },
          marginBottom: '24px'
        }}
        onClick={() => append({})}
      >
        + اضافة رقم{' '}
      </Typography>
      <Typography className='custom-style-label '>
        <Address /> العنوان{' '}
      </Typography>
      <Controller
        name='location'
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange } }) => (
          <CustomTextField
            fullWidth
            value={value ? value : ''}
            onChange={onChange}
            placeholder='المنطقة-تفاصيل الموقع'
            error={Boolean(errors.location)}
            helperText={errors.location?.message}
          />
        )}
      />
      <Typography className='custom-style-label '>
        <User /> اسم المستخدم{' '}
      </Typography>
      <Controller
        name='user_name'
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange } }) => (
          <CustomTextField
            fullWidth
            value={value ? value : ''}
            onChange={onChange}
            placeholder='اسم المستخدم'
            error={Boolean(errors.user_name)}
            helperText={errors.user_name?.message}
          />
        )}
      />
      <Typography className='custom-style-label '>
        <Password /> كلمة السر{' '}
      </Typography>
      <Controller
        name='password'
        control={control}
        rules={{ required: true }}
        render={({ field: { value, onChange } }) => (
          <CustomTextField
            fullWidth
            value={value}
            sx={{ mb: 4 }}
            onChange={onChange}
            placeholder='كلمة السر'
            error={Boolean(errors.password)}
            helperText={errors.password?.message}
          />
        )}
      />

      <Typography className='custom-style-label '> المندوبين المسؤول عنهم </Typography>
      <Stack direction={'column'} justifyContent={'start'} width={'100%'}>
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            alignItems: 'center'
          }}
        ></Box>
        {/* ***************** */}
        <Box>
          {store?.SalesMan?.map((category, i) => (
            <Controller
              key={category.id}
              name={`salesmen[${i}]`}
              control={control}
              render={({ field }) => (
                <Stack direction='row' justifyContent='space-between' alignItems='right' spacing={10} sx={{ mb: 2 }}>
                  <Stack direction='row' alignItems='right' spacing={3} sx={{ mb: 2 }}>
                    <Avatar src='' alt='user' sx={{ width: 40, height: 40 }} />
                    <Typography sx={{ fontWeight: '500', fontSize: '16px', alignContent: 'center' }}>
                      {category?.name}
                    </Typography>
                  </Stack>
                  <Checkbox
                    {...field}
                    checked={Boolean(field.value)}
                    onChange={e => field.onChange(e.target.checked ? category.id : '')}
                  />
                </Stack>
              )}
            />
          ))}
        </Box>
      </Stack>
    </>
  )
}
