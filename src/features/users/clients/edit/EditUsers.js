import { useEffect, useState } from 'react'
import Drawer from '@mui/material/Drawer'
import Button from '@mui/material/Button'
import MenuItem from '@mui/material/MenuItem'
import { styled } from '@mui/material/styles'
import Box from '@mui/material/Box'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import { Avatar, Divider, Typography } from '@mui/material'

import Address from '../../../../../public/images/cards/Location'

import City from '../../../../../public/images/cards/city'
import Phone from '../../../../../public/images/cards/Telephone'
import Delete from '../../../../../public/images/cards/deleteWihte'
import Arrowleft from '../../../../../public/images/cards/arrowleft'
import User from '../../../../../public/images/cards/user'
import Password from '../../../../../public/images/cards/password'
import { Stack } from '@mui/system'
import { useDispatch, useSelector } from 'react-redux'
import Edit from '../../../../../public/images/cards/edit'
import DefaultPalette from 'src/@core/theme/palette'
import { editUser } from 'src/store/apps/user'
import DeleteRed from 'public/images/cards/deleteRed'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const SidebarEditUsers = props => {
  // ** Props
  const [isEditing, setIsEditing] = useState(false)
  const store = useSelector(state => state.user)

  const dispatch = useDispatch()
  const { open, handleCloseDrawer, handleDeleteClick, Editing, data, mode } = props

  // console.log('🚀 ~ SidebarEditUsers ~ data:', data)

  const methods = useForm({
    defaultValues: {
      phone_number: data?.contacts?.map(contact => contact.phone_number) || [],
      customer_type: data.customer_type,
      address_id: data.address_id,
      user_name: data.user_name,
      name: data.name,
      location: data.location,
      password: data.user_password?.password
    },
    resolver: yupResolver(yup.Schema)
  })
  const { control, formState, getValues, setValue, watch } = methods

  const { errors } = formState

  // ** State
  // useEffect(()=>{
  //   setValue('name',data.name)
  // },[data,setValue])

  function handleSaveUser() {
    setValue('id', data.id)
    dispatch(editUser(getValues()))
    handleCloseDrawer()
  }

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'emails'
  })

  useEffect(() => {
    if (fields.length === 0) {
      append({})
    }
  }, [fields, append])

  const handleEditClick = () => {
    setIsEditing(true)
  }

  const drawerColor = DefaultPalette('light', 'bordered').drawer
  const { BranchDetails } = useSelector(state => state.branches)

  return (
    <>
      {!isEditing ? (
        <>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
            PaperProps={{
              sx: {
                width: { xs: 300, sm: 400 },
                backgroundColor: drawerColor
              }
            }}
          >
            <Header>
              <Avatar
                sx={{
                  width: 64,
                  height: 64,
                  fontSize: '24px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
                alt='dlivia Sparks'
              >
                {data?.name?.slice(0, 1).toUpperCase()}
              </Avatar>
              <Stack direction={'row'} alignItems={'center'} sx={{ gap: '24px' }}>
                <Box
                  className='hover-edit'
                  onClick={handleEditClick}
                  sx={{
                    p: '8px 12px',
                    borderRadius: 1,
                    gap: '8px',
                    backgroundColor: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                    display: 'flex',
                    alignItems: 'center',
                    cursor: 'pointer',
                    '&:hover': {
                      backgroundColor: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                    }
                  }}
                >
                  <Edit />
                  <Button
                    size='small'
                    sx={{
                      p: 0,
                      color: theme =>
                        theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                      fontSize: '16px',
                      '&:hover': {
                        backgroundColor: 'transparent'
                      }
                    }}
                  >
                    تعديل
                  </Button>
                </Box>

                <Box
                  onClick={handleDeleteClick}
                  sx={{
                    padding: '8px 12px',
                    borderRadius: 1,
                    fontSize: '16px',
                    color: '#fff',
                    backgroundColor: '#CE3446',
                    display: 'flex',
                    alignItems: 'center',
                    cursor: 'pointer',
                    '&:hover': {
                      backgroundColor: '#CE3446'
                    }
                  }}
                >
                  <Delete />

                  <Button
                    size='small'
                    sx={{
                      p: 0,
                      fontSize: '16px',
                      color: '#fff',
                      '&:hover': {
                        backgroundColor: 'transparent',
                        color: '#fff'
                      }
                    }}
                  >
                    حذف
                  </Button>
                </Box>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />
            <Stack padding={' 24px'} direction={'column'} gap={'24px'}>
              <Box>
                <Typography className='custom-style-label2 '>
                  <City /> {BranchDetails?.data?.city?.country?.name + ' - ' + BranchDetails?.data?.city?.name}
                </Typography>
              </Box>
              <Box>
                <Typography className='custom-style-label2 '>الاسم</Typography>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.name}
                </Typography>
              </Box>
              <Box>
                <Typography className='custom-style-label2 '>النوع</Typography>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.customer_type}
                </Typography>
              </Box>

              <Box>
                <Typography className='custom-style-label2'>
                  <Phone /> أرقام التواصل
                </Typography>

                {data?.contacts && data.contacts.length > 0 ? (
                  <ul>
                    {data.contacts.map((contact, index) => (
                      <li key={index} style={{ fontWeight: '400', fontSize: '14px', margin: '3px 0px' }}>
                        {contact.phone_number}
                      </li>
                    ))}
                  </ul>
                ) : (
                  'No contacts available'
                )}
              </Box>
              <Box>
                <Stack direction={'row'} alignItems={'center'} padding={'0 0 8px  0'} sx={{ gap: '8px' }}>
                  <Address />
                  <Typography>المنطقة</Typography>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.address?.area}
                </Typography>
              </Box>
              <Box>
                <Stack direction={'row'} alignItems={'center'} padding={'0 0 8px  0'} sx={{ gap: '8px' }}>
                  <Address />
                  <Typography>تفاصيل الموقع</Typography>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.location}
                </Typography>
              </Box>

              <Divider sx={{ marginY: '4px ' }} />

              <Box>
                <Stack direction={'row'} alignItems={'center'} padding={'0 0 8px  0'} sx={{ gap: '8px' }}>
                  <User />
                  <Typography className='custom-style-label2 '> اسم المستخدم</Typography>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.user_name}
                </Typography>
              </Box>
              <Box>
                <Stack direction={'row'} alignItems={'center'} padding={'0 0 8px  0'} sx={{ gap: '8px' }}>
                  <Password />
                  <Typography className='custom-style-label2 '> كلمة السر</Typography>
                </Stack>
                <Typography
                  sx={{
                    fontWeight: '400',
                    fontSize: '14px',
                    color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
                  }}
                >
                  {data?.user_password.password}
                </Typography>
              </Box>
            </Stack>
          </Drawer>
        </>
      ) : (
        <FormProvider {...methods}>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
          >
            <Header>
              <Avatar
                sx={{
                  width: 64,
                  height: 64,
                  fontSize: '24px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
                alt='dlivia Sparks'
              >
                {data?.name?.slice(0, 1).toUpperCase()}
              </Avatar>
              <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
                <Button
                  size='small'
                  onClick={handleSaveUser}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    color: '#ffff',
                    fontSize: '16px',
                    gap: '8px',
                    backgroundColor: '#73A3D0',
                    '&:hover': {
                      backgroundColor: '#73A3D0'
                    }
                  }}
                >
                  حفظ
                </Button>
                <Button
                  size='small'
                  onClick={handleCloseDrawer}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    fontSize: '16px',

                    backgroundColor: '#E0E0E0',
                    '&:hover': {
                      backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                    }
                  }}
                >
                  إلغاء
                </Button>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />

            <Stack padding={'24px'}>
              <Typography className='custom-style-label2 '>الاسم</Typography>
              <Controller
                name='name'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    onChange={onChange}
                    placeholder='John Doe'
                    error={Boolean(errors.fullName)}
                    {...(errors.fullName && { helperText: errors.fullName.message })}
                  />
                )}
              />

              <Typography className='custom-style-label '>
                <Phone /> أرقام التواصل{' '}
              </Typography>

              {fields.map((field, index) => (
                <div key={field.id}>
                  <Controller
                    name={`phone_number.[${index}]`}
                    control={control}
                    rules={{ required: 'Phone is required' }}
                    render={({ field: { value, onChange } }) => (
                      <CustomTextField
                        fullWidth
                        value={value}
                        sx={{ mb: '8px' }}
                        onChange={onChange}
                        error={Boolean(errors.emails?.[index]?.email)}
                        placeholder='0934343428'
                        {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                        InputProps={{
                          endAdornment: (
                            <Box>
                              {index > 0 ? (
                                <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                                  <DeleteRed />
                                </Box>
                              ) : (
                                ''
                              )}
                            </Box>
                          )
                        }}
                      />
                    )}
                  />
                </div>
              ))}
              <Typography
                sx={{
                  cursor: 'pointer',
                  '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
                }}
                onClick={() => append({})}
                variant='text'
              >
                + اضافة رقم{' '}
              </Typography>
              <Typography className='custom-style-label '>
                {' '}
                <Address /> المنطقة
              </Typography>

              <Controller
                name='address_id'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    select
                    fullWidth
                    id='validation-billing-select'
                    error={Boolean(errors.billing)}
                    aria-describedby='validation-billing-select'
                    {...(errors.billing && { helperText: errors.billing.message })}
                    SelectProps={{ value: value, onChange: e => onChange(e) }}
                  >
                    {store?.Locations?.map((area, i) => (
                      <MenuItem value={area?.id} key={i}>
                        {area?.area}
                      </MenuItem>
                    ))}
                  </CustomTextField>
                )}
              />
              <Typography className='custom-style-label '>
                <Address /> تفاصيل الموقع
              </Typography>

              <Controller
                name='location'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    onChange={onChange}
                    placeholder='Australia'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
              <Typography className='custom-style-label '>
                <User /> اسم المستخدم
              </Typography>

              <Controller
                name='user_name'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    onChange={onChange}
                    placeholder='johndoe'
                    error={Boolean(errors.username)}
                    {...(errors.username && { helperText: errors.username.message })}
                  />
                )}
              />
              <Typography className='custom-style-label '>
                <Password /> كلمة السر
              </Typography>

              <Controller
                name='password'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    onChange={onChange}
                    placeholder='johndoe'
                    error={Boolean(errors.username)}
                    {...(errors.username && { helperText: errors.username.message })}
                  />
                )}
              />
            </Stack>
          </Drawer>
        </FormProvider>
      )}

      {Editing ? (
        <FormProvider {...methods}>
          <Drawer
            open={open}
            anchor='right'
            variant='temporary'
            onClose={handleCloseDrawer}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
          >
            <Header>
              <Avatar
                sx={{
                  width: 64,
                  height: 64,
                  fontSize: '24px',
                  fontWeight: '700',
                  backgroundColor: 'rgba(115, 163, 208, 0.25)',
                  color: theme => theme.palette.accent.main
                }}
                src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
                alt='dlivia Sparks'
              >
                {data?.name?.slice(0, 1).toUpperCase()}
              </Avatar>
              <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
                <Button
                  size='small'
                  onClick={handleSaveUser}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    color: '#ffff',
                    fontSize: '16px',
                    gap: '8px',
                    backgroundColor: '#73A3D0',
                    '&:hover': {
                      backgroundColor: '#73A3D0'
                    }
                  }}
                >
                  حفظ
                </Button>
                <Button
                  size='small'
                  onClick={handleCloseDrawer}
                  sx={{
                    p: '8px 24px',
                    borderRadius: 1,
                    fontSize: '16px',

                    backgroundColor: '#E0E0E0',
                    '&:hover': {
                      backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                    }
                  }}
                >
                  إلغاء
                </Button>
              </Stack>
            </Header>
            <Divider sx={{ marginY: '4px' }} />

            <Stack padding={'24px'}>
              <Controller
                name='name'
                control={control}
                rules={{ required: true }}
                sx={{ mb: 0 }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    label='الاسم'
                    onChange={onChange}
                    placeholder='John Doe'
                    error={Boolean(errors.fullName)}
                    {...(errors.fullName && { helperText: errors.fullName.message })}
                  />
                )}
              />
              <Typography className='custom-style-label '>النوع</Typography>

              <Controller
                name='customer_type'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    select
                    fullWidth
                    id='validation-billing-select'
                    error={Boolean(errors.billing)}
                    aria-describedby='validation-billing-select'
                    {...(errors.billing && { helperText: errors.billing.message })}
                    SelectProps={{ value: value, onChange: e => onChange(e) }}
                  >
                    <MenuItem value='shop'>متجر</MenuItem>
                    <MenuItem value='center'>مركز</MenuItem>
                  </CustomTextField>
                )}
              />
              <Typography className='custom-style-label '>
                <Phone /> أرقام التواصل{' '}
              </Typography>

              {fields.map((field, index) => (
                <div key={field.id} style={{ marginBottom: '8px' }}>
                  <Controller
                    name={`phone_number.[${index}]`}
                    control={control}
                    rules={{ required: 'Phone is required' }}
                    render={({ field: { value, onChange } }) => (
                      <CustomTextField
                        fullWidth
                        value={value}
                        sx={{ mb: '8px' }}
                        onChange={onChange}
                        error={Boolean(errors.emails?.[index]?.email)}
                        placeholder='0934343428'
                        {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                        InputProps={{
                          endAdornment: (
                            <Box>
                              {index > 0 ? (
                                <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                                  <DeleteRed />
                                </Box>
                              ) : (
                                ''
                              )}
                            </Box>
                          )
                        }}
                      />
                    )}
                  />
                </div>
              ))}
              <Typography
                sx={{
                  cursor: 'pointer',
                  '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
                }}
                onClick={() => append({})}
                variant='text'
              >
                + اضافة رقم{' '}
              </Typography>
              <Typography className='custom-style-label '>
                {' '}
                <Address /> المنطقة
              </Typography>

              <Controller
                name='address_id'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    select
                    fullWidth
                    id='validation-billing-select'
                    error={Boolean(errors.billing)}
                    aria-describedby='validation-billing-select'
                    {...(errors.billing && { helperText: errors.billing.message })}
                    SelectProps={{ value: value, onChange: e => onChange(e) }}
                  >
                    {store?.Locations?.map((area, i) => (
                      <MenuItem value={area?.id} key={i}>
                        {area?.area}
                      </MenuItem>
                    ))}
                  </CustomTextField>
                )}
              />
              <Typography className='custom-style-label '>
                <Address /> تفاصيل الموقع
              </Typography>

              <Controller
                name='location'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    onChange={onChange}
                    placeholder='Australia'
                    error={Boolean(errors.country)}
                    {...(errors.country && { helperText: errors.country.message })}
                  />
                )}
              />
              <Typography className='custom-style-label '>
                <User /> اسم المستخدم
              </Typography>

              <Controller
                name='user_name'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    onChange={onChange}
                    placeholder='johndoe'
                    error={Boolean(errors.username)}
                    {...(errors.username && { helperText: errors.username.message })}
                  />
                )}
              />

              <Typography className='custom-style-label '>
                <Password /> كلمة السر
              </Typography>
              <Controller
                name='password'
                control={control}
                rules={{ required: true }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    onChange={onChange}
                    placeholder='johndoe'
                    error={Boolean(errors.username)}
                    {...(errors.username && { helperText: errors.username.message })}
                  />
                )}
              />
            </Stack>
          </Drawer>
        </FormProvider>
      ) : (
        ''
      )}
    </>
  )
}

export default SidebarEditUsers
