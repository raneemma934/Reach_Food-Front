import { Grid } from '@mui/material'
import React from 'react'
import CustomCards from 'src/@core/components/custom-cards'
import EmptyData from 'src/@core/components/empty-data'

export default function List({ data }) {
  const type = 'customer'

  return (
    <>
      {data.length > 0 ? (
        <Grid container spacing={6}>
          {data.length > 0 &&
            data.map((client, index) => (
              <Grid item sm={4} xs={12} key={index}>
                <CustomCards client={client} type={type} />
              </Grid>
            ))}
        </Grid>
      ) : (
        <EmptyData desc={'لا يوجد زبائن لعرضهم'} />
      )}
    </>
  )
}
