import * as yup from 'yup'

export const schema = yup.object().shape({
  name: yup.string().required('الاسم مطلوب').max(20, 'يجب ألا يتجاوز الاسم 20 حرفاً'),
  role: yup.string().required('المسمى الوظيفي مطلوب'),

  phone_number: yup
    .string()
    .required('رقم التواصل مطلوب ')
    .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,}$/, ''),
  customer_type: yup.string().required('يجب اختيار نوع الزبون').oneOf(['shop', 'center'], 'الرجاء اختيار نوع صحيح'),
  address_id: yup.string().required('يجب اختيار المنطقة'),
  location: yup.string().required('الموقع مطلوب'),
  user_name: yup.string().required('اسم المستخدم مطلوب').max(20, 'يجب ألا يتجاوز الاسم 20 حرفاً'),
  password: yup.string().required('كلمة المرور مطلوبة').max(20, 'يجب ألا تتجاوز كلمة السر 20 حرفاً'),
  branches: yup.array().of(
    yup.object().shape({
      branch_id: yup.string(),
      salesManager_id: yup.string(),
      permissions: yup.array().of(
        yup.object().shape({
          status: yup.boolean().oneOf([true], 'يجب اختيار صلاحية واحدة على الأقل')
        })
      )
    })
  ),
  trips: yup.array().of(
    yup.object().shape({
      address_id: yup.string(),
      day: yup.string(),
      start_time: yup.string()
    })
  )
})
