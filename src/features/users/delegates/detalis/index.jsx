import { Stack } from '@mui/system'
import React, { useState } from 'react'

import CustomDelegatesTabs from 'src/@core/components/custom-delegates-tabs'

export default function DetailsDelegates({ data }) {
  const [Show, setShow] = useState(false)

  const handleShow = () => {
    setShow(!Show)
  }

  return (
    <>
      <Stack direction={'column'} spacing={5}>
        <CustomDelegatesTabs data={data} />
      </Stack>
    </>
  )
}
