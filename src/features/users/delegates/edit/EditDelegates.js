import { useEffect, useState } from 'react'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import { fetchData, fetchDataSalesMan } from 'src/store/apps/user'
import ShowData from './ShowData'
import EditFormData from './EditFormData'
import { useDispatch, useSelector } from 'react-redux'

const SidebarEditDelegates = props => {
  const { open, handleCloseDrawer, handleDeleteClick, Editing, data } = props
  console.log('🚀 ~ SidebarEditDelegates ~ data:', data)
  const [isEditing, setIsEditing] = useState(false)

  const store = useSelector(state => state.user)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchDataSalesMan())
  }, [dispatch])

  const methods = useForm({
    defaultValues: {
      phone_number: data?.contacts?.map(contact => contact.phone_number) || [],
      customer_type: data.customer_type,
      address_id: data.address_id,
      user_name: data.user_name,
      name: data.name,
      location: data.location,
      password: data.user_password?.password
    },
    resolver: yupResolver(yup.Schema)
  })
  const { control, formState, getValues, watch } = methods

  const { errors } = formState

  const {
    fields: emailFields,
    append: appendEmail,
    remove: removeEmail
  } = useFieldArray({
    control,
    name: 'emails'
  })

  const {
    fields: flightFields,
    append: appendFlight,
    remove: removeFlight
  } = useFieldArray({
    control,
    name: 'flight'
  })

  const {
    fields: flightsFields,
    append: appendFlights,
    remove: removeFlights
  } = useFieldArray({
    control,
    name: 'flights'
  })

  const handleEditClick = () => {
    setIsEditing(true)
  }

  return (
    <>
      {!isEditing ? (
        <ShowData
          open={open}
          handleEditClick={handleEditClick}
          handleDeleteClick={handleDeleteClick}
          data={data}
          handleCloseDrawer={handleCloseDrawer}
        />
      ) : (
        <EditFormData
          methods={methods}
          open={open}
          handleEditClick={handleEditClick}
          handleDeleteClick={handleDeleteClick}
          data={data}
          handleCloseDrawer={handleCloseDrawer}
          fields={emailFields}
          append={appendEmail}
          remove={removeEmail}
          errors={errors}
          flightFields={flightFields}
          appendFlight={appendFlight}
          removeFlight={removeFlight}
          flightsFields={flightsFields}
          appendFlights={appendFlights}
          removeFlights={removeFlights}
        />
      )}

      {Editing ? (
        <EditFormData
          methods={methods}
          open={open}
          handleEditClick={handleEditClick}
          handleDeleteClick={handleDeleteClick}
          data={data}
          handleCloseDrawer={handleCloseDrawer}
          fields={emailFields}
          append={appendEmail}
          remove={removeEmail}
          errors={errors}
          flightFields={flightFields}
          appendFlight={appendFlight}
          removeFlight={removeFlight}
          flightsFields={flightsFields}
          appendFlights={appendFlights}
          removeFlights={removeFlights}
        />
      ) : (
        ''
      )}
    </>
  )
}

export default SidebarEditDelegates
