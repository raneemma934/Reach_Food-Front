import React, { useEffect, useState } from 'react'
import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import {
  Autocomplete,
  Avatar,
  Button,
  Checkbox,
  Divider,
  Drawer,
  FormControlLabel,
  FormGroup,
  Grid,
  MenuItem,
  Typography
} from '@mui/material'
import { styled } from '@mui/material/styles'
import { Box, Stack } from '@mui/system'
import City from '../../../../../public/images/cards/city'
import CustomTextField from 'src/@core/components/mui/text-field'
import Phone from '../../../../../public/images/cards/Telephone'
import Delete from '../../../../../public/images/cards/deleteRed'
import Address from '../../../../../public/images/cards/Location'
import User from '../../../../../public/images/cards/user'
import Star from '../../../../../public/images/cards/star'
import ArrowLeft from '../../../../../public/images/cards/arrowleft'
import Clock from '../../../../../public/images/cards/clock'
import Day from '../../../../../public/images/cards/day'

import Password from '../../../../../public/images/cards/password'
import { useDispatch, useSelector } from 'react-redux'
import { editUser, fetchDataManager, fetchDataSalesMan, fetchPermissions } from 'src/store/apps/user'
import DeleteRed from '../../../../../public/images/cards/deleteRed'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

const days = [
  { name: 'Sunday', name_ar: 'الاحد' },
  { name: 'Monday', name_ar: 'الاثنين' },
  { name: 'Tuesday', name_ar: 'الثلاثاء' },
  { name: 'Wednesday', name_ar: 'الاربعاء' },
  { name: 'Thursday', name_ar: 'الخميس' },
  { name: 'Friday', name_ar: 'الجمعة' },
  { name: 'Saturday', name_ar: 'السبت' }
]

export default function EditFormData({
  methods,
  handleCloseDrawer,
  handleEditClick,
  handleDeleteClick,
  data,
  fields,
  append,
  remove,
  flightFields,
  appendFlight,
  removeFlight,
  removeFlights,
  appendFlights,
  flightsFields
}) {
  const [checked, setChecked] = useState(false)
  const [selected, setSelected] = useState([])
  const branchName = JSON.parse(localStorage.getItem('branch'))
  const store = useSelector(state => state.user)
  const StoreBranches = useSelector(state => state.branches)
  const [selectSalesManager, SetSelectSalesManager] = useState([])

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchDataSalesMan())
    dispatch(fetchPermissions())
    dispatch(fetchDataManager(`branch_id=${branchName.data.id}}`))
  }, [dispatch, branchName?.data?.id])

  const handleSelectAll = event => {
    const isChecked = event.target.checked
    setChecked(isChecked)
    if (isChecked) {
      setSelected(store?.SalesMan?.map(item => item.id))
    } else {
      setSelected([])
    }
  }

  const handleAddBranch = () => {
    append({ branch_id: '', salesManager_id: '' })
  }

  useEffect(() => {
    if (fields.length === 0) {
      append({})
    }
  }, [fields, append])

  const { control, formState, getValues, setValue, watch } = methods

  const { errors } = formState

  function handleSaveUser() {
    setValue('id', data.id)
    dispatch(editUser(getValues()))
    handleCloseDrawer()
  }

  const {
    fields: FieldsTravel,
    append: AppendTravel,
    remove: RemoveTravel
  } = useFieldArray({
    control,
    name: 'trips'
  })
  const { BranchDetails } = useSelector(state => state.branches)

  return (
    <FormProvider {...methods}>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header>
          <Avatar
            sx={{
              width: 64,
              height: 64,
              fontSize: '24px',
              fontWeight: '700',
              backgroundColor: 'rgba(115, 163, 208, 0.25)',
              color: theme => theme.palette.accent.main
            }}
            src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
            alt='dlivia Sparks'
          >
            {data?.name?.slice(0, 1).toUpperCase()}
          </Avatar>
          <Stack direction={'row'} alignItems={'center'} spacing={'24px '}>
            <Button
              size='small'
              onClick={handleSaveUser}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                color: '#ffff',
                fontSize: '16px',
                gap: '8px',
                backgroundColor: '#73A3D0',
                '&:hover': {
                  backgroundColor: '#73A3D0'
                }
              }}
            >
              حفظ
            </Button>
            <Button
              size='small'
              onClick={handleCloseDrawer}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                fontSize: '16px',

                backgroundColor: '#E0E0E0',
                '&:hover': {
                  backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                }
              }}
            >
              إلغاء
            </Button>
          </Stack>
        </Header>
        <Divider sx={{ marginY: '4px' }} />
        <Stack padding={4} sx={{ gap: '8px' }}>
          <Box>
            <Typography className='custom-style-label2'>
              <City /> {BranchDetails?.data?.city?.country?.name + ' - ' + BranchDetails?.data?.city?.name}
            </Typography>
          </Box>
          <Typography className='custom-style-label '>الاسم</Typography>
          <Controller
            name='name'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='John Doe'
                error={Boolean(errors.fullName)}
                {...(errors.fullName && { helperText: errors.fullName.message })}
              />
            )}
          />

          <Typography className='custom-style-label '>
            <Phone /> أرقام التواصل{' '}
          </Typography>

          {fields.map((field, index) => (
            <div key={field.id}>
              <Controller
                name={`phone_number.[${index}]`}
                control={control}
                rules={{ required: 'Phone is required' }}
                render={({ field: { value, onChange } }) => (
                  <CustomTextField
                    fullWidth
                    value={value}
                    sx={{ mb: '8px' }}
                    onChange={onChange}
                    error={Boolean(errors.emails?.[index]?.email)}
                    placeholder='0934343428'
                    {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                    InputProps={{
                      endAdornment: (
                        <Box>
                          {index > 0 ? (
                            <Box style={{ cursor: 'pointer', marginTop: '6px' }} onClick={() => remove(index)}>
                              <DeleteRed />
                            </Box>
                          ) : (
                            ''
                          )}
                        </Box>
                      )
                    }}
                  />
                )}
              />
            </div>
          ))}
          <Typography
            sx={{
              cursor: 'pointer',
              '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
            }}
            onClick={() => append({})}
            variant='text'
          >
            + اضافة رقم{' '}
          </Typography>

          <Typography className='custom-style-label '>
            {' '}
            <Address /> العنوان
          </Typography>

          <Controller
            name='location'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='Australia'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />

          <Typography className='custom-style-label '>
            {' '}
            <Star /> صلاحيات المندوب{' '}
          </Typography>

          <Stack direction={'column'} justifyContent={'start'} width={'100%'}>
            {store?.permissioons?.map((category, i) => (
              <Controller
                key={i}
                name={`permissions[${i}].status`} // Use an appropriate field name
                control={control}
                render={({ field }) => (
                  <Typography sx={{ fontWeight: '500', fontSize: '16px' }}>
                    <Checkbox
                      {...field}
                      label='d'
                      checked={Boolean(field.value)} // Determine if the checkbox is checked
                      onChange={e => field.onChange(e.target.checked ? 1 : 0)}
                    />{' '}
                    {category?.name_ar + '  ' + 'الزبائن'}
                  </Typography>
                )}
              />
            ))}
          </Stack>

          <Typography className='custom-style-label '>
            <User /> اسم المستخدم
          </Typography>

          <Controller
            name='user_name'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='johndoe'
                error={Boolean(errors.username)}
                {...(errors.username && { helperText: errors.username.message })}
              />
            )}
          />
          <Typography className='custom-style-label '>
            <Password /> كلمة السر
          </Typography>

          <Controller
            name='password'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='johndoe'
                error={Boolean(errors.username)}
                {...(errors.username && { helperText: errors.username.message })}
              />
            )}
          />

          <Divider sx={{ paddingBottom: '24px', paddingTop: '24px' }} />
          <Box sx={{ marginTop: '24PX' }}>
            <Typography sx={{ fontSize: '16px', fontWeight: '500', lineHeight: '160%', margin: '8px 0px' }}>
              مدير المبيعات المسؤول
            </Typography>
            {fields.map((field, index) => (
              <div key={field.id}>
                <Stack direction='row' alignItems='center' justifyContent='space-between'>
                  <Typography>فرع {index + 1}</Typography>
                  <Typography
                    style={{
                      cursor: 'pointer',
                      marginTop: '6px'
                    }}
                    onClick={() => {
                      remove(field.id)
                    }}
                  >
                    ازالة
                  </Typography>
                </Stack>
                <Controller
                  name={`branches.${index}.branch_id`}
                  control={control}
                  render={({ field: { value, onChange } }) =>
                    index == 0 ? (
                      <CustomTextField
                        fullWidth
                        value={branchName.data.name ? branchName.data.name : ''}
                        error={Boolean(errors?.branches?.[index]?.branch_id)}
                        onChange={setValue('branches[0].branch_id', branchName.data.id)}
                        helperText={errors?.branches?.[index]?.branch_id?.message || ' الفرع الاساسي للمندوب'}
                        InputProps={{
                          readOnly: true
                        }}
                        sx={{ marginBottom: '24px' }}
                      />
                    ) : (
                      <Autocomplete
                        options={StoreBranches?.data?.data || []}
                        fullWidth
                        id={`autocomplete-branch-${index}`}
                        getOptionLabel={option => option.name}
                        value={StoreBranches?.data?.data?.find(branch => branch.id === value) || null}
                        onChange={(event, newValue) => {
                          onChange(newValue ? newValue.id : '')
                          handaleChoos(newValue ? newValue.id : '')
                        }}
                        renderInput={params => (
                          <CustomTextField
                            {...params}
                            value={value ? value : ''}
                            fullWidth
                            placeholder='الفرع'
                            id={`validation-branch-${index}-select`}
                            aria-describedby={`validation-branch-${index}-select`}
                            error={Boolean(errors?.branches?.[index]?.branch_id)}
                            helperText={errors?.branches?.[index]?.branch_id?.message || ''}
                          />
                        )}
                      />
                    )
                  }
                />
                <Controller
                  name={`branches.${index}.salesManager_id`}
                  control={control}
                  render={({ field: { value, onChange } }) => (
                    <Autocomplete
                      options={store?.manager || []}
                      fullWidth
                      id={`autocomplete-sales-manager-${index}`}
                      getOptionLabel={option => option.name || selectSalesManager[index].name}
                      value={store?.manager?.find(manager => manager.id === value) || null}
                      sx={{ marginY: '14px' }}
                      onChange={(event, newValue) => {
                        onChange(newValue ? newValue.id : '')
                        SetSelectSalesManager(prevItems => {
                          const newItems = [...prevItems]
                          newItems[index] = { ...newItems[index], name: newValue ? newValue.name : '' }

                          return newItems
                        })
                      }}
                      renderInput={params => (
                        <CustomTextField
                          {...params}
                          fullWidth
                          value={value || ''}
                          sx={{ mb: '8px' }}
                          placeholder='مدير المبيعات'
                          id={`validation-sales-manager-${index}-select`}
                          aria-describedby={`validation-sales-manager-${index}-select`}
                          error={Boolean(errors?.branches?.[index]?.salesManager_id)}
                          helperText={errors?.branches?.[index]?.salesManager_id?.message || ''}
                        />
                      )}
                    />
                  )}
                />
              </div>
            ))}
            <Typography
              sx={{
                cursor: 'pointer',
                '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
              }}
              onClick={handleAddBranch}
            >
              + اضافة{' '}
            </Typography>
          </Box>
        </Stack>
      </Drawer>
    </FormProvider>
  )
}
