import { Avatar, Button, Divider, Drawer, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React from 'react'
import Address from '../../../../../public/images/cards/Location'

import City from '../../../../../public/images/cards/city'
import Phone from '../../../../../public/images/cards/Telephone'
import Delete from '../../../../../public/images/cards/deleteWihte'
import User from '../../../../../public/images/cards/user'
import Password from '../../../../../public/images/cards/password'
import { styled } from '@mui/material/styles'
import Edit from '../../../../../public/images/cards/edit'
import ArrowLeft from '../../../../../public/images/cards/arrowleft'
import FaceUser from '../../../../../public/images/cards/faceUser'
import { useSelector } from 'react-redux'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

export default function ShowData({ data, handleDeleteClick, handleEditClick, handleCloseDrawer }) {
  const { BranchDetails } = useSelector(state => state.branches)

  return (
    <>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header>
          <Stack direction={'row'} alignItems={'center'} sx={{ gap: '18px' }}>
            <Avatar
              sx={{
                width: 64,
                height: 64,
                fontSize: '24px',
                fontWeight: '700',
                backgroundColor: 'rgba(115, 163, 208, 0.25)',
                color: theme => theme.palette.accent.main
              }}
              src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
              alt='dlivia Sparks'
            >
              {data?.name?.slice(0, 1).toUpperCase()}
            </Avatar>
            <Typography className='custom-style-label2'>{data?.name}</Typography>
          </Stack>
        </Header>
        <Divider sx={{ marginY: '4px' }} />
        <Stack padding={'0px 24px'} direction={'column'}>
          <Box sx={{ mb: '24px' }}>
            <Typography className='custom-style-label '>
              <City /> {BranchDetails?.data?.city?.country?.name + ' - ' + BranchDetails?.data?.city?.name}
            </Typography>
          </Box>
          {data?.work_branches?.map((branch, i) => (
            <Box key={i} display={'flex'} flexDirection={'column'} gap={'24px'}>
              <Stack>
                <Stack direction={'row'} alignItems={'center'}>
                  <ArrowLeft />
                  <Typography className='custom-style-label2 '> الفرع</Typography>
                </Stack>
                <Typography className='custom-style-label2 '> {branch?.branch?.name}</Typography>
              </Stack>

              <Stack>
                <Stack direction={'row'} alignItems={'center'}>
                  <User />
                  <Typography className='custom-style-label2 '> مدير المبيعات المسؤول</Typography>
                </Stack>
                <Typography className='custom-style-label2 '> {branch?.sales_manager?.name}</Typography>
              </Stack>
              <Divider sx={{ marginY: '10px !important ' }} />
            </Box>
          ))}
        </Stack>
      </Drawer>
    </>
  )
}
