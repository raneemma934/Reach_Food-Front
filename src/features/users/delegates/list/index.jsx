import { Grid } from '@mui/material'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomCards from 'src/@core/components/custom-cards'
import EmptyData from 'src/@core/components/empty-data'
import { fetchData } from 'src/store/apps/user'

export default function List({ value, data }) {
  const type = 'delegates'

  return (
    <div>
      {data.length > 0 ? (
        <Grid container spacing={6}>
          {data &&
            data.map((client, index) => (
              <Grid item sm={4} xs={12} key={index}>
                <CustomCards value={value} client={client} type={type} />
              </Grid>
            ))}
        </Grid>
      ) : (
        <EmptyData desc={'لا يوجد مندوبين لعرضهم'} />
      )}
    </div>
  )
}
