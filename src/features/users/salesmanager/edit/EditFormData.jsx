import React, { useEffect, useState } from 'react'
import { Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import {
  Avatar,
  Button,
  Checkbox,
  Divider,
  Drawer,
  FormControlLabel,
  FormGroup,
  MenuItem,
  Typography
} from '@mui/material'
import { styled } from '@mui/material/styles'
import { Box, Stack } from '@mui/system'
import City from '../../../../../public/images/cards/city'
import CustomTextField from 'src/@core/components/mui/text-field'
import Phone from '../../../../../public/images/cards/Telephone'
import Delete from '../../../../../public/images/cards/deleteRed'
import Address from '../../../../../public/images/cards/Location'
import User from '../../../../../public/images/cards/user'

import Password from '../../../../../public/images/cards/password'
import { useDispatch, useSelector } from 'react-redux'
import { editUser, fetchDataSalesMan } from 'src/store/apps/user'
import EmptyData from 'src/@core/components/empty-data'
import DeleteRed from '../../../../../public/images/cards/deleteRed'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

export default function EditFormData({
  methods,
  handleCloseDrawer,
  handleEditClick,
  handleDeleteClick,
  data,
  fields,
  append,
  remove
}) {
  const [checked, setChecked] = useState(false)
  const [selected, setSelected] = useState([])

  const store = useSelector(state => state.user)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchDataSalesMan())
  }, [dispatch])

  const handleSelectAll = event => {
    const isChecked = event.target.checked
    setChecked(isChecked)
    if (isChecked) {
      setSelected(store?.SalesMan?.map(item => item.id))
    } else {
      setSelected([])
    }
  }

  const handleSelectItem = id => event => {
    const selectedIndex = selected.indexOf(id)
    let newSelected = []

    if (selectedIndex === -1) {
      newSelected = [...selected, id]
    } else {
      newSelected = selected.filter(item => item !== id)
    }

    setSelected(newSelected)
    setChecked(newSelected.length === store?.SalesMan?.length)
  }

  const { control, formState, getValues, setValue, watch } = methods

  const { errors } = formState

  function handleSaveUser() {
    setValue('id', data.id)
    dispatch(editUser(getValues()))
    handleCloseDrawer()
  }
  const { BranchDetails } = useSelector(state => state.branches)

  return (
    <FormProvider {...methods}>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header>
          <Avatar
            sx={{
              width: 64,
              height: 64,
              fontSize: '24px',
              fontWeight: '700',
              backgroundColor: 'rgba(115, 163, 208, 0.25)',
              color: theme => theme.palette.accent.main
            }}
            src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
            alt='dlivia Sparks'
          >
            {data?.name?.slice(0, 1).toUpperCase()}
          </Avatar>
          <Stack direction={'row'} alignItems={'center'} spacing={'24px'}>
            <Button
              size='small'
              onClick={handleSaveUser}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                color: '#ffff',
                fontSize: '16px',
                gap: '8px',
                backgroundColor: '#73A3D0',
                '&:hover': {
                  backgroundColor: '#73A3D0'
                }
              }}
            >
              حفظ
            </Button>
            <Button
              size='small'
              onClick={handleCloseDrawer}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                fontSize: '16px',

                backgroundColor: '#E0E0E0',
                '&:hover': {
                  backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.16)`
                }
              }}
            >
              إلغاء
            </Button>
          </Stack>
        </Header>
        <Divider sx={{ marginY: '4px' }} />

        <Stack padding={'24px'}>
          <Box>
            <Typography className='custom-style-label2'>
              <City /> {BranchDetails?.data?.city?.country?.name + ' - ' + BranchDetails?.data?.city?.name}
            </Typography>
          </Box>
          <Box>
            <Typography className='custom-style-label'>الاسم</Typography>
            <Controller
              name='name'
              control={control}
              rules={{ required: true }}
              render={({ field: { value, onChange } }) => (
                <CustomTextField
                  fullWidth
                  value={value}
                  onChange={onChange}
                  placeholder='John Doe'
                  error={Boolean(errors.fullName)}
                  {...(errors.fullName && { helperText: errors.fullName.message })}
                />
              )}
            />
          </Box>
          <Box>
            <Typography className='custom-style-label'>
              <Phone /> أرقام التواصل
            </Typography>

            {fields.map((field, index) => (
              <div key={field.id} style={{ marginBottom: '8px' }}>
                <Controller
                  name={`phone_number.[${index}]`}
                  control={control}
                  rules={{ required: 'Phone is required' }}
                  render={({ field: { value, onChange } }) => (
                    <CustomTextField
                      fullWidth
                      value={value}
                      sx={{ mb: '8px' }}
                      onChange={onChange}
                      error={Boolean(errors.emails?.[index]?.email)}
                      placeholder='0934343428'
                      {...(errors.emails?.[index]?.email && { helperText: errors.emails[index].email.message })}
                      InputProps={{
                        endAdornment: (
                          <Box>
                            {index > 0 ? (
                              <Box style={{ cursor: 'pointer', marginTop: '8px' }} onClick={() => remove(index)}>
                                <DeleteRed />
                              </Box>
                            ) : (
                              ''
                            )}
                          </Box>
                        )
                      }}
                    />
                  )}
                />
              </div>
            ))}
            <Typography
              sx={{
                cursor: 'pointer',
                '&:hover': { backgroundColor: 'transparent', color: '#73A3D0' }
              }}
              onClick={() => append({})}
              variant='text'
            >
              + اضافة رقم{' '}
            </Typography>
          </Box>

          <Typography className='custom-style-label '>
            <Address /> تفاصيل الموقع
          </Typography>

          <Controller
            name='location'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='Australia'
                error={Boolean(errors.country)}
                {...(errors.country && { helperText: errors.country.message })}
              />
            )}
          />
          <Typography className='custom-style-label '>
            <User /> اسم المستخدم
          </Typography>

          <Controller
            name='user_name'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='johndoe'
                error={Boolean(errors.username)}
                {...(errors.username && { helperText: errors.username.message })}
              />
            )}
          />
          <Typography className='custom-style-label '>
            <Password /> كلمة السر
          </Typography>

          <Controller
            name='password'
            control={control}
            rules={{ required: true }}
            render={({ field: { value, onChange } }) => (
              <CustomTextField
                fullWidth
                value={value}
                onChange={onChange}
                placeholder='كلمة المرور'
                error={Boolean(errors.username)}
                {...(errors.username && { helperText: errors.username.message })}
              />
            )}
          />

          <Typography className='custom-style-label '>المندوبين المسؤول عنهم </Typography>
          {store?.SalesMan.length > 0 ? (
            <div>
              <FormGroup row>
                <FormControlLabel
                  label='Select All'
                  control={<Checkbox checked={checked} onChange={handleSelectAll} />}
                />
              </FormGroup>
              {store?.SalesMan?.map(item => (
                <FormGroup row key={item.id}>
                  <FormControlLabel
                    label={item.name}
                    control={<Checkbox checked={selected.includes(item.id)} onChange={handleSelectItem(item.id)} />}
                  />
                </FormGroup>
              ))}
            </div>
          ) : (
            <EmptyData desc={'لايوجد مندوبين ضمن الفرع'} obj={'16px'} />
          )}
        </Stack>
      </Drawer>
    </FormProvider>
  )
}
