import { useEffect, useState } from 'react'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller, useFormContext, FormProvider, useFieldArray } from 'react-hook-form'
import { fetchData, fetchDataSalesMan } from 'src/store/apps/user'
import ShowData from './ShowData'
import EditFormData from './EditFormData'
import { useDispatch, useSelector } from 'react-redux'

const SidebarEditSalesManager = props => {
  const [isEditing, setIsEditing] = useState(false)

  const { open, handleCloseDrawer, handleDeleteClick, Editing, data } = props

  // console.log('🚀 ~ SidebarEditSalesManager ~ data:', data)
  const store = useSelector(state => state.user)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchDataSalesMan())
  }, [dispatch])

  const methods = useForm({
    defaultValues: {
      phone_number: data?.contacts?.map(contact => contact.phone_number) || [],
      customer_type: data.customer_type,
      address_id: data.address_id,
      user_name: data.user_name,
      name: data.name,
      location: data.location,
      password: data.user_password?.password
    },
    resolver: yupResolver(yup.Schema)
  })
  const { control, formState, getValues, watch } = methods

  const { errors } = formState

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'emails'
  })

  const handleEditClick = () => {
    setIsEditing(true)
  }

  useEffect(() => {
    if (fields.length === 0) {
      append({})
    }
  }, [fields, append])

  return (
    <>
      {!isEditing ? (
        <ShowData
          open={open}
          handleEditClick={handleEditClick}
          handleDeleteClick={handleDeleteClick}
          data={data}
          handleCloseDrawer={handleCloseDrawer}
        />
      ) : (
        <EditFormData
          methods={methods}
          open={open}
          handleEditClick={handleEditClick}
          handleDeleteClick={handleDeleteClick}
          data={data}
          handleCloseDrawer={handleCloseDrawer}
          fields={fields}
          append={append}
          remove={remove}
          errors={errors}
        />
      )}

      {Editing ? (
        <EditFormData
          methods={methods}
          open={open}
          handleEditClick={handleEditClick}
          handleDeleteClick={handleDeleteClick}
          data={data}
          handleCloseDrawer={handleCloseDrawer}
          fields={fields}
          append={append}
          remove={remove}
          errors={errors}
        />
      ) : (
        ''
      )}
    </>
  )
}

export default SidebarEditSalesManager
