import { Avatar, Button, Divider, Drawer, Typography } from '@mui/material'
import { Box, Stack } from '@mui/system'
import React from 'react'
import Address from '../../../../../public/images/cards/Location'

import City from '../../../../../public/images/cards/city'
import Phone from '../../../../../public/images/cards/Telephone'
import Delete from '../../../../../public/images/cards/deleteWihte'
import User from '../../../../../public/images/cards/user'
import Password from '../../../../../public/images/cards/password'
import { styled } from '@mui/material/styles'
import Edit from '../../../../../public/images/cards/edit'
import { FormProvider } from 'react-hook-form'
import { useSelector } from 'react-redux'

const Header = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(6),
  justifyContent: 'space-between'
}))

export default function ShowData({ data, handleDeleteClick, handleEditClick, handleCloseDrawer }) {
  const { BranchDetails } = useSelector(state => state.branches)

  return (
    <FormProvider>
      <Drawer
        open={open}
        anchor='right'
        variant='temporary'
        onClose={handleCloseDrawer}
        ModalProps={{ keepMounted: true }}
        sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 400 } } }}
      >
        <Header>
          <Avatar
            sx={{
              width: 64,
              height: 64,
              fontSize: '24px',
              fontWeight: '700',
              backgroundColor: 'rgba(115, 163, 208, 0.25)',
              color: theme => theme.palette.accent.main
            }}
            src={process.env.NEXT_PUBLIC_IMAGES + '/' + data?.image}
            alt='dlivia Sparks'
          >
            {data?.name?.slice(0, 1).toUpperCase()}
          </Avatar>
          <Stack direction={'row'} alignItems={'center'} sx={{ gap: '24px' }}>
            {/* <Button
              size='small'
              onClick={handleEditClick}
              sx={{
                p: '8px 24px',
                borderRadius: 1,

                fontSize: '16px',
                gap: '8px',
                boxShadow: '0px 0.8px 0.9px 0px ',
                backgroundColor: '#fff',

                '&:hover': {
                  backgroundColor: '#fff'
                }
              }}
            >
              تعديل
            </Button> */}
            <Box
              className='hover-edit'
              onClick={handleEditClick}
              sx={{
                p: '8px 12px',
                borderRadius: 1,
                gap: '8px',
                backgroundColor: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light,
                display: 'flex',
                alignItems: 'center',
                cursor: 'pointer',
                '&:hover': {
                  backgroundColor: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.base.dark : theme.palette.base.light
                }
              }}
            >
              <Edit />
              <Button
                size='small'
                sx={{
                  p: 0,
                  color: theme =>
                    theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light,
                  fontSize: '16px',
                  '&:hover': {
                    backgroundColor: 'transparent'
                  }
                }}
              >
                تعديل
              </Button>
            </Box>

            {/* <Button
              size='small'
              onClick={handleDeleteClick}
              sx={{
                p: '8px 24px',
                borderRadius: 1,
                fontSize: '16px',
                color: '#fff',
                backgroundColor: '#CE3446',
                '&:hover': {
                  backgroundColor: '#CE3446'
                }
              }}
            >
              <Delete /> حذف
            </Button> */}
            <Box
              onClick={handleDeleteClick}
              sx={{
                padding: '8px 12px',
                gap: '8px',
                borderRadius: 1,
                fontSize: '16px',
                color: '#fff',
                backgroundColor: '#CE3446',
                display: 'flex',
                alignItems: 'center',
                cursor: 'pointer',
                '&:hover': {
                  backgroundColor: '#CE3446'
                }
              }}
            >
              <Delete />

              <Button
                size='small'
                sx={{
                  fontSize: '16px',
                  p: 0,
                  color: '#fff',
                  '&:hover': {
                    backgroundColor: 'transparent',
                    color: '#fff'
                  }
                }}
              >
                حذف
              </Button>
            </Box>
          </Stack>
        </Header>
        <Divider sx={{ marginY: '4px' }} />
        <Stack padding={' 24px'} direction={'column'} sx={{ gap: '24px' }}>
          <Box>
            <Typography className='custom-style-label2 '>
              <City /> {BranchDetails?.data?.city?.country?.name + ' - ' + BranchDetails?.data?.city?.name}
            </Typography>
          </Box>

          <Box>
            <Typography className='custom-style-label2 '>الاسم</Typography>
            <Typography
              sx={{
                fontWeight: '400',
                fontSize: '14px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
              }}
            >
              {data?.name}
            </Typography>
          </Box>

          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <Typography className='custom-style-label2'>
              <Phone /> أرقام التواصل
            </Typography>
            <Typography>
              {data?.contacts && data.contacts.length > 0 ? (
                <ul>
                  {data.contacts.map((contact, index) => (
                    <li key={index} style={{ fontWeight: '400', fontSize: '14px', margin: '3px 0px' }}>
                      {contact.phone_number}
                    </li>
                  ))}
                </ul>
              ) : (
                'لا يوجد ارقام اتصال'
              )}
            </Typography>
          </Box>

          {/* <Box>
            <Typography className='custom-style-label '>
              <Address /> العنوان
            </Typography>
            <Typography sx={{ fontWeight: '400', fontSize: '14px',   color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light }}>
              {data?.user_details?.location}
            </Typography>
          </Box> */}

          <Stack sx={{ gap: '8px' }}>
            <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
              <Address />
              <Box>
                <Typography className='custom-style-label2 '> العنوان</Typography>
              </Box>
            </Stack>

            <Typography
              sx={{
                fontWeight: '400',
                fontSize: '14px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
              }}
            >
              {data?.location}
            </Typography>
          </Stack>

          {/* <Divider sx={{ marginY: '4px ' }} />
          <Box>
            <Typography className='custom-style-label '>
              <User /> اسم المستخدم
            </Typography>
            <Typography sx={{ fontWeight: '400', fontSize: '14px',   color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light }}>{data?.user_name}</Typography>
          </Box> */}

          <Stack sx={{ gap: '8px' }}>
            <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
              <User />
              <Box>
                <Typography className='custom-style-label2 '> أسم المستخدم</Typography>
              </Box>
            </Stack>

            <Typography
              sx={{
                fontWeight: '400',
                fontSize: '14px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
              }}
            >
              {data?.user_name}
            </Typography>
          </Stack>

          {/* <Typography className='custom-style-label '>
            <Password /> كلمة السر
          </Typography>
          <Typography sx={{ fontWeight: '400', fontSize: '14px',   color: theme =>
                      theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light }}>{data?.password}</Typography> */}

          <Stack sx={{ gap: '8px' }}>
            <Stack direction={'row'} alignItems={'center'} sx={{ gap: '8px' }}>
              <Password />
              <Box>
                <Typography className='custom-style-label2 '> كلمة السر</Typography>
              </Box>
            </Stack>
            <Typography
              sx={{
                fontWeight: '400',
                fontSize: '14px',
                color: theme =>
                  theme.palette.mode === 'dark' ? theme.palette.secondary.dark : theme.palette.secondary.light
              }}
            >
              {data?.user_password.password}
            </Typography>
          </Stack>
        </Stack>
      </Drawer>
    </FormProvider>
  )
}
