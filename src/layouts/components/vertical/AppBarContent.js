import { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Box, IconButton, Typography, Button } from '@mui/material'
import Icon from 'src/@core/components/icon'
import ModeToggler from 'src/@core/layouts/components/shared-components/ModeToggler'
import UserDropdown from 'src/@core/layouts/components/shared-components/UserDropdown'
import CustomTypographySection from 'src/@core/components/custom-title-section/Title'
import Map from '../../../../public/images/icons/map'
import { fetchbrancheDetailsById } from 'src/store/apps/branches'
import { useAuth } from 'src/hooks/useAuth'
import HelloCard from './helloCard'

const AppBarContent = ({ hidden, settings, saveSettings, toggleNavVisibility }) => {
  const dispatch = useDispatch()
  const { BranchDetails, status } = useSelector(state => state.branches)
  const title = useSelector(state => state.title)

  const [open, setOpen] = useState(false)

  const hasData = BranchDetails && BranchDetails.data

  const auth = useAuth()
  const { logout } = useAuth()

  const handleLogout = useCallback(() => {
    logout()
  }, [logout])

  useEffect(() => {
    if (!hasData) {
      dispatch(fetchbrancheDetailsById())
      setOpen(true)
    }
  }, [dispatch, hasData])

  useEffect(() => {
    if (hasData) {
      localStorage.setItem('branch', JSON.stringify(BranchDetails))
    }
  }, [BranchDetails, hasData])

  return (
    <Box sx={{ width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
      <Box className='actions-left' sx={{ mr: 2, display: 'flex', alignItems: 'center' }}>
        {hidden && !settings.navHidden && (
          <IconButton color='inherit' sx={{ ml: -2.75 }} onClick={toggleNavVisibility}>
            <Icon fontSize='1.5rem' icon='tabler:menu-2' />
          </IconButton>
        )}
        <CustomTypographySection text={title?.text} />
      </Box>

      <Box className='actions-right' sx={{ display: 'flex', alignItems: 'center' }}>
        {hasData ? (
          <Typography
            sx={{
              fontWeight: 500,
              fontSize: '16px',
              lineHeight: '32px',
              display: 'flex',
              alignItems: 'center',
              marginRight: '20px'
            }}
          >
            <Map />
            {`${BranchDetails.data.city.country.name} - ${BranchDetails.data.city.name} - ${BranchDetails.data.name}`}
          </Typography>
        ) : (
          <>
            {open ? <HelloCard open={open} setOpen={setOpen} /> : null}
            <Typography
              sx={{
                fontWeight: 500,
                fontSize: '16px',
                lineHeight: '32px',
                display: 'flex',
                alignItems: 'center',
                marginRight: '20px'
              }}
            >
              الرجاء اختيار فرع
            </Typography>
          </>
        )}
        <ModeToggler settings={settings} saveSettings={saveSettings} />
        {auth.user && auth.user.role !== 'sales manager' ? (
          <UserDropdown settings={settings} />
        ) : (
          <Button
            size='small'
            onClick={handleLogout}
            sx={{
              lineHeight: 'normal',
              p: '16px',
              borderRadius: 1,
              fontSize: '14px',
              color: '#fff',
              backgroundColor: '#D12E3D',
              '&:hover': { backgroundColor: '#D12E3D' }
            }}
          >
            تسجيل الخروج
          </Button>
        )}
      </Box>
    </Box>
  )
}

export default AppBarContent
