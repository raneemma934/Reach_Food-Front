import { Card, Grid, IconButton, Typography } from '@mui/material'
import Button, { ButtonProps } from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import { Box, color, fontWeight } from '@mui/system'
import Dialog from '@mui/material/Dialog'
import { DataGrid } from '@mui/x-data-grid'
import CustomChip from 'src/@core/components/mui/chip'
import CustomAvatar from 'src/@core/components/mui/avatar'
import CloseIcon from '@mui/icons-material/Close'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import { useDispatch } from 'react-redux'
import { setOpenDrawer } from 'src/store/apps/user'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'

function HelloCard({ open, setOpen }) {
  const dispatch = useDispatch()

  const BootstrapButton = styled(Button)({
    boxShadow: 'none',
    backgroundColor: '#73A3D0',
    '&:hover': {
      backgroundColor: '#73A3D0'
    }
  })

  const handleClick = () => {
    dispatch(setOpenDrawer(true))
    setOpen(false)
  }

  return (
    <>
      <Dialog
        open={open}
        sx={{
          '& .MuiPaper-elevation': { overflow: 'visible', transform: 'translateY(34px)' },
          '& .MuiDialogActions-root': { justifyContent: 'center', gap: 3 }
        }}
        onClose={() => setOpen(false)}
      >
        <DialogContent>
          <Box sx={{ position: 'absolute', top: '-121px', left: '193px' }}>
            <img src='/images/products/hey-01.png' alt='' style={{ width: '210px' }} />
          </Box>
          <Typography
            sx={{
              textAlign: 'center',
              fontSize: '20px',
              fontWeight: 600,
              fontFamily: 'Noto Kufi Arabic',
              color: 'black',
              mb: '20px'
            }}
          >
            مرحباً!
          </Typography>
          <Typography
            sx={{
              textAlign: 'left',
              fontSize: '15px',
              fontWeight: 400,
              fontFamily: 'Noto Kufi Arabic',
              lineHeight: '160%'
            }}
          >
            أهلاً بك في {<span style={{ color: '#73A3D0' }}>"اسم التطبيق"</span>}! للبدء، يرجى إكمال الخطوة الأولى، وهي
            إضافة فرع لشركتك، حيث سيساعدك على اتكوين الإعدادات الأساسية اللازمة لتشغيل النظام بسلاسة.{<br />}سنقوم
            يتحديد المدينة التي يتواجد بها الفرع، ومن ثم التصنيفات التي يختص هذا الفرع ببيعها وتوزيعها، مثلاً (فرع مدينة
            دمشق - الغذائيات).{<br />}بعد إكمال هذه الخطوة، سنواصل باقي الإجراءات المطلوبة لإعداد النظام.{<br />}نحن هنا
            لمساعدتك في كل خطوة. اضغط على "إضافة فرع" للبدء.
          </Typography>
        </DialogContent>
        <DialogActions>
          <BootstrapButton
            variant='contained'
            sx={{
              p: '8px 32px',
              fontSize: '16px',
              gap: '8px',
              fontWeight: 500,
              fontFamily: 'Noto Kufi Arabic'
            }}
            onClick={handleClick}
          >
            إضافة فرع
            <ArrowBackIcon />
          </BootstrapButton>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default HelloCard
