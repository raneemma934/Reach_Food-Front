import UsersIcon from '../../../public/images/icons/Users'
import ArchiveRequest from '../../../public/images/icons/ArchiveRequest'
import Complins from '../../../public/images/icons/complins'
import Notefications from '../../../public/images/icons/Notefications'
import FlightTracking from '../../../public/images/icons/FlightTracking'
import Requests from '../../../public/images/icons/Requests'
import PriceList from '../../../public/images/icons/PriceList'
import Products from '../../../public/images/icons/Products'
import Flights from '../../../public/images/icons/Flights'
import Branches from '../../../public/images/icons/Branches'
import BranchManagers from '../../../public/images/icons/branchManagers'
import { useDispatch, useSelector } from 'react-redux'

const navigation = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks

  // eslint-disable-next-line react-hooks/rules-of-hooks

  // eslint-disable-next-line react-hooks/rules-of-hooks

  return [
    {
      icon: <UsersIcon />,
      title: 'المستخدمين',
      path: '/users',
      subject: 'users'
    },
    {
      icon: <Flights />,
      title: 'الرحلات',
      path: '/flights',
      subject: 'trips'
    },
    {
      icon: <Products />,
      title: 'المنتجات',
      path: '/products',
      subject: 'products'
    },
    {
      icon: <PriceList />,
      title: 'قائمة الأسعار',
      path: '/listprices',
      subject: 'listprices'
    },
    {
      icon: <Requests />,
      title: 'الطلبات الحالية ',
      path: '/requests',
      subject: 'requests'
    },
    {
      icon: <ArchiveRequest />,
      title: 'أرشيف الطلبات ',
      path: '/archive',
      subject: 'archive'
    },
    {
      icon: <FlightTracking />,
      title: 'تتبع الرحلات ',
      path: '/flighttracking',
      subject: 'flighttracking'
    },
    {
      icon: <Notefications />,
      title: 'الإشعارات  ',
      path: '/notifications',
      subject: 'notifications'
    },
    {
      icon: <Complins />,
      title: 'الشكاوي  ',
      path: '/complaints',
      subject: 'complaints'
    },
    {
      sectionTitle: 'الإدارة'
    },
    {
      icon: <Branches />,
      title: 'الأفرع',
      path: '/branches'
    },

    {
      icon: <BranchManagers />,
      title: 'مدراء الأفرع ',
      path: '/branchManagers'
    }
  ]
}

export default navigation
