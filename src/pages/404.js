import { styled } from '@mui/material/styles'
import { Button, Grid, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'
import Link from 'next/link'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { useTheme } from '@emotion/react'

// ** Layout Import
import BlankLayout from 'src/@core/layouts/BlankLayout'

// ** Styled Components
const BoxWrapper = styled(Box)(({ theme }) => ({
  [theme.breakpoints.down('md')]: {
    width: '90vw'
  }
}))

const Error404 = () => {
  const theme = useTheme()

  return (
    <Box className='content-center'>
      <Box
        sx={{
          p: 5,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          height: '100%'
        }}
      >
        <Grid container spacing={2}>
          <Grid
            item
            xs={6}
            sx={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'end',
              textAlign: 'center'
            }}
          >
            <img width={350} alt='error-illustration' src='/images/icons/notFound.png' />
          </Grid>
          <Grid
            item
            xs={6}
            sx={{
              pl: '24px !important',

              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'start'
            }}
          >
            <Typography
              variant='h2'
              sx={{ fontSize: '70px', mb: 1, fontWeight: 600, color: '#3B3B3B', fontFamily: 'Noto Kufi Arabic' }}
            >
              404!
            </Typography>
            <Typography
              sx={{ fontWeight: 500, mb: 4, fontSize: '30px', color: 'black', fontFamily: 'Noto Kufi Arabic' }}
            >
              المعذرة، هذه الصفحة غير موجودة.
            </Typography>
            <Button
              href='/users'
              component={Link}
              variant='contained'
              sx={{
                fontWeight: 500,
                backgroundColor: theme.palette.accent.main,
                fontFamily: 'Noto Kufi Arabic',
                p: '12px',
                fontSize: '18px',
                '&:hover': {
                  backgroundColor: theme.palette.accent.main
                }
              }}
              endIcon={<ArrowBackIcon />}
            >
              {`العودة إلى الصفحة الرئيسية `}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  )
}
Error404.getLayout = page => <BlankLayout>{page}</BlankLayout>

export default Error404
