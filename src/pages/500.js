// ** Next Import
import Link from 'next/link'

// ** MUI Components
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'

// ** Layout Import

import BlankLayout from 'src/@core/layouts/BlankLayout'
import { Grid } from '@mui/material'

const Error500 = () => {
  return (
    <Box
      sx={{
        p: 5,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%'
      }}
    >
      <Grid container spacing={2}>
        <Grid
          item
          xs={6}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'end',
            textAlign: 'center'
          }}
        >
          <img width={350} alt='error-illustration' src='/images/icons/notFound.png' />
        </Grid>
        <Grid
          item
          xs={6}
          sx={{
            pl: '24px !important',

            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'start'
          }}
        >
          <Typography
            variant='h2'
            sx={{ fontSize: '96px', mb: '12px', fontWeight: 600, color: '#3B3B3B', fontFamily: 'Noto Kufi Arabic' }}
          >
            500!
          </Typography>
          <Typography
            sx={{ fontWeight: 500, mb: '24px', fontSize: '30px', color: 'black', fontFamily: 'Noto Kufi Arabic' }}
          >
            عذرا، حدث خطأ ما
          </Typography>
          <Button
            href='/'
            component={Link}
            variant='contained'
            sx={{ fontWeight: 500, fontFamily: 'Noto Kufi Arabic', p: '12px 16px', fontSize: '16px' }}
            endIcon={<ArrowBackIcon />}
          >
            {`العودة إلى الصفحة الرئيسية `}
          </Button>
        </Grid>
      </Grid>
    </Box>
  )
}
Error500.getLayout = page => <BlankLayout>{page}</BlankLayout>

export default Error500
