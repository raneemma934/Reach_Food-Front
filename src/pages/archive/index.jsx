// ** React Imports
import { useEffect, useState, useCallback } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import { DataGrid } from '@mui/x-data-grid'
import CustomArchiveTabs from 'src/@core/components/custom-archiv-tab'
import { useDispatch } from 'react-redux'
import { handleSelectTitle } from 'src/store/apps/title'
import { TabContext, TabPanel } from '@mui/lab'
import dynamic from 'next/dynamic'

// Dynamically import the TableArchive component
const TableArchive = dynamic(() => import('src/features/archive/DataGrid'), { ssr: false })

const TableServerSide = () => {
  const [value, setValue] = useState('1')
  const dispatch = useDispatch()

  const handleChange = useCallback((event, newValue) => {
    setValue(newValue)
  }, [])

  useEffect(() => {
    dispatch(handleSelectTitle('أرشيف الطلبات'))
  }, [dispatch])

  return (
    <TabContext value={value}>
      <CustomArchiveTabs value={value} handleChange={handleChange} />
      <TabPanel value='1' sx={{ p: '24px 0 0 0 ' }}>
        <TableArchive status='' />
      </TabPanel>
      <TabPanel value='2' sx={{ p: '24px 0 0 0 ' }}>
        <TableArchive status='status=delivered' />
      </TabPanel>
      <TabPanel value='3' sx={{ p: '24px 0 0 0 ' }}>
        <TableArchive status='status=canceled' />
      </TabPanel>
    </TabContext>
  )
}

TableServerSide.acl = {
  action: 'manage',
  subject: 'archive'
}

export default TableServerSide
