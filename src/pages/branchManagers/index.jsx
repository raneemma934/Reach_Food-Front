import { Box } from '@mui/system'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomFilterBranchManagers from 'src/@core/components/custom-filterBranchManagers'
import List from 'src/features/branchManagers/list'
import { handleSelectTitle } from 'src/store/apps/title'
import { FormProvider, useForm } from 'react-hook-form'
import SidebarAddBranchManagers from 'src/features/branchManagers/add/AddBranchManagersDrawer'
import { fetchbranchesByCity } from 'src/store/apps/branches'
import { fetchData } from 'src/store/apps/user'

export default function BranchManagers() {
  const [addUserOpen, setAddUserOpen] = useState(false)

  const { data, status, error } = useSelector(state => state.user)

  const toggleAddUserDrawer = () => setAddUserOpen(!addUserOpen)
  const [filterData, setFilterData] = useState([])

  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(handleSelectTitle('مدراء الأفرع'))
    dispatch(fetchData('role=admin'))
  }, [dispatch])

  const methods = useForm({
    mode: 'onChange'
  })

  return (
    <>
      <Box sx={{ padding: '24px 0px' }}>
        <CustomFilterBranchManagers toggle={toggleAddUserDrawer} setFilterData={setFilterData} data={data} />
      </Box>
      <List data={filterData} />
      <FormProvider {...methods}>
        <SidebarAddBranchManagers open={addUserOpen} toggle={toggleAddUserDrawer} />
      </FormProvider>
    </>
  )
}
