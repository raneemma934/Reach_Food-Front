import { Box } from '@mui/system'
import React, { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomTabsBranches from 'src/@core/components/custom-branchs-tab'
import List from 'src/features/branches/list'
import { fetchbranchesByCity } from 'src/store/apps/branches'
import { handleSelectTitle } from 'src/store/apps/title'

export default function Branches() {
  const dispatch = useDispatch()
  const [filterData, setFilterData] = useState([])

  // console.log('🚀 ~ Branches ~ filterData:', filterData)
  const { branchesByCity, status, error } = useSelector(state => state.branches)

  useEffect(() => {
    dispatch(handleSelectTitle('الأفرع'))
    dispatch(fetchbranchesByCity())
  }, [dispatch])

  return (
    <>
      <CustomTabsBranches setFilterData={setFilterData} data={branchesByCity.data} />
      <Box sx={{ padding: '24px 0px' }}>
        <List data={filterData} />
      </Box>
    </>
  )
}
