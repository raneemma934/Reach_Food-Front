import React, { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import { useDispatch, useSelector } from 'react-redux'
import CustomComplaintsTabs from 'src/@core/components/custom-complaints'
import { Box } from '@mui/system'
import { handleSelectTitle } from 'src/store/apps/title'
import { fetchFeedback } from 'src/store/apps/complaints'
import List from 'src/features/complaints/list'

export default function Complaints() {
  const dispatch = useDispatch()
  const { data, status, error } = useSelector(state => state.feedback)
  const [filterData, setFilterData] = useState([])

  useEffect(() => {
    dispatch(handleSelectTitle('الشكاوي'))
    dispatch(fetchFeedback())
  }, [dispatch])

  return (
    <>
      <CustomComplaintsTabs setFilterData={setFilterData} data={data?.data} />
      <Box sx={{ padding: '24px 0px' }}>
        <List data={filterData} />
      </Box>
    </>
  )
}

Complaints.acl = {
  action: 'manage',
  subject: 'complaints'
}
