import React, { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Box } from '@mui/system'
import { TabContext, TabPanel } from '@mui/lab'

import CustomFilterFlights from 'src/@core/components/custom-fillter-flights/CustomFilterFlights'
import CustomFlightsTabs from 'src/@core/components/custom-flight-tab'
import List from 'src/features/flights/list'
import { handleSelectTitle } from 'src/store/apps/title'
import { fetchTrips } from 'src/store/apps/trips'

const days = ['Saturday', 'Sunday', 'Monday', 'Thursday', 'Wednesday', 'Tuesday', 'Friday']

const FlightList = React.memo(({ type, data, handleSelectDelete, SelectDelete }) => (
  <Box sx={{ padding: '24px 0px' }}>
    <List type={type} data={data} handleSelectDelete={handleSelectDelete} SelectDelete={SelectDelete} />
  </Box>
))

const Flights = () => {
  const [value, setValue] = useState('1')
  const [SelectDelete, setSelectDelete] = useState(false)
  const [Select, setSelect] = useState(false)
  const [filterData, setFilterData] = useState([])

  const store = useSelector(state => state.trips)
  const dispatch = useDispatch()

  const handleSelectDelete = event => {
    if (event) {
      event.stopPropagation()
    }
    setSelectDelete(prev => !prev)
  }

  const handleSelect = () => {
    setSelect(prev => !prev)
  }

  const handleChange = (event, newValue) => {
    if (newValue >= 1 && newValue <= 7) {
      dispatch(fetchTrips(`day=${days[newValue - 1]}`))
    }
    setValue(newValue)
  }

  useEffect(() => {
    dispatch(fetchTrips('day=Saturday'))
    dispatch(handleSelectTitle(' الرحلات'))
  }, [dispatch])

  const tabPanels = useMemo(
    () =>
      days.map((day, index) => (
        <TabPanel key={day} value={`${index + 1}`}>
          <FlightList
            type={day}
            data={filterData}
            handleSelectDelete={handleSelectDelete}
            SelectDelete={SelectDelete}
          />
        </TabPanel>
      )),
    [filterData, SelectDelete]
  )

  return (
    <>
      <Box sx={{ paddingBottom: '24px' }}>
        <CustomFilterFlights
          handleSelectDelete={handleSelectDelete}
          SelectDelete={SelectDelete}
          data={store?.trips?.data}
          setFilterData={setFilterData}
        />
      </Box>

      <TabContext value={value}>
        <CustomFlightsTabs value={value} handleChange={handleChange} handleSelect={handleSelect} Select={Select} />
        {tabPanels}
      </TabContext>
    </>
  )
}

Flights.acl = {
  action: 'manage',
  subject: 'trips'
}

export default Flights
