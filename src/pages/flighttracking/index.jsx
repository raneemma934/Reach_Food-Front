import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { handleSelectTitle } from 'src/store/apps/title'
import TableFlightTracking from 'src/features/flighttracking/DataGird'

export default function FlightTracking() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(handleSelectTitle('تتبع الرحلات'))
  }, [dispatch])

  return <TableFlightTracking />
}

FlightTracking.acl = {
  action: 'manage',
  subject: 'flighttracking'
}
