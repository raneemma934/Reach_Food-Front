import React, { useEffect } from 'react'
import dynamic from 'next/dynamic'
import { useDispatch } from 'react-redux'
import { handleSelectTitle } from 'src/store/apps/title'
import { FormProvider, useForm } from 'react-hook-form'

//change the loading .....
const TablePrices = dynamic(() => import('src/features/ListPrices/DataGird'), {
  loading: () => <p></p>
})

const Index = React.memo(() => {
  const dispatch = useDispatch()

  const methods = useForm({
    mode: 'onChange'
  })

  useEffect(() => {
    dispatch(handleSelectTitle('قائمة الأسعار'))
  }, [dispatch])

  return (
    <FormProvider {...methods}>
      <TablePrices />
    </FormProvider>
  )
})
Index.acl = {
  action: 'manage',
  subject: 'listprices'
}

export default Index
