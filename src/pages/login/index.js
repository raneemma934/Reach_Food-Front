// ** React Imports
import { useState } from 'react'

// ** Next Imports
import Link from 'next/link'

// ** MUI Components
import Alert from '@mui/material/Alert'
import Button from '@mui/material/Button'
import Divider from '@mui/material/Divider'
import Checkbox from '@mui/material/Checkbox'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'
import Box from '@mui/material/Box'
import useMediaQuery from '@mui/material/useMediaQuery'
import { styled, useTheme } from '@mui/material/styles'
import InputAdornment from '@mui/material/InputAdornment'
import MuiFormControlLabel from '@mui/material/FormControlLabel'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

// ** Third Party Imports
import * as yup from 'yup'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

// ** Hooks
import { useAuth } from 'src/hooks/useAuth'
import useBgColor from 'src/@core/hooks/useBgColor'
import { useSettings } from 'src/@core/hooks/useSettings'

// ** Configs
import themeConfig from 'src/configs/themeConfig'

// ** Layout Import
import BlankLayout from 'src/@core/layouts/BlankLayout'

// ** Demo Imports
import Animations from 'src/@core/components/animations'
import { Stack } from '@mui/system'
import Password from '../../../public/images/cards/password'
import Email from '../../../public/images/cards/email'

// ** Styled Components
const LoginIllustration = styled('img')(({ theme }) => ({
  zIndex: 2,
  maxHeight: 680,
  marginTop: theme.spacing(12),
  marginBottom: theme.spacing(12),
  [theme.breakpoints.down(1540)]: {
    maxHeight: 550
  },
  [theme.breakpoints.down('lg')]: {
    maxHeight: 500
  }
}))

const RightWrapper = styled(Box)(({ theme }) => ({
  width: '100%',
  [theme.breakpoints.up('md')]: {
    maxWidth: 450
  },
  [theme.breakpoints.up('lg')]: {
    maxWidth: 700
  },
  [theme.breakpoints.up('xl')]: {
    maxWidth: 750
  }
}))

const schema = yup.object().shape({
  user_name: yup.string().required(),
  password: yup.string().min(5).required()
})

const defaultValues = {
  password: 'password',
  user_name: 'admin01'
}

const LoginPage = () => {
  const [rememberMe, setRememberMe] = useState(true)
  const [showPassword, setShowPassword] = useState(false)

  // ** Hooks
  const auth = useAuth()
  const theme = useTheme()
  const bgColors = useBgColor()
  const { settings } = useSettings()
  const hidden = useMediaQuery(theme.breakpoints.down('md'))

  // ** Vars
  const { skin } = settings

  const {
    control,
    setError,
    handleSubmit,
    formState: { errors }
  } = useForm({
    defaultValues,
    mode: 'onBlur',
    resolver: yupResolver(schema)
  })

  const onSubmit = data => {
    const { user_name, password } = data
    auth.login({ user_name, password, rememberMe }, () => {
      setError('user_name', {
        type: 'manual',
        message: 'الرجاء ادخال بريد الكتروني صحيح'
      })
      setError('password', {
        type: 'manual',
        message: 'الرجاء ادخال كلمة مرور صحيحة'
      })
    })
  }

  return (
    <Box className='content-right' sx={{ backgroundColor: 'background.paper', height: '10vh' }}>
      <RightWrapper sx={{ padding: '64px 96px', width: '100%' }}>
        <Stack direction={'column'} justifyContent={'space-between'} sx={{ gap: '30px' }}>
          <Box>
            <Typography sx={{ fontSize: '40px', color: '#73A3D0', fontWeight: '700' }}>اللوغو والاسم</Typography>
          </Box>
          <Stack
            sx={{
              p: [8, 12],
              height: '100%'
            }}
          >
            <Box>
              <Typography sx={{ color: '#3B3B3B', fontSize: '24px', fontWeight: '600', mb: '40px' }}>
                تسجيل الدخول
              </Typography>
              <Box sx={{ width: '100%', maxWidth: 400 }}>
                <form noValidate autoComplete='off' onSubmit={handleSubmit(onSubmit)}>
                  <Box sx={{ mb: 5 }}>
                    <Typography className='custom-style-label ' sx={{ margin: '8px 0px !important' }}>
                      <Email /> البريد الألكتروني
                    </Typography>
                    <Controller
                      name='user_name'
                      control={control}
                      rules={{ required: true }}
                      render={({ field: { value, onChange, onBlur } }) => (
                        <CustomTextField
                          fullWidth
                          autoFocus
                          value={value}
                          sx={{ backgroundColor: '#E0E0E0' }}
                          onBlur={onBlur}
                          onChange={onChange}
                          placeholder='admin@vuexy.com'
                          error={Boolean(errors.user_name)}
                          {...(errors.user_name && { helperText: errors.user_name.message })}
                        />
                      )}
                    />
                  </Box>
                  <Box sx={{ mb: 1.5 }}>
                    <Typography className='custom-style-label ' sx={{ margin: '8px 0px !important' }}>
                      <Password /> كلمة المرور
                    </Typography>

                    <Controller
                      name='password'
                      control={control}
                      rules={{ required: true }}
                      render={({ field: { value, onChange, onBlur } }) => (
                        <CustomTextField
                          fullWidth
                          value={value}
                          onBlur={onBlur}
                          onChange={onChange}
                          sx={{ backgroundColor: '#E0E0E0' }}
                          id='auth-login-v2-password'
                          error={Boolean(errors.password)}
                          {...(errors.password && { helperText: errors.password.message })}
                          type={showPassword ? 'text' : 'password'}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position='end'>
                                <IconButton
                                  edge='end'
                                  onMouseDown={e => e.preventDefault()}
                                  onClick={() => setShowPassword(!showPassword)}
                                >
                                  <Icon fontSize='1.25rem' icon={showPassword ? 'tabler:eye' : 'tabler:eye-off'} />
                                </IconButton>
                              </InputAdornment>
                            )
                          }}
                        />
                      )}
                    />
                    <Box
                      sx={{
                        borderRadius: '8px',
                        padding: '6px 16px 6px 16px ',
                        backgroundColor: '#73A3D0',
                        my: '35px',
                        cursor: 'pointer'
                      }}
                    >
                      <Button fullWidth type='submit' sx={{ color: '#F2F4F8', padding: '0px' }}>
                        تسجيل الدخول
                      </Button>
                    </Box>
                  </Box>
                </form>
              </Box>
            </Box>
          </Stack>
        </Stack>
        <Stack direction={'column'} spacing={2} sx={{ marginTop: '55px' }}>
          <Typography
            sx={{
              fontSize: '14px',
              lineHeight: '25px',
              fontWeight: '400',
              color: '#5A5A5A'
            }}
          >
            تم تطويره من قبل شركة codeshield للحلول البرمجية
          </Typography>
          <Typography
            sx={{
              fontSize: '14px',
              lineHeight: '25px',
              fontWeight: '400',
              color: '#5A5A5A'
            }}
          >
            www.codeshield.com{' '}
          </Typography>
        </Stack>
      </RightWrapper>

      {!hidden ? <Animations /> : null}
    </Box>
  )
}
LoginPage.getLayout = page => <BlankLayout>{page}</BlankLayout>
LoginPage.guestGuard = true

export default LoginPage
