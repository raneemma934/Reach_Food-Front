import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import TableNotifications from 'src/features/notifications/DataGird'
import { handleSelectTitle } from 'src/store/apps/title'

export default function Notifications() {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(handleSelectTitle('الإشعارات'))
  }, [dispatch])

  return (
    <>
      <TableNotifications />
    </>
  )
}

Notifications.acl = {
  action: 'manage',
  subject: 'notifications'
}
