import { Box } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CustomFilterProducts from 'src/@core/components/custom-fillter-products'
import ScrollingPagination from 'src/@core/components/ScrollingPagination'
import List from 'src/features/products/list'
import { fetchPriceProducts, fetchProducts, setEdit } from 'src/store/apps/products'
import { handleSelectTitle } from 'src/store/apps/title'

const Products = React.memo(() => {
  const dispatch = useDispatch()
  const [filterData, setFilterData] = useState([])
  const { data, isEdit, status, error } = useSelector(state => state.products)

  const [allData, setAllData] = useState([])

  const [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    dispatch(handleSelectTitle('المنتجات'))
    dispatch(fetchPriceProducts())
  }, [dispatch])

  return (
    <div>
      <Box sx={{ padding: '24px 0px' }}>
        <CustomFilterProducts setFilterData={setFilterData} data={data.data} />
      </Box>
      <ScrollingPagination data={data} currentPage={currentPage} setCurrentPage={setCurrentPage} />
      <List data={filterData} />
    </div>
  )
})

Products.acl = {
  action: 'manage',
  subject: 'products'
}

export default Products
