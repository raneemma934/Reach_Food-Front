import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import DetailsProducts from 'src/features/requests/detalis'
import { fetchRequestsById } from 'src/store/apps/requests'

export default function Detalis() {
  const router = useRouter()
  const { id } = router.query
  const dispatch = useDispatch()
  const store = useSelector(state => state.request)

  useEffect(() => {
    dispatch(fetchRequestsById(id))
  }, [dispatch, id])

  return <DetailsProducts data={store?.order?.data} />
}
