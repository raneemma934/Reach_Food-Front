import React, { useEffect, useState } from 'react'
import List from 'src/features/requests/all-requests/list'
import { useParams } from 'next/navigation'
import { Box } from '@mui/system'
import CustomFilterRequests from 'src/@core/components/custom-fillter-requests'
import { fetchRequests } from 'src/store/apps/requests'
import { useDispatch, useSelector } from 'react-redux'

export default function AllRequests() {
  const dispatch = useDispatch()
  const store = useSelector(state => state.request)
  const [filterData, setFilterData] = useState()

  // console.log('🚀 ~ AllRequests ~ filterData:', filterData)

  useEffect(() => {
    dispatch(fetchRequests())
  }, [dispatch])

  return (
    <>
      <Box sx={{ padding: '0 0 24px 0' }}>
        {' '}
        <CustomFilterRequests data={store?.data?.data?.data} setFilterData={setFilterData} />
      </Box>
      <List data={filterData} />
    </>
  )
}
