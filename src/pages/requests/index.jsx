import React, { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import CustomTabs from 'src/@core/components/custom-tabs'
import { Box } from '@mui/system'
import { TabContext, TabPanel } from '@mui/lab'
import { useDispatch } from 'react-redux'
import { handleSelectTitle } from 'src/store/apps/title'
import CustomFilterRequests from 'src/@core/components/custom-fillter-requests'
import CustomRequestsTabs from 'src/@core/components/custom-request-tabs'
import EmptyData from 'src/@core/components/empty-data/index.js'
import AllRequests from './all-requests'
import AcceptedRequests from './accepted-requests'
import RejectedRequests from './rejected-requests'

// Memoize each tab content component
const MemoizedAllRequests = React.memo(AllRequests)
const MemoizedAcceptedRequests = React.memo(AcceptedRequests)
const MemoizedRejectedRequests = React.memo(RejectedRequests)

const Requests = React.memo(() => {
  const [value, setValue] = useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(handleSelectTitle('الطلبات الحالية'))
  }, [dispatch])

  return (
    <TabContext value={value}>
      <CustomRequestsTabs value={value} handleChange={handleChange} />
      <TabPanel sx={{ p: '24px 0 0 0 ' }} value='1'>
        <MemoizedAllRequests />
      </TabPanel>
      <TabPanel sx={{ p: '24px 0 0 0 ' }} value='2'>
        <MemoizedAcceptedRequests />
      </TabPanel>
      <TabPanel sx={{ p: '24px 0 0 0 ' }} value='3'>
        <MemoizedRejectedRequests />
      </TabPanel>
    </TabContext>
  )
})

Requests.acl = {
  action: 'manage',
  subject: 'requests'
}

export default Requests
