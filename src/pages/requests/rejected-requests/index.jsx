import React, { useEffect, useState } from 'react'
import List from 'src/features/requests/rejected-requests/list'
import CustomFilterRequests from 'src/@core/components/custom-fillter-requests'
import { Box } from '@mui/system'
import { fetchRequests } from 'src/store/apps/requests'
import { useDispatch, useSelector } from 'react-redux'

const MemoizedCustomFilterRequests = React.memo(CustomFilterRequests)

export default function RejectedRequests() {
  const dispatch = useDispatch()
  const store = useSelector(state => state.request)
  const [filterData, setFilterData] = useState([])

  useEffect(() => {
    dispatch(fetchRequests('status=canceled'))
  }, [dispatch])

  return (
    <>
      <Box sx={{ padding: '0 0 24px 0' }}>
        {' '}
        <MemoizedCustomFilterRequests data={store?.data?.data?.data} setFilterData={setFilterData} />
      </Box>
      <List data={filterData} />
    </>
  )
}
