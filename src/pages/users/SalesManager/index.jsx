import { Box } from '@mui/system'
import { useEffect, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useDispatch, useSelector } from 'react-redux'
import CustomFilterUsers from 'src/@core/components/custom-fillter-users'
import SidebarAddClients from 'src/features/users/clients/add/AddClientsDrawer'
import List from 'src/features/users/salesmanager/list'
import { fetchData } from 'src/store/apps/user'

export default function SalesManager() {
  const [addUserOpen, setAddUserOpen] = useState(false)
  const toggleAddUserDrawer = () => setAddUserOpen(!addUserOpen)
  const dispatch = useDispatch()
  const { data, status, error } = useSelector(state => state.user)
  const [filterData, setFilterData] = useState([])

  const methods = useForm({
    mode: 'onChange'
  })

  useEffect(() => {
    const branch_id = localStorage.getItem('branch_id')
    dispatch(fetchData(`role=sales manager&branch_id=${branch_id}`))
  }, [dispatch])

  return (
    <>
      <Box sx={{ padding: '24px 0px' }}>
        <CustomFilterUsers toggle={toggleAddUserDrawer} setFilterData={setFilterData} data={data} />
      </Box>
      <List data={filterData} />

      <FormProvider {...methods}>
        <SidebarAddClients open={addUserOpen} toggle={toggleAddUserDrawer} />
      </FormProvider>
    </>
  )
}
