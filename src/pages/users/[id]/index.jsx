import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import DetailsDelegates from 'src/features/users/delegates/detalis'
import { fetchUserById } from 'src/store/apps/user'

export default function DetalisDelegates() {
  const router = useRouter()
  const { id } = router.query
  const dispatch = useDispatch()
  const { user, status, error } = useSelector(state => state.user)
  console.log('🚀 ~ DetalisDelegates ~ user:', user)

  useEffect(() => {
    dispatch(fetchUserById(id))
  }, [dispatch, id])

  return <DetailsDelegates data={user} />
}
