import { Box } from '@mui/material'
import React, { useState, useCallback, useEffect } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import SidebarAddClients from 'src/features/users/clients/add/AddClientsDrawer'

import CustomFilterUsers from 'src/@core/components/custom-fillter-users'
import List from 'src/features/users/clients/list'
import { useDispatch, useSelector } from 'react-redux'
import { fetchData } from 'src/store/apps/user'
import Error from 'src/pages/error'

const Clients = () => {
  const [addUserOpen, setAddUserOpen] = useState(false)
  const toggleAddUserDrawer = useCallback(() => setAddUserOpen(prev => !prev), [])
  const dispatch = useDispatch()
  const { data, status, error } = useSelector(state => state.user)
  const [filterData, setFilterData] = useState([])

  const methods = useForm({
    mode: 'onChange',
    defaultValues: {
      role: 'customer'
    }
  })

  useEffect(() => {
    const City_id = localStorage.getItem('City_id')
    dispatch(fetchData(`role=customer&city_id=${City_id}`))
  }, [dispatch])

  return (
    <>
      <Box sx={{ padding: '24px 0px' }}>
        <CustomFilterUsers toggle={toggleAddUserDrawer} setFilterData={setFilterData} data={data} />
      </Box>

      <List data={filterData} />
      <FormProvider {...methods}>
        <SidebarAddClients open={addUserOpen} toggle={toggleAddUserDrawer} />
      </FormProvider>
    </>
  )
}

export default React.memo(Clients)
