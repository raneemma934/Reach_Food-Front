import React, { useEffect, useState } from 'react'
import CustomTabs from 'src/@core/components/custom-tabs'
import { TabContext, TabPanel } from '@mui/lab'
import { useDispatch, useSelector } from 'react-redux'
import { handleSelectTitle } from 'src/store/apps/title'
import Clients from './clients'
import Delegates from './delegates'
import SalesManager from './SalesManager'
import { setPage } from 'src/store/apps/user'

const Index = React.memo(() => {
  const [value, setValue] = useState('1')
  const dispatch = useDispatch()

  const { page } = useSelector(state => state.user)

  // console.log('🚀 ~ Index ~ page:', page)

  const handleChange = (event, newValue) => {
    dispatch(setPage(newValue))
    setValue(newValue)
  }

  useEffect(() => {
    dispatch(handleSelectTitle('المستخدمين'))
  }, [dispatch])

  return (
    <TabContext value={page}>
      <CustomTabs value={page} handleChange={handleChange} />
      <TabPanel value='1' sx={{ p: 0 }}>
        <Clients />
      </TabPanel>
      <TabPanel value='2' sx={{ p: 0 }}>
        <Delegates value={page} />
      </TabPanel>
      <TabPanel value='3' sx={{ p: 0 }}>
        <SalesManager />
      </TabPanel>
    </TabContext>
  )
})

Index.acl = {
  action: 'manage',
  subject: 'users'
}

export default Index
