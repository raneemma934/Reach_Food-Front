import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import api from 'src/@core/utils/axios-instance'
import { ShowErrorToast } from 'src/@core/utils/showErrorToast'
import { ShowSuccessToast } from 'src/@core/utils/ShowSuccesToast'

export const fetchbranches = createAsyncThunk('branches/fetchbranches', async (_, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/branch/branches`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const fetchbranchesByCity = createAsyncThunk('branches/fetchbranchesByCity', async (_, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/cities`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const fetchbrancheDetailsById = createAsyncThunk(
  'branches/fetchbrancheDetailsById',
  async (_, { rejectWithValue }) => {
    try {
      const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/branch/show/${localStorage.branch_id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      })

      return response.data
    } catch (error) {
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchAllCountry = createAsyncThunk('branches/fetchAllCountry', async (_, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/cities/without-admin`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const fetchCountry = createAsyncThunk('branches/fetchCountry', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/address/countries`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const fetchAdminWithoutBranch = createAsyncThunk(
  'branches/fetchAdminWithoutBranch',
  async (params, { rejectWithValue }) => {
    try {
      const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/user/admins-without-city`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      })

      return response.data
    } catch (error) {
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchCity = createAsyncThunk('branches/fetchCity', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(
      `${process.env.NEXT_PUBLIC_BASE_URL}/api/cities/without-branches?country_id=${params}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      }
    )

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const fetchCategoriesSuper = createAsyncThunk(
  'branches/fetchCategoriesSuper',
  async (params, { rejectWithValue }) => {
    try {
      const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/address/countries`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      })

      return response.data.data
    } catch (error) {
      return rejectWithValue(error.response.data)
    }
  }
)

// ** Add branches
export const addbranches = createAsyncThunk('branches/addbranches', async (data, { getState, dispatch }) => {
  const response = await api
    .post('/api/branch/branches', data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${localStorage.accessToken}`
      }
    })
    .then(data => {
      ShowSuccessToast('تمت الاضافة بنجاح')
      dispatch(fetchbranchesByCity())
    })
    .catch(errors => {
      ShowErrorToast(
        errors?.response.data?.errors
          ? errors?.response.data?.errors
          : errors?.data?.message
          ? errors?.data?.message
          : 'حدث خطأ !'
      )
    })

  return response.data
})

// ** Edit branches
export const editbranches = createAsyncThunk('branches/editbranches', async (data, { getState, dispatch }) => {
  const response = await api
    .put(`/api/cities/${data.id}`, data, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.accessToken}`
      }
    })
    .then(data => {
      ShowSuccessToast('تمت تعديل بنجاح')
      dispatch(fetchbranchesByCity())
    })
    .catch(errors => {
      console.log(errors?.data?.message)
      ShowErrorToast(
        errors?.response.data?.errors
          ? errors?.response.data?.errors
          : errors?.data?.message
          ? errors?.data?.message
          : 'حدث خطأ !'
      )
    })

  return response.data
})

// export const deletebranches = createAsyncThunk('branches/deletebranches', async (id, { getState, rejectWithValue }) => {
//   try {
//     await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/branch/${id}`, {
//       headers: {
//         Authorization: `Bearer ${localStorage.getItem('accessToken')}`
//       }
//     })
//     const state = getState().feedback
//     const newData = state.data.filter(item => item.id !== id)

//     return { id, newData }
//   } catch (error) {
//     return rejectWithValue(error.response.data)
//   }
// })

export const deletebranches = createAsyncThunk(
  'branches/deletebranches',
  async (data, { getState, rejectWithValue, dispatch }) => {
    try {
      const response = await axios.delete(`/api/cities/${data.id}`, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.accessToken}`
        },
        data // Including data in the delete request
      })

      ShowSuccessToast('تم الحذف بنجاح')
      dispatch(fetchbranchesByCity())

      return response.data
    } catch (error) {
      ShowErrorToast(error)

      return rejectWithValue(error.response.data)
    }
  }
)

const BranchesSlice = createSlice({
  name: 'branches',
  initialState: {
    data: {},
    status: 'idle',
    error: null,
    deleteStatus: 'idle',
    deleteError: null,
    cities: [],
    country: [],
    CategoriesSuper: [],
    AllCountry: [],
    BranchDetails: {},
    branchesByCity: [],
    admin: []
  },
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(fetchCity.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.cities = action.payload
      })
      .addCase(fetchAdminWithoutBranch.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.admin = action.payload
      })
      .addCase(fetchbranchesByCity.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.branchesByCity = action.payload
      })
      .addCase(fetchbrancheDetailsById.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.BranchDetails = action.payload
      })
      .addCase(fetchAllCountry.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.AllCountry = action.payload
      })
      .addCase(fetchCountry.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.country = action.payload
      })
      .addCase(fetchCategoriesSuper.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.CategoriesSuper = action.payload
      })

      .addCase(fetchbranches.pending, state => {
        state.status = 'loading'
      })

      .addCase(fetchbranches.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.data = action.payload
      })
      .addCase(fetchbranches.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.payload
      })
      .addCase(deletebranches.pending, state => {
        state.deleteStatus = 'loading'
      })
      .addCase(deletebranches.fulfilled, (state, action) => {
        state.deleteStatus = 'succeeded'
      })
      .addCase(deletebranches.rejected, (state, action) => {
        state.deleteStatus = 'failed'
        state.deleteError = action.payload
      })
  }
})

export default BranchesSlice.reducer
