import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import api from 'src/@core/utils/axios-instance'
import { ShowErrorToast } from 'src/@core/utils/showErrorToast'
import { ShowSuccessToast } from 'src/@core/utils/ShowSuccesToast'

export const fetchFeedback = createAsyncThunk('feedback/fetchFeedback', async (_, { rejectWithValue }) => {
  try {
    const response = await api.get(
      `${process.env.NEXT_PUBLIC_BASE_URL}/api/feedback/feedback?branch_id=${localStorage.getItem('branch_id')}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      }
    )

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const deleteFeedback = createAsyncThunk(
  'feedback/deleteFeedback',
  async (id, { getState, rejectWithValue, dispatch }) => {
    try {
      await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/feedback/feedback/${id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      })

      ShowSuccessToast('تم حذف الشكوى')
      dispatch(fetchFeedback())
    } catch (error) {
      ShowErrorToast()

      return rejectWithValue(error.response.data)
    }
  }
)

const feedbackSlice = createSlice({
  name: 'feedback',
  initialState: {
    data: [],
    status: 'idle',
    error: null,
    deleteStatus: 'idle',
    deleteError: null
  },
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(fetchFeedback.pending, state => {
        state.status = 'loading'
      })
      .addCase(fetchFeedback.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.data = action.payload
      })
      .addCase(fetchFeedback.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.payload
      })
      .addCase(deleteFeedback.pending, state => {
        state.deleteStatus = 'loading'
      })
      .addCase(deleteFeedback.fulfilled, (state, action) => {
        state.deleteStatus = 'succeeded'
      })
      .addCase(deleteFeedback.rejected, (state, action) => {
        state.deleteStatus = 'failed'
        state.deleteError = action.payload
      })
  }
})

export default feedbackSlice.reducer
