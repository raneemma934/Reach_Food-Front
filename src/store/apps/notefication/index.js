import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

// ** Axios Imports
import axios from 'axios'
import toast from 'react-hot-toast'
import { ShowSuccessToast } from 'src/@core/utils/ShowSuccesToast'
import api from 'src/@core/utils/axios-instance'
import { ShowErrorToast } from 'src/@core/utils/showErrorToast'

// ** Fetch Notifications from Api
export const fetchNotification = createAsyncThunk('notification/fetchNotification', async params => {
  const response = await api.get(`/api/notifications?branch_id=${localStorage.getItem('branch_id')}`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

// **   RollBack Notification
export const RollBackNotification = createAsyncThunk(
  'notification/RollBackNotification',
  async (props, { getState, dispatch }) => {
    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_BASE_URL}/api/notifications/back/${props}`,
        {},
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
          }
        }
      )

      ShowSuccessToast('تم تراجع بنجاح')

      dispatch(fetchNotification())

      return response.data
    } catch (error) {
      ShowErrorToast(error)
      throw error
    }
  }
)

export const appNotificationSlice = createSlice({
  name: 'notification',
  initialState: {
    notification: []
  },
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchNotification.fulfilled, (state, action) => {
      state.notification = action.payload
    })
  }
})

export default appNotificationSlice.reducer
