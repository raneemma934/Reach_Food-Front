import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import api from 'src/@core/utils/axios-instance'
import { ShowErrorToast } from 'src/@core/utils/showErrorToast'
import { ShowSuccessToast } from 'src/@core/utils/ShowSuccesToast'

export const fetchProducts = createAsyncThunk('products/fetchProducts', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(
      `${process.env.NEXT_PUBLIC_BASE_URL}/api/product/products?branch_id=${localStorage.branch_id}&page=${params}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      }
    )

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

// ** Add Products
export const addProducts = createAsyncThunk('products/addProducts', async (data, { getState, dispatch }) => {
  const response = await api.post(`/api/product/products?branch_id=${localStorage.branch_id}`, data, {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })
  dispatch(fetchPriceProducts())

  return response.data
})

// ** Edit Products
export const editProducts = createAsyncThunk('products/editProducts', async (data, { getState, dispatch }) => {
  try {
    const response = await api.post(`/api/product/products/${data.id}?branch_id=${localStorage.branch_id}`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${localStorage.accessToken}`
      }
    })
    dispatch(setEdit(true))
    dispatch(fetchPriceProducts())
    ShowSuccessToast('تم التعديل بنجاح')
  } catch (error) {
    ShowErrorToast(error)
  }

  return response.data
})

// export const deleteProducts = createAsyncThunk('products/deleteProducts', async (id, { getState, rejectWithValue }) => {
//   try {
//     await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/product/${id}`, {
//       headers: {
//         Authorization: `Bearer ${localStorage.getItem('accessToken')}`
//       }
//     })
//     const state = getState().feedback
//     const newData = state.data.filter(item => item.id !== id)

//     return { id, newData }
//   } catch (error) {
//     return rejectWithValue(error.response.data)
//   }
// })

export const deleteProducts = createAsyncThunk(
  'products/deleteProducts',
  async (id, { getState, rejectWithValue, dispatch }) => {
    try {
      await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/product/${id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      })

      ShowSuccessToast('تم الحذف ')
      dispatch(fetchPriceProducts())
    } catch (error) {
      ShowErrorToast()

      return rejectWithValue(error.response.data)
    }
  }
)

//fetch Data ListPrice

export const fetchPriceProducts = createAsyncThunk('products/fetchPriceProducts', async (_, { rejectWithValue }) => {
  try {
    const response = await api.get(
      `${process.env.NEXT_PUBLIC_BASE_URL}/api/product/list-prices?branch_id=${localStorage.branch_id}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      }
    )

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

//edit Data ListPrice

export const editPricePProducts = createAsyncThunk(
  'products/editPricePProducts',
  async (data, { getState, dispatch }) => {
    try {
      const response = await api.post(`/api/product/updatePrice?branch_id=${localStorage.branch_id}`, data, {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${localStorage.accessToken}`
        }
      })
      dispatch(fetchPriceProducts())
      ShowSuccessToast('تم تعديل الاسعار')

      return response.data
    } catch (error) {
      ShowErrorToast(error)
    }
  }
)

const ProductsSlice = createSlice({
  name: 'products',
  initialState: {
    data: [],
    status: 'idle',
    error: null,
    deleteStatus: 'idle',
    deleteError: null,
    isEdit: false
  },
  reducers: {
    setEdit: (state, action) => {
      state.isEdit = action.payload
    }
  },
  extraReducers: builder => {
    builder
      .addCase(fetchProducts.pending, state => {
        state.status = 'loading'
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.data = action.payload
      })
      .addCase(fetchProducts.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.payload
      })
      .addCase(deleteProducts.pending, state => {
        state.deleteStatus = 'loading'
      })
      .addCase(deleteProducts.fulfilled, (state, action) => {
        state.deleteStatus = 'succeeded'
      })
      .addCase(deleteProducts.rejected, (state, action) => {
        state.deleteStatus = 'failed'
        state.deleteError = action.payload
      })
      .addCase(fetchPriceProducts.pending, state => {
        state.status = 'loading'
      })
      .addCase(fetchPriceProducts.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.data = action.payload
      })
      .addCase(fetchPriceProducts.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.payload
      })
  }
})

export const { setEdit } = ProductsSlice.actions

export default ProductsSlice.reducer
