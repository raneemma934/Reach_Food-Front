import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import { ShowSuccessToast } from 'src/@core/utils/ShowSuccesToast'
import api from 'src/@core/utils/axios-instance'
import { ShowErrorToast } from 'src/@core/utils/showErrorToast'

export const fetchRequests = createAsyncThunk('requests/fetchRequests', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(
      `${process.env.NEXT_PUBLIC_BASE_URL}/api/order/index?branch_id=${localStorage.getItem('branch_id')}&${params}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      }
    )

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const fetchRequestsArc = createAsyncThunk('requests/fetchRequestsArc', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(
      `${process.env.NEXT_PUBLIC_BASE_URL}/api/order/index?branch_id=${localStorage.getItem('branch_id')}&${params}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      }
    )

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const fetchRequestsById = createAsyncThunk('requests/fetchRequestsById', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/order/show/${params}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const deleteRequest = createAsyncThunk(
  'requests/deleteRequest',
  async (id, { getState, rejectWithValue, dispatch }) => {
    try {
      await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/order/${id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      })

      ShowSuccessToast()

      dispatch(fetchRequests('is_archived=1'))
    } catch (error) {
      ShowErrorToast()

      return rejectWithValue(error.response.data)
    }
  }
)

export const deleteProducts = createAsyncThunk('products/deleteProducts', async (id, { getState, rejectWithValue }) => {
  try {
    await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/product/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })
    const state = getState().feedback
    const newData = state.data.filter(item => item.id !== id)

    return { id, newData }
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

// ** Add Products
export const addProducts = createAsyncThunk('products/addProducts', async (data, { getState, dispatch }) => {
  const response = await api
    .post('/api/product/products?branch_id=${localStorage.branch_id}', data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${localStorage.accessToken}`
      }
    })
    .then(() => {
      ShowSuccessToast('تم تعديل حالة الطلب')
      dispatch(fetchProducts())
    })
    .catch(() => {
      ShowErrorToast()
    })

  return response.data
})

// ** Edit Products
export const editProducts = createAsyncThunk('products/editProducts', async (data, { getState, dispatch }) => {
  const response = await api.post(`/api/product/products/${data.id}?category_id=1`, data, {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })
  dispatch(fetchProducts())

  return response.data
})

//fetch Data ListPrice

export const fetchPriceProducts = createAsyncThunk('products/fetchPriceProducts', async (_, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/product/products?category_id=1`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

//edit Data ListPrice

export const editArchiveRequests = createAsyncThunk(
  'products/editArchiveRequests',
  async (params, { getState, dispatch }) => {
    const response = await api
      .put(`/api/order/archived/${params.id}  `, params.data, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.accessToken}`
        }
      })
      .then(() => {
        ShowSuccessToast('تم تعديل حالة الطلب')
        dispatch(fetchRequests('is_archived=1'))
      })
      .catch(() => {
        ShowErrorToast()
      })
    dispatch(fetchRequests('is_archived=1'))

    return response.data
  }
)

const RequestsSlice = createSlice({
  name: 'requests',
  initialState: {
    data: {},
    status: 'idle',
    error: null,
    deleteStatus: 'idle',
    deleteError: null,
    arcRequests: [],
    order: {},
    archivedDay: '2024-06'
  },
  reducers: {
    setDay: (state, action) => {
      state.archivedDay = action.payload
    }
  },
  extraReducers: builder => {
    builder
      .addCase(fetchRequests.pending, state => {
        state.status = 'loading'
      })
      .addCase(fetchRequests.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.data = action.payload
      })
      .addCase(fetchRequestsArc.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.arcRequests = action.payload
      })
      .addCase(fetchRequests.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.payload
      })
      .addCase(deleteProducts.pending, state => {
        state.deleteStatus = 'loading'
      })
      .addCase(fetchRequestsById.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.order = action.payload
      })
      .addCase(deleteProducts.fulfilled, (state, action) => {
        state.deleteStatus = 'succeeded'
      })
      .addCase(deleteProducts.rejected, (state, action) => {
        state.deleteStatus = 'failed'
        state.deleteError = action.payload
      })
      .addCase(fetchPriceProducts.pending, state => {
        state.status = 'loading'
      })
      .addCase(fetchPriceProducts.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.data = action.payload
      })

      .addCase(fetchPriceProducts.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.payload
      })
  }
})

export const { setDay } = RequestsSlice.actions

export default RequestsSlice.reducer
