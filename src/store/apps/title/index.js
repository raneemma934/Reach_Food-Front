import { createSlice } from '@reduxjs/toolkit'

export const TitleSlice = createSlice({
  name: 'Title',
  initialState: {
    text: ''
  },
  reducers: {
    handleSelectTitle: (state, action) => {
      state.text = action.payload
    }
  }
})

export const { handleSelectTitle } = TitleSlice.actions

export default TitleSlice.reducer
