import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import api from 'src/@core/utils/axios-instance'
import { ShowErrorToast } from 'src/@core/utils/showErrorToast'
import { ShowSuccessToast } from 'src/@core/utils/ShowSuccesToast'

export const fetchTrips = createAsyncThunk('products/fetchTrips', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(
      `${process.env.NEXT_PUBLIC_BASE_URL}/api/trip/trips?branch_id=${localStorage.getItem('branch_id')}&${params}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      }
    )

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const deleteTrips = createAsyncThunk(
  'products/deleteTrips',
  async (props, { getState, rejectWithValue, dispatch }) => {
    try {
      await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/trip/trips/${props.id}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      })

      ShowSuccessToast('تم حذف الرحلة بنجاح')

      // dispatch(fetchTrips())
      dispatch(fetchTrips(`day=${props.type}`))
    } catch (error) {
      ShowErrorToast()

      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchTracing = createAsyncThunk('products/fetchTracing', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/tracing/index?${params}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

export const fetchTracingUpload = createAsyncThunk(
  'products/fetchTracingUpload',
  async (params, { rejectWithValue }) => {
    try {
      const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/tracing/index?${params}`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      })

      return response.data
    } catch (error) {
      return rejectWithValue(error.response.data)
    }
  }
)

export const fetchRequestsById = createAsyncThunk('products/fetchRequestsById', async (params, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/order/show/${params}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

// ** Add Products
export const addProducts = createAsyncThunk('products/addProducts', async (data, { getState, dispatch }) => {
  const response = await api.post('/api/product/products?category_id=1', data, {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })
  dispatch(fetchProducts())

  return response.data
})

// ** Edit Products
export const editProducts = createAsyncThunk('products/editProducts', async (data, { getState, dispatch }) => {
  const response = await api.post(`/api/product/products/${data.id}?category_id=1`, data, {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })
  dispatch(fetchProducts())

  return response.data
})

export const deleteProducts = createAsyncThunk('products/deleteProducts', async (id, { getState, rejectWithValue }) => {
  try {
    await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/product/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })
    const state = getState().feedback
    const newData = state.data.filter(item => item.id !== id)

    return { id, newData }
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

//fetch Data ListPrice

export const fetchPriceProducts = createAsyncThunk('products/fetchPriceProducts', async (_, { rejectWithValue }) => {
  try {
    const response = await api.get(`${process.env.NEXT_PUBLIC_BASE_URL}/api/product/products?category_id=1`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    return response.data
  } catch (error) {
    return rejectWithValue(error.response.data)
  }
})

//edit Data ListPrice

export const editPricePProducts = createAsyncThunk(
  'products/editPricePProducts',
  async (data, { getState, dispatch }) => {
    const response = await api.post(`/api/product/updatePrice`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${localStorage.accessToken}`
      }
    })
    dispatch(fetchPriceProducts())

    return response.data
  }
)

const TripsSlice = createSlice({
  name: 'trips',
  initialState: {
    data: {},
    trips: [],
    status: 'idle',
    error: null,
    deleteStatus: 'idle',
    deleteError: null,
    order: {},
    upload: []
  },
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(fetchTrips.pending, state => {
        state.status = 'loading'
      })
      .addCase(fetchTrips.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.trips = action.payload
      })
      .addCase(fetchTracing.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.tracer = action.payload
      })
      .addCase(fetchTracingUpload.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.upload = action.payload
      })
      .addCase(fetchTrips.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.payload
      })
      .addCase(deleteProducts.pending, state => {
        state.deleteStatus = 'loading'
      })
      .addCase(fetchRequestsById.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.order = action.payload
      })
      .addCase(deleteProducts.fulfilled, (state, action) => {
        state.deleteStatus = 'succeeded'
      })
      .addCase(deleteProducts.rejected, (state, action) => {
        state.deleteStatus = 'failed'
        state.deleteError = action.payload
      })
      .addCase(fetchPriceProducts.pending, state => {
        state.status = 'loading'
      })
      .addCase(fetchPriceProducts.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.data = action.payload
      })
      .addCase(fetchPriceProducts.rejected, (state, action) => {
        state.status = 'failed'
        state.error = action.payload
      })
  }
})

export default TripsSlice.reducer
