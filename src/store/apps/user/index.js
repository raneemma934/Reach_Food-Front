import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

// ** Axios Imports
import axios from 'axios'
import { set } from 'nprogress'
import toast from 'react-hot-toast'
import { ShowSuccessToast } from 'src/@core/utils/ShowSuccesToast'
import api from 'src/@core/utils/axios-instance'
import { ShowErrorToast } from 'src/@core/utils/showErrorToast'
import { fetchTrips } from '../trips'

// ** Fetch Users
export const fetchData = createAsyncThunk('appUsers/fetchData', async params => {
  const response = await api.get(`/api/user/users?${params}`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

// ** Fetch Users
export const fetchDataDrawer = createAsyncThunk('appUsers/fetchDataDrawer', async params => {
  const response = await api.get(`/api/user/users?${params}`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

export const fetchDataSalesMan = createAsyncThunk('appUsers/fetchDataSalesMan', async params => {
  const response = await api.get(`/api/user/users?role=salesman&${params}`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

export const fetchUserById = createAsyncThunk('appUsers/fetchUserById', async params => {
  const response = await api.get(`/api/user/show-salesman/${params}?day=Sunday&branch_id=1`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

export const fetchDataManager = createAsyncThunk('appUsers/fetchDataManager', async params => {
  const response = await api.get(`/api/user/users?role=sales manager&${params}`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

// ** permissions
export const fetchPermissions = createAsyncThunk('appUsers/fetchPermissions', async params => {
  const response = await api.get(`/api/user/permissions`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

// ** location
export const fetchLocation = createAsyncThunk('appUsers/fetchLocation', async params => {
  const response = await api.get(`/api/address/addresses/${localStorage.City_id}`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

export const fetchCustomerByAddress = createAsyncThunk('appUsers/fetchCustomerByAddress', async params => {
  const response = await api.get(`/api/user/address?address_id=${params}`, {
    headers: {
      Authorization: `Bearer ${localStorage.accessToken}`
    }
  })

  return response.data.data
})

// ** Days

// ** Add User
export const addUser = createAsyncThunk('appUsers/addUser', async (data, { getState, dispatch }) => {
  const response = await api
    .post(
      `/api/auth/register?branch_id=${localStorage.getItem('branch_id')}&city_id=${localStorage.getItem('City_id')}`,
      data,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${localStorage.accessToken}`
        }
      }
    )
    .then(data => {
      ShowSuccessToast('تمت الاضافة بنجاح')
      if (data?.data?.data?.user?.role === 'customer') {
        dispatch(setPage('1'))
      } else if (data?.data?.data?.user?.role === 'salesman') {
        dispatch(setPage('2'))
      } else if (data?.data?.data?.user?.role === 'sales manager') {
        dispatch(setPage('3'))
      }
      dispatch(
        fetchData(
          `role=${data?.data?.data?.user?.role || 'admin'}&city_id=${localStorage.City_id}&branch_id=${
            localStorage.branch_id
          }`
        )
      )
    })
    .catch(errors => {
      console.log(errors?.data?.message)
      ShowErrorToast(errors)
    })

  // dispatch(fetchData(`role=${data?.data?.data?.user?.role}`))

  return response.data
})

// ** Add User
export const editUser = createAsyncThunk('appUsers/editUser', async (data, { getState, dispatch }) => {
  const response = await api
    .post(`/api/user/update/${data.id}`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${localStorage.accessToken}`
      }
    })
    .then(data => {
      ShowSuccessToast('تم التعديل بنجاح')
      if (data?.data?.data?.user?.role === 'customer') {
        dispatch(setPage('1'))
      } else if (data?.data?.data?.user?.role === 'salesman') {
        dispatch(setPage('2'))
      } else if (data?.data?.data?.user?.role === 'sales manager') {
        dispatch(setPage('3'))
      }
      dispatch(
        fetchData(
          `role=${data?.data?.data?.role || 'admin'}&city_id=${localStorage.City_id}&branch_id=${
            localStorage.branch_id
          }`
        )
      )
    })
    .catch(() => {
      ShowErrorToast(error?.data?.errors)
    })

  return response.data
})

// ** Add Trip
export const addTrip = createAsyncThunk('appUsers/addTrip', async (data, { getState, dispatch }) => {
  const response = await api
    .post(`/api/trip/trips?branch_id=${localStorage.getItem('branch_id')}`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${localStorage.accessToken}`
      }
    })
    .then(() => {
      ShowSuccessToast('تم اضافة الرحلة بنجاح')
    })
    .catch(error => {
      ShowErrorToast(error)
    })

  return response.data
})

// ** Add Trip
export const editTrip = createAsyncThunk('appUsers/editTrip', async (data, { getState, dispatch }) => {
  const response = await api
    .put(`/api/trip/trips/${data.id}?branch_id=${localStorage.getItem('branch_id')}`, data, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.accessToken}`
      }
    })
    .then(() => {
      ShowSuccessToast('تم تعديل الرحلة بنجاح')
    })
    .catch(error => {
      ShowErrorToast(error)
    })
  dispatch(fetchTrips(`day=${data.day}`))

  return response.data
})

// ** Delete User
export const deleteUser = createAsyncThunk('appUsers/deleteUser', async (props, { getState, dispatch }) => {
  try {
    const response = await axios.delete(`${process.env.NEXT_PUBLIC_BASE_URL}/api/user/users/${props.id}`, {
      data: { id: props.id },
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
      }
    })

    ShowSuccessToast('تم الحذف بنجاح')

    dispatch(fetchData(`role=${props.type}&city_id=${localStorage.City_id}&branch_id=${localStorage.branch_id}`))

    return response.data
  } catch (error) {
    ShowErrorToast(error)
    throw error
  }
})

export const appUsersSlice = createSlice({
  name: 'appUsers',
  initialState: {
    data: [],
    Categories: [],
    permissioons: [],
    days: [],
    Locations: [],
    total: 1,
    Users: [],
    params: {},
    openDrawer: false,
    page: '1',
    allData: [],
    SalesMan: [],
    Customer: [],
    manager: [],
    user: {}
  },
  reducers: {
    setPage: (state, action) => {
      state.page = action.payload
    },
    setOpenDrawer: (state, action) => {
      state.openDrawer = action.payload
    }
  },
  extraReducers: builder => {
    builder.addCase(fetchData.fulfilled, (state, action) => {
      state.data = action.payload
      state.total = action.payload.total
      state.params = action.payload.params
      state.allData = action.payload.allData
    })
    builder.addCase(fetchDataDrawer.fulfilled, (state, action) => {
      state.Users = action.payload
    })
    builder.addCase(fetchUserById.fulfilled, (state, action) => {
      state.user = action.payload
    })

    builder.addCase(fetchDataSalesMan.fulfilled, (state, action) => {
      state.SalesMan = action.payload
    })
    builder.addCase(fetchCustomerByAddress.fulfilled, (state, action) => {
      state.Customer = action.payload
    })
    builder.addCase(fetchDataManager.fulfilled, (state, action) => {
      state.manager = action.payload
    })

    builder.addCase(fetchPermissions.fulfilled, (state, action) => {
      state.permissioons = action.payload
    })
    builder.addCase(fetchLocation.fulfilled, (state, action) => {
      state.Locations = action.payload
    })
  }
})

export const { setPage, setOpenDrawer } = appUsersSlice.actions

export default appUsersSlice.reducer
