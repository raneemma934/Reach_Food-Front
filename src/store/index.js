// ** Toolkit imports
import { configureStore } from '@reduxjs/toolkit'

// ** Reducers
import chat from 'src/store/apps/chat'
import user from 'src/store/apps/user'
import email from 'src/store/apps/email'
import invoice from 'src/store/apps/invoice'
import calendar from 'src/store/apps/calendar'
import permissions from 'src/store/apps/permissions'
import branches from 'src/store/apps/branches'
import title from 'src/store/apps/title'
import feedbackReducer from './apps/complaints/index'
import productsReducer from './apps/products/index'
import RequestsSlice from './apps/requests/index'
import TripsSlice from './apps/trips/index'
import appNotificationSlice from './apps/notefication/index'

export const store = configureStore({
  reducer: {
    user,
    branches,
    chat,
    email,
    request: RequestsSlice,
    invoice,
    calendar,
    permissions,
    title,
    feedback: feedbackReducer,
    products: productsReducer,
    trips: TripsSlice,
    notification: appNotificationSlice
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false
    })
})
